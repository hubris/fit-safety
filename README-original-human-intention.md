# human_intention

Research on Human Intention.

## How to set up
Note we have two catkin workspaces `ur5_ws` and `hri_setup`. `ur5_ws` provides functions for ur robot, and are all run in Python 2. `hri_setup` is initialized with `Python 3` and is intended for perception algorithms which usually requires deep learning packages.

### ur5_ws
Follow the instructions in [this link](ur5_ws/README.md) until you finish [Usage](ur5_ws/README.md#usage). Remember to use `catkin build` instead of `catkin_make`.

### hri_setup
1. Follow [this link](hri_setup/doc/README-OpenPose.md) to set up OpenPose.
2. Follow [this link](hri_setup/doc/README-rosop-v2.md) to set up rosop until you finish [Set up Python3 Virtual Environment for rosop](hri_setup/doc/README-rosop-v2.md#set-up-python3-virtual-environment-for-rosop).
3. Set up `intention-env` by running
    ```
    cd ~/human_intention
    virtualenv -p /usr/bin/python3 intention-env
    source intention-env/bin/activate
    pip install scipy
    pip install matplotlib
    pip install torch==1.7.1
    pip install pyyaml
    pip install rospkg
    deactivate
    ```
4. Build `mifwlstm` by running
    ```
    cd ~/human_intention/hri_setup && source devel/setup.bash
    catkin build mifwlstm
    ```

## How to run intention filter developed in Q6.
Check [Run Intention Filter](doc/run-intention-filter-cleaned.md#run-intention-filter).

## How to run extrinsic calibration with realsense camera.
Check [here](doc/run-extrinsic-calibration-realsense.md).

## How to run admittance control.
Check [Admittance Control](ur5_ws/README.md#admittance-control). In short summary, open the first terminal and run
```
cd ~/human_intention/ur5_ws && source devel/setup.bash
roslaunch ur_control_wrapper ur5e_jenny_control.launch
```
Open the second terminal and run:
```
cd ~/human_intention/ur5_ws && source devel/setup.bash
rosrun ur_control_wrapper demo_test_ted.py
```
Open the third terminal and run:
```
cd ~/human_intention/ur5_ws && source devel/setup.bash
rosrun ur_control_wrapper admittance_control.py
```
Go back to the second terminal, and type the command `sac` to switch to admittance control mode. You may now apply force to the FT sensor to move the end-effector. You may see the following printed in the third terminal.
```
Force torque sensor is zeroed.
Current controller is scaled_pos_joint_traj_controller.
Controller successfully switched to joint_group_vel_controller.
Controller switch failed.
```
It is okay, and the implementation is working fine.

### Potential issues about the current admittance
If you run
```
rostopic hz /joint_group_vel_controller/command
```
you will find `average rate: 40.392`.

If you run
```
rostopic hz /wrench
```
you will find `average rate: 499.892`. We should plan on solving the low speed of sending commands. One way is to continuously sending commands at a high rate. The command itself like `self.command` is modified in `wrench_callback()` function in `admittance_control.py`.


## Info
Everything is being cleaned now.

`ur5_ws/` is from the branch `add_contact_detection` of `ur5_ws` github repo, and `hri_setup/` is from the branch of `ros-openpose` of `hri_setup/` github repo. `HumanIntention` inside `hri_setup/` is from the branch `short_prediction` of `HumanIntention` github repo from Ye-Ji's account.