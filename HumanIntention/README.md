# Human Trajectory Prediction
This is the implementation adopted from the paper

### Long term trajectory prediction framework using Mutable Intention Filter and Warp LSTM

##### [Zhe Huang](https://github.com/tedhuang96/), Aamir Hasan, Kazuki Shin, Ruohua Li, [Katherine Driggs-Campbell](https://krdc.web.illinois.edu/)

published in [RA-L](https://www.ieee-ras.org/publications/ra-l/special-issues/cfp-special-long-term-human-motion-prediction).

[[Paper](https://arxiv.org/abs/2007.00113)] [Project]

Human behavior patterns with surrounding environments and intentions about the desired goal position provide critical information for forecasting long-term pedestrian trajectories. In this work, we introduce a Mutable Intention Filter and a Warp LSTM (MIF-WLSTM) to simultaneously estimate human intention and perform  trajectory prediction. 

<div align='center'>
  <img src='images/framework.png' width='1000px'>
</div>

The Mutable Intention Filter propagates particles representing the belief over intentions, and the Warp LSTM takes as input the observed trajectory and the intention hypotheses preserved in particles to output multi-modal trajectories. Thanks to the intention mutation mechanism, the intention estimation is robust against intention-changing scenarios. The probability distribution of intentions and predicted trajectories with intention hypotheses allow only the trajectories corresponding to the N most probable intentions to be the output, so as to control the multi-modality.

<div align='center'>
<img src="images/demo.gif"></img>
</div>

### Citation
If you find this repo useful, please cite
```
@article{huang2020long,
  title={Long-term Pedestrian Trajectory Prediction using Mutable Intention Filter and Warp LSTM},
  author={Huang, Zhe and Hasan, Aamir and Shin, Kazuki and Li, Ruohua and Driggs-Campbell, Katherine},
  journal={IEEE Robotics and Automation Letters},
  year={2020},
  publisher={IEEE}
}
```

### Setup
All code was developed and tested on Ubuntu 18.04 with CUDA 10.2, Python 3.6.9, and PyTorch 1.7.1. The code was also successfully tested with CUDA 11.2 and PyTorch 1.8.1. 
##### 1. Create a virtual environment. (optional)
```
virtualenv -p /usr/bin/python3 myenv
source myenv/bin/activate
```
##### 2. Install packages.
If you use CUDA 10.2, run
```
pip install scipy
pip install matplotlib
pip install tensorboardX
pip install torch==1.7.1
```
or
```
pip install -r requirements.txt
```
If you use CUDA 11.X, run
```
pip install scipy
pip install matplotlib
pip install tensorboardX
pip install torch==1.8.1+cu111 -f https://download.pytorch.org/whl/torch_stable.html
```

##### 3. Create folders and download datasets.
```
sh download_datasets.sh
```
### Training & Evaluation
##### 1. process the data.
```
process_short_prediction_multi_intention_data_slow.ipynb
```
##### 2. Train Intention LSTM.
```
load_short_prediction_data_offset_multi_intention_slow.ipynb
```

### Contact
Please feel free to open an issue or send an email to zheh4@illinois.edu or yejimun2@illinois.edu.

# Regarding High Level Intention Estimation
Filter the bag based on time.
```
rosbag filter output.bag test.bag "t.to_sec() >= 1640559257 and t.to_sec() <= 1640559284"
```
Check specific frame.
```
cd ~/human_intention/HumanIntention
python high_raw_data_images.py
rm -rf high_level_images/*
```
Process trajectories.
```
cd ~/human_intention/HumanIntention
python high_raw_data_processing.py
```
