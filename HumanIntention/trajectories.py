import pickle
import math
import torch
from torch.utils.data import Dataset
import numpy as np

def load_raw_traj_file(filepath):
    with open(filepath, 'rb') as f:
        trajs = pickle.load(f)
        print(filepath + ' is loaded.')
    return trajs

class TrajectoriesDataset(Dataset):
    def __init__(
        self,
        obs_seq_len=10,
        skip=1,
        mode=None,
        ):
        self.obs_seq_len = obs_seq_len
        self.skip = skip
        filepath = "211230_demo.pickle"
        trajs = load_raw_traj_file(filepath)
        # rel_traj = trajs['relative_wrist_ee']
        # annotation_traj = trajs['annotation']
        num_sequences = math.floor((len(trajs['relative_wrist_ee'])-(self.obs_seq_len+1))/self.skip)+1
        if mode is None:
            idx_range = range(0, num_sequences * self.skip, skip)
        elif mode == 'train':
            idx_range = range(0, int((num_sequences * self.skip)*0.8), skip)
        # elif mode == 'val':
        #     idx_range = range(int((num_sequences * self.skip)*0.8), int((num_sequences * self.skip)*0.9), skip)
        # elif mode == 'test':
        #     idx_range = range(int((num_sequences * self.skip)*0.9), num_sequences * self.skip, skip)
        elif mode == 'val':
            idx_range = range(int((num_sequences * self.skip)*0.8), num_sequences * self.skip, skip)
        elif mode == 'test':
            idx_range = range(int((num_sequences * self.skip)*0.8), num_sequences * self.skip, skip)
        else:
            raise RuntimeError("Wrong mode for TrajectoriesDataset.")
        start_annotation_list = []
        rel_traj_list = []
        end_annotation_list = []
        for idx in idx_range:
            # 0 -> [1,0]; 1 -> [0,1]
            start_annotation = np.zeros(2)
            start_annotation[trajs['annotation'][idx]] = 1.
            rel_traj = trajs['relative_wrist_ee'][idx+1:idx+self.obs_seq_len+1] # t+1, ..., t+T_obs
            end_annotation = np.zeros(2)
            end_annotation[trajs['annotation'][idx+self.obs_seq_len]] = 1. # t+T_obs
            start_annotation_list.append(start_annotation) # (2,)
            rel_traj_list.append(rel_traj) # (T_obs, 3)
            end_annotation_list.append(end_annotation) # (2,)
        self.start_annotations = torch.from_numpy(np.stack(start_annotation_list, axis=0)).type(torch.float) # (N, 2)
        self.rel_trajs = torch.from_numpy(np.stack(rel_traj_list, axis=0)).type(torch.float) # (N, T_obs, 3)
        self.end_annotations = torch.from_numpy(np.stack(end_annotation_list, axis=0)).type(torch.float) # (N, 2)
        self.num_seq = len(self.rel_trajs)
        print(self.start_annotations.shape)
        print(self.rel_trajs.shape)
        print(self.end_annotations.shape)
        print(self.end_annotations[:,1].sum()/self.end_annotations.shape[0])

    def __len__(self):
        return self.num_seq

    def __getitem__(self, index):
        return self.start_annotations[index], self.rel_trajs[index], self.end_annotations[index]




filepath = "211230_demo.pickle"
trajs = load_raw_traj_file(filepath)
print(trajs.keys())
# print(len(trajs['annotation'])) # list
print(trajs['annotation'].shape)
print(trajs['relative_wrist_ee'].shape)
print(trajs['time'].shape)
print(trajs['time_step'].shape)
print(trajs['wrist'].shape)
print(trajs['ee'].shape)

obs_seq_len=10
skip=2
mode = 'train'
print(mode)
td = TrajectoriesDataset(
    obs_seq_len=obs_seq_len,
    skip=skip,
    mode=mode,
)
mode = 'val'
print(mode)
td = TrajectoriesDataset(
    obs_seq_len=obs_seq_len,
    skip=skip,
    mode=mode,
)
mode = 'test'
print(mode)
td = TrajectoriesDataset(
    obs_seq_len=obs_seq_len,
    skip=skip,
    mode=mode,
)

# (2647,)
# (2647, 3)
# (2647,)
# (2647,)
# (2647, 3)
# (2647, 3)