import pathhack
import rosbag
from os.path import join
import numpy as np
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import pickle

bag_path = join(pathhack.pkg_path, 'high_level_raw', '2021-12-26-16-52-44.bag')
bag = rosbag.Bag(bag_path)
t_prev = 0.
t_step = 0.
time_traj = []
time_step_traj = []
wrist_traj = []
ee_traj = []
annotation_traj = []

not_interact_annotations = \
    [[1640559170.90, 1640559183.04],
     [1640559187.50, 1640559209.05],
     [1640559213.70, 1640559216.62],
     [1640559227.18, 1640559257.51]]
interact_annotations = \
    [[1640559183.04, 1640559187.50],
     [1640559209.05, 1640559213.70],
     [1640559216.62, 1640559227.18],
     [1640559257.51, 1640559267.61],
     [1640559267.61, 1640559284.00]]

annotated_timestamps = [1640559170.90, 1640559183.04, 1640559187.50, 1640559209.05, 1640559213.70, \
    1640559216.62, 1640559227.18, 1640559257.51, 1640559267.61, 1640559284.00]
annotated_interactive_intention = 0 # start with 0 and then switch
annotated_i = 0

for topic, msg, t in bag.read_messages(topics=['/hri/human_robot_pos']):
    t_bag = t.secs+1e-9*t.nsecs
    raw_data = np.array(msg.data)
    t_msg, wrist_pos, robot_pos = raw_data[0], raw_data[1:4], raw_data[4:] # wrist_pos # (3,)
    robot_pos = robot_pos.reshape(6,3) # (6,3)
    ee_pos = robot_pos[-1] # (3,)
    if abs(t_bag-t_msg)>0.001:
        print("time recording has some problems.")
    if t_msg < annotated_timestamps[0] or t_msg > annotated_timestamps[-1]:
        t_prev = t_msg
        continue
    if t_prev != 0.:
        t_step = t_msg - t_prev
        # print(t_step)
        if t_step < 0.02:
            t_prev = t_msg
            # print("step is too small")
            continue
        if t_step > 0.06:
            t_prev = t_msg
            # print("step is too big.")
            continue
        time_step_traj.append(t_step)
    else:
        time_step_traj.append(0.)
        pass
    time_traj.append(t_msg)
    wrist_traj.append(wrist_pos)
    ee_traj.append(ee_pos)
    if annotated_i+1<len(annotated_timestamps) and t_msg < annotated_timestamps[annotated_i+1]:
        annotation_traj.append(annotated_interactive_intention)
    else:
        annotated_interactive_intention = 1 - annotated_interactive_intention
        annotation_traj.append(annotated_interactive_intention)
        annotated_i += 1
    t_prev = t_msg
# 0.004606875594031704
# 0.04256246064316114
print(np.std(time_step_traj[1:]))
print(np.mean(time_step_traj[1:]))


time_traj = np.array(time_traj)
time_step_traj = np.array(time_step_traj)
annotation_traj = np.array(annotation_traj)

wrist_traj = np.stack(wrist_traj, axis=0)
ee_traj = np.stack(ee_traj, axis=0)

fig1 = plt.figure()
plt.plot(time_traj, annotation_traj, '.')
fig1.savefig("annotation.png")

# fig2 = plt.figure()
# ax = Axes3D(fig2)
# ax.axis('equal')
# # ax.set_xlim3d(0, 1000)
# # ax.set_ylim3d(0, 1000)
# # ax.set_zlim3d(0, 1000)
# # fig = plt.figure()#plt.figure(figsize=(4,4))
# # ax = fig.add_subplot(111, projection='3d')
rel_traj = wrist_traj - ee_traj
# # ax.scatter(ee_traj[:,0],ee_traj[:,1],ee_traj[:,2]) # plot the point (2,3,4) on the figure
# # ax.scatter(wrist_traj[:,0],wrist_traj[:,1],wrist_traj[:,2]) # plot the point (2,3,4) on the figure
# ax.scatter(rel_traj[:,0],rel_traj[:,1],rel_traj[:,2]) # plot the point (2,3,4) on the figure
# plt.show()

print(len(annotation_traj))
print(len(rel_traj))
print(len(time_traj))
print(len(time_step_traj))
print(len(wrist_traj))
print(len(ee_traj))
# time_traj = []
# time_step_traj = []
# wrist_traj = []
# ee_traj = []
# annotation_traj = []
trajs = {}
trajs['annotation'] = annotation_traj
trajs['relative_wrist_ee'] = rel_traj
trajs['time'] = time_traj
trajs['time_step'] = time_step_traj
trajs['wrist'] = wrist_traj
trajs['ee'] = ee_traj

result_filename = "211230_demo.pickle"
with open(result_filename, 'wb') as f:
    pickle.dump(trajs, f)
    print(result_filename+' is created.')

traj_no_interactive = []
traj_interactive = []
for annotation, rel_pos in zip(annotation_traj, rel_traj):
    if annotation == 0:
        traj_no_interactive.append(rel_pos)
    else:
        traj_interactive.append(rel_pos)

traj_no_interactive = np.stack(traj_no_interactive, axis=0)
traj_interactive = np.stack(traj_interactive, axis=0)

fig2 = plt.figure()
ax = Axes3D(fig2)
ax.axis('auto')
# ax.axis('equal')
rel_traj = wrist_traj - ee_traj
ax.scatter(traj_no_interactive[:,0],traj_no_interactive[:,1],traj_no_interactive[:,2]) # plot the point (2,3,4) on the figure
ax.scatter(traj_interactive[:,0],traj_interactive[:,1],traj_interactive[:,2]) # plot the point (2,3,4) on the figure
# plt.show()
fig2.savefig("annotation2.png")

bag.close()