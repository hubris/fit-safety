import pathhack
import rosbag
from os.path import join
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from cv_bridge import CvBridge, CvBridgeError
import cv2

bag_path = join(pathhack.pkg_path, 'high_level_raw', '2021-12-26-16-52-44.bag')
bag = rosbag.Bag(bag_path)
t_prev = 0.
t_step = 0.
time_traj = []
time_step_traj = []
wrist_traj = []
ee_traj = []
bridge = CvBridge()
time_checked = 1640559267
for topic, msg, t in bag.read_messages(topics=['/hri/color/image_raw']):
    t_bag = t.secs+1e-9*t.nsecs
    snap_time= t_bag
    if time_checked-1. < t_bag < time_checked+1.:
        img = bridge.imgmsg_to_cv2(msg,"bgr8")
        cv2.imwrite(join(pathhack.pkg_path, 'high_level_images', '{0:.2f}.png'.format(snap_time)), img)

bag.close()