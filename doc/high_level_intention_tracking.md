```
cd ~/human_intention/ur5_ws/ && source devel/setup.bash
roslaunch ur_control_wrapper ur5e_jenny_control.launch
```

```
cd ~/human_intention/hri_setup/ && source devel/setup.bash
roslaunch rosop human_tracker_robot_multi_camera.launch
```

```
cd ~/human_intention/ur5_ws/ && source devel/setup.bash
roslaunch ur5_e_moveit_config moveit_rviz.launch config:=true
```


```
cd ~/human_intention/hri_setup/ && source devel/setup.bash
cd ~/human_intention/hri_setup/src/hierarchical_intention_tracker/scripts
python human_robot_position_listener.py
```

```
source intention-env/bin/activate
cd ~/human_intention/hri_setup/ && source devel/setup.bash
cd ~/human_intention/hri_setup/src/hierarchical_intention_tracker/scripts
python3 online_intention_tracker.py
```

```
rostopic echo hri/high_intention_prob_dist
```

```
For data collection, use ur5ws-env for python3 visualization.
```