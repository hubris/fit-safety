```
cd ~/fit-safety/ur5_ws && source devel/setup.bash
roslaunch ur_control_wrapper ur5e_jenny_control.launch
```

```
source ur5_ws/devel/setup.bash
rosrun ur_control_wrapper demo_test_ted_origin.py
```

`d`, `grip`, `gcl`, `zft`

```
cd ~/fit-safety/ur5_ws && source devel/setup.bash
rosrun ur_control_wrapper coco_trajectory_planner.py
```

```
cd ~/fit-safety/ur5_ws && source devel/setup.bash
rosrun ur_control_wrapper coco_control.py
```

```
cd ~/fit-safety/hri_setup/ && source devel/setup.bash
roslaunch rosop human_tracker_robot_multi_camera.launch
```

```
cd ~/fit-safety/ur5_ws/ && source devel/setup.bash
rosrun ur_control_wrapper coco_task_planner.py
```

```
cd ~/fit-safety/hri_setup/ && source devel/setup.bash
roslaunch mifwlstm online_filter_no_tracking.launch num_intentions:=5
```

```
cd ~/fit-safety/hri_setup/ && source devel/setup.bash
cd ~/fit-safety/hri_setup/src/hierarchical_intention_tracker/scripts
python human_robot_position_listener.py
```

```
cd ~/fit-safety
source intention-env/bin/activate
cd ~/fit-safety/hri_setup/ && source devel/setup.bash
cd ~/fit-safety/hri_setup/src/hierarchical_intention_tracker/scripts
python3 online_intention_tracker.py
```

```
cd ~/fit-safety/ur5_ws && source devel/setup.bash
rosrun ur_control_wrapper iros_data_clock.py
```

```
cd ~/fit-safety/ur5_ws && source devel/setup.bash
rosrun ur_control_wrapper iros_data_collection_pipeline.py
```

```
cd ~/fit-safety/
source intention-env/bin/activate
python ~/fit-safety/ur5_ws/src/ur_control_wrapper/nodes_coco/iros_data_analysis.py
```
