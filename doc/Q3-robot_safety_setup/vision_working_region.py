import argparse
import math

def calculate_vision_working_region(camera_height, human_height, cam_hFOV, cam_vFOV):
    distance = camera_height - human_height # meters 
    hFOV = cam_hFOV * (math.pi/180) # converting to radians 
    vFOV = cam_vFOV * (math.pi/180) # converting to radians 
    width = 2 * distance * math.tan(hFOV/2) 
    height = 2 * distance * math.tan(vFOV/2) 
    print('Width:', width, 'meters') 
    print('Height:', height, 'meters') 

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="Calculate camera's vision working region.")
    parser.add_argument('--camera_height', type=float, default=3, 
                        help='Height of the camera in meters.')
    parser.add_argument('--human_height', type=float, default=1.4, 
                        help='Height of the human in meters.')
    parser.add_argument('--camera_hFOV', type=float, default=87, 
                        help='Height FOV angle of the camera in degree.')
    parser.add_argument('--camera_vFOV', type=float, default=58, 
                        help='Width FOV angle of the camera in degree.')
    args = parser.parse_args()
    
    calculate_vision_working_region(args.camera_height, args.human_height, args.camera_hFOV, args.camera_vFOV)
