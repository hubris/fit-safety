

import numpy as np


# Convert 3D world coordinates to pixel coordinates 
def world_to_image(world_coord, K_e, K_i):
    # world coord -> camera coord
    world_coord = np.concatenate([world_coord,np.array([[1,1,1,1]]).T],axis=1)
    camera_coord = np.dot(K_e, world_coord.T)

    camera_coord = camera_coord[:3]
    Z_t = camera_coord[2,:]

    # camera coord -> pixel coord
    pixel_coord = np.dot(K_i, camera_coord)
    for i in range(len(Z_t)):
        pixel_coord[:,i] = pixel_coord[:,i]/Z_t[i]
    pixel_coord = pixel_coord[:2].T
    return pixel_coord

# Convert 3D world coordinates to pixel coordinates 
def image_to_world(pixel_coord, K_i_inv, K_e_inv, Z_t = 1.0): # Z_t is the distance from camera to the object
    # pixel coord -> camera coord
    pixel_coord.append(1.0)
    pixel_coord = np.array(pixel_coord)
    camera_coord = np.dot(K_i_inv, pixel_coord)
    camera_coord = camera_coord * Z_t
    camera_coord = np.append(camera_coord, 1)

    # camera coord -> world coord
    world_coord = np.dot(K_e_inv, camera_coord)
    return world_coord

def generate_random_trajectories(num_trajectories, num_points):
    robot_trajectories = np.random.randint(low=10,high=15, size=(num_trajectories, num_points))
    return robot_trajectories

def get_safety_zone_world_coord(robot_trajectories, K_e, K_i):
    # Get the min/max x & y values of the robot trajectories
    min_x = min(robot_trajectories[:,0])
    max_x = max(robot_trajectories[:,0])
    min_y = min(robot_trajectories[:,1])
    max_y = max(robot_trajectories[:,1])
    min_z = min(robot_trajectories[:,2])

    # Generate vertices of safety zone in world coordinate with a safety margin
    margin = 10
    vertices = np.array([[min_x-margin, max_y+margin, min_z-margin], [min_x-margin, min_y-margin, min_z-margin], \
    [max_x-margin, min_y-margin, min_z-margin], [max_x+margin, max_y+margin, min_z-margin]])
    return vertices



if __name__ == "__main__":
    ## Intrinsic and extrinsic calibration can be obtained by Q5 calibration method

    # Intrinsic
    fx = 918.44970703125
    fy = 918.126220703125
    ox = 640.32861328125
    oy = 357.2995910644531
    K_i = np.zeros((3,3))
    K_i[0,0] = fx
    K_i[1,1] = fy
    K_i[0,2] = ox
    K_i[1,2] = oy
    K_i[2,2] = 1
    K_i_inv = np.linalg.inv(K_i)

    # Extrinsic
    K_e = np.zeros((4,4))
    K_e = np.array( 
    [[0.05791677184332739, 0.9981225390285365, 0.01992464381424572, -0.0679819144260702],   \
    [0.9969790587646147, -0.05679275086829541, -0.052983834992759375, 0.5077813319500044], \
    [-0.05175278457816044, 0.022933105318806886, -0.9983965754139671, 1.820766490646183],  \
    [0.0, 0.0, 0.0, 1.0]])
    K_e_inv = np.linalg.inv(K_e)


    ## Generate random robot trajectories
    robot_trajectories = generate_random_trajectories(100,3) 

    if __name__ == "__main__":

        # Get vertices of safety zone in world coordinate with a safety margin from robot trajectories
        world_coord = get_safety_zone_world_coord(robot_trajectories, K_e, K_i)

        # convert 3D world coordinates to pixel coordinates 
        pixel_coord = world_to_image(world_coord, K_e, K_i)

        print(pixel_coord)




    ### Code Verification ###
    # # pixel coord -> world coord -> pixel coord

    # # pixel_coord for safety zone
    # x1 = 340
    # y1 = 200
    # x2 = 790
    # y2 = 600

    # camera_coord = image_to_world([0,0], 0)
    # camera_z = camera_coord[2]

    # points = []
    # points.append(image_to_world([x1,y1], K_i_inv, K_e_inv,camera_z))
    # points.append(image_to_world([x1,y2], K_i_inv, K_e_inv,camera_z))
    # points.append(image_to_world([x2,y2], K_i_inv, K_e_inv,camera_z))
    # points.append(image_to_world([x2,y1], K_i_inv, K_e_inv,camera_z))
    

    # vertices = np.array(points)[:, :3]

    # # Get vertices of safety zone in pixel coordinate
    # pixel_coord = world_to_image(vertices, K_e, K_i)
    # print(pixel_coord)


