# Setup Guide for Vision-based Safety System
## Introduction
Our vision-based safety system's effective working region, designed primarily to detect human workers, is determined by the camera's field of view and the performance of the human detection algorithm. Our findings indicate that the Openpose human detection algorithm's efficiency is significantly impacted by the observation of the human head. Thus, we provide a method for calculating this zone, factoring in human and camera height. 

The system features a safety-critical zone, depicted as a yellow rectangle in the digital replay system (blue in real-world images), in which the robot operates to guarantee safety in case of human intrusion. We provide code for designing this safety-critical zone based on robot trajectories, and mapping the image to and from the world's coordinate system.

## Camera
For our analysis, we'll be considering a realsense D435i camera, which has a depth FOV of 87° horizontally (hFOV) and 58° vertically (vFOV).

## The Geometry
The diagram below depicts the relationship between the FOV, distance from the camera, and the observed human worker. The FOV of a camera is governed by its horizontal and vertical angles, and can be visualized as a pyramid, with the lens at the apex.

[<img src="fit_safety_vision.png" width="400"/>](fit_safety_vision.png)

Figure: **Camera Setup and Calculation of the Effective Working Area.**  Safety system setup illustrating the calculation of the effective working area with respect to the camera position, the human worker, and camera's FOV defined by its horizontal and vertical angles. 





## The Mathmatics
Using trigonometry, we can calculate the dimensions of the effective working region.
The formulas used are as follows:

`width = 2 * distance * tan(hFOV/2)`
`height = 2 * distance * tan(vFOV/2)`

where:

- `distance` refers to the height difference between the camera and the human.
- `hFOV` is the horizontal field of view, converted to radians (in this case, 87°).
- `vFOV` is the vertical field of view, also converted to radians (in this case, 58°).

These calculations will yield the width and height of the effective working region of the camera. Please remember that the actual viewable region can be influenced by lens distortions, as well as the camera's position and orientation. This analysis assumes that the camera is perpendicular to the surface.

Thus, by understanding these dimensions, one can optimize the camera's position to improve camera view coverage and enhance the overall safety of the observed area.

Code to calculate the effective working region of the camera is [here](vision_working_region.py).


## Map the Image to the World's Coordinate System
Image space calibration is essential for monitoring human entry into the safety-critical zone, denoted as a blue rectangle in camera images and a yellow rectangle in the digital replay system. This zone is identified as an area where the robot will cease movement to ensure safety should a human intrude.

To map the captured image to the world coordinate system, we use the camera's intrinsic parameters and the extrinsic parameters. The camera's intrinsic parameters, including the focal length, principal point, and distortion coefficients, are obtained through camera calibration (provided in Y2Q1). The extrinsic parameters describe the transformation from the world coordinate system to the camera coordinate system. The image-world coordinate system mapping is performed using the following equation:


$$
 \text{Point}_\text{image} = K \cdot T \cdot \text{Point}_\text{world}
$$
Where:

$\text{Point}_\text{world}$ represents the 3D point in the world's coordinate system.

$\text{Point}_\text{image}$ represents the projection point in pixels.

$K$ is the camera's intrinsic matrix.

$T$ is the extrinsic matrix.

[<img src="robot_safety_setup.png" width="600"/>](robot_safety_setup.png)

Figure: **Visualization of Safety Critical Zone.** The safety-critical zone is depicted as a blue rectangle in real-world images (top row) and yellow in a 3D simulator (bottom row).

We provide a image-world coordinate system mapping and a world-image coordinate system mapping code to adjust the safety-critical zone based on the robot's trajectory. The code is provided [here](safety_zone_mapping.py).


