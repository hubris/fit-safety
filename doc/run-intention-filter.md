# Run Intention Filter

```
cd ~/human_intention/hri_setup/ && source devel/setup.bash
source ~/human_intention/intention-env/bin/activate
cd ~/human_intention/hri_setup/src/mifwlstm
python scripts/online_intention_visualizer.py
```

```
cd ~/human_intention/hri_setup/ && source devel/setup.bash
source ~/human_intention/intention-env/bin/activate
cd ~/human_intention/hri_setup/src/mifwlstm
python scripts/online_intention_filter_hri.py --tau 0.2 --mutable --prediction_method ilm --obs_seq_len 8 --pred_seq_len 12
```

python scripts/online_intention_filter_hri.py --tau 0.1 --mutable --prediction_method ilstm
python scripts/online_intention_filter_hri.py --tau 0.2 --mutable --prediction_method ilm --obs_seq_len 8 --pred_seq_len 12
python scripts/online_intention_filter_hri.py --tau 0.1 --mutable --prediction_method ilm

# without robot
```
roslaunch realsense2_camera rs_camera.launch align_depth:=True
```
```
cd ~/human_intention/hri_setup/
source devel/setup.bash
source openpose-env/bin/activate
roscd rosop
python scripts/human_skeleton_tracker_robust.py
```


python scripts/human_skeleton_detector.py


```
cd ~/human_intention/hri_setup/
source devel/setup.bash
source openpose-env/bin/activate
roscd rosop
python scripts/human_skeleton_visualizer.py
```
<!-- rosrun tf static_transform_publisher 0 0 0 0 0 0 map base_link 50 -->
```
cd ~/human_intention/hri_setup/ && source devel/setup.bash
roslaunch rosop camera_link_broadcaster.launch add_base_link:=true
```

# with robot
```
roslaunch realsense2_camera rs_camera.launch align_depth:=True
```
```
cd ~/human_intention/hri_setup/
source devel/setup.bash
source openpose-env/bin/activate
roscd rosop
python scripts/human_skeleton_tracker_robust.py
```

```
cd ~/human_intention/hri_setup/
source devel/setup.bash
source openpose-env/bin/activate
roscd rosop
python scripts/human_skeleton_visualizer.py
```
```
cd ~/human_intention/hri_setup/ && source devel/setup.bash
roslaunch rosop camera_link_broadcaster.launch add_base_link:=false
```
```
cd ~/human_intention/ur5_ws/ && source devel/setup.bash
roslaunch ur_control_wrapper ur5e_hande_jenny_control.launch
```
```
cd ~/human_intention/ur5_ws/ && source devel/setup.bash
roslaunch ur5_e_moveit_config moveit_rviz.launch config:=true
```

# demo q6 wrench supervision
Run
```
cd ~/human_intention/ur5_ws/ && source devel/setup.bash
rosrun ur_control_wrapper demo_test_ted.py
```
and input `zft` to zero out the measurement for initialization. Input `d` to go to default position.

```
cd ~/human_intention/ur5_ws/ && source devel/setup.bash
rosrun ur_control_wrapper wrench_supervisor.py
```

```
cd ~/human_intention/ur5_ws/ && source devel/setup.bash
rosrun ur_control_wrapper demo_q6.py
```

```
cd ~/human_intention/ur5_ws/ && source devel/setup.bash
rosrun ur_control_wrapper check_task_clock_talker.py
```

Run `rostopic echo /wrench` or code below to monitor the wrench measurement.

