```
cd ~/human_intention/
source intention-env/bin/activate
python ~/human_intention/ur5_ws/src/ur_control_wrapper/nodes_coco/iros_data_analysis.py
```

# Admittance Baseline
```
cd ~/human_intention/ur5_ws && source devel/setup.bash
roslaunch ur_control_wrapper admittance.launch
```

press `r` when you finish one round to reset for the other round. We can do the same for all the other baselines. One file to reset for the other files.

```
cd ~/human_intention/ur5_ws && source devel/setup.bash
rosrun ur_control_wrapper iros_data_collection_pipeline.py
```


# Coexistence Baseline (no repeated)
```
cd ~/human_intention/ur5_ws && source devel/setup.bash
roslaunch ur_control_wrapper coexistence.launch
```

```
cd ~/human_intention/hri_setup/ && source devel/setup.bash
roslaunch rosop coexistence.launch
```

```
cd ~/human_intention/ur5_ws/ && source devel/setup.bash
rosrun ur_control_wrapper task_planner.py
```

```
cd ~/human_intention/ur5_ws && source devel/setup.bash
rosrun ur_control_wrapper iros_data_collection_pipeline.py
```

For test.
```
cd ~/human_intention/
source intention-env/bin/activate
python ~/human_intention/ur5_ws/src/ur_control_wrapper/nodes_coco/iros_data_analysis.py
```

# Coexistence Baseline (repeated)
```
cd ~/human_intention/ur5_ws && source devel/setup.bash
roslaunch ur_control_wrapper coexistence.launch
```

```
cd ~/human_intention/hri_setup/ && source devel/setup.bash
roslaunch rosop coexistence.launch
```

```
cd ~/human_intention/ur5_ws/ && source devel/setup.bash
rosrun ur_control_wrapper repeated_task_planner.py
```

```
cd ~/human_intention/ur5_ws && source devel/setup.bash
rosrun ur_control_wrapper iros_data_collection_pipeline.py
```

For test.
```
cd ~/human_intention/
source intention-env/bin/activate
python ~/human_intention/ur5_ws/src/ur_control_wrapper/nodes_coco/iros_data_analysis.py
```

