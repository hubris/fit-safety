part resting area:

ep
position: 
  x: -0.558274046731
  y: 0.439526868285
  z: 0.196421173417
orientation: 
  x: -0.706875190586
  y: -0.00236151243358
  z: 0.707328351879
  w: 0.00291390139763

detected
<!-- [-0.646087110042572, 0.49977242946624756, 0.01027890108525753] -->

<!-- [-0.5860015749931335, 0.3536125719547272, 0.0396394357085228] -->

<!-- [-0.5890320539474487, 0.49977242946624756, 0.03108592890202999] -->

after calibration
# 5  
[-0.5598507523536682, 0.49975278973579407, 0.027858423069119453] # resting
# 1-4
[-0.748094916343689, 0.0647515058517456, 0.018717484548687935], # top right
[-0.4190538823604584, 0.05610278993844986, 0.0061339931562542915], # top left
[-0.4384427070617676, 0.2963732182979584, 0.01882524974644184], # bottom left
[-0.7381047010421753, 0.2954587936401367, 0.0319279320538044], # bottom right



camera height and width are updated for realsense. Not gonna affect our implementation. Be aware when you do calibration.
We need to change camera_transformation_matrix in `hri_setup/src/rosop/scripts/human_skeleton_detector_base_frame.py`. Change `hri_setup/src/rosop/launch/camera_link_broadcaster.launch`.

Then measure the positions of hand again.


```
cd ~/human_intention/ur5_ws && source devel/setup.bash
roslaunch ur_control_wrapper ur5e_jenny_control.launch
```

```
source ~/human_intention/ur5_ws/devel/setup.bash
rosrun ur_control_wrapper demo_test_ted_origin.py
```
`fs` `fe`

```
cd ~/human_intention/hri_setup/ && source devel/setup.bash
roslaunch rosop human_tracker_robot_multi_camera.launch
```

```
rostopic echo /hri/human_skeleton_position_tracking_3D_floatarray
```