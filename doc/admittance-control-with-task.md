# admittance control with task

# how to run

```
cd ~/human_intention/ur5_ws && source devel/setup.bash
roslaunch ur_control_wrapper ur5e_jenny_control.launch
```
Open the second terminal and run:
```
cd ~/human_intention/ur5_ws && source devel/setup.bash
rosrun ur_control_wrapper simple_route.py
```
Open the third terminal and run:
```
cd ~/human_intention/ur5_ws && source devel/setup.bash
rosrun ur_control_wrapper admittance_control_taskbased.py
```
