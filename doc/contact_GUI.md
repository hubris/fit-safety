# Run Y3Q2 Contact Based Safety
## Prerequisite
Please checkout branch `y3q2-contact-based-safety` before running the GUI.

Please follow the instruction under branch `user_interface` on how to install node-red and run the user interface.

You can download the GUI file [here](y3q2-contact-based-safety-GUI.json).


## Usage
### Start the Node-Red User Interface
To start node-red, type the following command in a terminal
```sh
node-red
```
From a browser, access http://localhost:1880 and open the interface. 

### Using the Interface

There are multiple buttons on this GUI with each executes a shell command.

Button | Functionality
---|---
START RUNNING | bring up the robot
GO TO DEFAULT POSE | go the the default position
RUN SIMPLE ROUTE | set robot trajectory and velocity
ADMITTANCE CONTROL | run demo trajectory
HUMAN TOUCH | enable admittance control
RUN JOINT CONTACT DETECTION | start joint contact detection

Slider | Functionality
---|---
Robot Velocity | set robot velocity (range from 0 to 4)
Lower Threshold Joint 0 | lower threshold for joint 0 (range from 0 to 0.5)
Upper Threshold Joint 0 | upper threshold for joint 0 (range from 0 to 0.5)
Lower Threshold Joint 1 | lower threshold for joint 1 (range from 0 to 0.5)
Upper Threshold Joint 1 | upper threshold for joint 1 (range from 0 to 0.5)


To run the Y3Q2 contact based safety system, first set the required parameters.
Then press the buttons in the following order.
`START RUNNING` → `GO TO DEFAULT POSE` → `RUN SIMPLE ROUTE` → `ADMITTANCE CONTROL` → `HUMAN TOUCH` → `RUN JOINT CONTACT DETECTION`.

## Note
When killing programs, always first kill ur5e_jenny_control.launch to immediately stop the robot. Otherwise, the joint velocity command may keep moving the robot to hit the table, itself or the human and can be dangerous.

If you don't have the Python package SciPy installed, run the following commands.
```sh
sudo apt-get install python-scipy
```
```sh
pip install scipy
```
