# Extrinsic Calibration for RealSense

1. Remember to use the second front port to connect the camera. Don't know why, but the third one simply does not work. The second port may have issues with data overflow because it is USB 2. The best way is to plug into the port on the back, which are USB 3 ports.
2. Check `demo.py` and see if the path in `image_snapshot`, and also `self.filename` are correct.
3. Run
```
cd ~/fit-safety/ur5_ws/ && source devel/setup.bash
roslaunch easy_handeye handeye_calibrate_realsense.launch
```

```
cd ~/fit-safety/ur5_ws/ && source devel/setup.bash
rosrun ur_control_wrapper demo.py
```
4. Remember to create folder `ur5_ws/data` before you collect images and robot positions.
5. I recommend using a virtual environment for running jupyter notebooks for extrinsic calibration.
```
cd ~/fit-safety/ur5_ws
virtualenv -p /usr/bin/python3 ur5ws-env
source ur5ws-env/bin/activate
pip install jupyterlab
pip install numpy
pip install opencv-python
pip install pupil-apriltags
pip install scipy
pip install matplotlib
```