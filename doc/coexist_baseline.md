```
cd ~/human_intention/ur5_ws && source devel/setup.bash
roslaunch ur_control_wrapper ur5e_jenny_control.launch
```

```
source ur5_ws/devel/setup.bash
rosrun ur_control_wrapper demo_test_ted_origin.py
```

`d`, `jvc`, `0 0 0`

```
cd ~/human_intention/ur5_ws && source devel/setup.bash
rosrun ur_control_wrapper trajectory_planner.py
```

```
cd ~/human_intention/ur5_ws && source devel/setup.bash
rosrun ur_control_wrapper potential_field_dynamic.py
```

```
cd ~/human_intention/hri_setup/ && source devel/setup.bash
roslaunch rosop human_tracker_robot_multi_camera.launch
```

```
cd ~/human_intention/hri_setup/ && source devel/setup.bash
roslaunch mifwlstm online_filter_no_tracking.launch num_intentions:=4
```

```
cd ~/human_intention/ur5_ws/ && source devel/setup.bash
rosrun ur_control_wrapper task_planner.py
```

For debugging the trajectory_planner, do
```
rostopic pub -1 /low_intention_command std_msgs/Float64MultiArray "layout:
  dim:
  - label: ''
    size: 0
    stride: 0
  data_offset: 0
data:
- 2"
```




## ep
close to not assembled part top:
ep
position: 
  x: -0.733687489382
  y: 0.266676668356
  z: 0.201851806561
orientation: 
  x: -0.693048528731
  y: 0.0594816323426
  z: 0.718036318936
  w: 0.0238645537596

close to assembled part top:
ep
position: 
  x: -0.733435702081
  y: 0.271588202479
  z: 0.18800932687
orientation: 
  x: -0.696864054993
  y: 0.0551011826659
  z: 0.71486010213
  w: 0.017871287318

close to ground:

intention 1
position: 
  x: -0.741595347966
  y: 0.0188166741001
  z: 0.137131506006
orientation: 
  x: 0.687797605953
  y: -0.025988687052
  z: -0.725190716135
  w: 0.0189067877464


intention 2
position: 
  x: -0.396880886853
  y: 0.0208367675825
  z: 0.129209775555
orientation: 
  x: 0.714678790534
  y: 0.0232118944284
  z: -0.696563222501
  w: 0.0591194669883


intention 3
ep
position: 
  x: -0.389820968661
  y: 0.249793106665
  z: 0.130675708751
orientation: 
  x: -0.694205174924
  y: 0.00202642951961
  z: 0.718377085605
  w: 0.0448266836774



intention 4
ep
position: 
  x: -0.738169003388
  y: 0.263045140345
  z: 0.134673302207
orientation: 
  x: 0.70665322684
  y: -0.0128041252666
  z: -0.707443083916
  w: 0.00124675266062