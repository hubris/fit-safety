# Potential Field (Developing)

# how to run(potential field with static robot)

```
cd ~/human_intention/ur5_ws && source devel/setup.bash
roslaunch ur_control_wrapper ur5e_jenny_control.launch
```
or
```
roslaunch ur_control_wrapper ur5e_hande_jenny_control.launch
```

```
cd ~/human_intention/hri_setup/ && source devel/setup.bash
roslaunch rosop human_tracker_robot_multi_camera.launch
```

```
cd ~/human_intention/ur5_ws && source devel/setup.bash
rosrun ur_control_wrapper demo_test_ted_origin.py
```
Input `d` to go to default position, then input `x+20` and `z-20` to a better location for demo. If you ran `potential_field_static_test.py`, and then you want to use `demo_test_ted.py` again to adjust position, first input `sc` to switch controller, then input `scaled_pos`, and then input `d`. This is because `potential_field_static_test.py` is using joint velocity controller, while the position control in `demo_test_ted.py` use scaled position controller.

If you want to make the robot be away from human, run
```
cd ~/human_intention/ur5_ws && source devel/setup.bash
rosrun ur_control_wrapper potential_field_static_test.py
```
elseif you want to make the robot approach human, run
```
cd ~/human_intention/ur5_ws && source devel/setup.bash
rosrun ur_control_wrapper potential_field_static_test.py --attract
```



# how to run(potential field with a trajectory following robot)

```
cd ~/human_intention/ur5_ws && source devel/setup.bash
roslaunch ur_control_wrapper ur5e_jenny_control.launch
```
or
```
roslaunch ur_control_wrapper ur5e_hande_jenny_control.launch
```

```
cd ~/human_intention/hri_setup/ && source devel/setup.bash
roslaunch rosop human_tracker_robot_multi_camera.launch
```

```
cd ~/human_intention/ur5_ws && source devel/setup.bash
rosrun ur_control_wrapper simple_route.py
```

If you want to make the robot be away from human, run
```
cd ~/human_intention/ur5_ws && source devel/setup.bash
rosrun ur_control_wrapper potential_field_dynamic_test.py
```
elseif you want to make the robot approach human, run
```
cd ~/human_intention/ur5_ws && source devel/setup.bash
rosrun ur_control_wrapper potential_field_dynamic_test.py --attract
```