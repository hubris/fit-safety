# Download Datasets

## Instructions on Saving and Loading datasets
TBD by 11/28/21.


## Datasets

### drafts/q6
```
cd ~/human_intention
sh drafts/q6/download_datasets.sh
```

### HumanIntention
```
cd ~/human_intention
sh HumanIntention/download_datasets.sh
```
### mifwlstm
```
cd ~/human_intention
cd hri_setup/src/mifwlstm && sh run/setup.sh
cd ~/human_intention
```
test the setup
```
cd ~/human_intention/hri_setup/ && source devel/setup.bash
roslaunch mifwlstm offline_filter.launch
```
Terminate the launch file after the offline filtering is finished.