# Run Safety Digital Twin User Interface
## Prerequisite
### Ubuntu 18.04

To check your Ubuntu version, type the following command in your terminal.
```sh
cat /etc/os-release
```
### ROS Melodic

To check your ROS version, type the following command in your terminal.
```sh
echo $ROS_DISTRO
```
To install ROS Melodic, follow the instruction [here](http://wiki.ros.org/melodic/Installation/Ubuntu).

## Building Project from Source
### Clone Repository
Please clone this repository to your `$HOME` directory and checkout branch `q3_DT_GUI`.

### Build ur5_ws
```
cd ~/fit-safety/ur5_ws/
pip3 install --user ur_rtde
sudo apt update -qq
rosdep update
rosdep install --from-paths src --ignore-src -y
catkin_make
```

## Setting up Node-Red
Node-RED applications run on flows through different nodes, where each node is an operator that performs some defined tasks. For this robot control interface, two kinds of special nodes are required -- Daemon node and Dashboard nodes. The Daemon node is for executing rosrun, and Dashboard nodes are for interface display. 

### Install Node-Red
Follow the guide [here](https://nodered.org/docs/getting-started/raspberrypi) to install node-red, or simply run the command below.
```sh
bash <(curl -sL https://raw.githubusercontent.com/node-red/linux-installers/master/deb/update-nodejs-and-nodered)
```

### Import Dashboard Nodes

First, type `node-red` in a terminal to start the node-red process.

After opening the Node-RED editor at http://localhost:1880, click on the upper right turtle sign (i.e. menu sign), and click **Manage palette** to open the Palette Manager. 

<img src="doc/setup_images/menu_palette.png" width="600" />

Inside it, click on the **Install** tab. Enter `dashboard` and search. Install the package named `node-red-dashboard`. Note that in the image below, the "install" button says "installed". This is because I have already installed this package. 

<img src="doc/setup_images/install_dashboard.png" width="600" />

For a video demonstration, watch  [this video](https://www.youtube.com/watch?v=Ho1vMuA43ks) from 30'' to 1'30''. 

### Import Daemon Node
After opening the Node-RED editor at http://localhost:1880, click on the upper right turtle sign (i.e. menu sign), and click **Manage palette** to open the Palette Manager. 

<img src="doc/setup_images/menu_palette.png" width="600" />

Inside it, click on the **Install** tab. Enter `Daemon` and search. Install the package named `node-red-node-daemon`. Note that in the image below, the "install" button says "installed". This is because I have already installed this package. 

<img src="doc/setup_images/install_daemon.png" width="600" />

For a video demonstration, watch  [this video](https://www.youtube.com/watch?v=Ho1vMuA43ks) from 30'' to 1'30''. 

### Import Flows

Now import the flows for the interface. Again, click on the upper right turtle sign (i.e. menu sign), and click **Import**. 

<img src="doc/setup_images/open_import.png" width="600" />

Then, choose **select a file to import**, and select the `user_interface.json` file. Then click **Import**. 

You can download the file [here](user_interface.json).

<img src="doc/setup_images/import_flow.png" width="600" />

After returning to the Node-RED editor page, click the upper right **Deploy** button to deploy the changes. 

<img src="doc/setup_images/deploy.png" width="600" />

Note: If you make changes to the interface in the future, you need to save your changes by clicking the **Deploy** button. 

### Open the Interface
After importing the flows, click on the upper right turtle sign (i.e. menu sign), mouse over to **View**, and select **Dashboard** from the dropdown menu. 

<img src="doc/setup_images/menu_view_dashboard.png" width="600" />

Then, click on the little square-and-arrow sign on the right side of the **Theme** tab to open the interface. 

<img src="doc/setup_images/open_interface.png" width="600" />

After openning the interface, you should be at the home page of the interface, and you should see something like this:

<img src="doc/setup_images/home.png" width="600" />

### Interface Theme Change (Optional)
Upon import, by default, the interface is under a light theme, which is not as pretty as dark ;) To change the theme, click on the upper right turtle sign (i.e. menu sign), mouse over to **View**, and select **Dashboard** from the dropdown menu. Then, click on the **Theme** tab to change the theme. 

## Usage
### Start the Node-Red User Interface
To start node-red, type the following command in a terminal
```sh
node-red
```
From a browser, access http://localhost:1880 and open the interface following the **Open the Interface** section above. 

### Using the Interface

There are multiple buttons on this GUI with each executes a shell command.

Button | Functionality
---|---
START RUNNING | bring up the robot
START CAMERA | launch the RealSense camera
START PERCEPTION NODE | run the human pose estimation script
START Q3 NODE | move the robot to its default position
START FSM | start the robot working on its assigned task
STOP FSM | stop the robot
LAUNCH RVIZ | launch Rviz for replay
Record | toggle to start/stop recording
REPLAY BAG | replay the recorded data in Rviz

To run the safety monitoring system, press the buttons in the following order.

`START RUNNING` → `START CAMERA` → `START PERCEPTION NODE` → `START Q3 NODE` → `START FSM`.

To record human pose and robot joint data, toggle the `Record` switch.

To replay the recording, press `LAUNCH RVIZ` and then `REPLAY BAG`.

## Troubleshoot
1. No module named pip

    Please make sure you have the ***latest version*** of pip and pip3 installed

    The rosdep package requires pip to install all dependencies and we need to use the ***latest*** version of pip3 to install the `ur_rtde` package

    To update your pip3, follow the instruction [here](https://stackoverflow.com/questions/38613316/how-to-upgrade-pip3)

2. Unable to init server: Could not connect: Connection refused

    This issue occured when you start your node-red process using the command `node-red-start`.

    One possible solution is typing `node-red-stop; node-red` in another terminal to restart your node-red process.

3. Error: listen EADDRINUSE: address already in use 0.0.0.0:1880
    ```
    sudo lsof -i :3000
    ```
    Kill the process using PID
    ```
    kill -9 <PID>
    ```

4. Delete all imported json file
    ```
    sudo rm -r ~/.node-red/flows.json
    ```
