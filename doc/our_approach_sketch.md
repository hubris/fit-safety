```
cd ~/human_intention/ur5_ws && source devel/setup.bash
roslaunch ur_control_wrapper ur5e_jenny_control.launch
```

```
cd ~/human_intention/ur5_ws && source devel/setup.bash
rosrun ur_control_wrapper coco_trajectory_planner.py
```

```
source ur5_ws/devel/setup.bash
rosrun ur_control_wrapper demo_test_ted_origin.py
```

`d`, `grip`, `gcl`, `zft`, `jvc`, `0 0 0`

```
cd ~/human_intention/ur5_ws && source devel/setup.bash
rosrun ur_control_wrapper coco_control.py
```



```
cd ~/human_intention/ur5_ws/ && source devel/setup.bash
rosrun ur_control_wrapper coco_task_planner.py
```

```
cd ~/human_intention/hri_setup/ && source devel/setup.bash
roslaunch rosop human_tracker_robot_multi_camera.launch
```

```
cd ~/human_intention/hri_setup/ && source devel/setup.bash
roslaunch mifwlstm online_filter_no_tracking.launch num_intentions:=5 tau:=0.05
```

roslaunch mifwlstm online_filter_no_tracking.launch num_intentions:=5

```
cd ~/human_intention/hri_setup/ && source devel/setup.bash
cd ~/human_intention/hri_setup/src/hierarchical_intention_tracker/scripts
python human_robot_position_listener.py
```

```
source intention-env/bin/activate
cd ~/human_intention/hri_setup/ && source devel/setup.bash
cd ~/human_intention/hri_setup/src/hierarchical_intention_tracker/scripts
python3 online_intention_tracker.py
```

```
rostopic echo /hri/high_intention_prob_dist
```

For debugging the trajectory_planner, do
```
rostopic pub -1 /low_intention_command std_msgs/Float64MultiArray "layout:
  dim:
  - label: ''
    size: 0
    stride: 0
  data_offset: 0
data:
- 2"
```

Tune this
```
col_human_motion_predictor = CollaborativeHumanMotionPredictor(
    intention_api.human_intention_sampler.intention_center_coordinates/100, # now unit: m
    dt=0.03,
    Tp=args.Tp,
    intention_std=0.12,# 0.3,# 0.2, # 0.08 fast
    process_std=0.08, #0.25,# 0.125, # 0.05 fast
    min_vel=0.05,
)
```
in `online_intention_tracker.py`.
Tune this

roslaunch mifwlstm online_filter_no_tracking.launch num_intentions:=5 tau:=0.05

probably also obs_seq_len, pred_seq_len and the mutation probability. for both low level and high level.