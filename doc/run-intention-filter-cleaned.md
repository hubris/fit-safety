# Run Intention Filter
```
cd ~/fit-safety/hri_setup/ && source devel/setup.bash
roslaunch mifwlstm online_filter.launch num_intentions:=2
```

```
cd ~/fit-safety/ur5_ws/ && source devel/setup.bash
roslaunch ur_control_wrapper hri_human_intention.launch
```

# After calibration
Put `T_cam_in_base` in `human_intention/hri_setup/src/rosop/scripts/camera_link_broadcaster.py`, and compute 7-dim array. Put that array in `human_intention/hri_setup/src/rosop/launch/camera_link_broadcaster.launch` as arguments of `camera_link_broadcaster`.

Rename `T_cam_in_base` as `transformation_matrix` and put in `human_intention/hri_setup/src/mifwlstm/scripts/online_intention_filter_hri.py`.

Rename `T_cam_in_base` as `transformation_matrix` and put in `human_intention/hri_setup/src/interactive_intention_estimator/scripts/human_robot_position_listener.py`.

# Fast check on wrist tracking data
Go to `human_intention/hri_setup/src/mifwlstm/scripts/online_intention_filter_hri.py` and find class `OnlineMIF` and  `tracking_callback`, and uncomment printing of `wrist_pos_tf`.

# Change intention coordinates
First check the wrist position on the desired intention, and then in `human_intention/hri_setup/src/mifwlstm/src/api/hri_intention_application_interface.py`, edit `load_fit_human_intention_sampler` function, and replace `intention_coordinates` with the detected wrist positions. Remember that the scale should be `100` to change from unit of `m` to `cm`.


# Change temperature of Online Mutable Intention Filter
```
cd ~/human_intention/hri_setup/ && source devel/setup.bash
roslaunch mifwlstm online_filter.launch tau:=0.05
```

# Set up multiple intentions more than 2
In the current example, `num_intentions` is set as 6. We need to make sure the number of intentions is consistent with the intention coordinates in `load_fit_human_intention_sampler`, which is in the file `human_intention/hri_setup/src/mifwlstm/src/api/hri_intention_application_interface.py`. You may want to switch between `211202` and `211203` for two intentions and six intentions. 
```
cd ~/human_intention/hri_setup/ && source devel/setup.bash
roslaunch mifwlstm online_filter.launch num_intentions:=6
```
Also, if `num_intentions` is not 2, then it is currently in conflict with `human_intention/ur5_ws/src/ur_control_wrapper/node/demo_q6.py`. We will fix that later. But for just creating an occlusion scenario with the moving robot, you can just comment out `rospy.Subscriber("hri/human_intention_prob_dist", Float32MultiArray, self.callback_human_intention, queue_size=1)`. But note that now the robot won't stop for human because of the intention. So be careful.

# Test on tracking
```
cd ~/human_intention/hri_setup/ && source devel/setup.bash
roslaunch rosop human_tracker_no_robot.launch
```

```
cd ~/human_intention/hri_setup/ && source devel/setup.bash
roslaunch rosop human_tracker_no_robot_multi_camera.launch
```

or

```
cd ~/human_intention/hri_setup/ && source devel/setup.bash
roslaunch rosop human_tracker_robot_multi_camera.launch
```


# Remember after we fix the camera name, the calibration will need to change camera name as well.
```
rviz
```



<!-- -0.8148344159126282, 0.0380699560046196, 0.007328375708311796 # top right
-0.8119269609451294, 0.27325040102005005, 0.00861838739365339 # bottom right
-0.5134246945381165, 0.3031236529350281, 0.0011400006478652358 # bottom left
-0.49480947852134705, 0.0488872155547142, -0.010111686773598194 # top left -->