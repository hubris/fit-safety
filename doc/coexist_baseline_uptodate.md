```
cd ~/human_intention/ur5_ws && source devel/setup.bash
roslaunch ur_control_wrapper ur5e_jenny_control.launch
```

```
source ~/human_intention/ur5_ws/devel/setup.bash
rosrun ur_control_wrapper demo_test_ted_origin.py
```

`d`, `grip`, `gcl`, `zft`, `jvc`, `0 0 0`

```
cd ~/human_intention/ur5_ws && source devel/setup.bash
rosrun ur_control_wrapper random_trajectory_planner.py
```

```
cd ~/human_intention/ur5_ws && source devel/setup.bash
rosrun ur_control_wrapper potential_field_dynamic.py
```

```
cd ~/human_intention/hri_setup/ && source devel/setup.bash
roslaunch rosop human_tracker_robot_multi_camera.launch
```

```
cd ~/human_intention/ur5_ws/ && source devel/setup.bash
rosrun ur_control_wrapper task_planner.py
```

<!-- ```
cd ~/human_intention/hri_setup/ && source devel/setup.bash
roslaunch mifwlstm online_filter_no_tracking.launch num_intentions:=4
``` -->

```
cd ~/human_intention/hri_setup/ && source devel/setup.bash
roslaunch mifwlstm online_filter_no_tracking.launch num_intentions:=5
```


```
cd ~/human_intention/ur5_ws && source devel/setup.bash
rosrun ur_control_wrapper iros_data_clock.py
```

```
cd ~/human_intention/ur5_ws && source devel/setup.bash
rosrun ur_control_wrapper iros_data_collection_pipeline.py
```

```
cd ~/human_intention/
source intention-env/bin/activate
python ~/human_intention/ur5_ws/src/ur_control_wrapper/nodes_coco/iros_data_analysis.py
```

For debugging the trajectory_planner, do
```
rostopic pub -1 /low_intention_command std_msgs/Float64MultiArray "layout:
  dim:
  - label: ''
    size: 0
    stride: 0
  data_offset: 0
data:
- 2"
```
