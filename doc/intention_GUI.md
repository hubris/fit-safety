# Run Q6 Intention
## Prerequisite
Please checkout branch `q6-intention` before running the GUI.

Please follow the instruction under branch `user_interface` on how to install node-red and run the user interface.

You can download q6-intention UI file [here](Q6_Intention_UI.json).


## Usage
### Start the Node-Red User Interface
To start node-red, type the following command in a terminal
```sh
node-red
```
From a browser, access http://localhost:1880 and open the interface. 

### Using the Interface

There are multiple buttons on this GUI with each executes a shell command.

To run the safety intention detection system, press the buttons in the following order.

`RUN INTENTION FILTER` → `BRING UP ROBOT`.



