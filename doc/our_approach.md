```
cd ~/human_intention/ur5_ws && source devel/setup.bash
roslaunch ur_control_wrapper ur5e_jenny_control.launch
```

```
cd ~/human_intention/ur5_ws && source devel/setup.bash
rosrun ur_control_wrapper coco_trajectory_planner.py
```

```
cd ~/human_intention/ur5_ws && source devel/setup.bash
rosrun ur_control_wrapper coco_control.py
```

```
cd ~/human_intention/hri_setup/ && source devel/setup.bash
roslaunch rosop human_tracker_robot_multi_camera.launch
```

```
cd ~/human_intention/ur5_ws/ && source devel/setup.bash
rosrun ur_control_wrapper coco_task_planner.py
```

```
cd ~/human_intention/hri_setup/ && source devel/setup.bash
roslaunch mifwlstm online_filter_no_tracking.launch num_intentions:=5
```



For debugging the trajectory_planner, do
```
rostopic pub -1 /low_intention_command std_msgs/Float64MultiArray "layout:
  dim:
  - label: ''
    size: 0
    stride: 0
  data_offset: 0
data:
- 2"
```

```
cd ~/human_intention/ur5_ws && source devel/setup.bash
rosrun ur_control_wrapper iros_data_clock.py
```

```
cd ~/human_intention/ur5_ws && source devel/setup.bash
rosrun ur_control_wrapper iros_data_collection_pipeline.py
```

```
cd ~/human_intention && source intention-env/bin/activate
python ~/human_intention/ur5_ws/src/ur_control_wrapper/nodes_coco/iros_data_analysis.py
```