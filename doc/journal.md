1. Use James' network setup.
```
cd ~/human_intention/ur5_ws && source devel/setup.bash
roslaunch ur_robot_driver ur5e_jenny_bringup.launch robot_ip:=katherine.csl.illinois.edu limited:=true
```

```
cd ~/human_intention/ur5_ws && source devel/setup.bash
roslaunch ur5_e_moveit_config ur5_e_moveit_planning_execution.launch limited:=true pipeline:=ompl
```

```
cd ~/human_intention/ur5_ws && source devel/setup.bash
roslaunch ur_control_wrapper ur_control_wrapper_jenny.launch robot_ip:=katherine.csl.illinois.edu
```

```
cd ~/human_intention/ur5_ws && source devel/setup.bash
rosrun ur_control_wrapper demo.py
```
