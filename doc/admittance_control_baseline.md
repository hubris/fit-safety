```
cd ~/human_intention/ur5_ws && source devel/setup.bash
roslaunch ur_control_wrapper ur5e_jenny_control.launch
```

```
source ~/human_intention/ur5_ws/devel/setup.bash
rosrun ur_control_wrapper demo_test_ted_origin.py
```

`d`, `grip`, `gcl`, `zft`, `jvc`, `0 0 0`

```
cd ~/human_intention/ur5_ws && source devel/setup.bash
rosrun ur_control_wrapper admittance_control_baseline.py
```

```
cd ~/human_intention/ur5_ws && source devel/setup.bash
rosrun ur_control_wrapper iros_data_clock.py
```

```
cd ~/human_intention/ur5_ws && source devel/setup.bash
rosrun ur_control_wrapper iros_data_collection_pipeline.py
```



```
cd ~/human_intention/
source intention-env/bin/activate
python ~/human_intention/ur5_ws/src/ur_control_wrapper/nodes_coco/iros_data_analysis.py
```