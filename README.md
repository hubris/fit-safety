# fit-safety

Safety deliverables for FIT project.

## Contributors

Zhe Huang, Ye-Ji Mun, Haonan Chen.

## Update log

Q3->Q4->Q7->Q6->Q5

## Installation

### ur5_ws
Run

```
cd ~/fit-safety/ur5_ws/
pip3 install --user ur_rtde
sudo apt update -qq
rosdep update
rosdep install --from-paths src --ignore-src -y
catkin_make
```

### hri_setup
Follow [here](hri_setup/doc/README-rosop-v2.md) to build `rosop` package.

For `Q5-calibration`, set up `ur5ws-env` by running

```
cd ~/fit-safety/ur5_ws
virtualenv -p /usr/bin/python3 ur5ws-env
source ur5ws-env/bin/activate
pip install jupyterlab
pip install numpy
pip install opencv-python
pip install pupil-apriltags
pip install scipy
pip install matplotlib
```

For `Q6-intention`, build `mifwlstm` package by running

```
cd ~/fit-safety/hri_setup && source devel/setup.bash
catkin build mifwlstm
```

For `Q6-intention`, set up `intention-env` by running
```
cd ~/fit-safety/hri_setup
virtualenv -p /usr/bin/python3 intention-env
source intention-env/bin/activate
pip install scipy
pip install matplotlib
pip install torch==1.7.1
pip install pyyaml
pip install rospkg
deactivate
```

## Q3-vision

### How to run
0. Robot and camera setup guidance is in [here](/doc/Q3-robot_safety_setup)
1. Open a new terminal T1. Launch top view camera. Use the serial number of your top view camera for argument `serial_no`.

```
roslaunch realsense2_camera rs_camera.launch align_depth:=True serial_no:=845112072414
```

2. Open a new terminal T2. Modify `openpose_dir` in `rosop/scripts/hand_pixel_perception.py` to the absolute path of OpenPose folder. Run

```
cd ~/fit-safety/hri_setup/
source devel/setup.bash
source openpose-env/bin/activate
roscd rosop
python scripts/hand_pixel_perception.py
```

If you run into an error like `cv_bridge.core.CvBridgeError: encoding specified as bgr8, but image has incompatible type 8UC1`, that means you fail to load the model, and need to modify the absolute path of OpenPose folder.

3. Open a new terminal T3. Run

```
rviz
```

Add Image under `/q3/hand_pixel` in rviz to visualize hand tracking.

4. Open a new terminal T4 to bring up the robot and all the nodes for the communication and control/planning.

```
cd ~/fit-safety/ur5_ws && source devel/setup.bash
roslaunch ur_control_wrapper ur5e_jenny_control.launch
```

5. Open a new terminal T5 to run Q3 node.

```
cd ~/fit-safety/ur5_ws && source devel/setup.bash
rosrun ur_control_wrapper demo_q3.py
```

Input 'q3' on the text interface of `demo_q3.py` and press Enter key.

6. After the robot reaches the default position and stops, open a new terminal T6, and run the clock node to start the finite state machine.

```
python ~/fit-safety/hri_setup/src/rosop/scripts/talker_check_task_clock.py
```

Now we should see the robot keeps moving between two positions, and humans can start interacting with the robot.

### Code explanation
1. hri_setup is a new catkin workspace created solely for rosop (integration of ros-openpose integration). <br/>
Important scripts include <br/>
`~/fit-safety/hri_setup/src/rosop/scripts/hand_pixel_perception.py` <br/>
`~/fit-safety/hri_setup/src/rosop/scripts/talker_check_task_clock.py` <br/>
`~/fit-safety/hri_setup/src/rosop/scripts/talker_keyboard.py` <br/>
where `hand_pixel_perception.py` is used to integrate OpenPose Python API with ROS and `talker_check_task_clock.py` is used to provide a clock with fixed frequency that helps check the task completion asychronously for the finite state machine. <br/>
`talker_keyboard.py` is a temporary file for debugging purposes that can be used to provide human intervention signals by pressing keys. <br/>

2. ur5_ws here is built based on the original repo of ur5_ws for general purposes. New files include <br/>
`ur5_ws/src/ur_control_wrapper/srv/ExecutePause.srv` <br/>
`ur5_ws/src/ur_control_wrapper/node/demo_q3.py` <br/>
where `ExecutePause.srv` provides a ROS service of stopping the robot plan execution, and `demo_q3.py` is an asynchronous finite state machine that checks human intervention and perform accordingly to guarantee safety of the human and the robot. <br/>
Updated files include <br/>
`ur5_ws/src/ur_control_wrapper/CMakeLists.txt` <br/>
`ur5_ws/src/ur_control_wrapper/node/ur_kinematics.py` <br/>
`ur5_ws/src/Universal_Robots_ROS_Driver/ur_robot_driver/config/ur5e_controllers.yaml` <br/>
in which `CMakeLists.txt` adds `ExecutePause.srv` into `add_service_files()`, `ur_kinematics.py` adds services `execute_pause` and `execute_cartesian_plan_asynchronous`, and `ur5e_controllers.yaml` sets `stop_trajectory_duration` as 0.0 to eliminate delay for stopping execution on the software side.

## Q4-contact


### How to run
1. Open a new terminal T1 to bring up the robot and all the nodes for the communication and control/planning.

```
cd ~/fit-safety/ur5_ws && source devel/setup.bash
roslaunch ur_control_wrapper ur5e_jenny_control.launch
```

2. Open a new terminal T2 to run Q4 node.

```
cd ~/fit-safety/ur5_ws && source devel/setup.bash
rosrun ur_control_wrapper demo_q4.py
```

3. After the robot reaches the default position and stops, open a new terminal T3, and run the clock node to start the finite state machine.

```
python ~/fit-safety/hri_setup/src/rosop/scripts/talker_check_task_clock_q4.py
```

4. Open a new terminal T4 and run contact detection algorithm using joint current values.

```
cd ~/fit-safety/ur5_ws && source devel/setup.bash
rosrun ur_control_wrapper human_touch_joints.py
```

### Q4 Notes
1. If you want to visualize the actual joint currents and the expected joint currents of joint 0 (base joint, which can be set as other joints as well), open a new terminal and run

```
cd ~/fit-safety/ur5_ws && source devel/setup.bash
rosrun ur_control_wrapper joint_currents_visualizer.py
```

2. We found that even after execute_pause function (`move_group.stop()`) is finished, the robot still takes noticeable time to finally comes to a pause. The time is longer after the firmware is updated. We probably need to investigate this issue.


## Q5-calibration
Implemented extrinsic calibration for RealSense camera.

### Check before implementation
1. Check `~/fit-safety/ur5_ws/src/ur_control_wrapper/node/demo.py`. Check the path of `self.filename` in `__init__` function and the path of `cv2.imwrite` in `image_snapshot` function. Make sure they match the path of `ur5_ws`.

2. Make sure the folder `~/fit-safety/ur5_ws/data` is created.

### How to run
1. Open a new terminal T1 to start handeye extrinsic calibration program. `serial_no` is used to identify the camera you want to calibrate. `947122061051` is top view camera, and `845112072414` is side view camera at UIUC IRL.

```
cd ~/fit-safety/ur5_ws/ && source devel/setup.bash
roslaunch easy_handeye handeye_calibrate_realsense.launch serial_no:=947122061051
```

2. After running the launch file, two rqt windows and rviz pop out. We only use the `automatic movement` window, and rviz. In rviz, add by topic `/tag_detections_image` and select `Image`. You should see the frame with the apritag of id 0 identified.

3. Open a new terminal T2 to run the demo file for data collection.

```
cd ~/fit-safety/ur5_ws/ && source devel/setup.bash
rosrun ur_control_wrapper demo.py
```

4. Go to the `automatic movement` window, click on `check starting pose`. Click on `Next Pose`, `Plan`, and `Execute` in order. After the end-effector finishes moving, wait for about half second to make sure apriltag detection is stablized in `Image` on rviz. Then input `sa` and press `Enter` key in T2 to collect end-effector pose and image.

5. Keep repeating `Next Pose`, `Plan`, and `Execute` in the `automatic movement` window, `sa` and `Enter` key in T2 until 17 samples are collected. Then click on `check starting pose` in  the `automatic movement` window. Then repeat the data collection for a second round. After you finish, you should have collected 34 samples.

6. Confirm in `~/fit-safety/data` that you have 34 images and a txt file. Now data collection phase is over. Kill all the programs in T1 and T2. 

7. Create a folder named `~/fit-safety/data/2x2_alu`, and move all 34 images and the txt file into `~/fit-safety/data/2x2_alu`.

8. Open a new terminal T3, and start jupyter notebook.

```
cd ~/fit-safety/ur5_ws && source ur5ws-env/bin/activate && jupyter notebook
```

9. Click on `src` and open `Extrinsic_calibration.ipynb`. Make sure that you have the correct camera instrinsics parameters in the parameter `camera` in the second cell. If you are not sure about the camera instrinsic parameters, open a new terminal T4, start the RealSense ROS node.

```
roslaunch realsense2_camera rs_camera.launch align_depth:=True serial_no:=947122061051
```

Open a new terminal T5, and run

```
rostopic echo /camera/color/camera_info
```

to retrieve camera intrinsic information. Then kill programs in T4 and T5.

10. Follow instructions in `Extrinsic_calibration.ipynb` and run all the cells. The final error should be around `89.75` for RealSense at the end of iteration. `T_cam_in_base` is what we need. Here are the typical error statistics for Intel RealSense D435.

```
mean error (pixels) = 0.36
min error (pixels) = 0.03
max error (pixels) = 1.10
median error (pixels) = 0.34
```

## Q6-intention

### How to run
0. Change the camera serial numbers in [human_tracker_robot.launch](/hri_setup/src/rosop/launch/human_tracker_robot.launch).


   Change the args of camera_link_broadcaster_top and camera_link_broadcaster_right in [camera_link_broadcaster.launch](/hri_setup/src/rosop/launch/camera_link_broadcaster.launch). The args for camera_link_broadcaster_top/right can be computed in [camera_link_broadcaster.py](/hri_setup/src/rosop/scripts/camera_link_broadcaster.py). Please make sure to copy the corresponding `T_cam_in_base` from Extrinsic_calibration.ipynb to the file first.


   Change the openpose_dir and [camera_transformation_matrix](/hri_setup/src/rosop/scripts/human_skeleton_detector_base_frame.py#L205) in [human_skeleton_detector_base_frame.py](/hri_setup/src/rosop/scripts/human_skeleton_detector_base_frame.py).

   Customize [intention_coordinates](/hri_setup/src/mifwlstm/src/api/hri_intention_application_interface.py#L418) for your case. To get the values, put your right wrist at the desired part locations, and check the position published in topic `/hri/human_wrist_pos_in_base`.


1. Open a new terminal T1 to run intention filter.

```
cd ~/fit-safety/hri_setup/ && source devel/setup.bash
roslaunch mifwlstm online_filter.launch num_intentions:=2
```

2. Open a new terminal T2 to bring up the robot, and control nodes which respond to human intention estimation.

```
cd ~/fit-safety/ur5_ws/ && source devel/setup.bash
roslaunch ur_control_wrapper hri_human_intention.launch
```

## Q7-contact-2

### How to run

1. Open a new terminal T1 to bring up the robot and all the nodes for the communication and control/planning.

```
cd ~/fit-safety/ur5_ws && source devel/setup.bash
roslaunch ur_control_wrapper ur5e_jenny_control.launch
```

2. Open a new terminal T2 and initialize the robot.

```
cd ~/fit-safety/ur5_ws && source devel/setup.bash
rosrun ur_control_wrapper demo_test_ted_origin.py
```

Go to `d`. To stop robot, do `jvc`, and then do `0 0 0`, and then do `zft`.

3. Open a new terminal T3 and run:

```
cd ~/fit-safety/ur5_ws && source devel/setup.bash
rosrun ur_control_wrapper simple_route_q7.py
```

4. Open a new terminal T4 and run:

```
cd ~/fit-safety/ur5_ws && source devel/setup.bash
rosrun ur_control_wrapper admittance_control_q7.py
```

5. Open a new terminal T5 and run:
```
cd ~/fit-safety/ur5_ws && source devel/setup.bash
rosrun ur_control_wrapper human_touch_ee.py
```

### Q7 notes
When killing programs, always first kill `ur5e_jenny_control.launch` to immediately stop the robot. Otherwise, the joint velocity command may keep moving the robot to hit the table, itself or the human and can be dangerous.



## Y3Q1-vision-DT-GUI
Run
```
sudo apt-get install ros-$ROS_DISTRO-realsense2-description

```
Note that the UR5e and hand models will appear in RViz while replaying the recorded YAML file. 

Follow [here](doc/digital_twin_GUI.md) to run using user interface.
Import `user_interface.json` to Node-RED.


## Y3Q2-contact-GUI
Follow [here](doc/contact_GUI.md) to run using user interface.
Import `y3q2-contact-based-safety-GUI.json` to Node-RED.

## Intention-GUI
Follow [here](doc/intention_GUI.md) to run using user interface.
Import `Q6_Intention_UI.json` to Node-RED.


## Y4Q1 joint velocity control for safety monitoring
Follw [here](README_q13.md)