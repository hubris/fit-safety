import pickle
import numpy as np
import pathhack
from os.path import join
with open(join(pathhack.pkg_path, 'raw_datasets', '0.p'), 'rb') as f:
    data = pickle.load(f)
intentions, trajs = dataset['intentions'], dataset['trajs']
print(intentions) # np array (40,) # with intentions 1, 2, and 3
print((intentions==1).sum()) # 12
print((intentions==2).sum()) # 16
print((intentions==3).sum()) # 12