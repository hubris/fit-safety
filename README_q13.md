## Y4Q1 joint velocity control for safety monitoring
Run joint velocity control for safety monitoring.

```
cd ~/fit-safety/ur5_ws && source devel/setup.bash
roslaunch ur_control_wrapper ur5e_jenny_control.launch
```

```
cd ~/fit-safety/ur5_ws && source devel/setup.bash
rosrun ur_control_wrapper joint_velocity_control.py
```

```
cd ~/fit-safety/ur5_ws && source devel/setup.bash
rosrun ur_control_wrapper simple_route_q12.py
```

Then run any combinations of vision safety, contact detection (joints) and contact detection (end-effector) as below. You can run any 1, 2 or all 3 of them.

(1) Vision:
```
roslaunch realsense2_camera rs_camera.launch align_depth:=True
```

```
cd ~/fit-safety/hri_setup/
source devel/setup.bash
source openpose-env/bin/activate
roscd rosop
python scripts/hand_pixel_perception.py
```

Run
```
rviz
```
Add Image under `/q3/hand_pixel` in rviz to visualize hand tracking.

(2) Contact detection (joints):
```
cd ~/fit-safety/ur5_ws && source devel/setup.bash
rosrun ur_control_wrapper human_touch_joints.py
```

(3) Contact detection (end-effector):
```
cd ~/fit-safety/ur5_ws && source devel/setup.bash
rosrun ur_control_wrapper human_touch_ee.py
```

You can see the terminal where you run `joint_velocity_control.py` to see the pause/resume message and cause (vision/end-effector contact/joint contact). Or you can run
```
cd ~/fit-safety/ur5_ws && source devel/setup.bash
rostopic echo hri/log
```
to read the log messages.

### Q12 notes
1. Always kill the program in the terminal which you run `roslaunch ur_control_wrapper ur5e_jenny_control.launch` first. Otherwise the robot will move joints by the last joint velocity command which is nonzero until it triggers safety stop.
2. Tune the speed of the robot by changing `self.large_vel` and `self.small_vel` in line 16 and line 17 of `~/fit-safety/ur5_ws/src/ur_control_wrapper/nodes_vision_monitor_q12/simple_route_q12.py`.
3. Set the shared space for vision safety by changing `self.shared_space` in line 28 of `~/fit-safety/hri_setup/src/rosop/scripts/hand_pixel_perception.py`.
4. Tune the force thresholds of joint contact detection by changing `self.std_thresholds` in line 19 of `~/fit-safety/ur5_ws/src/ur_control_wrapper/node/human_touch_joints.py`.
5. Tune the force thresholds of end-effector contact detection by changing `f_thresholds` in line 13 of `~/fit-safety/ur5_ws/src/ur_control_wrapper/nodes_q7/human_touch_ee.py`.