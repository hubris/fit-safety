# rosop: ros-openpose integration

### Prerequisites

You need to finish the tutorial on OpenPose Installation. Setting up Python3 API of OpenPose is required for ros-openpose integration. Check [here](./README-OpenPose.md) if you have not done so.

### Install Intel RealSense packages

We are using a Intel D435i RealSense Depth Camera. Now we need to set up Intel RealSense. Check out librealsense2 [[ROS wiki]](http://wiki.ros.org/librealsense2) [[installation]](https://github.com/IntelRealSense/librealsense/blob/master/doc/distribution_linux.md#installing-the-packages), and realsense2_camera [[ROS wiki]](http://wiki.ros.org/realsense2_camera) [[installation]](https://github.com/IntelRealSense/realsense-ros#method-1-the-ros-distribution) for setting up Intel RealSense.
1. Set up librealsense2
    - Register the server's public key by running
        ```
        sudo apt-key adv --keyserver keys.gnupg.net --recv-key F6E65AC044F831AC80A06380C8B3A55A6F3EFCDE || sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-key F6E65AC044F831AC80A06380C8B3A55A6F3EFCDE
        ```
    In case the public key still cannot be retrieved, check and specify proxy settings by running 
        ```
        export http_proxy="http://<proxy>:<port>"
        ```
    Then rerun the command. See additional methods [here](https://unix.stackexchange.com/questions/361213/unable-to-add-gpg-key-with-apt-key-behind-a-proxy).  
    - Add the server to the list of repositories by running
        ```
        sudo add-apt-repository "deb https://librealsense.intel.com/Debian/apt-repo $(lsb_release -cs) main" -u
        ```
    - Install the libraries (see section below if upgrading packages) by running
        ```
        sudo apt-get install librealsense2-dkms
        sudo apt-get install librealsense2-utils
        ```
2. Test the setup of librealsense2 by running
    ```
    realsense-viewer
    ```
3. Set up realsense2_camera by running
    ```
    sudo apt-get install ros-$ROS_DISTRO-realsense2-camera
    ```
    where `$ROS_DISTRO` is `melodic` in our case.
4. Test realsense2_camera by running
    ```
    roslaunch realsense2_camera rs_camera.launch align_depth:=True
    ```
    Then open another terminal, and run
    ```
    rviz
    ```
    Click `Add` button, select `By topic` tab, and click on the option `/camera/color/image_raw/image (raw)`, and click `OK` button. You should see `Image` tab on live.

### Fix Compatibility Issue between Python3, OpenCV, and ROS 
The key problem of setting up Python3 Environments for rosop is a bug related to `cv_bridge`. This happens when you want to use Python3, OpenCV, and ROS together. Check out the [webpage](https://stackoverflow.com/questions/49221565/unable-to-use-cv-bridge-with-ros-kinetic-and-python3) for the solution.
Note the steps below are adjusted for ROS Melodic and Python 3.6.9.
We find there is no harm to ignore warnings in step 2, 3, 4 and 8.

1. Install necessary packages by running
    ```
    sudo apt-get install python-catkin-tools python3-dev python3-catkin-pkg-modules python3-numpy python3-yaml ros-melodic-cv-bridge
    ```
2. Go to the root folder and initialize a catkin workspace.
    ```
    cd hri_setup/
    catkin init
    ```
3. Instruct catkin to set cmake variables by running
    ```
    catkin config -DPYTHON_EXECUTABLE=/usr/bin/python3 -DPYTHON_INCLUDE_DIR=/usr/include/python3.6m -DPYTHON_LIBRARY=/usr/lib/x86_64-linux-gnu/libpython3.6m.so
    ```
4. Clone cv_bridge src by running
    ```
    git clone https://github.com/ros-perception/vision_opencv.git src/vision_opencv
    ```
5. Find version of cv_bridge in your repository by running
    ```
    apt-cache show ros-melodic-cv-bridge | grep Version
    ```
    In our case, the output is
    ```
    Version: 1.13.0-0bionic.20210505.032238
    ```
6. Checkout right version in git repo. In our case, it is 1.13.0.
    ```
    cd src/vision_opencv/
    git checkout 1.13.0
    cd ../../
    ```
7. Build `cv_bridge` and `rosop` by running
    ```
    catkin build cv_bridge
    catkin build rosop
    ```
    If there is an import error `ModuleNotFoundError: No module named 'em'` when building `rosop`, install the package and build `rosop` again.
    ```
    pip install empy
    catkin build rosop
    ```
9. Set up environment with new packages by running
    ```
    source devel/setup.bash
    ```
10. Test if the bug is fixed by running the following without error. (Make sure you use `python3.6`!)
    ```
    $ python3.6
    Python 3.6.9 (default, Jan 26 2021, 15:33:00) 
    [GCC 8.4.0] on linux
    Type "help", "copyright", "credits" or "license" for more information.
    >>> from cv_bridge.boost.cv_bridge_boost import getCvType
    >>> 
    ```

### Set up Python3 Virtual Environment for rosop

We use `virtualenv` to creat python3 virtual environment for rosop. It is generally a good habit to create a virtual environment to isolate packages to avoid conflicts.
1. Install `virtualenv` by running
    ```
    sudo apt install virtualenv
    ```
    We do not recommend running `pip install virtualenv`.
2. Create a virtualenv by running
    ```virtualenv -p /usr/bin/python3 openpose-env```
3. Enter the environment and install packages. Run
    ```
    source openpose-env/bin/activate
    pip install -r src/rosop/requirements.txt
    ```

### Run Q3 Human-Robot Interaction Vision Baseline
1. Open a new terminal T1. Run
    ```
    roslaunch realsense2_camera rs_camera.launch align_depth:=True
    ```
2. Open a new terminal T2. Modify `openpose_dir` in `rosop/scripts/hand_pixel_perception.py` to the absolute path of OpenPose folder. Run
    ```
    cd hri_setup/
    source devel/setup.bash
    source openpose-env/bin/activate
    roscd rosop
    python scripts/hand_pixel_perception.py
    ```
    If you run into an error like `cv_bridge.core.CvBridgeError: encoding specified as bgr8, but image has incompatible type 8UC1`, that means you fail to load the model, and need to modify the absolute path of OpenPose folder.
3. Open a new terminal T3. Run <br/>
    ```
    rviz
    ```
4. Add Image under `/q3/hand_pixel` in rviz to visualize hand tracking.
5. When done running the implementation, kill the programs. If you need to exit the virtual environment in T2, run
    ```
    deactivate
    ```