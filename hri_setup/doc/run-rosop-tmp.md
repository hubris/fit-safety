# without robot
```
roslaunch realsense2_camera rs_camera.launch align_depth:=True
```


```
cd ~/hri_setup/
source devel/setup.bash
source openpose-env/bin/activate
roscd rosop
python scripts/human_skeleton_tracker_robust.py
```

python scripts/human_skeleton_detector.py


```
cd ~/hri_setup/
source devel/setup.bash
source openpose-env/bin/activate
roscd rosop
python scripts/human_skeleton_visualizer.py
```
<!-- rosrun tf static_transform_publisher 0 0 0 0 0 0 map base_link 50 -->
```
cd ~/hri_setup/ && source devel/setup.bash
roslaunch rosop camera_link_broadcaster.launch add_base_link:=true
```

# with robot
```
roslaunch realsense2_camera rs_camera.launch align_depth:=True
```
```
cd ~/hri_setup/
source devel/setup.bash
source openpose-env/bin/activate
roscd rosop
python scripts/human_skeleton_detector.py
```
```
cd ~/hri_setup/
source devel/setup.bash
source openpose-env/bin/activate
roscd rosop
python scripts/human_skeleton_visualizer.py
```
```
cd ~/hri_setup/ && source devel/setup.bash
roslaunch rosop camera_link_broadcaster.launch add_base_link:=false
```
```
cd ~/ur5_ws/ && source devel/setup.bash
roslaunch ur_control_wrapper ur5e_hande_jenny_control.launch
```
```
cd ~/ur5_ws/ && source devel/setup.bash
roslaunch ur5_e_moveit_config moveit_rviz.launch config:=true
```
