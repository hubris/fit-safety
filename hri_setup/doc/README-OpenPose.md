# Tutorial on OpenPose Installation

This is a tutorial on installing OpenPose from source, and running the demo in the [original repo](https://github.com/CMU-Perceptual-Computing-Lab/openpose). Materials are re-organized based on the guide from the original repo to fit our purpose. 
### Ubuntu Prerequisites

Check out the [webpage](https://github.com/CMU-Perceptual-Computing-Lab/openpose/blob/master/doc/installation/1_prerequisites.md#ubuntu-prerequisites) about **Ubuntu Prerequisites**.

1. You should **NOT** run OpenPose in a conda environment.
2. Install CMake GUI:
    - Ubuntu 18:
        - Uninstall your current Cmake-gui version by running
            ```sudo apt purge cmake-qt-gui```
        - Install OpenSSL for building CMake by running
            ```sudo apt install libssl-dev```
        - Run
            ```sudo apt-get install qtbase5-dev```
        - Download the `Latest Release` of `CMake Unix/Linux Source` from the [CMake download website](https://cmake.org/download/), called `cmake-X.X.X.tar.gz` in the root folder `hri_setup/`. 
            - By May 27, 2021 the newest version is `cmake-3.20.3.tar.gz`. We use this name for the following setup.
        - Unzip it and run the following commands. Make sure no error occurred. 
            ```
            cd cmake-3.20.3/
            ./configure --qt-gui
            ./bootstrap && make -j`nproc` && sudo make install -j`nproc`
            ```
        - !!! **DO NOT** install the default CMake-gui version (3.10) via `sudo apt-get install cmake-qt-gui`. Required CMake version >= 3.12.
3. Make sure Nvidia GPU is set up. We have successfully installed OpenPose with the pair of RTX2080-CUDA10.2 and the pair of RTX3090-CUDA11.2.
4. Install OpenCV by running
    ```sudo apt-get install libopencv-dev```
5. Now we can finally clone the repo. Go back to the root folder `hri_setup/`. Run
    ```
    git clone https://github.com/CMU-Perceptual-Computing-Lab/openpose
    cd openpose/
    git submodule update --init --recursive --remote
    ```
6. Install Caffe dependencies by running
    ```sudo bash ./scripts/ubuntu/install_deps.sh```

### Compile and Run OpenPose from Source

Check out the [webpage](https://github.com/CMU-Perceptual-Computing-Lab/openpose/blob/master/doc/installation/0_index.md#compiling-and-running-openpose-from-source
) about **Compiling and Running OpenPose from Source**.

1. Create folder `build/` in `openpose` and run `cmake-gui` using the absolute filepath, which is shown in my machine as below.
    ```
    cd openpose
    mkdir build/
    cd build/
    /home/haonan/fit-safety/hri_setup/cmake-3.20.3/bin/cmake-gui ..
    ```
2. Confirm that in the GUI, the path for `Where is the source code` should be `/home/haonan/fit-safety/hri_setup/openpose`. The path for `Where to build the binaries` should be `/home/haonan/fit-safety/hri_setup/openpose/build`.
3. Press `Configure` button, and choose the generator as `Unix Makefiles`. Make sure no error occurred.
4. Check the value of `BUILD_PYTHON`, because we want to have Python API. Confirm that the value of `GPU_MODE` is `CUDA`. Press `Generate` button. Make sure no error occurred. Close GUI.
5. Confirm that you are in `openpose/build/`, and compile OpenPose by running
    ```
    cd openpose/build/
    make -j`nproc`
    ```
6. Run a video demo to test if OpenPose is successfully compiled. [UPDATE on 10/06/21] If you come across out-of-memory error, you may want to check your cudnn version. The version needs to be below `8.0.0`. `7.6.5` works for my RTX 2080 with 7973 MB memory. Uninstall the current cudnn, download `deb` packages for both developer and runtime libraries below `8.0.0` and install them.
    ```
    cd openpose/
    ./build/examples/openpose/openpose.bin --video examples/media/video.avi
    ```
7. Plug in an Intel RealSense Camera and test whether OpenPose can be run on a camera. The camera index `--camera 4` is used for d435i. [UPDATE on 10/06/21] This seems to not work now. An error pops up with the message `VideoCapture (webcam) could not be opened.` Though this does not work, the ros-openpose integration can still work. No worries. [UPDATE on 10/16/21] This seems to still work on the LittleAlien machine with RealSense Camera.
    ```
    cd openpose/
    ./build/examples/openpose/openpose.bin --camera 4
    ```
    

### Set up Python3 API

Check out the [webpage](https://github.com/CMU-Perceptual-Computing-Lab/openpose/blob/master/doc/03_python_api.md#exporting-python-openpose) about setting up Python3 API of OpenPose. 
It is optional to set up Python3 API if you only need to run OpenPose exclusively. If you want to integrate OpenPose with ROS (check [here](./README-rosop.md)), Python3 API setup is required. 

1. Go to the build folder and install OpenPose.
    ```
    cd openpose/build/
    sudo make install
    ```
2. Test if you can run the following lines without error. (Make sure you use `python3.6`!)
    ```
    $ python3.6
    Python 3.6.9 (default, Jan 26 2021, 15:33:00) 
    [GCC 8.4.0] on linux
    Type "help", "copyright", "credits" or "license" for more information.
    >>> import sys
    >>> sys.path.append('/usr/local/python')
    >>> from openpose import pyopenpose as op
    >>> 
    ```

3. You may now put the following lines in your python script, and use OpenPose Python3 API at any location. (Make sure you use `python3.6`!)
    ```
    import sys
    sys.path.append('/usr/local/python')
    from openpose import pyopenpose as op
    ```
Additional steps are needed to run OpenPose with ROS. Check [here](./README-rosop.md) for more details.

### Uninstall or Reinstall OpenPose.

Check out the [webpage](https://github.com/CMU-Perceptual-Computing-Lab/openpose/blob/1f1aa9c59fe59c90cca685b724f4f97f76137224/doc/installation/0_index.md#uninstalling-reinstalling-or-updating-openpose) about uninstalling or reinstalling OpenPose.
If you want to reinstall or update OpenPose, uninstall OpenPose and redo the previous steps. Below are the steps to uninstall OpenPose.

1.  If you ran `sudo make install` , then run
    ```
    cd openpose/build/
    sudo make uninstall
    ```
2. Remove the whole `openpose` folder.
    ```
    rm -rf openpose/
    ```









