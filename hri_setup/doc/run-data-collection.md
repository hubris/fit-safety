Use normal python2.
```
cd hri_setup/
source devel/setup.bash
roscd rosop
cd scripts/
python data_collection_pipeline.py
```

```
1 # for loading/initializing trajectory index file
sn Zhe
ii 1
s
e
s
e
s
e
ii 2
s
e
s
e
```
...