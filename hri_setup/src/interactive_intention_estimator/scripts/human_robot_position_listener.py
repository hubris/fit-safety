#!/usr/bin/env python
import rospy
import tf
import numpy as np
import time
from std_msgs.msg import Float64MultiArray
from rosop.msg import HumanSkeletonPosition


class InteractiveIntentionEstimator:
    def __init__(self, args, tracking_topic_name, transformation_matrix):
        self.args = args
        self.tracking_topic_name = tracking_topic_name
        self.transformation_matrix = transformation_matrix
        # self.robot_links = ['/upper_arm_link', '/forearm_link', '/wrist_1_link', \
        #     '/wrist_2_link', '/wrist_3_link', '/tool0_controller']
        self.robot_links = ['/wrist_3_link']
        rospy.init_node('interactive_intention_estimator')
        self.tf = tf.TransformListener()
        self.human_robot_pos_pub = rospy.Publisher('/hri/human_robot_pos', Float64MultiArray, queue_size=10)
        rospy.Subscriber(self.tracking_topic_name, HumanSkeletonPosition, self.tracking_callback)
    
    def tracking_callback(self, data):
        pos = np.array(data.data).reshape(data.n_humans, data.n_keypoints, data.n_dim) # (n_human, 25, 3)
        wrist_pos_camera = pos[0,4] # (3,) right wrist
        wrist_pos = np.dot(self.transformation_matrix, np.concatenate((wrist_pos_camera,[1.]),axis=0))[:3] # (3,) # with respect to base frame
        robot_pos = self.get_robot_positions().reshape(-1) # (6*3,)
        human_robot_pos_msg = Float64MultiArray()
        human_robot_pos_msg.data = [time.time()]+list(wrist_pos)+list(robot_pos) # (22,)
        self.human_robot_pos_pub.publish(human_robot_pos_msg)

    def get_robot_positions(self):
        """
        Get 3D positions of robot links (joints). Realtime speed, around 1e-6 sec.
        Outputs:
            - trans_np
                # robot positions with respect to base frame.
                # np. (num_robot_links, 3)
                # num_robot_links is now 6.
        """
        try:
            trans_list = []
            for robot_link in self.robot_links:
                (trans,_) = self.tf.lookupTransform('/base_link', robot_link, rospy.Time(0))
                trans_list.append(trans)
            robot_pos = np.stack(trans_list, axis=0) # (num_robot_links,3)
            return robot_pos
        except (tf.LookupException, tf.ConnectivityException, tf.ExtrapolationException):
            return np.zeros((6,3))


if __name__ == '__main__':
    args = None
    tracking_topic_name = "/hri/human_skeleton_position_tracking_3D"
    # 211226
    # transformation_matrix = np.array(
    #     [[0.0032898886322115834, 0.9984757643360561, -0.05509303744120117, -0.46333356292109323],
    #     [0.9997763891577924, -0.002133516020186328, 0.021035624419735218, -0.011359965937976602],
    #     [0.020886036500205544, -0.0551498715484418, -0.9982596618269273, 1.840785921626763],
    #     [0.0, 0.0, 0.0, 1.0]]
    # )
    # 211226 # side view
    # transformation_matrix = np.array(
    #     [[0.5979921712580036, 0.05913075806381358, 0.7993178152508659, -1.3656591135495784],
    #     [-0.8008567963509614, 0.08408779588492497, 0.5929230030401672, -0.3095152349593127],
    #     [-0.03215288494182629, -0.9947023658342837, 0.0976391079106965, 0.1346075431587562],
    #     [0.0, 0.0, 0.0, 1.0]]
    # )
    # 211230 # side view
    transformation_matrix = np.array(
        [[0.6055597400674477, 0.048974329049555995, 0.7942914890114778, -1.3654056583511713],
        [-0.7957994657520805, 0.038289313468252845, 0.6043485682909427, -0.30953485205521564],
        [-0.0008153101204324168, -0.998065851421597, 0.06216020829670067, 0.13329137327503668],
        [0.0, 0.0, 0.0, 1.0]]
    )
    iie = InteractiveIntentionEstimator(args, tracking_topic_name, transformation_matrix)
    rospy.spin()