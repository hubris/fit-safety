#!/usr/bin/env python
import rospy
import tf
import numpy as np
import time


# '/base_link', '/upper_arm_link', '/forearm_link', '/wrist_1_link', '/wrist_2_link', '/wrist_3_link', '/tool0_controller'
# (trans_tool_base, rot_tool_base) = self.tf.lookupTransform('/base_link', '/tool0_controller', rospy.Time(0))
if __name__ == '__main__':
    rospy.init_node('robot_position_listener')
    listener = tf.TransformListener()
    rate = rospy.Rate(30.0)
    moving_links = ['/upper_arm_link', '/forearm_link', '/wrist_1_link', '/wrist_2_link', '/wrist_3_link', '/tool0_controller']
    while not rospy.is_shutdown():
        try:
            trans_list = []
            for moving_link in moving_links:
                (trans,_) = listener.lookupTransform('/base_link', moving_link, rospy.Time(0))
                trans_list.append(trans)
            trans_np = np.stack(trans_list, axis=0)
            print(trans_np)
            start_t = time.time()
            print(time.time()-start_t)
        except (tf.LookupException, tf.ConnectivityException, tf.ExtrapolationException):
            continue
        

        # angular = 4 * math.atan2(trans[1], trans[0])
        # linear = 0.5 * math.sqrt(trans[0] ** 2 + trans[1] ** 2)
        # cmd = geometry_msgs.msg.Twist()
        # cmd.linear.x = linear
        # cmd.angular.z = angular
        # turtle_vel.publish(cmd)

        rate.sleep()