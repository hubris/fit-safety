from pathhack import pkg_path
import time
import numpy as np
import torch
import argparse
import pickle

from mifwlstm.src.mif.intention_particle_filter import IntentionParticleFilter
from mifwlstm.src.api.hri_intention_application_interface import HRIIntentionApplicationInterface
# from src.api.pedestrian_intention_application_interface import PedestrianIntentionApplicationInterface
from mifwlstm.src.utils import load_preprocessed_train_test_dataset
import time
import rospy
from rosop.msg import HumanSkeletonPosition
from std_msgs.msg import Float32MultiArray
# from tf import transformations as tfs

def arg_parse():
    parser = argparse.ArgumentParser()
    # * Intention LSTM
    # Model Options
    parser.add_argument('--bidirectional', action='store_true')
    parser.add_argument('--num_layers', default=1, type=int)
    parser.add_argument('--embedding_size', default=32, type=int)
    parser.add_argument('--hidden_size', default=32, type=int)
    parser.add_argument('--dropout', default=0., type=float)
    parser.add_argument('--obs_seq_len', default=20, type=int)
    parser.add_argument('--pred_seq_len', default=20, type=int)
    parser.add_argument('--motion_dim', default=3, type=int)
    parser.add_argument('--device', default="cuda:0", type=str)
    # Scene Options
    parser.add_argument('--num_intentions', default=2, type=int)
    parser.add_argument('--random_seed', default=0, type=int)

    # * Mutable Intention Filter
    # filtering
    parser.add_argument('--num_particles_per_intention', default=100, type=int,\
        help="10, 30, 50, 100.")
    parser.add_argument('--Tf', default=20, type=int, help="should be equal to pred_seq_len")
    parser.add_argument('--T_warmup', default=20, type=int, help="Number of steps for minimum truncated observation. At least 2 for heuristic in ilm. should be equal to obs_seq_len.")
    parser.add_argument('--step_per_update', default=2, type=int, help="1, 2")
    parser.add_argument('--tau', default=0.1, type=float, help="1., 10.")
    parser.add_argument('--prediction_method', default='ilstm', type=str, help='ilstm, wlstm or ilm.')
    parser.add_argument('--mutable', action='store_true')
    parser.add_argument('--mutation_prob', default=1e-2, type=float)
    parser.add_argument('--scene', default='fit', type=str)
    # evaluation
    parser.add_argument('--num_top_intentions', default=3, type=int, help='Number of top probability intentions. Options: 1, 3.')
    return parser.parse_args()


class OnlineMIF:
    def __init__(self, args, device, tracking_topic_name):
        self.intention_api = HRIIntentionApplicationInterface(
            method=args.prediction_method,
            scene=args.scene,
            args=args,
            pkg_path=pkg_path,
            device=device,
        )
        print("tau: ",args.tau)
        self.intention_pf = IntentionParticleFilter(
            self.intention_api.get_num_intentions(),
            args.num_particles_per_intention,
            self.intention_api,
            args.tau,
            args.mutation_prob,
        )
        self.args = args
        # self.transformation_matrix = transformation_matrix
        # ! without max traj len, trajectory will keep growing.
        # ! Have a reset button for bunch of experiments?
        rospy.init_node('online_mutable_intention_filter', anonymous=True)
        self.intention_prob_dist_pub = rospy.Publisher("hri/human_intention_prob_dist", Float32MultiArray, queue_size=1)
        self.human_wrist_pos_in_base_pub = rospy.Publisher("hri/human_wrist_pos_in_base", Float32MultiArray, queue_size=1)
        rospy.Subscriber(tracking_topic_name, HumanSkeletonPosition, self.tracking_callback)
        self.reset()
    
    def reset(self):
        self.intention_pf.reset()
        self.trajectory = np.empty((0,args.motion_dim))
        self.current_filtering_results = {}
        self.current_filtering_results['intention_prob_dist'] = self.intention_pf.get_intention_probability()
        self.current_filtering_results['predicted_trajectories'] = None
        self.current_filtering_results['particle_intentions'] = self.intention_pf.get_intention()
        intention_prob_dist_msg = Float32MultiArray()
        intention_prob_dist_msg.data = list(self.current_filtering_results['intention_prob_dist'].astype(np.float32))
        self.intention_prob_dist_pub.publish(intention_prob_dist_msg)

    def tracking_callback(self, data):
        pos = np.array(data.data).reshape(data.n_humans, data.n_keypoints, data.n_dim) # (n_human, 25, 3)
        wrist_pos = pos[0,4] # (3,) right wrist
        if np.linalg.norm(wrist_pos)<1e-5:
            # invalid, only happens in the beginning
            return
        # wrist_pos_tf = np.dot(self.transformation_matrix, np.concatenate((wrist_pos,[1.]),axis=0))[:3] # (3,)
        wrist_pos_tf = wrist_pos
        human_wrist_pos_in_base_msg = Float32MultiArray()
        human_wrist_pos_in_base_msg.data = list(wrist_pos_tf.astype(np.float32))
        self.human_wrist_pos_in_base_pub.publish(human_wrist_pos_in_base_msg)
        # print(wrist_pos_tf)
        # right [-0.76313097  0.18699681  0.07209993] # before 211202
        # left [-0.51391511  0.22016075  0.05691014] # before 211202
        # left [-0.54554506  0.18753483  0.0330741 ] # 211202
        # right [-0.80372266  0.17263423  0.02678953] # 211202
        # B1 [-0.54554506  0.18753483  0.0330741 ] # 211203
        # B2 [-0.67009807  0.17764449  0.03653744] # 211203
        # B3 [-0.80372266  0.17263423  0.02678953] # 211203
        # B4 [-0.5378981  -0.04835606  0.01345574] # 211203
        # B5 [-0.66472658 -0.04871283  0.0049764 ] # 211203
        # B6 [-0.80013095 -0.04855835  0.01280885] # 211203
        self.trajectory = np.concatenate((self.trajectory, wrist_pos_tf[np.newaxis,:]*100), axis=0) # (t, 3)
        if len(self.trajectory) > 500:
            self.trajectory = self.trajectory[-500:] # at most length of (500, 3)
        if len(self.trajectory) < self.args.Tf+self.args.T_warmup:
            return
        if (len(self.trajectory)-(self.args.Tf+self.args.T_warmup)) % self.args.step_per_update == 0:
            x_obs = self.trajectory # [-self.args.Tf-self.args.T_warmup:]
            self.intention_pf.predict(x_obs)
            self.intention_pf.update_weight(x_obs)
            self.intention_pf.resample()
            if self.args.mutable:
                self.intention_pf.mutate()
            intention_prob_dist = self.intention_pf.get_intention_probability() # (2,)
            predicted_trajectories = self.intention_api.predict_trajectories(
                x_obs,
                self.intention_pf.get_intention(),
                truncated=True, # does not matter for ilstm, does matter for ilm
            )
            self.current_filtering_results['intention_prob_dist'] = intention_prob_dist
            self.current_filtering_results['predicted_trajectories'] = predicted_trajectories
            self.current_filtering_results['particle_intentions'] = self.intention_pf.get_intention()
            intention_prob_dist_msg = Float32MultiArray()
            intention_prob_dist_msg.data = list(intention_prob_dist.astype(np.float32))
            self.intention_prob_dist_pub.publish(intention_prob_dist_msg)
    
    def get_current_intention_prob_dist(self):        
        return self.current_filtering_results['intention_prob_dist']

    def run(self):
        while not rospy.is_shutdown():
            command_input = input("\
                \n=====================================================\
                \nReset for next round: r\
                \nGet current intention probability distribution: g\
                \n=====================================================\
                \nCommand: ")
            if command_input == "r":
                self.reset()
            elif command_input == "g":
                curr_intention_prob_dist = self.get_current_intention_prob_dist()
                print()
                print(curr_intention_prob_dist)
                print()
            else:
                print("Th command ["+command_input+"] is invalid.")

if __name__ == '__main__':
    tracking_topic_name = "/hri/human_skeleton_position_tracking_3D"
    # 211202 # top down view
    # transformation_matrix = np.array(
    #     [[0.0021317922423761374, 0.9983572246475552, -0.05725579065951769, -0.4592949889034955],
    #     [0.9998419196667168, -0.0011175089383325249, 0.017741605160713724, -0.008644474966984353],
    #     [0.017648492498782677, -0.057284510011343534, -0.9982019349353066, 1.8398017828469182],
    #     [0.0, 0.0, 0.0, 1.0]]
    # )
    # 211226 # top down view
    # transformation_matrix = np.array(
    #     [[0.0032898886322115834, 0.9984757643360561, -0.05509303744120117, -0.46333356292109323],
    #     [0.9997763891577924, -0.002133516020186328, 0.021035624419735218, -0.011359965937976602],
    #     [0.020886036500205544, -0.0551498715484418, -0.9982596618269273, 1.840785921626763],
    #     [0.0, 0.0, 0.0, 1.0]]
    # )
    # 211230 # side view
    # transformation_matrix = np.array(
    #     [[0.6055597400674477, 0.048974329049555995, 0.7942914890114778, -1.3654056583511713],
    #     [-0.7957994657520805, 0.038289313468252845, 0.6043485682909427, -0.30953485205521564],
    #     [-0.0008153101204324168, -0.998065851421597, 0.06216020829670067, 0.13329137327503668],
    #     [0.0, 0.0, 0.0, 1.0]]
    # )
    args = arg_parse()
    args.Tf = args.pred_seq_len
    args.T_warmup = args.obs_seq_len
    torch.manual_seed(args.random_seed)
    np.random.seed(args.random_seed)
    device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
    print(device)
    try:
        online_mif = OnlineMIF(
            args,
            device,
            tracking_topic_name,
        )
        online_mif.run()
    except rospy.ROSInterruptException:
        pass

