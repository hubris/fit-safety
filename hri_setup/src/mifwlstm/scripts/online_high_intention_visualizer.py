#!/usr/bin/env python
r"""Visualize actual and target currents of one joint on ur robot in real time."""
import sys
import argparse
import rospy
import matplotlib.pyplot as plt
from std_msgs.msg import Float32MultiArray
import numpy as np

def arg_parse():
    parser = argparse.ArgumentParser()
    # Scene Options
    parser.add_argument('--num_intentions', default=2, type=int) # ! high number intentions = 2
    return parser.parse_args()


class IntentionVisualizer: 
    def __init__(self, num_intentions):
        # !!! Start here
        self.fig, self.ax = plt.subplots(figsize=(4, 6))
        self.fig.canvas.set_window_title('high_level')
        plt.tight_layout()
        self.num_intentions = num_intentions
        self.colors = ['C1', 'C0']
        self.prob_threshold = 0.9
        # set up probability distribution plot
        self.ax.set_xlim([0, self.num_intentions+1]) # look symmetric
        self.ax.set_ylim([0, 1])
        self.ax.set_yticks([0, 0.5, self.prob_threshold, 1])
        self.ax.set_xticks(range(1, self.num_intentions+1))
        self.ax.set_xticklabels(["cooperation", "coexistence"])
        self.ax.set_yticklabels([0, 0.5, "thre-\nshold", 1])
        self.ax.axhline(y=0.9, color='k', linestyle='--')
        # labels = []
        # for i in range(num_intentions):
        #     labels.append(str(i+1))
        # self.ax.set_xticklabels(labels)
        self.prob_dist = self.ax.bar(range(1, self.num_intentions+1),\
            1. / self.num_intentions * np.ones(self.num_intentions))
        for j, b in enumerate(self.prob_dist):
            b.set_color(self.colors[j])
        rospy.init_node('high_intention_visualizer', anonymous=True)
        rospy.Subscriber("/hri/high_intention_prob_dist", Float32MultiArray, self.callback_realtime_plot, queue_size=1)
    

    def callback_realtime_plot(self, data):
        intention_prob_dist = data.data
        for j, b in enumerate(self.prob_dist):
            b.set_height(intention_prob_dist[j])
            # if intention_prob_dist[j] < self.prob_threshold:
            #     b.set_color(self.colors[0])
            # else:
            #     b.set_color(self.colors[1])
        plt.draw()


if __name__ == '__main__':
    args = arg_parse()
    iv = IntentionVisualizer(args.num_intentions)
    plt.show()
    rospy.spin()