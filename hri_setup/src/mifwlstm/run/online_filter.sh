#!/bin/bash
. $1/../../intention-env/bin/activate
if [ $2 = true ]
then
    mutable=--mutable
else
    mutable=
fi
python $1/scripts/online_intention_filter_hri.py $mutable --tau $3 --prediction_method $4 \
    --obs_seq_len $5 --pred_seq_len $6 --num_intentions $7 --mutation_prob $8
# $2 --mutable or None
# $4 ilm or ilstm
# python $1/scripts/online_intention_filter_hri.py --mutable --tau 0.2 --prediction_method ilm --obs_seq_len 8 --pred_seq_len 12


# python -u $1/scripts/offline_filtering_process_hri.py --tau $tau --prediction_method $prediction_method \
#     $mutable | tee -a $1/logs/offline_filtering.txt



# cd ~/human_intention/hri_setup/ && source devel/setup.bash
# source ~/human_intention/intention-env/bin/activate
# cd ~/human_intention/hri_setup/src/mifwlstm
# python scripts/online_intention_visualizer.py



# cd ~/human_intention/hri_setup/ && source devel/setup.bash
# source ~/human_intention/intention-env/bin/activate
# cd ~/human_intention/hri_setup/src/mifwlstm

