#!/usr/bin/env python
import rospy
import pathhack
from os.path import join, isdir
from os import makedirs
from rosop.msg import HumanSkeletonPosition
import pickle
# import numpy as np
from os.path import exists


class DataCollectionPipeline:
    def __init__(
        self,
        topic_name="/hri/human_skeleton_position_tracking_3D",
        message_type=HumanSkeletonPosition,
        ):
        self.raw_datasets_folderpath = join(pathhack.pkg_path, "raw_datasets")
        if not isdir(self.raw_datasets_folderpath):
            makedirs(self.raw_datasets_folderpath)
        self.message_type = message_type
        self.subject_name = ""
        self.subject_folderpath = ""
        self.intention_index = 0 # invalid. Valid index starts from 1. e.g. 1, 2, ...
        self.collection_status = "off"
        self.data = []
        rospy.Subscriber(topic_name, self.message_type, self.callback)

    def change_subject_name(self, subject_name):
        self.subject_name = subject_name
        if self.subject_name != "":
            self.subject_folderpath = join(self.raw_datasets_folderpath, subject_name)
            if not isdir(self.subject_folderpath):
                makedirs(self.subject_folderpath)
            if self.subject_name not in self.trajectory_index.keys():
                self.trajectory_index[self.subject_name] = {}
                self.save_trajectory_index()
    
    def change_intention_index(self, intention_index_str):
        self.intention_index = int(intention_index_str)
        if self.subject_name != "" and self.trajectory_index != 0:
            if self.intention_index not in self.trajectory_index[self.subject_name].keys():
                self.trajectory_index[self.subject_name][self.intention_index] = 0
                self.save_trajectory_index()

    def change_collection_status(self, command_input):
        if command_input == "s":
            self.collection_status = "on"
        elif command_input == "e" and self.collection_status == "on":
            self.collection_status = "off"
            self.trajectory_index[self.subject_name][self.intention_index] += 1
            self.save_trajectory_index()
            # save data
            trajectory_data_filename = str(self.intention_index)+"_"\
                +str(self.trajectory_index[self.subject_name][self.intention_index])+".p"
            trajectory_data_filepath = join(self.subject_folderpath, trajectory_data_filename)
            with open(trajectory_data_filepath, 'wb') as f:
                pickle.dump(self.data, f)
                print(trajectory_data_filepath+" is dumped.")
            self.data = []
        else:
            raise RuntimeError("The command input is wrong for changing collection status.")

    def callback(self, data):
        if self.collection_status != "on":
            return
        if self.subject_name == "":
            self.collection_status = "failed"
            print("The subject name is empty. Collection failed.")
            return
        if self.intention_index == 0:
            self.collection_status = "failed"
            print("The intention index is 0. Collection failed.")
            return
        self.data.append(data)
        return
    
    def print_state(self):
        print("Subject name: ", self.subject_name)
        print("Intention index: ", self.intention_index)
        if self.subject_name in self.trajectory_index.keys() and \
            self.intention_index in self.trajectory_index[self.subject_name].keys():
            print("Trajectory index: ", self.trajectory_index[self.subject_name][self.intention_index])
        else:
            print("The trajectory index is not initialized for current subject and intention.")
        print("Collection status: ", self.collection_status)

    def save_trajectory_index(self):
        with open(self.trajectory_index_filepath, 'wb') as f:
            pickle.dump(self.trajectory_index, f)
            print(self.trajectory_index_filepath+" is updated.")
    
    def initialize_trajectory_index(self, file_id_str):
        self.trajectory_index_filepath = join(self.raw_datasets_folderpath, \
            "trajectory_index_"+file_id_str+".p")
        if not exists(self.trajectory_index_filepath):
            self.trajectory_index = {} # {'zhe': {1: 2}} means zhe has 2 trajectories of intention 1.
            self.save_trajectory_index()
        else:
            with open(self.trajectory_index_filepath, 'rb') as f:
                self.trajectory_index = pickle.load(f)


    def run(self):
        command_input = raw_input("\
                \n=====================================================\
                \nPlease input the id for trajectory index file. E.g. trajectory_index_ID.p \
                \n=====================================================\
                \nCommand: ")
        self.initialize_trajectory_index(command_input)
        while not rospy.is_shutdown():
            command_input = raw_input("\
                \n=====================================================\
                \nSubject name: sn NAME\
                \nIntention index: ii INDEX\
                \nStart collection: s\
                \nEnd collection: e\
                \nPrint current state: state\
                \n=====================================================\
                \nCommand: ")
            if command_input[:2] == "sn":
                self.change_subject_name(command_input[3:])
            elif command_input[:2] == "ii":
                self.change_intention_index(command_input[3:])
            elif command_input == "s" or command_input == "e":
                self.change_collection_status(command_input)
            elif command_input == "state":
                self.print_state()
            else:
                print("Th command ["+command_input+"] is invalid.")

if __name__ == '__main__':
    try:
        rospy.init_node('data_collection_pipeline', anonymous=True)
        dcp = DataCollectionPipeline()
        dcp.run()
    except rospy.ROSInterruptException:
        pass