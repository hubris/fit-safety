import sys
import copy
import argparse

import cv2
import rospy
import numpy as np
import message_filters

from cv_bridge import CvBridge, CvBridgeError
from std_msgs.msg import Bool, String, Int32, Float32MultiArray
from sensor_msgs.msg import Image, CameraInfo
from rosop.msg import HumanSkeletonPosition
from kalman_filter import KalmanFilter
from track import Track


class HumanWristTracker:
    def __init__(
        self,
        dt=0.03,
        max_age=30,
        n_init=3,
        low_threshold=[-0.9, -0.1, 0.005],
        high_threshold=[-0.3, 0.65, 0.6], 
        considered_wrist="right",
    ):
        rospy.init_node("human_wrist_tracker")
        self.position_tracking_3D_pub = rospy.Publisher("hri/human_skeleton_position_tracking_3D", HumanSkeletonPosition, queue_size=10)
        self.position_tracking_3D_floatarray_pub = rospy.Publisher("hri/human_skeleton_position_tracking_3D_floatarray", Float32MultiArray, queue_size=10)
        self.detections = {}
        self.detections['cam_top'] = None
        self.detections['cam_right'] = None
        rospy.Subscriber("cam_top/hri/wrist_position_3D", Float32MultiArray, self.cam_top_detection_callback)
        rospy.Subscriber("cam_right/hri/wrist_position_3D", Float32MultiArray, self.cam_right_detection_callback)
        # tracking filter
        # boundary of the considered shared workspace
        self.low_threshold = low_threshold
        self.high_threshold = high_threshold
        self.kf = KalmanFilter(dt=dt)
        self.tracks = [] # can only have one track
        self._next_id = 1
        self.max_age = max_age
        self.n_init = n_init
        if considered_wrist=="left":
            self.wrist_idx = 7
        elif considered_wrist=="right":
            self.wrist_idx = 4
        else:
            raise RuntimeError("Wrist can only be left or right.")
        return

    def predict(self):
        """
        Propagate track state distributions one time step forward.
        This function should be called once every time step, before `update`.
        """
        if len(self.tracks)==1:
            for track in self.tracks:
                track.predict(self.kf)

    def check_misdetection(self, detection):
        # True -> misdetected
        # False -> detected
        # low_threshold = np.array([0, 0.2, 0.3])
        # high_threshold = np.array([1,1.2,0.6])
        # return (detection > low_threshold).prod()* (detection < high_threshold).prod()
        # return (detection[2] < self.z_threshold[0]) or (detection[2] > self.z_threshold[1]) or sum(abs(detection))==0.
        # return False
        return 1-(detection > self.low_threshold).prod()*(detection < self.high_threshold).prod()
        
    def update(self, camera_name):
        """
        Perform measurement update and track management.
        Parameters
        ----------
        detections : List[deep_sort.detection.Detection]
            A list of detections at the current time step.
        """
        if self.detections[camera_name] is None:
            return
        if self.check_misdetection(self.detections[camera_name]):
            # out of range misdetection
            # print("misdetection: ", detection)
            if len(self.tracks)==1:
                # misdetected
                self.tracks[0].mark_missed()
        else:
            if len(self.tracks)==0:
                self._initiate_track(self.detections[camera_name])
            elif len(self.tracks)==1:
                if self.tracks[0].is_deleted():
                    self._initiate_track(self.detections[camera_name])
                else:
                    if self.tracks[0].check_misdetection(self.kf, self.detections[camera_name]):
                        # misdetected due to squared mahalanobis gating
                        self.tracks[0].mark_missed()
                    else:
                        #print("detection distance:", self.kf.gating_distance(self.tracks[0].mean, self.tracks[0].covariance, detection, only_position=True))
                        self.tracks[0].update(self.kf, self.detections[camera_name])
            else:
                raise RuntimeError("By now self.tracks have at most length of 1.")
        self.tracks = [t for t in self.tracks if not t.is_deleted()] # for sanity
        self.detections[camera_name] = None # After update, make detection None
        return

    def _initiate_track(self, detection):
        mean, covariance = self.kf.initiate(detection)
        if len(self.tracks) == 1:
            self.tracks = []
        self.tracks.append(Track(
            mean, covariance, self._next_id, self.n_init, self.max_age))
        # self._next_id never change
    
    def cam_top_detection_callback(self, data):
        self.detections['cam_top'] = np.array(data.data)
    
    def cam_right_detection_callback(self, data):
        self.detections['cam_right'] = np.array(data.data)

    def publish_tracking_3d(self):
        self.predict()
        self.update('cam_top')
        self.update('cam_right')
        # now only one wrist is tracked
        position_realworld_tracked = np.zeros((1, 25, 3))
        position_3d_floatarray_msg = Float32MultiArray()
        position_3d_floatarray_msg.data = list(np.zeros(3))
        # (n_human, 25, 3)
        if len(self.tracks)>0 and not self.tracks[0].is_deleted():
            position_realworld_tracked[0,self.wrist_idx,:] = self.tracks[0].mean[:3]
            position_realworld_tracked[0,0,:] = np.sqrt(self.tracks[0].covariance[:3,:3].diagonal())
            position_3d_floatarray_msg.data = list(self.tracks[0].mean[:3])
        
        curr_stamp = rospy.Time.now()
        n_humans, n_keypoints = position_realworld_tracked.shape[:2]
        hsp_3d = HumanSkeletonPosition()
        hsp_3d.header.stamp = curr_stamp
        hsp_3d.n_humans, hsp_3d.n_keypoints, hsp_3d.n_dim = n_humans, n_keypoints, 3
        hsp_3d.data = list(position_realworld_tracked.reshape(-1).astype(np.float32))
        self.position_tracking_3D_pub.publish(hsp_3d)
        
        self.position_tracking_3D_floatarray_pub.publish(position_3d_floatarray_msg)
        # self.tracks[0].mean[:3]

if __name__ == '__main__':
    # ! Note if this transformation matrix is changed, it is not needed to change in online_intention_filter_hri, and need to set base_link as frame_id in visualizer.
    # low_threshold = [-1.1, -0.1, -0.1] # [-0.9, -0.1, -0.1]
    # high_threshold = [-0.3, 0.45, 1.0] # [-0.3, 0.65, 0.45] # [-0.3, 0.45, 0.45]
    # low_threshold=[-0.9, -0.1, -0.1]
    # high_threshold=[-0.3, 0.65, 1.0]
    freq = 30 # Hz
    hwt = HumanWristTracker(dt=1./freq)
    rate = rospy.Rate(freq)
    while not rospy.is_shutdown():
        hwt.publish_tracking_3d()
        rate.sleep()


