import rospy
import numpy as np
import cv2
from cv_bridge import CvBridge, CvBridgeError
from std_msgs.msg import Bool, String, Int32MultiArray
from sensor_msgs.msg import Image
import copy
import sys
import argparse
try:
    # If you run `make install` (default path is `/usr/local/python` for Ubuntu), you can also access the OpenPose/python module from there. This will install OpenPose and the python library at your desired installation path. Ensure that this is in your python path in order to use it.
    sys.path.append('/usr/local/python')
    from openpose import pyopenpose as op
except ImportError as e:
    print('Error: OpenPose library could not be found. Did you enable `BUILD_PYTHON` in CMake and have this Python script in the right folder?')
    raise e

class HandPixelPerception:
    def __init__(self, hand_size=(50, 50), resolution=(640, 480), openpose_dir=None, camera_name="top_cam"):
        self.hand_size = hand_size
        self.resolution = resolution
        self.bridge = CvBridge()
        self.image_pub = rospy.Publisher(camera_name+"/q3/hand_pixel", Image, queue_size=1)
        self.load_openpose(openpose_dir)
        self.shared_space = ((177, 217), (443, 480)) # ((x1, y1), (x2, y2))
        self.human_intervention = False
        self.compute_safety_space()
        self.bool_pub = rospy.Publisher(camera_name+'/q3/human_intervention', Bool, queue_size=10)
        self.time_pub = rospy.Publisher(camera_name+'/q3/time_stamp', String, queue_size=10)
        self.position_pub = rospy.Publisher(camera_name+"/q3/hand_pixel_position", Int32MultiArray, queue_size=10)


    def compute_safety_space(self):
        hs, ss = np.array(self.hand_size).astype(float), np.array(self.shared_space).astype(float)
        ss[0] -= hs/2.
        ss[1] += hs/2.
        self.safety_space = (tuple(ss[0]), tuple(ss[1])) # ((x1-xhand/2, y1-yhand/2), (x2+xhand/2, y2+yhand/2))

    def check_human_intervention(self, center_coordinates):
        # true -> there exists safety risk
        # false -> safe.
        # (x1 <= hand_x <= x2) and (y1 <= hand_y <= y2)
        curr_intervention_status = \
            (self.safety_space[0][0] <= center_coordinates[0]) and \
            (self.safety_space[1][0] >= center_coordinates[0]) and \
            (self.safety_space[0][1] <= center_coordinates[1]) and \
            (self.safety_space[1][1] >= center_coordinates[1])
        
        if curr_intervention_status and not self.human_intervention: # human intervention starts
            self.human_intervention = True
            self.bool_pub.publish(self.human_intervention)
            str_intervention = "Intervention starts %s" % rospy.get_time()
            self.time_pub.publish(str_intervention)
        elif not curr_intervention_status and self.human_intervention: # human intervention ends
            self.human_intervention = False
            self.bool_pub.publish(self.human_intervention)
            str_intervention = "Intervention ends %s" % rospy.get_time()
            self.time_pub.publish(str_intervention)

        return curr_intervention_status
        

    def load_openpose(self, openpose_dir):
        if openpose_dir is None:
            # ***********************************************************
            # !!! Change this folder to where you installed openpose. !!!
            openpose_dir = '/home/haonan/fit-safety/hri_setup/openpose/'
            # ***********************************************************
        # Flags
        parser = argparse.ArgumentParser()
        args = parser.parse_known_args()
        # Custom Params (refer to include/openpose/flags.hpp for more parameters)
        params = dict()
        params["model_folder"] = openpose_dir+"models/"
        for i in range(0, len(args[1])):
            curr_item = args[1][i]
            if i != len(args[1])-1: next_item = args[1][i+1]
            else: next_item = "1"
            if "--" in curr_item and "--" in next_item:
                key = curr_item.replace('-','')
                if key not in params:  params[key] = "1"
            elif "--" in curr_item and "--" not in next_item:
                key = curr_item.replace('-','')
                if key not in params: params[key] = next_item
        
        # Starting OpenPose
        self.opWrapper = op.WrapperPython()
        self.opWrapper.configure(params)
        self.opWrapper.start()
    

    def hand_rectangle(self, center_coordinates):
        # x1,y1 ------
        # |          |
        # |          |
        # |          |
        # --------x2,y2
        # shared space
        # x1 = 177, y1 = 217
        # x2 = 443, y2 = 480
        x1, y1 = center_coordinates[0]-int(self.hand_size[0]/2), center_coordinates[1]-int(self.hand_size[1]/2)
        x2, y2 = center_coordinates[0]+int(self.hand_size[0]/2), center_coordinates[1]+int(self.hand_size[1]/2)
        x1, y1 = max(x1, 0), max(y1, 0)
        x2, y2 = min(x2, self.resolution[0]), min(y2, self.resolution[1])
        return (x1, y1), (x2, y2)


    def callback(self, data):
        # time_raw_img = rospy.get_time()
        str_raw_image = "raw image %s" % rospy.get_time()
        self.time_pub.publish(str_raw_image)

        try:
            cv_image = self.bridge.imgmsg_to_cv2(data, "bgr8")
        except CvBridgeError as e:
            print(e)
       
        datum = op.Datum()
        datum.cvInputData = cv_image
        self.opWrapper.emplaceAndPop(op.VectorDatum([datum]))
        # time_op_out = rospy.get_time()
        str_op_out = "OpenPose output %s" % rospy.get_time()
        self.time_pub.publish(str_op_out)
        # print('openpose time: ', time_op_out-time_raw_img)

        out_image = cv2.rectangle(datum.cvOutputData, self.shared_space[0], self.shared_space[1], color=(255, 0, 0), thickness=2)

        if isinstance(datum.poseKeypoints,np.ndarray): 
            center_coordinates = tuple(datum.poseKeypoints.astype(int)[0, 7, :2]) # left wrist
            # center_coordinates = tuple(datum.poseKeypoints.astype(int)[0, 4, :2]) # right wrist
            if not center_coordinates[0] == 0 or not center_coordinates[1] == 0:
                position_msg = Int32MultiArray()
                position_msg.data = list(center_coordinates)
                self.position_pub.publish(position_msg)
                (x1, y1), (x2, y2) = self.hand_rectangle(center_coordinates)
                if self.check_human_intervention(center_coordinates):
                    out_image = cv2.rectangle(out_image, (x1, y1), (x2, y2), color=(0, 0, 255), thickness=2)
                else:
                    out_image = cv2.rectangle(out_image, (x1, y1), (x2, y2), color=(0, 255, 0), thickness=2)
        out_data = self.bridge.cv2_to_imgmsg(out_image, 'bgr8')
        self.image_pub.publish(out_data)
        str_image_pub = "Image pub %s" % rospy.get_time()
        self.time_pub.publish(str_image_pub)


if __name__ == '__main__':
    # conservative hand size: (100, 100)
    hpp = HandPixelPerception()
    rospy.init_node('hand_pixel_perception',anonymous=True)
    rospy.Subscriber('top_cam/color/image_raw', Image, hpp.callback)
    rospy.spin()





