import numpy as np
import scipy.linalg


"""
Table for the 0.95 quantile of the chi-square distribution with N degrees of
freedom (contains values for N=1, ..., 9). Taken from MATLAB/Octave's chi2inv
function and used as Mahalanobis gating threshold.
"""
# chi2inv95 = {
#     1: 3.8415,
#     2: 5.9915,
#     3: 7.8147,
#     4: 9.4877,
#     5: 11.070,
#     6: 12.592,
#     7: 14.067,
#     8: 15.507,
#     9: 16.919}


class KalmanFilter(object):
    """
    A simple Kalman filter for tracking human wrist in 3D space.
    The 8-dimensional state space
        x, y, z, vx, vy, vz
    contains the human wrist position (x, y, z) and their respective velocities.
    Object motion follows a constant velocity model.
    """

    def __init__(self, dt=0.03, std_pos=0.04, std_vel=0.1):
        self.ndim = 3
        # Create Kalman filter model matrices.
        self._motion_mat = np.eye(2 * self.ndim, 2 * self.ndim)
        for i in range(self.ndim):
            self._motion_mat[i, self.ndim + i] = dt
        self._update_mat = np.eye(self.ndim, 2 * self.ndim)
        # Motion and observation uncertainty are chosen relative to the current
        # state estimate. These weights control the amount of uncertainty in
        # the model. This is a bit hacky.
        self._std_weight_position = 0.05 #1. / 20 # unit m.
        self._std_weight_velocity = 0.02 # / 160
        self.chi2inv95 = {
            1: 3.8415,
            2: 5.9915,
            3: 7.8147,
            4: 9.4877,
            5: 11.070,
            6: 12.592,
            7: 14.067,
            8: 15.507,
            9: 16.919}

    def initiate(self, measurement):
        """
        Create track from unassociated measurement.
        Parameters
        ----------
        measurement : ndarray
            human wrist coordinates (x, y, z).
        Returns
        -------
        (ndarray, ndarray)
            Returns the mean vector (8 dimensional) and covariance matrix (8x8
            dimensional) of the new track. Unobserved velocities are initialized
            to 0 mean.
        """
       
        # Initiation assumes not occlusion.
        mean_pos = measurement
        mean_vel = np.zeros_like(mean_pos)
        mean = np.r_[mean_pos, mean_vel]
        std = [
            1 * self._std_weight_position,
            1 * self._std_weight_position,
            1 * self._std_weight_position,
            1 * self._std_weight_velocity,
            1 * self._std_weight_velocity,
            1 * self._std_weight_velocity]
        covariance = np.diag(np.square(std))
        return mean, covariance

    def predict(self, mean, covariance):
        """
        Run Kalman filter prediction step.
        Parameters
        ----------
        mean : ndarray
            The 6 dimensional mean vector of the wrist state at the previous
            time step.
        covariance : ndarray
            The 6x6 dimensional covariance matrix of the wrist state at the
            previous time step.
        Returns
        -------
        (ndarray, ndarray)
            Returns the mean vector and covariance matrix of the predicted
            state. Unobserved velocities are initialized to 0 mean.
        """
        std_pos = [
            self._std_weight_position,
            self._std_weight_position,
            self._std_weight_position]
        std_vel = [
            self._std_weight_velocity,
            self._std_weight_velocity,
            self._std_weight_velocity]
        motion_cov = np.diag(np.square(np.r_[std_pos, std_vel])) *1/100.# *1/10.

        mean = np.dot(self._motion_mat, mean)
        covariance = np.linalg.multi_dot((
            self._motion_mat, covariance, self._motion_mat.T)) + motion_cov
        # print(covariance)
        # covariance = np.diag(np.diag(covariance)) 
        return mean, covariance

    def project(self, mean, covariance):
        """
        Project state distribution to measurement space.
        Parameters
        ----------
        mean : ndarray
            The state's mean vector (6 dimensional array).
        covariance : ndarray
            The state's covariance matrix (6x6 dimensional).
        Returns
        -------
        (ndarray, ndarray)
            Returns the projected mean and covariance matrix of the given state
            estimate.
        """
        std = [
            self._std_weight_position,
            self._std_weight_position,
            self._std_weight_position]
        innovation_cov = np.diag(np.square(std))
        mean = np.dot(self._update_mat, mean)
        covariance = np.linalg.multi_dot((
            self._update_mat, covariance, self._update_mat.T))
        return mean, covariance + innovation_cov

    def update(self, mean, covariance, measurement):
        """
        Run Kalman filter correction step.
        Parameters
        ----------
        mean : ndarray
            The predicted state's mean vector (6 dimensional).
        covariance : ndarray
            The state's covariance matrix (6x6 dimensional).
        measurement : ndarray
            The 3 dimensional measurement vector (x, y, z).
        Returns
        -------
        (ndarray, ndarray)
            Returns the measurement-corrected state distribution.
        """
         # ! Todo: measurement z threshold for robot occlusion.
        projected_mean, projected_cov = self.project(mean, covariance)
        
        projected_cov = np.diag(np.diag(projected_cov))
        # print(projected_cov)
        # kalman_gain = np.dot(covariance, self._update_mat.T).dot(np.diag(1./np.diag(projected_cov)))

        chol_factor, lower = scipy.linalg.cho_factor(
            projected_cov, lower=True, check_finite=False)
        kalman_gain = scipy.linalg.cho_solve(
            (chol_factor, lower), np.dot(covariance, self._update_mat.T).T,
            check_finite=False).T
        
        # print(covariance)
        # print(kalman_gain)
        # print(kalman_gain)
        # kalman_gain = np.diag(np.diag(kalman_gain)) # only keep diagonal to not consider velocity (i.e. zero velocity)
        # print(kalman_gain)
        innovation = measurement - projected_mean
        # print(mean)
        new_mean = mean + kalman_gain.dot(innovation) # np.dot(innovation, kalman_gain.T)
        # print(new_mean)
        new_covariance = covariance - np.linalg.multi_dot((
            kalman_gain, projected_cov, kalman_gain.T))
        # print(new_covariance)
        return new_mean, new_covariance

    def gating_distance(self, mean, covariance, measurements,
                        only_position=False):
        """
        Compute gating distance between state distribution and measurements.
        A suitable distance threshold can be obtained from `chi2inv95`. If
        `only_position` is False, the chi-square distribution has 4 degrees of
        freedom, otherwise 2.
        Parameters
        ----------
        mean : ndarray
            Mean vector over the state distribution (8 dimensional).
        covariance : ndarray
            Covariance of the state distribution (8x8 dimensional).
        measurements : ndarray
            An Nx4 dimensional matrix of N measurements, each in
            format (x, y, a, h) where (x, y) is the bounding box center
            position, a the aspect ratio, and h the height.
        only_position : Optional[bool]
            If True, distance computation is done with respect to the bounding
            box center position only.
        Returns
        -------
        ndarray
            Returns an array of length N, where the i-th element contains the
            squared Mahalanobis distance between (mean, covariance) and
            `measurements[i]`.
        """
        mean, covariance = self.project(mean, covariance)
        if only_position:
            mean, covariance = mean[:self.ndim], covariance[:self.ndim, :self.ndim]
            # measurements = measurements[:, :self.ndim]

        cholesky_factor = np.linalg.cholesky(covariance)
        d = measurements - mean
        z = scipy.linalg.solve_triangular(
            cholesky_factor, d.T, lower=True, check_finite=False,
            overwrite_b=True) # d.T meaningless to d with dim (3,)
        squared_maha = np.sum(z * z, axis=0)
        return squared_maha # (d.T*Cov^(-1)*d)