#!/usr/bin/env python3
from os.path import isdir, join
import pickle
# import numpy as np
import matplotlib.pyplot as plt
# import rosbag
import glob
# from sklearn.model_selection import train_test_split
from itertools import groupby
from operator import itemgetter
import os
# import torch
import logging
import sys
import pdb
import numpy as np
from mpl_toolkits.mplot3d import Axes3D
from tf import transformations as tfs
"""
source myenv/bin/activate
source /opt/ros/melodic/setup.bash
python data_processing.py
Interval & length of the sequence is different, but doesn't seem to matter from the original dataset.
Do we need a window size?
Don't need a target label?
There is no mask with all zeros?
"""


import pathhack
from os.path import join, split
dataset_folderpath_1='../raw_datasets/zhe/fast'
dataset_folderpath_2='../raw_datasets/zhe/slow'
traj_path_1 = glob.glob(join(dataset_folderpath_1,'*.p'))
traj_path_2 = glob.glob(join(dataset_folderpath_2,'*.p'))
# traj_path = traj_path_2
traj_path = traj_path_1+traj_path_2

# x y z qx qy qz qw
# -0.46120499 -0.00580957  1.83695167  0.70863286  0.70481574 -0.01105552 -0.03085395 base_link camera_link
T = tfs.translation_matrix([-0.46120499,-0.00580957,1.83695167])
R = tfs.quaternion_matrix([0.70863286,0.70481574,-0.01105552,-0.03085395])
transformation_matrix = tfs.concatenate_matrices(T,R)


trajs = []
# fast_traj_paths
# slow_traj_paths
labels = []
# e.g. traj_path = "../raw_datasets/zhe/fast/1_11.p"
for traj_path in traj_path:
    with open(traj_path, 'rb') as f:
        data = pickle.load(f)
        # print(traj_path+" is loaded.")
    _, filename = split(traj_path)
    labels.append(int(filename.split('_')[0])-1) # 1,2,3 -> 0,1,2
    traj = []
    traj_time = []
    traj_time_diff = []
    for msg in data:
        curr_t = msg.header.stamp.to_sec()
        if len(traj_time)==0:
            traj_time.append(curr_t)
        else:
            if curr_t-traj_time[-1]>=0.03:
                traj_time_diff.append( curr_t-traj_time[-1])
                # import pdb; pdb.set_trace();
                traj_time.append(curr_t)
                pos = np.array(msg.data).reshape(msg.n_humans, msg.n_keypoints, msg.n_dim) # (n_human, 25, 3)
                wrist_pos = pos[0,4] # (3,) right wrist
                wrist_pos_tf = np.dot(transformation_matrix, np.concatenate((wrist_pos,[1.]),axis=0))[:3]
                traj.append(wrist_pos_tf)
            else:
                # print(curr_t-traj_time[-1])
                pass
    traj = np.stack(traj, axis=0) # (T, 3)
    # print("length: ", len(traj))
    trajs.append(traj)
import pdb; pdb.set_trace();
# ../raw_datasets/Zhe/1_7.p is loaded.
# ('length: ', 309)
# ../raw_datasets/Zhe/1_8.p is loaded.
# ('length: ', 217)

# dataset = {}
# dataset['intentions'] = labels
# dataset['trajs'] = trajs

# with open('multi_intention_dataset_fast.p', 'wb') as f:
#     pickle.dump(dataset, f)
#     print('multi_intention_dataset_fast.p is dumped.')


# fig = plt.figure()
# ax = Axes3D(fig)
# ax.axis('equal')
# for label, traj in zip(labels, trajs):
#     color_traj = 'C'+str(label)
#     ax.scatter(traj[:,0],traj[:,1],traj[:,2], c=color_traj) # plot the point (2,3,4) on the figure
#     ax.plot(traj[:,0],traj[:,1],traj[:,2], c=color_traj)

# plt.show()
