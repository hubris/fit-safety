#!/usr/bin/env python
# -*- coding: utf-8 -*-

# visualizer.py: rviz visualizer
# Author: Ravi Joshi
# Date: 2019/10/01

# import modules
import math
import rospy
import numpy as np
# from ros_openpose.msg import Frame
from std_msgs.msg import ColorRGBA
from geometry_msgs.msg import Vector3, Point
from visualization_msgs.msg import Marker, MarkerArray
from rosop.msg import HumanSkeletonPosition

class HumanSkeletonVisualizer:
    def __init__(self, considered_wrist="left"):
        rospy.init_node("human_skeleton_visualizer")
        self.initialize_visual_parameters()
        self.marker_pub = rospy.Publisher("hri/human_skeleton_marker", MarkerArray, queue_size=1) # queue: 1 or 5
        self.marker_wrist_pub = rospy.Publisher("hri/human_wrist_marker", MarkerArray, queue_size=1) # queue: 1 or 5
        rospy.Subscriber("hri/human_skeleton_position_3D", HumanSkeletonPosition, self.callback)
        rospy.Subscriber("hri/human_skeleton_position_tracking_3D", HumanSkeletonPosition, self.wrist_callback)
        if considered_wrist=="left":
            self.wrist_idx = 7
        elif considered_wrist=="right":
            self.wrist_idx = 4
        else:
            raise RuntimeError("Wrist can only be left or right.")
        print(self.wrist_idx)
        return
    
    def initialize_visual_parameters(self):
        # colors for different people
        self.skeleton_line_width = 0.01
        self.id_text_size = 0.2
        self.id_text_offset = -0.05
        # self.sphere_radius = 0.5
        self.colors = [ColorRGBA(0.12, 0.63, 0.42, 1.00),
                       ColorRGBA(0.98, 0.30, 0.30, 1.00),
                       ColorRGBA(0.26, 0.09, 0.91, 1.00),
                       ColorRGBA(0.77, 0.44, 0.14, 1.00),
                       ColorRGBA(0.92, 0.73, 0.14, 1.00),
                       ColorRGBA(0.00, 0.61, 0.88, 1.00),
                       ColorRGBA(1.00, 0.65, 0.60, 1.00),
                       ColorRGBA(0.59, 0.00, 0.56, 1.00)]
        self.upper_body_ids = [0, 1, 8]
        self.hands_ids = [4, 3, 2, 1, 5, 6, 7]
        self.legs_ids = [22, 11, 10, 9, 8, 12, 13, 14, 19]
        self.body_parts = [self.upper_body_ids, self.hands_ids, self.legs_ids]
        # self.body_parts = [[6,7],[3,4]]
        # write person id on the top of his head
        self.nose_id = 0
        return

    def create_marker(self, index, color, marker_type, size, time):
        marker = Marker()
        marker.id = index
        marker.color = color
        marker.action = Marker.ADD
        marker.type = marker_type
        if marker_type == Marker.LINE_STRIP:
            marker.scale.x = size
        elif marker_type == Marker.TEXT_VIEW_FACING:
            marker.scale.z = size
        elif marker_type == Marker.SPHERE:
            marker.scale.x = size
            marker.scale.y = size
            marker.scale.z = size
        else:
            marker.scale = Vector3(size, size, size)
        marker.pose.orientation.w = 1
        marker.header.stamp = time
        marker.header.frame_id = "base_link"#"camera_link", #"base_link"
        marker.lifetime = rospy.Duration(0.1)  # 1 second
        return marker

    def callback(self, data):
        assert data.n_keypoints==25 and data.n_dim==3
        pos_3d = np.array(data.data).reshape(data.n_humans, data.n_keypoints, data.n_dim)
        curr_stamp = rospy.Time.now()
        marker_array = MarkerArray()
        marker_id = 0
        for human_idx in [0]: # range(data.n_humans):
            marker_color = self.colors[human_idx % len(self.colors)]
            # body_markers = []
            # marker_id += len(self.body_parts)
            for body_part_idx, body_part in enumerate(self.body_parts):
                body_marker_points = []
                for joint_idx in body_part:
                    if sum(abs(pos_3d[human_idx, joint_idx])) != 0.:
                        joint_point = Point()
                        joint_point.x, joint_point.y, joint_point.z = pos_3d[human_idx, joint_idx]
                        body_marker_points.append(joint_point) # ! if all zero invalid. Consider later.
                if len(body_marker_points) >= 2:
                    body_marker = self.create_marker(marker_id+body_part_idx, marker_color, Marker.LINE_STRIP, self.skeleton_line_width, curr_stamp)
                    body_marker.points = body_marker_points
                    marker_array.markers.append(body_marker)
                    marker_id += 1

            if sum(abs(pos_3d[human_idx, 0])) != 0.:
                # human_idx_marker = self.create_marker(marker_id, marker_color, Marker.TEXT_VIEW_FACING, self.id_text_size, curr_stamp)
                human_idx_marker = self.create_marker(marker_id, marker_color, Marker.TEXT_VIEW_FACING, 0.1, curr_stamp)
                marker_id += 1
                # assign person id and 3D position
                human_idx_marker.text = str(human_idx)
                nose_joint_point = Point()
                nose_joint_point.x, nose_joint_point.y, nose_joint_point.z = pos_3d[human_idx, 0]+np.array([0,self.id_text_offset,0])
                human_idx_marker.pose.position = nose_joint_point
                marker_array.markers.append(human_idx_marker)
            
            # if sum(abs(pos_3d[human_idx, self.wrist_idx])) != 0.: 
            #     wrist_marker = self.create_marker(marker_id, marker_color, Marker.SPHERE, 0.1, curr_stamp)
            #     wrist_point = Point()
            #     wrist_point.x, wrist_point.y, wrist_point.z = pos_3d[human_idx, self.wrist_idx]
            #     wrist_marker.pose.position = wrist_point
            #     marker_array.markers.append(wrist_marker)

            self.marker_pub.publish(marker_array)


    def wrist_callback(self, data):
        assert data.n_keypoints==25 and data.n_dim==3
        pos_3d = np.array(data.data).reshape(data.n_humans, data.n_keypoints, data.n_dim)
        curr_stamp = rospy.Time.now()
        marker_array = MarkerArray()
        marker_id = 0
        for human_idx in [0]: # range(data.n_humans):
            marker_color = self.colors[human_idx % len(self.colors)]
            if sum(abs(pos_3d[human_idx, self.wrist_idx])) != 0.: 
                # marker_size = np.mean(pos_3d[human_idx, 0])*5 # std mean over xyz
                marker_size = 0.1
                # print(marker_size)
                wrist_marker = self.create_marker(marker_id, marker_color, Marker.SPHERE, marker_size, curr_stamp)
                wrist_point = Point()
                wrist_point.x, wrist_point.y, wrist_point.z = pos_3d[human_idx, self.wrist_idx]
                wrist_marker.pose.position = wrist_point
                marker_array.markers.append(wrist_marker)
            self.marker_wrist_pub.publish(marker_array)

if __name__ == '__main__':
    hst = HumanSkeletonVisualizer(considered_wrist="right")
    rospy.spin()