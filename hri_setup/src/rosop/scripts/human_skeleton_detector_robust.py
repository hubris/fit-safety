import sys
import copy
import argparse

import cv2
import rospy
import numpy as np
import message_filters

from cv_bridge import CvBridge, CvBridgeError
from std_msgs.msg import Bool, String, Int32
from sensor_msgs.msg import Image, CameraInfo
from rosop.msg import HumanSkeletonPosition

try:
    # If you run `make install` (default path is `/usr/local/python` for Ubuntu), you can also access the OpenPose/python module from there. This will install OpenPose and the python library at your desired installation path. Ensure that this is in your python path in order to use it.
    sys.path.append('/usr/local/python')
    from openpose import pyopenpose as op
except ImportError as e:
    print('Error: OpenPose library could not be found. Did you enable `BUILD_PYTHON` in CMake and have this Python script in the right folder?')
    raise e

class HumanSkeletonDetector:
    def __init__(self, openpose_dir=None):
        self.bridge = CvBridge()
        rospy.init_node("human_skeleton_detector")
        self.image_pub = rospy.Publisher("hri/color/image_raw", Image, queue_size=1)
        self.position_2D_pub = rospy.Publisher("hri/human_skeleton_position_2D", HumanSkeletonPosition, queue_size=10)
        self.position_3D_pub = rospy.Publisher("hri/human_skeleton_position_3D", HumanSkeletonPosition, queue_size=10)
        self.load_openpose(openpose_dir)
        camera_info_topic = "camera/aligned_depth_to_color/camera_info"
        color_topic = "camera/color/image_raw"
        depth_topic = "camera/aligned_depth_to_color/image_raw"
        camera_info = rospy.wait_for_message(camera_info_topic, CameraInfo)
        # [[fx, 0, cx],
        #  [ 0,fy, cy],
        #  [ 0, 0,  1]]
        self.fx = camera_info.K[0]
        self.fy = camera_info.K[4]
        self.cx = camera_info.K[2]
        self.cy = camera_info.K[5]
        encoding = rospy.wait_for_message(depth_topic, Image).encoding
        # unit: meter # realsense -> 16UC1
        if encoding == "16UC1":
            self.unit_scale = 0.001
        else:
            self.unit_scale = 1. 
        image_sub = message_filters.Subscriber(color_topic, Image)
        depth_sub = message_filters.Subscriber(depth_topic, Image)
        self.time_synchronizer = message_filters.ApproximateTimeSynchronizer([image_sub, depth_sub], 1, 1./30)
        self.time_synchronizer.registerCallback(self.callback)
        return

    def load_openpose(self, openpose_dir):
        if openpose_dir is None:
            # ***********************************************************
            # !!! Change this folder to where you installed openpose. !!!
            openpose_dir = '/home/haonan/fit-safety/hri_setup/openpose/'
            # ***********************************************************
        # Flags
        parser = argparse.ArgumentParser()
        args = parser.parse_known_args()
        # Custom Params (refer to include/openpose/flags.hpp for more parameters)
        params = dict()
        params["model_folder"] = openpose_dir+"models/"
        for i in range(0, len(args[1])):
            curr_item = args[1][i]
            if i != len(args[1])-1: next_item = args[1][i+1]
            else: next_item = "1"
            if "--" in curr_item and "--" in next_item:
                key = curr_item.replace('-','')
                if key not in params:  params[key] = "1"
            elif "--" in curr_item and "--" not in next_item:
                key = curr_item.replace('-','')
                if key not in params: params[key] = next_item
        # Starting OpenPose
        self.opWrapper = op.WrapperPython()
        self.opWrapper.configure(params)
        self.opWrapper.start()
        return

    def callback(self, image_data, depth_data):
        try:
            cv_image = self.bridge.imgmsg_to_cv2(image_data, "bgr8")
            cv_depth = self.bridge.imgmsg_to_cv2(depth_data, "passthrough")
            # print(cv_depth.shape) # (480, 640)
        except CvBridgeError as e:
            print(e)
        datum = op.Datum()
        datum.cvInputData = cv_image
        self.opWrapper.emplaceAndPop(op.VectorDatum([datum]))
        try:
            output_image_data = self.bridge.cv2_to_imgmsg(datum.cvOutputData, 'bgr8')
            self.image_pub.publish(output_image_data)
        except CvBridgeError as e:
            print(e)
        if isinstance(datum.poseKeypoints,np.ndarray):
            # print("helloworld")
            # note 0 means we only consider one (the first) person in the detector.
            # print(datum.poseKeypoints.dtype) # float32 # np # (n_human, 25, 3)
            position_pixel = datum.poseKeypoints[:,:,:2].astype(int) # 2D pixel position (n_human, 25, 2) 
            position_realworld = self.depth_and_2D_to_3D(cv_depth, position_pixel) # 3D real world position (n_human, 25, 3)
            # print(sum(abs(cv_depth)))
            curr_stamp = rospy.Time.now()
            n_humans, n_keypoints = position_pixel.shape[:2]
            hsp_2d = HumanSkeletonPosition()
            hsp_2d.header.stamp = curr_stamp
            hsp_2d.n_humans, hsp_2d.n_keypoints, hsp_2d.n_dim = n_humans, n_keypoints, 2
            hsp_2d.data = list(position_pixel.reshape(-1).astype(np.float32))
            hsp_3d = HumanSkeletonPosition()
            hsp_3d.header.stamp = curr_stamp
            hsp_3d.n_humans, hsp_3d.n_keypoints, hsp_3d.n_dim = n_humans, n_keypoints, 3
            hsp_3d.data = list(position_realworld.reshape(-1).astype(np.float32))
            self.position_2D_pub.publish(hsp_2d)
            self.position_3D_pub.publish(hsp_3d)
            # center_coordinates = tuple(datum.poseKeypoints.astype(int)[0, 7, :2]) # left wrist
            # center_coordinates = tuple(datum.poseKeypoints.astype(int)[0, 4, :2]) # right wrist


    def depth_and_2D_to_3D(self, cv_depth, position_pixel):
        u, v = position_pixel[:,:,0], position_pixel[:,:,1] # (n_human, 25)
        # cv_depth (480, 640)
        # u > 480 -> (v, u) # u->x, v->y
        valid_mask = (v<cv_depth.shape[0])*(u<cv_depth.shape[1]) # (n_human, 25) # body may be outside view.
        u_valid, v_valid = u*valid_mask, v*valid_mask
        xyz = np.zeros((position_pixel.shape[0], position_pixel.shape[1], 3), dtype=np.float32) # (n_human, 25, 3)
        xyz[:,:,2] = cv_depth[v_valid, u_valid]*self.unit_scale # z
        xyz[:,:,2] = xyz[:,:,2]*valid_mask # invalid x,y,z will be (0,0,0)
        xyz[:,:,0] = xyz[:,:,2]/self.fx*(u-self.cx) # x # no need to use u_valid, v_valid here
        xyz[:,:,1] = xyz[:,:,2]/self.fy*(v-self.cy) # y
        # ! may need to think about the case only upper body is detected and lower body is missing.
        # Fit the wrist position for robustness.
        wrist_pos_2d = np.array([u[0, 4], v[0, 4]])
        elbow_pos_2d = np.array([u[0, 3], v[0, 3]])
        half_point_pos_2d = ((wrist_pos_2d+elbow_pos_2d)/2).astype(int)
        proportion_forearm = []
        z_forearm = []
        # 0: elbow # 1: halfpoint # 2: wrist
        for i in range(0,5):
            # [0,1,2,3,4]/4
            # 0: elbow
            i_pos_2d = (i/4.*half_point_pos_2d+(1-i/4.)*elbow_pos_2d).astype(int)
            z_forearm.append(cv_depth[i_pos_2d[1], i_pos_2d[0]]*self.unit_scale)
            proportion_forearm.append(i/4.)
        z_forearm = np.array(z_forearm)
        proportion_forearm = np.array(proportion_forearm)
        # data = np.array([[1,5], [2,10], [3,15], [4,20], [5,25]])
        fit = np.polyfit(proportion_forearm, z_forearm, 1) #The use of 1 signifies a linear fit.
        line = np.poly1d(fit)
        z_wrist_fitted = line(2.)
        if abs(xyz[0, 4, 2]-z_wrist_fitted)>0.3:
            print("occluded")
            xyz[0, 4, 2] = z_wrist_fitted

        return xyz

if __name__ == '__main__':
    hsd = HumanSkeletonDetector()
    rospy.spin()






