import pickle
import numpy as np
with open('multi_intention_dataset_slow.p', 'rb') as f:
    dataset = pickle.load(f)
intentions, trajs = dataset['intentions'], dataset['trajs']
print(intentions) # np array (40,) # with intentions 1, 2, and 3
intentions = np.asarray(intentions)
print((intentions==0).sum()) # 12
print((intentions==1).sum()) # 16
print((intentions==2).sum()) # 12