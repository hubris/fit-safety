import sys
import copy
import argparse

import cv2
import rospy
import numpy as np
import message_filters

from cv_bridge import CvBridge, CvBridgeError
from std_msgs.msg import Bool, String, Int32
from sensor_msgs.msg import Image, CameraInfo
from rosop.msg import HumanSkeletonPosition
from std_msgs.msg import Float32MultiArray

try:
    # If you run `make install` (default path is `/usr/local/python` for Ubuntu), you can also access the OpenPose/python module from there. This will install OpenPose and the python library at your desired installation path. Ensure that this is in your python path in order to use it.
    sys.path.append('/usr/local/python')
    from openpose import pyopenpose as op
except ImportError as e:
    print('Error: OpenPose library could not be found. Did you enable `BUILD_PYTHON` in CMake and have this Python script in the right folder?')
    raise e

def arg_parse():
    parser = argparse.ArgumentParser()
    parser.add_argument('--camera_name', default="cam_top", type=str, help="cam_top, or cam_right")
    parser.add_argument('--wrist', default="right", type=str, help="left or right")
    return parser.parse_args()


class HumanSkeletonDetector:
    def __init__(
        self,
        camera_name,
        transformation_matrix,
        openpose_dir=None,
        considered_wrist="right",
    ):
        self.camera_name = camera_name
        self.transformation_matrix = transformation_matrix
        self.bridge = CvBridge()
        rospy.init_node("human_skeleton_detector_"+camera_name)
        self.image_pub = rospy.Publisher(camera_name+"/hri/color/image_raw", Image, queue_size=1)
        self.position_2D_pub = rospy.Publisher(camera_name+"/hri/human_skeleton_position_2D", HumanSkeletonPosition, queue_size=10)
        self.position_3D_pub = rospy.Publisher(camera_name+"/hri/human_skeleton_position_3D", HumanSkeletonPosition, queue_size=10)
        self.wrist_position_3D_pub = rospy.Publisher(camera_name+"/hri/wrist_position_3D", Float32MultiArray, queue_size=10)
        self.load_openpose(openpose_dir)
        camera_info_topic = camera_name+"/aligned_depth_to_color/camera_info"
        color_topic = camera_name+"/color/image_raw"
        depth_topic = camera_name+"/aligned_depth_to_color/image_raw"
        camera_info = rospy.wait_for_message(camera_info_topic, CameraInfo)
        # [[fx, 0, cx],
        #  [ 0,fy, cy],
        #  [ 0, 0,  1]]
        self.fx = camera_info.K[0]
        self.fy = camera_info.K[4]
        self.cx = camera_info.K[2]
        self.cy = camera_info.K[5]
        self.camera_info_height = camera_info.height
        self.camera_info_width = camera_info.width
        encoding = rospy.wait_for_message(depth_topic, Image).encoding
        # unit: meter # realsense -> 16UC1
        if encoding == "16UC1":
            self.unit_scale = 0.001
        else:
            self.unit_scale = 1. 
        image_sub = message_filters.Subscriber(color_topic, Image)
        depth_sub = message_filters.Subscriber(depth_topic, Image)
        self.time_synchronizer = message_filters.ApproximateTimeSynchronizer([image_sub, depth_sub], 1, 1./30)
        self.time_synchronizer.registerCallback(self.callback)
        if considered_wrist=="left":
            self.wrist_idx = 7
        elif considered_wrist=="right":
            self.wrist_idx = 4
        else:
            raise RuntimeError("Wrist can only be left or right.")
        return

    def load_openpose(self, openpose_dir):
        if openpose_dir is None:
            # ***********************************************************
            # !!! Change this folder to where you installed openpose. !!!
            openpose_dir = '/home/haonan/fit-safety/hri_setup/openpose/'
            # ***********************************************************
        # Flags
        parser = argparse.ArgumentParser()
        args = parser.parse_known_args()
        # Custom Params (refer to include/openpose/flags.hpp for more parameters)
        params = dict()
        params["model_folder"] = openpose_dir+"models/"
        # ! mask the customized arguments for detector
        # ! for i in range(0, len(args[1])):
        for i in range(0,0):
            curr_item = args[1][i]
            if i != len(args[1])-1: next_item = args[1][i+1]
            else: next_item = "1"
            if "--" in curr_item and "--" in next_item:
                key = curr_item.replace('-','')
                if key not in params:  params[key] = "1"
            elif "--" in curr_item and "--" not in next_item:
                key = curr_item.replace('-','')
                if key not in params: params[key] = next_item
        # Starting OpenPose
        self.opWrapper = op.WrapperPython()
        self.opWrapper.configure(params)
        self.opWrapper.start()
        return

    def callback(self, image_data, depth_data):
        try:
            cv_image = self.bridge.imgmsg_to_cv2(image_data, "bgr8")
            cv_depth = self.bridge.imgmsg_to_cv2(depth_data, "passthrough")
            # print(cv_depth.shape) # (480, 640)
        except CvBridgeError as e:
            print(e)
        datum = op.Datum()
        datum.cvInputData = cv_image
        self.opWrapper.emplaceAndPop(op.VectorDatum([datum]))
        try:
            output_image_data = self.bridge.cv2_to_imgmsg(datum.cvOutputData, 'bgr8')
            self.image_pub.publish(output_image_data)
        except CvBridgeError as e:
            print(e)
        if isinstance(datum.poseKeypoints,np.ndarray):
            # print("helloworld")
            # note 0 means we only consider one (the first) person in the detector.
            # print(datum.poseKeypoints.dtype) # float32 # np # (n_human, 25, 3)
            position_pixel = datum.poseKeypoints[:,:,:2].astype(int) # 2D pixel position (n_human, 25, 2) 
            position_realworld = self.depth_and_2D_to_3D(cv_depth, position_pixel) # 3D real world position (n_human, 25, 3)
            position_realworld = position_realworld.reshape(-1, 3).T # (3,n_human*25)
            position_realworld_homogeneous = np.concatenate((position_realworld, np.ones((1,position_realworld.shape[1]))), axis=0) # (4, n_human*25)
            position_realworld = np.dot(self.transformation_matrix, position_realworld_homogeneous)[:3] # (3, n_human*25)
            position_realworld = position_realworld.T.reshape(-1, 25, 3)
            wrist_detection = position_realworld[0,self.wrist_idx,:] # (3,)
            wrist_3d = Float32MultiArray()
            wrist_3d.data = list(wrist_detection.astype(np.float32))
            self.wrist_position_3D_pub.publish(wrist_3d)

            # print(sum(abs(cv_depth)))
            curr_stamp = rospy.Time.now()
            n_humans, n_keypoints = position_pixel.shape[:2]
            hsp_2d = HumanSkeletonPosition()
            hsp_2d.header.stamp = curr_stamp
            hsp_2d.n_humans, hsp_2d.n_keypoints, hsp_2d.n_dim = n_humans, n_keypoints, 2
            hsp_2d.data = list(position_pixel.reshape(-1).astype(np.float32))
            hsp_3d = HumanSkeletonPosition()
            hsp_3d.header.stamp = curr_stamp
            hsp_3d.n_humans, hsp_3d.n_keypoints, hsp_3d.n_dim = n_humans, n_keypoints, 3
            hsp_3d.data = list(position_realworld.reshape(-1).astype(np.float32))
            self.position_2D_pub.publish(hsp_2d)
            self.position_3D_pub.publish(hsp_3d)
            # center_coordinates = tuple(datum.poseKeypoints.astype(int)[0, 7, :2]) # left wrist
            # center_coordinates = tuple(datum.poseKeypoints.astype(int)[0, 4, :2]) # right wrist


    def depth_and_2D_to_3D(self, cv_depth, position_pixel):
        u, v = position_pixel[:,:,0], position_pixel[:,:,1] # (n_human, 25)
        # cv_depth (480, 640)
        # u > 480 -> (v, u) # u->x, v->y
        valid_mask = (v<cv_depth.shape[0])*(u<cv_depth.shape[1]) # (n_human, 25) # body may be outside view.
        u_valid, v_valid = u*valid_mask, v*valid_mask
        xyz = np.zeros((position_pixel.shape[0], position_pixel.shape[1], 3), dtype=np.float32) # (n_human, 25, 3)
        xyz[:,:,2] = cv_depth[v_valid, u_valid]*self.unit_scale # z
        xyz[:,:,2] = xyz[:,:,2]*valid_mask # invalid x,y,z will be (0,0,0)
        xyz[:,:,0] = xyz[:,:,2]/self.fx*(u-self.cx) # x # no need to use u_valid, v_valid here
        xyz[:,:,1] = xyz[:,:,2]/self.fy*(v-self.cy) # y
        # ! may need to think about the case only upper body is detected and lower body is missing.
        # Fit the wrist position for robustness.
        wrist_pos_2d = np.array([u[0, self.wrist_idx], v[0, self.wrist_idx]])
        elbow_pos_2d = np.array([u[0, self.wrist_idx-1], v[0, self.wrist_idx-1]])
        half_point_pos_2d = ((wrist_pos_2d+elbow_pos_2d)/2).astype(int)
        proportion_forearm = []
        z_forearm = []
        # 0: elbow # 1: halfpoint # 2: wrist
        for i in range(0,5):
            # [0,1,2,3,4]/4
            # 0: elbow
            i_pos_2d = (i/4.*half_point_pos_2d+(1-i/4.)*elbow_pos_2d).astype(int)
            z_forearm.append(cv_depth[i_pos_2d[1], i_pos_2d[0]]*self.unit_scale)
            proportion_forearm.append(i/4.)
        z_forearm = np.array(z_forearm)
        proportion_forearm = np.array(proportion_forearm)
        # data = np.array([[1,5], [2,10], [3,15], [4,20], [5,25]])
        fit = np.polyfit(proportion_forearm, z_forearm, 1) #The use of 1 signifies a linear fit.
        line = np.poly1d(fit)
        z_wrist_fitted = line(2.)
        if abs(xyz[0, self.wrist_idx, 2]-z_wrist_fitted)>0.3:
            # print("occluded")
            xyz[0, self.wrist_idx, 2] = z_wrist_fitted

        return xyz

if __name__ == '__main__':
    args = arg_parse()
    camera_name = args.camera_name
    if camera_name == "cam_top":
        # 211202 # top down view
        # camera_transformation_matrix = np.array(
        #     [[0.0021317922423761374, 0.9983572246475552, -0.05725579065951769, -0.4592949889034955],
        #     [0.9998419196667168, -0.0011175089383325249, 0.017741605160713724, -0.008644474966984353],
        #     [0.017648492498782677, -0.057284510011343534, -0.9982019349353066, 1.8398017828469182],
        #     [0.0, 0.0, 0.0, 1.0]]
        # )
        # 220218 # topdown
        camera_transformation_matrix = np.array(
            [[0.01358107488416605, 0.9982986584921361, -0.05670327121051353, -0.42975063215494486],
            [0.9994465349417819, -0.0118308603116114, 0.031089101625549207, -0.03229937621981415],
            [0.030365379162335334, -0.057094060423921934, -0.9979069592381584, 1.8434955310582708],
            [0.0, 0.0, 0.0, 1.0]]
        )
    elif camera_name == "cam_right":
        # 211230 # right side view
        # camera_transformation_matrix = np.array(
        #     [[0.6055597400674477, 0.048974329049555995, 0.7942914890114778, -1.3654056583511713],
        #     [-0.7957994657520805, 0.038289313468252845, 0.6043485682909427, -0.30953485205521564],
        #     [-0.0008153101204324168, -0.998065851421597, 0.06216020829670067, 0.13329137327503668],
        #     [0.0, 0.0, 0.0, 1.0]]
        # )
        # 220218 # side view
        camera_transformation_matrix = np.array(
            [[0.540541918159477, 0.006781005161744356, 0.8412897871090255, -1.4063105700357408],
            [-0.841077705055111, 0.028209520277435063, 0.5401782763471923, -0.2560758198129427],
            [-0.020069428560534522, -0.9995790318826745, 0.020951779345634098, 0.1354054430667181],
            [0.0, 0.0, 0.0, 1.0]]
        )
    else:
        raise RuntimeError("camera_name is not valid.")
    hsd = HumanSkeletonDetector(
        camera_name,
        camera_transformation_matrix,
        considered_wrist=args.wrist,
    )
    rospy.spin()






