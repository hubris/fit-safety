import sys
import copy
import argparse

import cv2
import rospy
import numpy as np
import message_filters

from cv_bridge import CvBridge, CvBridgeError
from std_msgs.msg import Bool, String, Int32
from sensor_msgs.msg import Image, CameraInfo
from rosop.msg import HumanSkeletonPosition
from kalman_filter import KalmanFilter
from track import Track

try:
    # If you run `make install` (default path is `/usr/local/python` for Ubuntu), you can also access the OpenPose/python module from there. This will install OpenPose and the python library at your desired installation path. Ensure that this is in your python path in order to use it.
    sys.path.append('/usr/local/python')
    from openpose import pyopenpose as op
except ImportError as e:
    print('Error: OpenPose library could not be found. Did you enable `BUILD_PYTHON` in CMake and have this Python script in the right folder?')
    raise e

class HumanSkeletonTracker:
    def __init__(self, openpose_dir=None, z_threshold=1.6, considered_wrist="left"):
        self.bridge = CvBridge()
        rospy.init_node("human_skeleton_tracker")
        self.image_pub = rospy.Publisher("hri/color/image_raw", Image, queue_size=1)
        self.position_2D_pub = rospy.Publisher("hri/human_skeleton_position_2D", HumanSkeletonPosition, queue_size=10)
        self.position_3D_pub = rospy.Publisher("hri/human_skeleton_position_3D", HumanSkeletonPosition, queue_size=10)
        self.position_tracking_3D_pub = rospy.Publisher("hri/human_skeleton_position_tracking_3D", HumanSkeletonPosition, queue_size=10)
        self.load_openpose(openpose_dir)
        camera_info_topic = "camera/aligned_depth_to_color/camera_info"
        color_topic = "camera/color/image_raw"
        depth_topic = "camera/aligned_depth_to_color/image_raw"
        camera_info = rospy.wait_for_message(camera_info_topic, CameraInfo)
        # [[fx, 0, cx],
        #  [ 0,fy, cy],
        #  [ 0, 0,  1]]
        self.fx = camera_info.K[0]
        self.fy = camera_info.K[4]
        self.cx = camera_info.K[2]
        self.cy = camera_info.K[5]
        encoding = rospy.wait_for_message(depth_topic, Image).encoding
        # unit: meter # realsense -> 16UC1
        if encoding == "16UC1":
            self.unit_scale = 0.001
        else:
            self.unit_scale = 1. 
        image_sub = message_filters.Subscriber(color_topic, Image)
        depth_sub = message_filters.Subscriber(depth_topic, Image)
        self.time_synchronizer = message_filters.ApproximateTimeSynchronizer([image_sub, depth_sub], 1, 1./30)
        self.time_synchronizer.registerCallback(self.callback)
        # tracking filter
        self.z_threshold = z_threshold # z in view from camera
        self.kf = KalmanFilter(dt=1, z_threshold=self.z_threshold)
        self.tracks = [] # can only have one track
        self._next_id = 1
        self.max_age = 100#30
        self.n_init = 3
        if considered_wrist=="left":
            self.wrist_idx = 7
        elif considered_wrist=="right":
            self.wrist_idx = 4
        else:
            raise RuntimeError("Wrist can only be left or right.")
        return

    def predict(self):
        """
        Propagate track state distributions one time step forward.
        This function should be called once every time step, before `update`.
        """
        if len(self.tracks)==1:
            for track in self.tracks:
                track.predict(self.kf)

    def check_misdetection(self, detection):
        # True -> misdetected
        # False -> detected
        return (detection[2] < self.z_threshold) or sum(abs(detection))==0.

    def update(self, detection):
        """
        Perform measurement update and track management.
        Parameters
        ----------
        detections : List[deep_sort.detection.Detection]
            A list of detections at the current time step.
        """
        if self.check_misdetection(detection):
            # print("misdetection: ", detection)
            if len(self.tracks)==1:
                # misdetected
                self.tracks[0].mark_missed()
        else:
            if len(self.tracks)==0:
                self._initiate_track(detection)
            elif len(self.tracks)==1:
                if self.tracks[0].is_deleted():
                    self._initiate_track(detection)
                else:
                    # print("detection distance:", self.kf.gating_distance(self.tracks[0].mean, self.tracks[0].covariance, detection, only_position=True))
                    self.tracks[0].update(self.kf, detection)
            else:
                raise RuntimeError("By now self.tracks have at most length of 1.")
        self.tracks = [t for t in self.tracks if not t.is_deleted()] # for sanity


    def _initiate_track(self, detection):
        mean, covariance = self.kf.initiate(detection)
        if len(self.tracks) == 1:
            self.track = []
        self.tracks.append(Track(
            mean, covariance, self._next_id, self.n_init, self.max_age))
        # self._next_id += 1
        

    def load_openpose(self, openpose_dir):
        if openpose_dir is None:
            # ***********************************************************
            # !!! Change this folder to where you installed openpose. !!!
            openpose_dir = '/home/haonan/fit-safety/hri_setup/openpose/'
            # ***********************************************************
        # Flags
        parser = argparse.ArgumentParser()
        args = parser.parse_known_args()
        # Custom Params (refer to include/openpose/flags.hpp for more parameters)
        params = dict()
        params["model_folder"] = openpose_dir+"models/"
        for i in range(0, len(args[1])):
            curr_item = args[1][i]
            if i != len(args[1])-1: next_item = args[1][i+1]
            else: next_item = "1"
            if "--" in curr_item and "--" in next_item:
                key = curr_item.replace('-','')
                if key not in params:  params[key] = "1"
            elif "--" in curr_item and "--" not in next_item:
                key = curr_item.replace('-','')
                if key not in params: params[key] = next_item
        # Starting OpenPose
        self.opWrapper = op.WrapperPython()
        self.opWrapper.configure(params)
        self.opWrapper.start()
        return

    def callback(self, image_data, depth_data):
        try:
            cv_image = self.bridge.imgmsg_to_cv2(image_data, "bgr8")
            cv_depth = self.bridge.imgmsg_to_cv2(depth_data, "passthrough")
            # print(cv_depth.shape) # (480, 640)
        except CvBridgeError as e:
            print(e)
        datum = op.Datum()
        datum.cvInputData = cv_image
        self.opWrapper.emplaceAndPop(op.VectorDatum([datum]))
        try:
            output_image_data = self.bridge.cv2_to_imgmsg(datum.cvOutputData, 'bgr8')
            self.image_pub.publish(output_image_data)
        except CvBridgeError as e:
            print(e)
        if isinstance(datum.poseKeypoints,np.ndarray):
            # print("helloworld")
            # note 0 means we only consider one (the first) person in the tracker.
            # print(datum.poseKeypoints.dtype) # float32 # np # (n_human, 25, 3)
            position_pixel = datum.poseKeypoints[:,:,:2].astype(int) # 2D pixel position (n_human, 25, 2) 
            position_realworld = self.depth_and_2D_to_3D(cv_depth, position_pixel) # 3D real world position (n_human, 25, 3)
            # (n_human, 25, 3)
            detection = position_realworld[0,self.wrist_idx,:] # (3,)

            self.predict()
            self.update(detection)
            # if len(self.tracks)>0 and not self.tracks[0].is_deleted():
            #     # print("tracking...")
            #     position_realworld[0,self.wrist_idx,:] = self.tracks[0].mean[:3]

            curr_stamp = rospy.Time.now()
            n_humans, n_keypoints = position_pixel.shape[:2]
            hsp_2d = HumanSkeletonPosition()
            hsp_2d.header.stamp = curr_stamp
            hsp_2d.n_humans, hsp_2d.n_keypoints, hsp_2d.n_dim = n_humans, n_keypoints, 2
            hsp_2d.data = list(position_pixel.reshape(-1).astype(np.float32))
            hsp_3d = HumanSkeletonPosition()
            hsp_3d.header.stamp = curr_stamp
            hsp_3d.n_humans, hsp_3d.n_keypoints, hsp_3d.n_dim = n_humans, n_keypoints, 3
            hsp_3d.data = list(position_realworld.reshape(-1).astype(np.float32))
            self.position_2D_pub.publish(hsp_2d)
            self.position_3D_pub.publish(hsp_3d)



    def depth_and_2D_to_3D(self, cv_depth, position_pixel):
        u, v = position_pixel[:,:,0], position_pixel[:,:,1] # (n_human, 25)
        # cv_depth (480, 640)
        # u > 480 -> (v, u)
        valid_mask = (v<cv_depth.shape[0])*(u<cv_depth.shape[1]) # (n_human, 25) # body may be outside view.
        u_valid, v_valid = u*valid_mask, v*valid_mask
        xyz = np.zeros((position_pixel.shape[0], position_pixel.shape[1], 3), dtype=np.float32) # (n_human, 25, 3)
        xyz[:,:,2] = cv_depth[v_valid, u_valid]*self.unit_scale # z
        xyz[:,:,2] = xyz[:,:,2]*valid_mask # invalid x,y,z will be (0,0,0)
        xyz[:,:,0] = xyz[:,:,2]/self.fx*(u-self.cx) # x # no need to use u_valid, v_valid here
        xyz[:,:,1] = xyz[:,:,2]/self.fy*(v-self.cy) # y
        # ! may need to think about the case only upper body is detected and lower body is missing.
        return xyz
    
    def publish_tracking_3d(self):
        # now only one wrist is tracked
        position_realworld_tracked = np.zeros((1, 25, 3))
        # (n_human, 25, 3)
        if len(self.tracks)>0 and not self.tracks[0].is_deleted():
            position_realworld_tracked[0,self.wrist_idx,:] = self.tracks[0].mean[:3]
            position_realworld_tracked[0,0,:] = np.sqrt(self.tracks[0].covariance[:3,:3].diagonal())
        curr_stamp = rospy.Time.now()
        n_humans, n_keypoints = position_realworld_tracked.shape[:2]
        hsp_3d = HumanSkeletonPosition()
        hsp_3d.header.stamp = curr_stamp
        hsp_3d.n_humans, hsp_3d.n_keypoints, hsp_3d.n_dim = n_humans, n_keypoints, 3
        hsp_3d.data = list(position_realworld_tracked.reshape(-1).astype(np.float32))
        self.position_tracking_3D_pub.publish(hsp_3d)

if __name__ == '__main__':
    # hst = HumanSkeletonTracker(z_threshold=0.5)
    hst = HumanSkeletonTracker(z_threshold=1.55, considered_wrist="right")
    # rospy.spin()
    # data_publisher = rospy.Publisher("/temperature", Float64, queue_size=1)
    # Create a rate
    rate = rospy.Rate(30)
    while not rospy.is_shutdown():
        hst.publish_tracking_3d()
        rate.sleep()






