#!/usr/bin/env python
import rospy
import numpy as np
from std_msgs.msg import Bool
import termios, sys

def talker():
    fd = sys.stdin.fileno()
    newattr = termios.tcgetattr(fd)
    newattr[3] = newattr[3] & ~termios.ICANON & ~termios.ECHO
    termios.tcsetattr(fd, termios.TCSANOW, newattr)
    pub = rospy.Publisher('/q3/human_intervention', Bool, queue_size=10)
    rospy.init_node('talker_keyboard', anonymous=True)
    
    intervention_state = False
    while not rospy.is_shutdown():
        c = sys.stdin.read(1)
        if c != "\n": # not Enter key
            str_msg = c
            rospy.loginfo(str_msg)
            if c == 's' and not intervention_state: # start intervention
                intervention_state = True
                # for i in range
                pub.publish(intervention_state)
            elif c == 'e' and intervention_state: # end intervention
                intervention_state = False
                pub.publish(intervention_state)

if __name__ == '__main__':
    try:
        talker()
    except rospy.ROSInterruptException:
        pass