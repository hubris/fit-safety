#!/usr/bin/env python
import rospy
import tf
import numpy as np
import time
from std_msgs.msg import Float32MultiArray


class HumanRobotPositionListener:
    def __init__(self, tracking_topic_name):
        # self.robot_links = ['/upper_arm_link', '/forearm_link', '/wrist_1_link', \
        #     '/wrist_2_link', '/wrist_3_link', '/tool0_controller']
        self.robot_links = ['/wrist_3_link'] # n_links
        rospy.init_node('human_robot_position_listener', anonymous=True)
        self.tf = tf.TransformListener()
        self.human_robot_pos_pub = rospy.Publisher("/hri/human_robot_pos", Float32MultiArray, queue_size=10)
        rospy.Subscriber(tracking_topic_name, Float32MultiArray, self.tracking_callback)
    
    def tracking_callback(self, data):
        wrist_pos = np.array(data.data).astype(np.float32)
        robot_pos = self.get_robot_positions().reshape(-1) # (n_links*3,)
        human_robot_pos_msg = Float32MultiArray()
        human_robot_pos_msg.data = [rospy.get_time()]+list(wrist_pos)+list(robot_pos) # (1+3+n_links*3,)
        # print(robot_pos-wrist_pos)
        self.human_robot_pos_pub.publish(human_robot_pos_msg)

    def get_robot_positions(self):
        """
        Get 3D positions of robot links (joints). Realtime speed, around 1e-6 sec.
        Outputs:
            - trans_np
                # robot positions with respect to base frame.
                # np. (num_robot_links, 3)
                # num_robot_links is now 6.
        """
        try:
            trans_list = []
            for robot_link in self.robot_links:
                (trans,_) = self.tf.lookupTransform('/base_link', robot_link, rospy.Time(0))
                trans_list.append(trans)
            robot_pos = np.stack(trans_list, axis=0) # (num_robot_links,3)
            return robot_pos
        except (tf.LookupException, tf.ConnectivityException, tf.ExtrapolationException):
            return np.zeros((len(self.robot_links),3)).astype(np.float32)


if __name__ == '__main__':
    tracking_topic_name = "hri/human_skeleton_position_tracking_3D_floatarray"
    hrpl = HumanRobotPositionListener(tracking_topic_name)
    rospy.spin()