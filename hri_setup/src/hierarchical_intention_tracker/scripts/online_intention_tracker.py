from pathhack import pkg_path

from mifwlstm.src.api.hri_intention_application_interface import HRIIntentionApplicationInterface
from hierarchical_intention_tracker.src.collaborative_human_motion_predictor import CollaborativeHumanMotionPredictor
from hierarchical_intention_tracker.src.hri_hierarchical_intention_tracker import HRIHierarchicalIntentionTracker

import argparse
import rospy
from std_msgs.msg import Float32MultiArray
import numpy as np

def arg_parse():
    parser = argparse.ArgumentParser()
    # * Intention LSTM
    # Model Options
    parser.add_argument('--bidirectional', action='store_true')
    parser.add_argument('--num_layers', default=1, type=int)
    parser.add_argument('--embedding_size', default=32, type=int)
    parser.add_argument('--hidden_size', default=32, type=int)
    parser.add_argument('--dropout', default=0., type=float)
    parser.add_argument('--obs_seq_len', default=6, type=int) # 20
    parser.add_argument('--pred_seq_len', default=6, type=int) # 20
    parser.add_argument('--motion_dim', default=3, type=int)
    parser.add_argument('--device', default="cuda:0", type=str)
    # Scene Options
    parser.add_argument('--num_intentions', default=5, type=int)
    parser.add_argument('--random_seed', default=0, type=int)

    # * Mutable Intention Filter
    # filtering
    parser.add_argument('--num_particles_per_intention', default=100, type=int,\
        help="10, 30, 50, 100.")
    parser.add_argument('--Tf', default=6, type=int, help="should be equal to pred_seq_len") # 20
    parser.add_argument('--T_warmup', default=6, type=int, help="Number of steps for minimum truncated observation. At least 2 for heuristic in ilm. should be equal to obs_seq_len.") # 20
    parser.add_argument('--step_per_update', default=2, type=int, help="1, 2")
    parser.add_argument('--tau', default=0.1, type=float, help="1., 10.")
    parser.add_argument('--prediction_method', default='ilm', type=str, help='ilstm, wlstm or ilm.')
    parser.add_argument('--mutable', action='store_true')
    parser.add_argument('--mutation_prob', default=1e-2, type=float)
    parser.add_argument('--scene', default='fit', type=str)
    # evaluation
    parser.add_argument('--num_top_intentions', default=3, type=int, help='Number of top probability intentions. Options: 1, 3.')

    # * Hierarchical Intention Tracker
    parser.add_argument('--Tp', default=6, type=int, help="should be equal to pred_seq_len, Tf, and step per update")
    parser.add_argument('--trajectory_window_len', default=100, type=int, help="Window length of the saved trajectories in tracker.")

    return parser.parse_args()

args = arg_parse()
args.Tf = args.pred_seq_len
args.T_warmup = args.obs_seq_len
args.Tp = args.pred_seq_len

intention_api = HRIIntentionApplicationInterface(
            method=args.prediction_method,
            scene=args.scene,
            args=args,
            pkg_path=pkg_path,
            device="cpu",
)

col_human_motion_predictor = CollaborativeHumanMotionPredictor(
    intention_api.human_intention_sampler.intention_center_coordinates/100, # now unit: m
    dt=0.03,
    Tp=args.Tp,
    intention_std=0.12, # 0.12 relatively appropriate # 0.3,# 0.2, # 0.08 fast
    process_std=0.08, #0.08 relatively appropriate #0.25,# 0.125, # 0.05 fast
    min_vel=0.05,
)

hierarchical_intention_tracker = HRIHierarchicalIntentionTracker(
    col_human_motion_predictor,
    high_intention_prior=[0.5, 0.5],
    high_alpha=0.99, #0.95,
    low_intention_prob_topic="hri/human_intention_prob_dist",
)

class OnlineIntentionTracker:
    def __init__(
        self,
        hierarchical_intention_tracker,
        tracking_topic_name,
        motion_dim=3,
        To=6,
        Tp=6,
        window_len=100,
    ):
        self.intention_tracker = hierarchical_intention_tracker
        self.motion_dim = motion_dim
        self.To = To
        self.Tp = Tp
        self.window_len = window_len
        rospy.init_node('online_intention_tracker', anonymous=True)
        self.high_intention_prob_dist_pub = rospy.Publisher("hri/high_intention_prob_dist", Float32MultiArray, queue_size=10)
        self.low_intention_prob_dist_pub = rospy.Publisher("hri/low_intention_prob_dist", Float32MultiArray, queue_size=10)
        rospy.Subscriber(tracking_topic_name, Float32MultiArray, self.tracking_callback)
        self.reset()

    def reset(self):
        self.intention_tracker.reset()
        self.human_trajectory = np.empty((0,self.motion_dim))
        self.robot_trajectory = np.empty((0,self.motion_dim))
        self.current_filtering_results = {}
        self.publish_intention_prob()
        self.count_steps = 0
    
    def publish_intention_prob(self):
        self.current_filtering_results['low_intention_prob_dist'] = self.intention_tracker.low_intention_prob_dist
        self.current_filtering_results['high_intention_prob_dist'] = self.intention_tracker.get_high_intention_probability()
        high_intention_prob_dist_msg = Float32MultiArray()
        high_intention_prob_dist_msg.data = list(self.current_filtering_results['high_intention_prob_dist'].astype(np.float32))
        self.high_intention_prob_dist_pub.publish(high_intention_prob_dist_msg)
        low_intention_prob_dist_msg = Float32MultiArray()
        low_intention_prob_dist_msg.data = list(self.current_filtering_results['low_intention_prob_dist'].astype(np.float32))
        self.low_intention_prob_dist_pub.publish(low_intention_prob_dist_msg)
        return
    
    def tracking_callback(self, data):
        pos_data = np.array(data.data)
        human_pos, robot_pos = pos_data[1:4], pos_data[4:7] # human wrist and robot end-effector positions
        if np.abs(human_pos).sum()<1e-4:
            return
        # import pdb; pdb.set_trace();
        self.human_trajectory = np.concatenate((self.human_trajectory, human_pos[np.newaxis,:]), axis=0) # (t, 3)
        self.robot_trajectory = np.concatenate((self.robot_trajectory, robot_pos[np.newaxis,:]), axis=0) # (t, 3)
        if len(self.human_trajectory) <= self.To:
            return
        self.count_steps += 1
        if len(self.human_trajectory) > self.window_len:
            self.human_trajectory = self.human_trajectory[1:]
            self.robot_trajectory = self.robot_trajectory[1:]
        if self.count_steps == self.Tp:
            considered_human_traj = self.human_trajectory[-(self.To+self.Tp):]
            considered_robot_traj = self.robot_trajectory[-(self.To+self.Tp):]
            self.intention_tracker.predict()
            self.intention_tracker.update(considered_human_traj, considered_robot_traj)
            self.publish_intention_prob()
            self.count_steps = 0

    def run(self):
        while not rospy.is_shutdown():
            command_input = input("\
                \n=====================================================\
                \nReset for next round: r\
                \n=====================================================\
                \nCommand: ")
            if command_input == "r":
                self.reset()
            else:
                print("Th command ["+command_input+"] is invalid.")
    
tracking_topic_name = "/hri/human_robot_pos"
online_intention_tracker = OnlineIntentionTracker(
    hierarchical_intention_tracker,
    tracking_topic_name,
    motion_dim=args.motion_dim,
    To = args.obs_seq_len,
    Tp = args.pred_seq_len,
    window_len=args.trajectory_window_len,
)
try:
    online_intention_tracker.run()
except rospy.ROSInterruptException:
    pass

# rospy.spin()