import numpy as np
from mifwlstm.src.mif.intention_particle_filter import IntentionParticleFilter
from hierarchical_intention_tracker.src.collaborative_human_motion_predictor import CollaborativeHumanMotionPredictor

class HRIHierarchicalIntentionTrackerWithLow:

    def __init__(
        self,
        intention_particle_filter: IntentionParticleFilter,
        collaborative_human_motion_predictor: CollaborativeHumanMotionPredictor,
        high_intention_prior: list = [0.5, 0.5],
        high_alpha: float = 0.95,
    ):
        self.intention_particle_filter = intention_particle_filter
        self.collaborative_human_motion_predictor = collaborative_human_motion_predictor
        self.high_intention_transition = np.array(
            [[high_alpha, 1.-high_alpha],
             [1.-high_alpha, high_alpha],]
        ) # 0 -> collaborative, need robot assistance; 1 -> human works alone
        self.high_intention_prior = high_intention_prior
        self.high_alpha = high_alpha
        self.reset()
        
    
    def reset(self):
        """
        Reset HRI hierarchical intention tracker.
        """
        self.intention_particle_filter.reset()
        self.high_intention_prob_dist = np.array(self.high_intention_prior)

    
    def predict(self):
        """
        Prediction step. Propagate forward intention probability distribution.
        """
        self.high_intention_prob_dist = self.high_intention_transition.dot(self.high_intention_prob_dist)
        self.intention_particle_filter.mutate() # ! Make args.mutation_prob a variable in class intention_particle_filter.

    def update(self, human_obs, robot_obs):
        """
        Update step.
        human_obs and robot_obs includes 1:t+T_p.
        """
        # high intention 0
        traj_prob_high_0 = self.collaborative_human_motion_predictor.predict_prob_dist(human_obs, robot_obs) # float
        # high intention 1 -> low intentions
        traj_prob_low = self.collaborative_human_motion_predictor.predict_prob_dist_fixed_intentions(human_obs) # np (n_low_intentions, )
        traj_prob_high_1 = (traj_prob_low * self.intention_particle_filter.get_intention_probability()).sum()
        traj_prob_high_intention = np.array([traj_prob_high_0, traj_prob_high_1])
        self.high_intention_prob_dist = traj_prob_high_intention * self.high_intention_prob_dist
        self.high_intention_prob_dist /= self.high_intention_prob_dist.sum()
        # Based on the equation of deriving probability conditioned on work alone collaborative intention,
        # we should use the predicted low level intention probability instead of the updated low level intention probability.
        self.intention_particle_filter.predict(human_obs)
        self.intention_particle_filter.update_weight(human_obs) # Make args.tau a variable in class intention_particle_filter.
        self.intention_particle_filter.resample()
    
    def get_high_intention_probability(self):
        return self.high_intention_prob_dist
    
    def get_all_intention_probability(self):
        """
                high level intention
                        /\
                       /  \
                      /    \
                     0     1
                    /      |
                   /  low level intention
                  /       /|\
                 /       0 1 2 ...
                /       /  |  \
               0       1   2  3 ...
        """
        all_intention_prob_dist = self.high_intention_prob_dist[1] * \
            self.intention_particle_filter.get_intention_probability()
        all_intention_prob_dist = np.concatenate((
            self.high_intention_prob_dist[0:1],
            all_intention_prob_dist,
        ), axis=0) # (high_intention_0_prob, low_intention_0_prob, low_intention_1_prob, ...)
        return all_intention_prob_dist
