import numpy as np
from hierarchical_intention_tracker.src.collaborative_human_motion_predictor import CollaborativeHumanMotionPredictor
import rospy
from std_msgs.msg import Float32MultiArray

class HRIHierarchicalIntentionTracker:

    def __init__(
        self,
        collaborative_human_motion_predictor: CollaborativeHumanMotionPredictor,
        high_intention_prior: list = [0.5, 0.5],
        high_alpha: float = 0.95,
        low_intention_prob_topic="hri/human_intention_prob_dist",
    ):
        self.collaborative_human_motion_predictor = collaborative_human_motion_predictor
        self.num_low_intentions = self.collaborative_human_motion_predictor.fixed_intention_coordinates.shape[0]
        self.high_intention_transition = np.array(
            [[high_alpha, 1.-high_alpha],
             [1.-high_alpha, high_alpha],]
        ) # 0 -> collaborative, need robot assistance; 1 -> human works alone
        self.high_intention_prior = high_intention_prior
        self.high_alpha = high_alpha
        rospy.Subscriber(low_intention_prob_topic, Float32MultiArray, self.low_intention_prob_callback)
        self.reset()
    

    def low_intention_prob_callback(self, data):
        self.low_intention_prob_dist = np.array(data.data)
        
    
    def reset(self):
        """
        Reset HRI hierarchical intention tracker.
        """
        self.high_intention_prob_dist = np.array(self.high_intention_prior)
        self.low_intention_prob_dist = np.ones(self.num_low_intentions)/self.num_low_intentions

    
    def predict(self):
        """
        Prediction step. Propagate forward intention probability distribution.
        """
        self.high_intention_prob_dist = self.high_intention_transition.dot(self.high_intention_prob_dist)

    def update(self, human_obs, robot_obs):
        """
        Update step.
        human_obs and robot_obs includes 1:t+T_p.
        """
        # high intention 0
        # print("human: ", human_obs[-1])
        # print("robot: ", robot_obs[-1])
        # human:  [-0.33473822  0.7135269   0.1048333 ]
        # robot:  [-0.56554812  0.13715847  0.50106156]
        traj_prob_high_0 = self.collaborative_human_motion_predictor.predict_prob_dist(human_obs, robot_obs) # float
        # high intention 1 -> low intentions
        traj_prob_low = self.collaborative_human_motion_predictor.predict_prob_dist_fixed_intentions(human_obs) # np (n_low_intentions, )
        
        # ! A trick for delay of tracking
        low_intention_prob_dist_laplacian_smoothed = (self.low_intention_prob_dist+0.1)/(self.low_intention_prob_dist+0.1).sum()
        traj_prob_high_1 = (traj_prob_low * low_intention_prob_dist_laplacian_smoothed).sum()
        
        # traj_prob_high_1 = (traj_prob_low * self.low_intention_prob_dist).sum()
        traj_prob_high_intention = np.array([traj_prob_high_0, traj_prob_high_1])
        # print(traj_prob_high_0, traj_prob_low)
        self.high_intention_prob_dist = traj_prob_high_intention * self.high_intention_prob_dist
        self.high_intention_prob_dist /= self.high_intention_prob_dist.sum()
    
    def get_high_intention_probability(self):
        return self.high_intention_prob_dist
    
    def get_all_intention_probability(self):
        """
                high level intention
                        /\
                       /  \
                      /    \
                     0     1
                    /      |
                   /  low level intention
                  /       /|\
                 /       0 1 2 ...
                /       /  |  \
               0       1   2  3 ...
        high level 0: collaboration
        high level 1: coexistence
        """
        all_intention_prob_dist = self.high_intention_prob_dist[1] * \
            self.low_intention_prob_dist
        all_intention_prob_dist = np.concatenate((
            self.high_intention_prob_dist[0:1],
            all_intention_prob_dist,
        ), axis=0) # (high_intention_0_prob, low_intention_0_prob, low_intention_1_prob, ...)
        return all_intention_prob_dist
