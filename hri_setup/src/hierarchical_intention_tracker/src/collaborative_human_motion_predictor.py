import numpy as np

class CollaborativeHumanMotionPredictor:
    def __init__(
        self,
        fixed_intention_coordinates,
        dt=0.03,
        Tp=6,
        motion_dim=3,
        intention_std=0.08,
        process_std=0.05,
        min_vel=0.05,
    ):
        """
        Inputs:
            - fixed_intention_coordinates: intention coordinates on the table. np, (n_intentions, 3)
            - dt: time step length. unit: sec.
            - Tp: prediction sequence length, also the time steps between updates on high level intention.
            - motion_dim: motion dimension
            - intention_std: width of box surrounding the robot position. unit: m.
            - process_std: process noise for uncertainty of human. Same as _std_weight_position for kalman filter in human wrist tracking.
            - min_vel: minimum velocity of human wrist.
        """
        self.fixed_intention_coordinates = fixed_intention_coordinates
        self.dt = dt
        self.Tp = Tp
        self.motion_dim = motion_dim
        self.intention_std = intention_std # ! we might later use gaussian, now just uniform
        self.process_std = process_std
        self.min_vel = min_vel
    
    def predict_prob_dist(
        self,
        human_traj,
        robot_traj,
    ):
        """
        Inputs:
            - human_traj: (To+Tp, 3). Includes human trajectory at time period t-To+1:t+Tp
            - robot_traj: (To+Tp, 3).
        """
        # ! we probably need to use log of probability.
        robot_traj[:,2] -= 0.05 # ! Consider 5 cm below EE which is on the gripper as where the human wants to hold 
        probs = []
        xt_var = 0
        human_xt_mean = human_traj[-self.Tp-1]
        for tt in range(self.Tp):
            human_xt = human_traj[-self.Tp+tt-1]
            human_xtplus = human_traj[-self.Tp+tt]
            intention_mean = robot_traj[-self.Tp+tt]
            human_vt = max(np.mean(np.linalg.norm(human_traj[1:-self.Tp+tt]-human_traj[:-self.Tp+tt-1], axis=1))/self.dt, self.min_vel)
            # print(human_vt) # 0.05-1.0 m/s
            one_step_prob, xtplus_mean, xtplus_var = self.one_step_prob_dist(human_xt_mean,human_xtplus,human_vt,intention_mean,xt_var,robot=True)
            human_xt_mean = xtplus_mean
            xt_var = xtplus_var
            probs.append(one_step_prob)
        return np.prod(probs)
    
    def predict_prob_dist_fixed_intentions(
        self,
        human_traj,
    ):
        """
        Inputs:
            - human_traj: (To+Tp, 3). Includes human trajectory at time period t-To+1:t+Tp
        Outputs:
            - traj_prob_low: np. (n_intentions,)
        """
        traj_prob_low = []
        for i, fixed_intention in enumerate(self.fixed_intention_coordinates):
            traj_prob_low.append(self.predict_prob_dist_single_fixed_intention(human_traj, fixed_intention, i))
        traj_prob_low = np.array(traj_prob_low)
        return traj_prob_low


    def predict_prob_dist_single_fixed_intention(
        self,
        human_traj,
        fixed_intention,
        i,
    ):
        """
        Inputs:
            - human_traj: (To+Tp, 3). Includes human trajectory at time period t-To+1:t+Tp
            - fixed_intention: (3,)
        Outputs:
            - np.prod(probs): float. 
        """
        # ! Not parallelized yet.
        probs = []
        xt_var = 0
        human_xt_mean = human_traj[-self.Tp-1]
        for tt in range(self.Tp):
            human_xt = human_traj[-self.Tp+tt-1]
            human_xtplus = human_traj[-self.Tp+tt]
            intention_mean = fixed_intention
            human_vt = max(np.mean(np.linalg.norm(human_traj[1:-self.Tp+tt]-human_traj[:-self.Tp+tt-1], axis=1))/self.dt, self.min_vel)
            one_step_prob, xtplus_mean, xtplus_var = self.one_step_prob_dist(human_xt_mean,human_xtplus,human_vt,intention_mean,xt_var,robot=False, index=i)
            human_xt_mean = xtplus_mean
            xt_var = xtplus_var
            probs.append(one_step_prob)
        return np.prod(probs)


    def one_step_prob_dist(
        self,
        xt,
        xtplus,
        vt,
        intention_mean,
        xt_var,
        robot=True,
        index=0,
    ):
        """
        Inputs:
            - xt: (3,)
            - xtplus: (3,)
            - vt: scalar. 
            - intention_mean: (3,)
        Outputs:
            - one_step_prob: scalar.
        """
        dist_to_intention = np.linalg.norm(intention_mean-xt)
        if dist_to_intention > 0.15 and vt < 0.3:
            # vt = max(vt, 0.5)
            vt = 1
        xtplus_mean = xt + (intention_mean-xt)*vt*self.dt/dist_to_intention
        xtplus_var = ((vt*self.dt/dist_to_intention)**2.)*self.intention_std**2.+self.process_std**2. + xt_var
        one_step_prob = np.exp(-((xtplus-xtplus_mean)**2.).sum()/(2*xtplus_var))/np.sqrt((2*np.pi*xtplus_var)**self.motion_dim)
        # if robot:
        #     print("robot")
        # else:
        #     print("index ", index)
        # # print("dist_to_intention: ", dist_to_intention)
        # # print("vt: ", vt)
        # print("deviation: ", np.abs(xtplus-xtplus_mean).sum())
        # # print("one_step_prob: ", one_step_prob)
        return one_step_prob, xtplus_mean, xtplus_var