# Set up linux offline simulator for the UR5e robot

The instructions here are based on the UR5e Simulator section in [UR5e Setup Guide](https://scazlab.github.io/ur5e_setup_guide.html), and are tested on Ubuntu 18.04.

## Install ur simulator

1. Download zip file.

Go to the [download website](https://www.universal-robots.com/download/?option=51846#section41511). Open `Robot arm size` tab, and click on `UR5e`. Open `Software` tab, and click on `Linux Offline Simulator`. Choose the version corresponding to our hardware (5.8), which is `Offline Simulator - e-Series - UR Sim for Linux 5.8.2`. Download the `tar.gz` file. You have to register an account in order to download this file. Unzip the file.
```
tar xvzf [FILE NAME]
cd ursim-5.X.X.XXXXX
```

2. Set up correct java version.

install java 8 (note: other version doesn’t work), and make sure it is the default version with `java -version`.
```
sudo apt install openjdk-8-jre
```
To switch the java version, refer to [this question](https://askubuntu.com/questions/740757/switch-between-multiple-java-versions).
```
update-java-alternatives --list
sudo update-java-alternatives --set /path/to/java/version
```

3. Install libxmlrpc for 32 bit executable.
```
sudo apt install libxmlrpc-c++8v5:i386
```
Change the file install.sh: in commonDependencies: Change libcurl3 to libcurl4. Run `./install.sh` to install the simulator.

4. Run the simulator.
```
cd ursim-5.8.2.10297/
./starturcontrol.sh
./start-ursim.sh
```

## Set up ur5_ws

Follow [README.md](../README.md) in the root folder to set up ur5_ws. 

[Note] By 2021/09/14, there are several bugs in ur5_ws that needs to be addressed. (1) ur-rtde installation may fail if you have a pip3 that has a too old version. Virtualenv can help fix the problem. (2) pylon-ros-camera requires installation of pylon software. If you do not use pylon, you may remove pylon-ros-camera from `src` folder and then you can successfully build the workspace.

## Install URCap for external control

1. Start the simulator.
```
cd ursim-5.8.2.10297/
./starturcontrol.sh
./start-ursim.sh
```
2. Install URCap in the simulator following the instructions [here](https://github.com/UniversalRobots/Universal_Robots_ROS_Driver/blob/master/ur_robot_driver/doc/install_urcap_e_series.md). To put the externalcontrol file in the folder, run
```
cd ~/ur5_ws/src/Universal_Robots_ROS_Driver/ur_robot_driver/resources
cp externalcontrol-1.0.4.urcap ~/ursim-5.8.2.10297/programs.UR5/
```

## How to run
1. Open a terminal and start the simulator.
```
cd ursim-5.8.2.10297/
./starturcontrol.sh
./start-ursim.sh
```
2. Go to `Installation` tab, select `URCaps`. In `External Control`, change Host IP and Host name both to `127.0.0.1`.
3. Go to `Program` tab, select `URCaps`, and click `External Control` to run external control program.
4. Open another terminal, and run the simulator roslaunch file.
```
cd ~/ur5_ws && source devel/setup.bash
roslaunch ur_control_wrapper sim_ur5e_jenny_control.launch
```
The continuous printing of `[ WARN] [1631675813.020228057]: Message on ReverseInterface received. The reverse interface currently does not support any message handling. This message will be ignored.
` means the connection to the virtual robot is built. If you don't see the continuous printing, terminate the roslaunch file, and rerun it until the continuous printing occurs. 
5. Test if demo file works. Open another terminal, and run demo file. If you run commands like `x+10`, `z+10`, you should see that in `Move` tab, the virtual robot executes the command accordingly.
```
cd ~/ur5_ws && source devel/setup.bash
rosrun ur_control_wrapper demo.py
```
6. If you want to visualize this virtual robot in rviz and control the robot using MoveIt! interface, run
```
roslaunch ur5_e_moveit_config moveit_rviz.launch config:=true
```
