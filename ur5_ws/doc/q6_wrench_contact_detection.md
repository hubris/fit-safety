## tmp
```
source devel/setup.bash
roslaunch ur_control_wrapper ur5e_hande_jenny_control.launch
```

Run
```
source devel/setup.bash
rosrun ur_control_wrapper demo_test_ted.py
```
and input `zft` to zero out the measurement for initialization.


```
source devel/setup.bash
rosrun ur_control_wrapper wrench_supervisor.py
```

```
source devel/setup.bash
rosrun ur_control_wrapper demo_q6.py
```

```
source devel/setup.bash
rosrun ur_control_wrapper check_task_clock_talker.py
```

Run `rostopic echo /wrench` or code below to monitor the wrench measurement.

```
```