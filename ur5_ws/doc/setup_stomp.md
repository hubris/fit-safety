# Set up STOMP planner.

The instructions here are based on [STOMP Planner](http://docs.ros.org/en/melodic/api/moveit_tutorials/html/doc/stomp_planner/stomp_planner_tutorial.html) and [stomp_ros repo](https://github.com/ros-industrial/stomp_ros), and are tested on Ubuntu 18.04. In the current repo, we have built STOMP package. Check if you need to set up STOMP for your customized robot configuration, and how to use STOMP.

## Build STOMP package.
Currently `stomp_ros` is already built. If it does not exist in `ur5_ws/src`. Follow the steps below.

1. Download the package.
```
cd ur5_ws/src
git clone https://github.com/ros-industrial/stomp_ros
```

2. Build the package. You can try `catkin_make`, but it is recommended to do as follows.
```
cd ur5_ws/
catkin build
```

3. Test the package.
```
catkin run_tests stomp_core
```

## Set up STOMP planner for your robot.
We have set up STOMP planner for `ur5_e_moveit_config` and `ur5_e_hande_moveit_config`. The steps take `ur5_e_moveit_config` as an example.

1. Copy the file [stomp_planning_pipeline.launch.xml](https://github.com/ros-planning/panda_moveit_config/blob/melodic-devel/launch/stomp_planning_pipeline.launch.xml) and paste in the folder `ur5_ws/src/fmauch_universal_robot/ur5_e_moveit_config/launch`. Change the line
```
<rosparam command="load" file="$(find panda_moveit_config)/config/stomp_planning.yaml" />
```
to
```
<rosparam command="load" file="$(find ur5_e_moveit_config)/config/stomp_planning.yaml" />
```
I.e. replace `panda_moveit_config` with the name of your MoveIt configuration package.

2. Copy the file [stomp_planning.yaml](https://github.com/ros-planning/panda_moveit_config/blob/melodic-devel/config/stomp_planning.yaml) and paste in the folder `ur5_e_moveit_config/config`. We need to change the first two lines to fit our robot names.
Change
```
stomp/panda_arm:	
  group_name: panda_arm
```
to
```
stomp/ur5_e_arm:
  group_name: manipulator
```
We also change below as the number of joints of the robot may vary. The length of stddev must match.
Change
```
	noise_generator:	
      - class: stomp_moveit/NormalDistributionSampling	
        stddev: [0.05, 0.8, 1.0, 0.8, 0.4, 0.4, 0.4]
```
to
```
    noise_generator:
      - class: stomp_moveit/NormalDistributionSampling
        stddev: [0.1, 0.1, 0.1, 0.1, 0.1, 0.1]
```
[211031] Note that `ur5_e_hande_config` includes the gripper, and we did not handle that yet and keep the same `stomp_planning.yaml` as `ur5_e_moveit_config`. We will fix that soon.

3. Modify the argument `pipeline` in `ur5_e_moveit_config/launch/move_group.launch`, `ur5_e_moveit_config/launch/ur5_e_moveit_planning_execution.launch`, and `ur_control/wrapper/launch/sim_ur5e_jenny_control.launch`, so that the argument `pipeline` can be passed towards the low level.

## How to run (in simulation)
Note that now we only changed the argument in `sim_ur5e_jenny_control.launch` and `sim_ur5e_hande_control.launch`. [211031] Note we will change the argument in the launch scripts that control the real robot, and test on them.

1. Follow [setup_ursim.md](./setup_ursim.md) to run the ursim.

2. If you want to include only the ur5e arm itself, run
```
cd ~/ur5_ws && source devel/setup.bash
roslaunch ur_control_wrapper sim_ur5e_jenny_control.launch pipeline:=stomp
```
If you want to include the hande gripper, run
```
cd ~/ur5_ws && source devel/setup.bash
roslaunch ur_control_wrapper sim_ur5e_hande_control.launch pipeline:=stomp
```
If you exclude `pipelie:=stomp` or include `pipeline:=ompl`, you will start the control with the default OMPL planner,

3. Start visualization with moveit motion planning interface.
```
cd ~/ur5_ws && source devel/setup.bash
roslaunch ur5_e_moveit_config moveit_rviz.launch config:=true
```

4. Run the program that creates a virtual box obstacle that can be moved by keyboard.
```
cd ~/ur5_ws && source devel/setup.bash
rosrun ur_control_wrapper move_obstacle_box.py
```


5. Run the demo file. Control the robot to move. No matter the robot is controlled by OMPL or STOMP planner, you can see that the robot will dodge the obstacle. STOMP usually will yield less exaggerated results.
```
cd ~/ur5_ws && source devel/setup.bash
rosrun ur_control_wrapper demo.py
```


## Tips

You should separate `plan()` and `execute()` in Python API to run STOMP. You also need to set start state before the plan every time you want to plan the motion of the robot. [211031] In future, we will make `wait=False` and test if it will help replanning.
```
arm_group = self.arm_group
state = self.robot.get_current_state()
arm_group.set_start_state(state)

arm_group.set_pose_target(pose)
plan = arm_group.plan()
arm_group.execute(plan, wait=True)
```