# Combined Vision and Contact Baseline

## How to run (tmp)

Open one terminal to bring up the robot.
```
cd ur5_ws && source devel/setup.bash
roslaunch ur_control_wrapper ur5e_jenny_control.launch
```
Open the 2nd terminal to run the clock node for asynchronous setup.
```
cd ur5_ws && source devel/setup.bash
rosrun ur_control_wrapper check_task_clock_talker.py
```
Open the 3rd terminal to run the code for the finite state machine of the robot. Now the robot starts moving to goal positions recursively.
```
cd ur5_ws && source devel/setup.bash
rosrun ur_control_wrapper demo_safety.py
```
Open the 4th terminal to run the code for the response to the contact between human and the whole arm.
```
cd ur5_ws && source devel/setup.bash
rosrun ur_control_wrapper human_touch_joints.py
```
Open the 5th terminal to launch the RealSense camera.
```
roslaunch realsense2_camera rs_camera.launch align_depth:=True
```
Open the 6th terminal to run the code for human pose tracking and detection of intrusion into workspace.
```
source ~/cv_bridge_ws/install/setup.bash --extend
source ~/ur5_ws/devel/setup.bash --extend
source ~/Documents/openpose-env/bin/activate
python ~/catkin_ws_q3/src/rosop/scripts/hand_pixel_perception.py
```
Open the 7th terminal to run rviz for visualization. Add Image under '/hri/hand_pixel' in rviz to visualize hand tracking.
```
rviz
```