# Contact Detection Baseline

## How to run (tmp)

Open one terminal to bring up the robot.
```
cd ur5_ws && source devel/setup.bash
roslaunch ur_control_wrapper ur5e_jenny_control.launch
```
Open the 2nd terminal to run the clock node for asynchronous setup.
```
cd ur5_ws && source devel/setup.bash
rosrun ur_control_wrapper check_task_clock_talker.py
```
Open the 3rd terminal to run the code for the finite state machine of the robot.
```
cd ur5_ws && source devel/setup.bash
rosrun ur_control_wrapper demo_q4.py
```
Open the 4th terminal to run the code for the response to the contact between human and the whole arm.
```
cd ur5_ws && source devel/setup.bash
rosrun ur_control_wrapper human_touch_joints.py
```
If you find the pause of robot action is delayed after the contact is detected, check out [this link](https://answers.ros.org/question/367963/real-ur5-robot-does-not-stop-immediately-upon-receiving-moveit-stop-command/) for the solution. Essentially, you need to set `stop_trajectory_duration` as 0 for all controllers in [ur5e_controllers.yaml](../../src/Universal_Robots_ROS_Driver/ur_robot_driver/config/ur5e_controllers.yaml).

