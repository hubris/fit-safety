# Open-Loop Model-E Assembly

## How to run

Open one terminal to bring up the robot.
```
source devel/setup.bash
roslaunch ur_control_wrapper ur5e_jenny_control.launch
```

Open another terminal to run the demo file for model-e.
```
source devel/setup.bash
rosrun ur_control_wrapper demo_modele.py
```

If you want the access to the camera, plug in the camera cable, open another terminal and run
```
roslaunch realsense2_camera rs_camera.launch align_depth:=True
```
Open another terminal and run `rviz`.

In the demo_modele window, input commands `d`, `modele-pickup`, `grip`, `gcl`, `d`, and `modele-ready`. After you see the male and female connectors are close, do multiple operations such as `z-1`, `z-0.5` until the assembly is finished. Then input commands `grip`, `gop`, `z+10`, `d` to move the robot back to the default position.

If you want to visualize wrench, run
```
source devel/setup.bash
rosrun ur_control_wrapper wrench_visualizer.py
```

If you want to zero force torque, run
```
source devel/setup.bash
rosrun ur_control_wrapper demo_test_ted.py
```
and input the command `zft`.
