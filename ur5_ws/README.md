# ur5_ws 
Main ROS workspace for hardware driver, including the ur5e arm and the hand-e gripper and control wrapper. 

## Prerequisites

Ubuntu 18.04

ROS Melodic

Install ros-controller:

`sudo apt-get install ros-melodic-ros-control ros-melodic-ros-controllers`

Install Moveit:

`sudo apt-get install ros-melodic-moveit`

Install the trac-ik solver:

`sudo apt-get install ros-melodic-trac-ik-kinematics-plugin`

Install python interface for Real-Time Data Exchange (RTDE) of UR robot. This is used for reading joint_currents. (Must use pip3, not pip.) 

`pip3 install --user ur_rtde`

## Installation

`git clone http://github.com/cobot-factory/ur5_ws.git`

`cd ur5_ws`

# install dependencies
`sudo apt update -qq`

`rosdep update`

`rosdep install --from-paths src --ignore-src -y`

`catkin_make`

## Updated on Oct. 31, 2021
Instead of `catkin_make`, we use `catkin build`. Also we added `apriltag` to avoid `No  <apriltag/apriltag.h>` error when building `apriltag_ros`. We also use that to build `stomp_ros`.

## Hardware preparation
Make sure the host desktop and the ur5e are on the same network. By default, the robot ip is `192.168.0.2`, the host computer ip is `192.168.0.1`
On teach pendant:
Power and release the arm

Activate the gripper under manual control mode

Switch to Automatic then remote control mode

Use pylon_viwer API to turn some of the camera parameters. 


## Usage
Open one terminal to bring up the robot and all the nodes for the communication and control/planning:

`cd ur5_ws && source devel/setup.bash`

`roslaunch ur_control_wrapper ur5e_jenny_control.launch`

Open another terminal to run user node, one can enter user instruction as prompted:

`cd ur5_ws && source devel/setup.bash`

`rosrun ur_control_wrapper demo.py`

For a simple pick and place demo, put "pp" as an input in the terminal. 
* The 'demo.py' includes a hard-coded example of a simple pick-and-place task.

### Joint Currents

If you want to adjust the publish rate of joint currents (default: 100), open one terminal and run:

`cd ur5_ws && source devel/setup.bash`

`roslaunch ur_control_wrapper ur5e_jenny_control.launch joint_current_publish_rate:=100`

To visualize the measurement in real time, open another terminal choose a joint index from 0 to 5 (UR has 6 joints, default: 0), and run:

`cd ur5_ws && source devel/setup.bash`

`rosrun ur_control_wrapper joint_currents_visualizer.py --joint_index 0`

Sometimes `ValueError: shape mismatch` may pop up on this visualization implementation, but you can ignore it as long as real time plot is working well except for this error.

## Balser camera ROS driver

If you only want to launch the camera driver, make sure you are ok with some of the default camera parameters through pylon_viewer. You can also modify some of the config in `pylon-ros-camera/pylon_camera/config/default.yaml`. Then, 

`cd ur5_ws && source devel/setup.bash`

`roslaunch pylon_camera pylon_camera_node.launch`


## AprilTag ROS driver

If you only want to launch the AprilTag ROS driver, specify the tag config in `apriltag_ros/apriltag_ros/config/tags.yaml` such as tag ID and size in (m). Then, 

`cd ur5_ws && source devel/setup.bash`

`roslaunch apriltag_ros continuous_detection.launch`

You can double check the detection through rostopic `\tag_detections` or by simply using RVIZ.

One thing needs to be careful is that although the `continuous_detection.launch` provides the parameters to modify the topic name of the camera, I found that for some reason, it doesn't work. The author hardcoded the camera topic name in `apriltag_ros/apriltag_ros/src/continuous_detector.cpp`. If you want to use a different camera, which may publish different topic name, make sure to modify the corresponding lines and re-compile the c++ code.

### Camera Extrinsic Cablibration

Make sure the robot hardware preparation has been done. Open one terminal to bring up the robot and all the nodes for comminication and control/planning, pylon camera ROS driver and apriltag ROS driver (I've made them into a single launch file):

`cd ur5_ws && source devel/setup.bash`

`roslaunch easy_handeye handeye_calibrate.launch`

Use the pop-up rqt GUI to take samples and compute the camera extrinsic. Take samples when the apriltag estimates are stable. (You can monitor the tag detection results by `rostopic echo /tag_detections`). Click the `compute extrinsic` when you are done running the default trajectory.

The default easy_handeye repo uses only one April Tag to do the calibraiton, which cannot get very high accuracy. If you need a high accuray camera extrinsic calibration, you can run our own `demo.py` file to collect the raw images and the corresponding robot end-effector pose:

`cd ur5_ws && source devel/setup.bash`

`roslaunch easy_handeye handeye_calibrate.launch`

Open another terminal:

`rosrun ur_control_wrapper demo.py`

Everytime when you take samples in the rqt GUI, you can hit `sa` in the 2nd terminal to collect the raw samples in the `../data` folder.

Once the data collection is done, you can run the jupter notebook to optimize the camera extrinsic: follow the instruction in `Extrinsic_calibration.ipynb` by making a poses.json, specify the correct data path and give a prior of the camera extrinsics (You can do this by giving the easy_handeye results or the tape measured results). Then, run all the cells to obtain the optimized camera extrinsic `T_cam_in_base`. It may take a while if the image resolution is high.

### Admittance Control
Open one terminal to bring up the robot and all the nodes for the communication and control/planning:

`cd ur5_ws && source devel/setup.bash`

`roslaunch ur_control_wrapper ur5e_jenny_control.launch`

Open the second terminal to run demo file with admittance control functions:

`cd ur5_ws && source devel/setup.bash`

`rosrun ur_control_wrapper demo_test_ted_origin.py`

Open the third terminal to run admittance control code:

`cd ur5_ws && source devel/setup.bash`

`rosrun ur_control_wrapper admittance_control.py`

Go back to the second terminal, and type the command `sac` to switch to admittance control mode. You may now apply force to the FT sensor to move the end-effector. If you want to use `d` or `x+` functions, type the command `sac` again to first switch the controller.


## Acknowledgements

We have referred to the guide and part of the code from [Universal_Robots_ROS_Driver](https://github.com/UniversalRobots/Universal_Robots_ROS_Driver), [robotiq](https://github.com/ros-industrial/robotiq.git), [ScazLab](https://github.com/ScazLab), [frdedynamics](https://github.com/frdedynamics/ros_robotiq_urcap_control), [apriltag_ros](http://wiki.ros.org/apriltag_ros), [easy_handeye](https://github.com/IFL-CAMP/easy_handeye) and [pylon-ros-camera](https://github.com/basler/pylon-ros-camera)

## Side Notes
If you copy `ur5_ws` somewhere else, you need to remove `build`, `devel`, `logs` and rebuild everything. Otherwise, you will find when you run
```
cd ur5_ws && source devel/setup.bash
```
you will be directed to the original `ur5_ws` folder from which you copied, not the one you are currently working on.