#! /usr/bin/env python

import rospy
import tf
from visualization_msgs.msg import Marker
from geometry_msgs.msg import Point
from birdcam_backprojection import *

# define the parameters of the safety ribbon here
x1 = 340
y1 = 200
x2 = 790
y2 = 600

camera_coord = image_to_world([0,0], 0)
camera_z = camera_coord[2]
#print(camera_z)
points = []
points.append(image_to_world([x1,y1],camera_z))
points.append(image_to_world([x1,y2],camera_z))
points.append(image_to_world([x2,y2],camera_z))
points.append(image_to_world([x2,y1],camera_z))
points.append(image_to_world([x1,y1],camera_z))

rospy.init_node('generate_safety_ribbon')
listener = tf.TransformListener()
listener.waitForTransform("/base_link", "/ur_arm_base_link", rospy.Time(), rospy.Duration(4.0))
marker_pub = rospy.Publisher("/visualization_marker", Marker, queue_size = 2)


marker = Marker()

marker.header.frame_id = "/ur_arm_base_link"
marker.header.stamp = rospy.Time.now()

marker.type = marker.LINE_STRIP
marker.action = marker.ADD
marker.id = 0
marker.points = []

for point in points:
  msg_point = Point()
  msg_point.x = point[0]
  msg_point.y = point[1]
  msg_point.z = point[2]
  marker.points.append(msg_point)


# marker scale
marker.scale.x = 0.02
marker.scale.y = 0.02
marker.scale.z = 0.02

# marker color
marker.color.a = 1.0
marker.color.r = 1.0
marker.color.g = 1.0
marker.color.b = 0.0

# marker orientaiton
marker.pose.orientation.x = 0.0
marker.pose.orientation.y = 0.0
marker.pose.orientation.z = 0.0
marker.pose.orientation.w = 1.0

# marker position
marker.pose.position.x = 0.0
marker.pose.position.y = 0.0
marker.pose.position.z = 0.0

while not rospy.is_shutdown():
  marker_pub.publish(marker)
  