#!/usr/bin/env python

import rospy
from sensor_msgs.msg import JointState
from std_msgs.msg import Header

joint_list = ['lift_joint', 
  'wheel_right_joint',
  'wheel_left_joint',
  'shoulder_pan_joint',
  'shoulder_lift_joint',
  'elbow_joint',
  'wrist_1_joint',
  'wrist_2_joint',
  'wrist_3_joint',
  'hande_joint_finger',
  'hande_right_finger_joint']


joint_pos = [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]

def main():
  pub = rospy.Publisher('joint_states', JointState, queue_size=10)
  rospy.init_node('fake_rosbag_node')
  rate = rospy.Rate(1) # 1hz

  while not rospy.is_shutdown():
    joint_state = JointState()
    joint_state.header = Header()
    joint_state.header.stamp = rospy.Time.now()
    joint_state.name = joint_list
    joint_pos[3] += 0.04
    joint_pos[4] += 0.04
    joint_pos[5] += 0.04
    joint_pos[6] += 0.04
    joint_pos[7] += 0.04
    joint_pos[8] += 0.04
    print(joint_pos)
    joint_state.position = joint_pos
    pub.publish(joint_state)
    rate.sleep() 

if __name__ == "__main__":
  try:
    main()
  except rospy.ROSInterruptException:
    print("")
  except KeyboardInterrupt:
    print("")
