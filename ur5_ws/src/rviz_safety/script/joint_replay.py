#!/usr/bin/env python

import yaml
import rospy

from sensor_msgs.msg import JointState
from std_msgs.msg import Header

joint_list = ['lift_joint', #0
  'wheel_right_joint',      #1
  'wheel_left_joint',       #2
  'shoulder_pan_joint',     #3
  'shoulder_lift_joint',    #4
  'elbow_joint',            #5
  'wrist_1_joint',          #6
  'wrist_2_joint',          #7
  'wrist_3_joint',          #8
  'hande_joint_finger',     #9
  'hande_right_finger_joint']

 
'''
joints in ROSBAG: 
['elbow_joint',         #0
'shoulder_lift_joint',  #1
'shoulder_pan_joint',   #2
'wrist_1_joint',        #3
'wrist_2_joint',        #4
'wrist_3_joint']        #5
'''

joint_pos = [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]


# publish one msg read from YAML file with the given publisher
def replay_joint_state(pub, msg):
  joint_state = JointState()
  joint_state.header = Header()
  joint_state.header.stamp = rospy.Time.now()
  joint_state.name = joint_list
  # elbow_joint
  joint_pos[5] = msg['position'][0]
  # shoulder_lift_joint
  joint_pos[4] = msg['position'][1]
  # shoulder_pan_joint
  joint_pos[3] = msg['position'][2]
  # wrist_1_joint
  joint_pos[6] = msg['position'][3]
  # wrist_2_joint
  joint_pos[7] = msg['position'][4]
  # wrist_3_joint
  joint_pos[8] = msg['position'][5]

  joint_state.position = joint_pos
  pub.publish(joint_state)

def main():
  rosbag_msgs = yaml.safe_load_all(open('joints.yaml'))
  
  pub = rospy.Publisher('joint_states', JointState, queue_size=10)
  rospy.init_node('fake_rosbag_node', anonymous=True)
  rate = rospy.Rate(200) # 200hz
  for msg in rosbag_msgs:
    # for all joints space msgs 
    if msg == None:
      continue
    if rospy.is_shutdown():
      break
    replay_joint_state(pub, msg)
    rate.sleep() 

if __name__ == "__main__":
  try:
    main()
  except rospy.ROSInterruptException:
    print("")
  except KeyboardInterrupt:
    print("")
