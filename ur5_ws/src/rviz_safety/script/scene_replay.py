#!/usr/bin/env python

import yaml
import rospy

from birdcam_backprojection import *
from joint_replay import *

RECORD_FILE = 'scene_z.yaml'
COUNTER_INIT = 50

def main():
  rospy.init_node('fake_rosbag_node', anonymous=True)
  rosbag_msgs = yaml.safe_load_all(open(RECORD_FILE))
  pub = rospy.Publisher('joint_states', JointState, queue_size=10)
  wrist_visible = True
  # 800hz, actually rate is ~200Hz
  rate = rospy.Rate(200)
  counter = COUNTER_INIT
  for msg in rosbag_msgs:
    # for all joints space msgs 
    if msg == None:
      continue
    if rospy.is_shutdown():
      break
    if msg['topic'] == "/joint_states":
      # joint states
      if counter != 0:
        counter -= 1
        continue
      else:
        replay_joint_state(pub, msg)
        counter = COUNTER_INIT
    elif msg['topic'] == "/q3/hand_pixel_position":
    # human hand pixel coord and depth
      if wrist_visible:
        if msg['depth'] == 0.0:
          # There are times when depth is 0.0. Reason is unknown
          continue
        world_coord = image_to_world(msg['data'], msg['depth'] / 1000.0)
        #world_coord = image_to_world([900, 650], 1800 / 1000.0)
        wrist_tf_broadcast(world_coord)
      else:
        hide_wrist()
    elif msg['topic'] == "/q3/hand_in_frame":
    # hand detected (bool)
      wrist_visible = msg['data']
      if not wrist_visible:
        hide_wrist()
    rate.sleep()

if __name__ == "__main__":
  try:
    main()
  except rospy.ROSInterruptException:
    print("")
  except KeyboardInterrupt:
    print("")
