#!/usr/bin/env python

from time import sleep

import yaml
import numpy as np
import rospy
import tf2_ros

from std_msgs.msg import Header
import geometry_msgs.msg

# intrinsic
fx = 918.44970703125
fy = 918.126220703125
ox = 640.32861328125
oy = 357.2995910644531
K_i = np.zeros((3,3))
K_i[0,0] = fx
K_i[1,1] = fy
K_i[0,2] = ox
K_i[1,2] = oy
K_i[2,2] = 1
K_i_inv = np.linalg.inv(K_i)

# extrinsic
K_e = np.zeros((4,4))
K_e = np.array( \
[[0.05791677184332739, 0.9981225390285365, 0.01992464381424572, -0.0679819144260702],   \
 [0.9969790587646147, -0.05679275086829541, -0.052983834992759375, 0.5077813319500044], \
 [-0.05175278457816044, 0.022933105318806886, -0.9983965754139671, 1.820766490646183],  \
 [0.0, 0.0, 0.0, 1.0]])

K_e_inv = np.linalg.inv(K_e)


# tf broadcaster for transformation between hand and base_link
br = tf2_ros.TransformBroadcaster()

# convert pixel coordinates to 3D world coordinates
# Z_t's unit is meter, by default set to 1.0
def image_to_world(pixel_coord, Z_t = 1.0):
  # pixel coord -> camera coord
  pixel_coord.append(1.0)
  pixel_coord = np.array(pixel_coord)
  camera_coord = np.dot(K_i_inv, pixel_coord)
  camera_coord = camera_coord * Z_t
  camera_coord = np.append(camera_coord, 1)

  # camera coord -> world coord
  world_coord = np.dot(K_e_inv, camera_coord)
  return world_coord

def hide_wrist():
  t = geometry_msgs.msg.TransformStamped()
  t.header.stamp = rospy.Time.now()
  t.header.frame_id = "ur_arm_base_link"
  t.child_frame_id = "wrist_link"
  t.transform.translation.x = 0.0
  t.transform.translation.y = 0.0
  t.transform.translation.z = -0.2
  t.transform.rotation.x = 0.0
  t.transform.rotation.y = 0.0
  t.transform.rotation.z = 1.0
  t.transform.rotation.w = 0.0
  br.sendTransform(t)
  

# broadcast tf between ur5 and human skeleton coords
def wrist_tf_broadcast(world_coord):
  t = geometry_msgs.msg.TransformStamped()
  t.header.stamp = rospy.Time.now()
  t.header.frame_id = "ur_arm_base_link"
  t.child_frame_id = "wrist_link"
  t.transform.translation.x = world_coord[0]
  t.transform.translation.y = world_coord[1]
  t.transform.translation.z = world_coord[2]
  # q = tf_conversions.transformations.quaternion_from_euler(0, 0, msg.theta)
  t.transform.rotation.x = 0.0
  t.transform.rotation.y = 0.0
  t.transform.rotation.z = 0.0
  t.transform.rotation.w = 1.0
  br.sendTransform(t)

def main():
  rosbag_msgs = yaml.safe_load_all(open('hand_pos.yaml'))
  rospy.init_node('human_pose_node')
  for msg in rosbag_msgs:
    # for all joints space msgs 
    if msg == None:
      continue
    if rospy.is_shutdown():
      break
    world_coord = image_to_world(msg['data'])

    wrist_tf_broadcast(world_coord)
    sleep(0.04)

if __name__ == "__main__":
  try:
    main()
  except rospy.ROSInterruptException:
    print("")
  except KeyboardInterrupt:
    print("")
