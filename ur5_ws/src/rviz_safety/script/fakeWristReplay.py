#!/usr/bin/env python

from time import sleep

import yaml
import numpy as np
import rospy
import tf2_ros

from sensor_msgs.msg import JointState
from std_msgs.msg import Header
import geometry_msgs.msg

fx = 918.44970703125
fy = 918.126220703125
ox = 640.32861328125
oy = 357.2995910644531
# intrinsic
K_i = np.zeros((3,3))
K_i[0,0] = fx
K_i[1,1] = fy
K_i[0,2] = ox
K_i[1,2] = oy
K_i[2,2] = 1
K_i_inv = np.linalg.inv(K_i)

# extrinsic
K_e = np.zeros((4,4))
K_e = np.array( \
[[0.05791677492522296, 0.9969791118163432, -0.05175278733205397, -0.4080803471158925],  \
 [0.9981225921411109, -0.056792753890377946, 0.022933106539134423, 0.05493675294835445],\
 [0.01992464381424547, -0.052983834992759764, -0.9983965754139653, 1.8461057466248456] ,\
 [0.0, 0.0, 0.0, 1.0]])

K_e_inv = np.linalg.inv(K_e)

def image_to_world(pixel_coord, Z_t = 1.0):
  # pixel coord -> camera coord
  pixel_coord.append(1.0)
  pixel_coord = np.array(pixel_coord)
  camera_coord = K_i_inv @ pixel_coord
  camera_coord = camera_coord * Z_t
  camera_coord = np.append(camera_coord, 1)

  # camera coord -> world coord
  world_coord = K_e_inv @ camera_coord
  return world_coord

def main():
  rosbag_msgs = yaml.safe_load_all(open('hand_pos.yaml'))
  
  rospy.init_node('human_pose_node')
  br = tf2_ros.TransformBroadcaster()
  z = 0.0
  for msg in rosbag_msgs:
    # for all joints space msgs 
    if msg == None:
      continue
    if rospy.is_shutdown():
      break
    world_coord = image_to_world(msg['data'])
    print(world_coord)

    t = geometry_msgs.msg.TransformStamped()
    t.header.stamp = rospy.Time.now()
    t.header.frame_id = "base_link"
    t.child_frame_id = "wrist_link"
    t.transform.translation.x = world_coord[0]
    t.transform.translation.y = world_coord[1]
    t.transform.translation.z = world_coord[2]
    # q = tf_conversions.transformations.quaternion_from_euler(0, 0, msg.theta)
    t.transform.rotation.x = 0.0
    t.transform.rotation.y = 0.0
    t.transform.rotation.z = 0.0
    t.transform.rotation.w = 1.0
    br.sendTransform(t)
    sleep(0.04)

if __name__ == "__main__":
  try:
    main()
  except rospy.ROSInterruptException:
    print("")
  except KeyboardInterrupt:
    print("")
