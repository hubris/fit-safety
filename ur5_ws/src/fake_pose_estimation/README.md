## Launch File Examples

1. Launch a two-dimensional oriented rectangles-based part detection node

    `roslaunch realsense2_camera rs_camera.launch`

    `roslaunch fake_pose_estimation pose_2d.launch`
