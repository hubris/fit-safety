#!/usr/bin/env python  
import rospy
# Because of transformations
import tf_conversions
import tf2_ros
import geometry_msgs.msg
import numpy as np
from tf import transformations as tfs

class fake_pose_broadcaster:
    def __init__(self):
        # Depreciated: orgin offset in world frame
        # self.parts_pose = {'main_shell': {'top':[4.5*0.001, -4.25*0.001, -(1.33+1.5)*0.001, np.pi/2.0, 0.0, np.pi/2.0], 
        #                                   'bottom':[4.5*0.001, 4.25*0.001, (1.33+1.5)*0.001, -np.pi/2.0, 0.0, -np.pi/2.0]},
        #                    'top_shell': {'top':[4.5*0.001, -4.25*0.001, -(1.33+1.5)*0.001, np.pi/2.0, 0.0, np.pi/2.0]}}
        
        # parts pose: first 3 elements represent the parts origin wrt parts center
        #             last 4 elements represent the euler angle of the parts
        parts_o2c = np.array([-4.25, -(1.33+1.5), 4.5])*0.001
        parts_o2c = np.array([0.0, 0.0, 0.0])*0.001
        self.parts_pose = {'main_shell': {'top':[parts_o2c[0], parts_o2c[1], parts_o2c[2], np.pi/2.0, 0.0, -np.pi/2.0], 
                                          'bottom':[parts_o2c[0], parts_o2c[1], parts_o2c[2], -np.pi/2.0, 0.0, np.pi/2.0],
                                          'left':[parts_o2c[0], parts_o2c[1], parts_o2c[2], -np.pi/2.0, 0.0, 0.0]},
                           'top_shell': {'top':[parts_o2c[0], parts_o2c[1], parts_o2c[2], np.pi/2.0, 0.0, np.pi/2.0]},
                           'insert_mold': {'bottom':[parts_o2c[0], parts_o2c[1], parts_o2c[2], 0.0, 0.0, -np.pi/2.0]}}
        self.lg = 0.037
        # parts center in world frame/currently world frame is the same as the robot base frame.
        self.parts_center = {'main_shell': [0.48736, -0.2830, 0.0045 - self.lg],
                             'top_shell': [-0.493, 0.143, 0.0015 - self.lg],
                             'insert_mold': [-0.4937125, 0.2619375, 0.0015 - self.lg]}
                             
        #self.pose_sub = rospy.Subscriber('/ee_pose_jenny', geometry_msgs.msg.Pose, handle_parts_pose, partsname)
        #rospy.Timer(rospy.Duration(0.05), self.handle_parts_pose)

        while not rospy.is_shutdown():
            rospy.sleep(0.05)
            self.handle_parts_pose('main_shell', 'left')
            self.handle_parts_pose('top_shell', 'top')
            self.handle_parts_pose('insert_mold', 'bottom')

    def handle_parts_pose(self, partsname, orientation):
        origin_offset = self.parts_pose[partsname][orientation][:3]
        euler = self.parts_pose[partsname][orientation][3:6]
        center = self.parts_center[partsname]

        M_p2w = tfs.euler_matrix(euler[0], euler[1], euler[2], 'sxyz')
        parts_origin = center + np.dot(M_p2w[:3,:3], origin_offset)

        br = tf2_ros.TransformBroadcaster()
        t = geometry_msgs.msg.TransformStamped()
        t.header.stamp = rospy.Time.now()
        t.header.frame_id = "world"
        t.child_frame_id = partsname
        # t.transform.translation.x = center[0] + origin_offset[0]
        # t.transform.translation.y = center[1] + origin_offset[1]
        # t.transform.translation.z = center[2] + origin_offset[2]
        t.transform.translation.x = parts_origin[0]
        t.transform.translation.y = parts_origin[1]
        t.transform.translation.z = parts_origin[2]
        q = tf_conversions.transformations.quaternion_from_euler(euler[0], euler[1], euler[2])
        t.transform.rotation.x = q[0]
        t.transform.rotation.y = q[1]
        t.transform.rotation.z = q[2]
        t.transform.rotation.w = q[3]
        br.sendTransform(t)


if __name__ == '__main__':
    rospy.init_node('tf2_parts_broadcaster')

    pose_tf_br = fake_pose_broadcaster()

    rospy.spin()