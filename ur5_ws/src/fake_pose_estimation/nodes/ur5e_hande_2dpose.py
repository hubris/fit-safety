#!/usr/bin/env python  
import rospy
import numpy as np
from cv_bridge import CvBridge, CvBridgeError
from sensor_msgs.msg import Image,CameraInfo
from std_msgs.msg import String
import message_filters
import cv2

class Pose_2D():
    def __init__(self):

        self.bridge = CvBridge()
        self.image_pub = rospy.Publisher('/camera/color/pose_2d', Image, queue_size=20)
        self.pose_sub = rospy.Subscriber('/camera/color/image_raw', Image, self.pose)
        self.info_sub = rospy.Subscriber('/camera/color/camera_info', CameraInfo, self.inverse)
        
    def inverse(self, msg):
        K = msg.K
        K = np.asarray(K).astype(float).reshape((3,3))
        self.K = np.linalg.inv(K)
        self.D = msg.D

    def pxpyz_to_xyz(self, px, py, z):
        projection = np.array([[px], [py], [1]])
        point_3d = np.matmul(self.K, projection)
        point_3d *= z / point_3d[2, 0]
        return point_3d[0, 0], point_3d[1, 0], point_3d[2, 0]

    def pose(self,img):
        try:
            img = self.bridge.imgmsg_to_cv2(img,'passthrough')
        except CvBridgeError as e:
            print("CvBridge could not convert images from realsense to opencv")
        rows,cols = img.shape[0], img.shape[1]
        img = cv2.undistort(image, self.K, self.D)

        img_gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        thresh = 10
        img_bw = cv2.threshold(img_gray, thresh, 255, cv2.THRESH_BINARY)[1]
        kernel = cv2.getStructuringElement(cv2.MORPH_ELLIPSE,(9,9))
        img_bw = cv2.dilate(img_bw, kernel)

        _, contours, _= cv2.findContours(img_bw, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)

        img_rect = img.copy()

        for cnt in contours:
            rect = cv2.minAreaRect(cnt)
            center, size, angle = rect
            if size[0] <= 60 or size[1] <= 60:
                continue

            center, size = tuple(map(int, center)), tuple(map(int, size))

            # Get rotation matrix for rectangle
            M = cv2.getRotationMatrix2D(center, angle, 1)
            # Perform rotation on src image
            img_bw_rotated = cv2.warpAffine(img_bw, M, img_bw.shape[:2])
            img_bw_crop = cv2.getRectSubPix(img_bw_rotated, size, center)

            argmax_angle_offset = 0
            max_match = -1

            img_bw_crop_rotated = img_bw_crop.copy()

            for angle_offset in range(0, 271, 90):
                h, w = img_bw_crop_rotated.shape
                small_crop = img_bw_crop_rotated[h*7/8:h*15/16, w*2/8:w*6/8]
                empty_pixel_count = np.count_nonzero(small_crop==0)

                if max_match < empty_pixel_count:
                    max_match = empty_pixel_count
                    argmax_angle_offset = angle_offset

                img_bw_crop_rotated = cv2.rotate(img_bw_crop_rotated, cv2.ROTATE_90_COUNTERCLOCKWISE)

            angle += argmax_angle_offset - 90
            angle = (angle + 360.0) % 360.0

            x, y, z = self.pxpyz_to_xyz(center[0], center[1], 0.1985)

            print((x, y), size, angle)

            angle_rad = np.radians(angle)
            box = cv2.boxPoints(rect)
            box = np.int0(box)
            img_rect = cv2.drawContours(img_rect,[box],0,(0,0,255),3)
            vector_start_point = np.array([center[0], center[1]])
            vector_end_point = vector_start_point + 100 * np.array([np.cos(angle_rad), np.sin(angle_rad)])
            start_point = (int(vector_start_point[0]), int(vector_start_point[1]))
            end_point = (int(vector_end_point[0]), int(vector_end_point[1]))
            img_rect = cv2.arrowedLine(img_rect, start_point, end_point, (0, 255, 0), 3)

        resized_img_rect = cv2.resize(img_rect, (1280, 720), interpolation = cv2.INTER_AREA)

        # Publish the resulting frame
        self.image_pub.publish(self.bridge.cv2_to_imgmsg(resized_img_rect, "rgb8"))

def main():

    while not rospy.is_shutdown():
        rospy.init_node('pose_2d')
        pose = Pose_2D()
        try:
            rospy.spin()
        except KeyboardInterrupt:
            print("Shutting down")
    cv2.destroyAllWindows()

if __name__ == '__main__':

    main()
