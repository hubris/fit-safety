#!/usr/bin/env python  
import rospy
# Because of transformations
import tf_conversions
import tf2_ros
import geometry_msgs.msg
import numpy as np
from tf import transformations as tfs

class fake_pose_broadcaster:
    def __init__(self):
        # Distance from the table surface to robot base surface
        # self.lg = 0.0375
        # parts center in world frame/currently world frame is the same as the robot base frame. parts thickness 3 mm, left 9 mm
        self.origin_offset = np.array([0.0, 0.0, 0.0])*0.001
        # self.parts_pose = {'0': {'partsname': 'main_shell', 'type': 'top',   'center': [0.600175, -0.28654, 0.0015 - self.lg], 'orientation': [0.0, np.pi, np.deg2rad(48.0)]},
        #                    '1': {'partsname': 'main_shell', 'type': 'top',   'center': [0.64975, -0.23563, 0.0015 - self.lg], 'orientation': [0.0, np.pi, np.deg2rad(100.0)]},
        #                    '2': {'partsname': 'main_shell', 'type': 'bottom','center': [0.65886, -0.17689, 0.0015 - self.lg], 'orientation': [0.0, 0.0, np.deg2rad(89.0)]},
        #                    '3': {'partsname': 'insert_mold', 'type': 'bottom','center': [0.58744, -0.18853, 0.0015 - self.lg], 'orientation': [0.0, 0.0, np.deg2rad(177.0)]},
        #                    '4': {'partsname': 'insert_mold', 'type': 'bottom','center': [0.55165, -0.13485, 0.0015 - self.lg], 'orientation': [0.0, 0.0, np.deg2rad(-65.0)]}}
        self.lg = 0.16
        self.parts_pose = {'0': {'partsname': 'main_shell', 'type': 'top',   'center': [-0.524987, 0.083192, 0.0015 + 0.30202-self.lg-0.0015], 'orientation': [np.pi, 0.0, 0.0]},
                           '1': {'partsname': 'main_shell', 'type': 'top',   'center': [-0.565113, 0.033042, 0.0015 + 0.30222-self.lg-0.0015], 'orientation': [0.0, np.pi, np.deg2rad(60.0)]},
                           '2': {'partsname': 'main_shell', 'type': 'bottom','center': [-0.535271, -0.016931, 0.0015 + 0.30233-self.lg-0.0015], 'orientation': [0.0, 0.0, np.deg2rad(60.0)]},
                           '3': {'partsname': 'main_shell', 'type': 'bottom','center': [-0.585496, -0.066859, 0.0015 + 0.30238-self.lg-0.0015], 'orientation': [0.0, 0.0, np.deg2rad(135.0)]},
                           '4': {'partsname': 'main_shell', 'type': 'top',   'center': [-0.505299, -0.086651, 0.0015 + 0.30246-self.lg-0.002], 'orientation': [0.0, np.pi, np.pi/2.0]}}

    def handle_parts_pose(self, partsID):

        euler = self.parts_pose[partsID]['orientation']
        center = self.parts_pose[partsID]['center']
        partsname = self.parts_pose[partsID]['partsname']
        face_type = self.parts_pose[partsID]['type']

        M_p2w = tfs.euler_matrix(euler[0], euler[1], euler[2], 'sxyz')
        parts_origin = center + np.dot(M_p2w[:3,:3], self.origin_offset)

        br = tf2_ros.TransformBroadcaster()
        t = geometry_msgs.msg.TransformStamped()
        t.header.stamp = rospy.Time.now()
        t.header.frame_id = "world"
        t.child_frame_id = 'poserbpf_gt/' + partsID + '/' + partsname + '/' + face_type

        t.transform.translation.x = parts_origin[0]
        t.transform.translation.y = parts_origin[1]
        t.transform.translation.z = parts_origin[2]
        q = tf_conversions.transformations.quaternion_from_euler(euler[0], euler[1], euler[2])
        t.transform.rotation.x = q[0]
        t.transform.rotation.y = q[1]
        t.transform.rotation.z = q[2]
        t.transform.rotation.w = q[3]
        br.sendTransform(t)


if __name__ == '__main__':
    rospy.init_node('tf2_parts_broadcaster_multi')

    pose_tf_br = fake_pose_broadcaster()

    while not rospy.is_shutdown():
        rospy.sleep(0.05)
        for key in pose_tf_br.parts_pose:
            pose_tf_br.handle_parts_pose(key)