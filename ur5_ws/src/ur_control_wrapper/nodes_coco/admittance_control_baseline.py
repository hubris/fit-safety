#!/usr/bin/env python
import argparse
import rospy
import numpy as np

from std_msgs.msg import Float32MultiArray, Float64MultiArray, Bool, Float64
from controller_manager_msgs.srv import SwitchController, SwitchControllerRequest, ListControllers
from ur_control_wrapper.srv import GetPose, GetJoints, InverseKinematics, InverseKinematicsRequest
from geometry_msgs.msg import Pose, Point, Quaternion, Vector3, WrenchStamped
import tf
# * admittance control
import message_filters
from std_msgs.msg import String, Bool, Float64MultiArray, Float64
from std_srvs.srv import Trigger, SetBool
from sensor_msgs.msg import JointState
from ur_control_wrapper.srv import SetPose
from ur_control_wrapper.srv import SetJoints
from ur_control_wrapper.srv import SetTrajectory
from ur_control_wrapper.srv import GetCartesianPlan
from ur_control_wrapper.srv import ExecuteCartesianPlan
from ur_control_wrapper.srv import InverseKinematics, InverseKinematicsRequest
import time
from tf import TransformListener





class CoCoControl:
    def __init__(
        self,
        args,
    ):
        # self.high_intention = 'coexistence' # * coexistence or collaboration
        self.high_intention = 'collaboration'
        self.admittance_control_mode = 'off'
        self.args = args
        rospy.init_node('coexistence_collaboration_control', anonymous=True)
        self.set_default_angles()
        self.zero_force_torque_sensor()
        # tracking_topic_name = 'hri/human_skeleton_position_tracking_3D_floatarray'    
        # goal_pos_topic_name = 'task_goal'
        # goal_vel_topic_name = 'task_vel'
        wrench_topic_name = '/wrench'
        # rospy.Subscriber(tracking_topic_name, Float32MultiArray, self.tracking_callback)
        # rospy.Subscriber(goal_pos_topic_name, Float64MultiArray, self.goal_pos_callback)
        # rospy.Subscriber(goal_vel_topic_name, Float64, self.goal_vel_callback)
        rospy.Subscriber(wrench_topic_name, WrenchStamped, self.wrench_callback)
        rospy.Subscriber("/admittance_control_reset", String, self.reset_callback)

        controller_state = self.check_controller_state(silent=True)
        self.joint_velocity_pub = rospy.Publisher('/joint_group_vel_controller/command', Float64MultiArray, queue_size=10)
        # self.collision_avoidance_pub = rospy.Publisher('/collision_avoidance', Bool, queue_size=10)
        # self.collaboration_goal_pub = rospy.Publisher('/collaboration_goal', Float64MultiArray, queue_size=10)
        self.admittance_control_mode_pub = rospy.Publisher('/admittance_control_mode', String, queue_size=10)
        if controller_state["joint_group_vel_controller"] != "running":
            self.switch_controller("joint_group")
        
        self.last_vel = np.zeros(3)
        self.last_mag = 0
        self.hand_pos = np.ones(3)*999.#np.zeros(3)
        self.goal_pos = np.zeros(3)
        self.force = np.zeros(3)
        self.goal_vec_scale = 0.
        # * admittance control parameters
        self.pose_vel_scale = 0.8
        self.vel_dec_scale = 1./15
        self.force_vel_scale = 1./10    # The amplifier of velocity
        self.force_acc_scale = 1./10    # The amplifier of acceleration
        self.force_threshold = 1.
        self.tf = TransformListener()
        self.rate = rospy.Rate(self.args.freq)

        # reach default first
        self.goal_pos = [[-0.57, 0.135, 0.50]] # default plan, default robot rest position
        self.goal_vec_scale = 2.

        self.reach_default_position()
        self.zero_force_torque_sensor()
        self.admittance_control_mode = 'on'

    def reset_callback(self, data):
        self.admittance_control_mode = 'off'
        self.set_default_angles()
        self.zero_force_torque_sensor()
        self.goal_pos = [[-0.57, 0.135, 0.50]] # default plan, default robot rest position
        self.reach_default_position()
        self.zero_force_torque_sensor()
        self.admittance_control_mode = 'on'

    def tracking_callback(self, data):
        self.hand_pos = np.array(data.data)

    def goal_pos_callback(self, data):
        self.goal_pos = np.array(data.data)
    
    def goal_vel_callback(self, data):
        self.goal_vec_scale = data.data
    
    def wrench_callback(self, data):
        self.force = np.array([data.wrench.force.x, data.wrench.force.y, data.wrench.force.z])


    # def coexistence_collaboration_control(self):
    #     if self.high_intention == 'collaboration':
    #         self.admittance_control()         
    #     else:
    #         raise RuntimeError("Wrong high intention command.")

    def reach_default_position(self):
        pos, _ = self.get_pose()
        while np.linalg.norm(pos-self.goal_pos) > 1e-2:
            self.goal_directed_motion()
            pos, _ = self.get_pose()

    def goal_directed_motion(self):
        goal_vec = self.goal_cal_vec(self.goal_pos)[0]
        self.vel_arrange(goal_vec)
        return

    def potential_deviate(self, attract=False):
        # #transform the current command to a directional vector if it is not one
        # cur_command = self.cmd2vec(cur_command)
        # print(self.hand_pos, self.goal_pos)
        #calculate repulsive vector based on hand position and current direction
        hand_vec = self.hand_cal_vec(self.hand_pos, attract)

        #calculate attractive vector based on goal position and current direction
        goal_vec = self.goal_cal_vec(self.goal_pos)

        if np.linalg.norm(hand_vec)>0.3:
            self.collision_avoidance_pub.publish(True)
        else:
            self.collision_avoidance_pub.publish(False)

        deviated_command = self.generate_deviated_command(goal_vec, hand_vec)
        # print(goal_vec, hand_vec)

        self.vel_arrange(deviated_command)
        return
    
    def hand_from_ee_displacement(self, hand_pos):
        current_pos, _ = self.get_pose()
        current_pos[2] -= 0.05 # ! Consider 5 cm below EE which is on the gripper as where the human wants to hold 
        displacement = hand_pos - current_pos
        return displacement

    def hand_cal_vec(self, hand_pos, attract):
        if attract:
            displacement = self.hand_from_ee_displacement(hand_pos)
        else:
            displacement = -self.hand_from_ee_displacement(hand_pos)
        dist = np.linalg.norm(displacement)
        if dist > self.args.distance_upper_bound or dist < self.args.distance_lower_bound:
            hand_vec = np.zeros(3)
            return hand_vec
        r = dist if dist > self.args.min_dist else self.args.min_dist
        unit_hand_vec = (displacement * 1.0) / r
        hand_vec = r*unit_hand_vec
        if attract:
            # * attractive
            hand_vec = hand_vec * self.args.hand_vec_scale * 12.*(2/(1+np.exp(-10*dist))-1)
            # print(self.args.hand_vec_scale * 6.*(2/(1+np.exp(-10*dist))-1))
        else:
            # * repulsive
            hand_vec = hand_vec * (1/(r**2)) * self.args.hand_vec_scale
        return hand_vec

    def goal_cal_vec(self, goal_pos):

        current_pos, _ = self.get_pose()
        displacement = goal_pos - current_pos
        dist = np.linalg.norm(displacement)
        r = dist if dist > self.args.min_dist else self.args.min_dist
        unit_goal_vec = (displacement * 1.0) / r

        # goal_vec = r*unit_goal_vec

        # propotional to 1/r^2
        # goal_vec = goal_vec * (1/(r**2)) * self.args.goal_vec_scale
        # # propotional to 1/r
        # goal_vec = goal_vec * (1/r) * self.args.goal_vec_scale
        # # constant
        # goal_vec = unit_goal_vec * self.args.goal_vec_scale
        goal_vec = unit_goal_vec * self.goal_vec_scale

        #algorithm TBD, can be non-linear
        return goal_vec
    
    def admittance_control(self):
        force_extra = (self.force>self.force_threshold)*(self.force-self.force_threshold)\
            +(self.force<-self.force_threshold)*(self.force+self.force_threshold)   
        force_magnitude = np.linalg.norm(force_extra)
        try:
            (trans_tool_base, rot_tool_base) = self.tf.lookupTransform("/base_link", "/wrist_3_link", rospy.Time(0))
            # print(trans_tool_base)
            force_base = self.qv_mult(rot_tool_base, force_extra)
            """
            # ! See later if we need this
            if force_magnitude > 2e-2:
                pose_offset_vel = self.pose_vel_scale*force_base/force_magnitude
            else:
                pose_offset_vel = np.zeros(3)

            or

            pose_offset_vel = self.pose_vel_scale*force_base/force_magnitude
            """ 
            if force_magnitude > 3:#2e-2:
                pose_offset_vel = self.pose_vel_scale*force_base/force_magnitude
            else:
                pose_offset_vel = np.zeros(3)
            # pose_offset_vel = self.pose_vel_scale*force_base/(force_magnitude+1e-4)
            # pose_offset_vel = self.pose_vel_scale*force_base/force_magnitude
            desired_vel = pose_offset_vel * force_magnitude
            # print(desired_vel)
            self.vel_arrange(desired_vel)
            # return desired_vel
        except (tf.LookupException, tf.ConnectivityException, tf.ExtrapolationException):
            pass
            # print("Error on tf.")
            # return np.zeros(3)
        return


    def generate_deviated_command(self, goal_vec, hand_vec):
        #algorithm TBD, can be non-linear
        devitaed_cmd = goal_vec + hand_vec
        return devitaed_cmd #or a joint space command

    def get_pose(self):
        rospy.wait_for_service("ur_control_wrapper/get_pose")
        get_current_pose = rospy.ServiceProxy("ur_control_wrapper/get_pose", GetPose)
        current_pose = None
        try:
            current_pose = get_current_pose().pose
        except rospy.ServiceException as exc:
            print ("Service did not process request: " + str(exc))
        return np.array([current_pose.position.x, current_pose.position.y, current_pose.position.z]), current_pose

    def get_angle(self):
        rospy.wait_for_service("ur_control_wrapper/get_joints")
        get_current_joints = rospy.ServiceProxy("ur_control_wrapper/get_joints", GetJoints)
        current_joints = None
        try:
            current_joints = get_current_joints().joints
        except rospy.ServiceException as exc:
            print ("Service did not process request: " + str(exc)) 
        return current_joints

    def vel_arrange(self, new_vel):
        last_vel = self.last_vel
        next_vel = (1-self.args.vel_dec_scale) * last_vel + self.args.vel_dec_scale * new_vel
        self.last_vel = next_vel
        self.joint_move(next_vel)


    def joint_move(self, next_vel):
        controller_state = self.check_controller_state(silent=True)
        if controller_state["joint_group_vel_controller"] != "running":
            print("joint_group_vel_controller is not started.")
            self.switch_controller("joint_group")
        # current_pose = self.get_pose()
        # current_pos = np.array([current_pose.position.x, current_pose.position.y, current_pose.position.z])
        current_pos, _ = self.get_pose() 
        pose_diff = next_vel
        magnitude = np.linalg.norm(pose_diff)
        if magnitude < 1e-5:
            self.set_joint_velocity(np.zeros(6))
            # print("The desired pose is already reached.")
            return
        pose_offset_vel = 0.08*pose_diff/magnitude
        desired_next_joint_state = self.compute_inverse_kinematics(pose_offset_vel)
        if len(desired_next_joint_state.position) == 0:
            self.set_joint_velocity(np.zeros(6))
            print("The desired pose cannot be reached.")
            # ! self.switch_controller("scaled_pos") # ! Let's test if this helps 220213
            return
        current_joint_state = self.get_angle()
        # print(velocity) # roughly 0-12
        jv_input = np.array(desired_next_joint_state.position)[:6] - np.array(current_joint_state.position)
        jv_input = jv_input * magnitude
        self.set_joint_velocity(jv_input)

    def qv_mult(self, q1, v1):
        # v1 = tf.transformations.unit_vector(v1)
        q2 = list(v1)
        q2.append(0.0)
        return tf.transformations.quaternion_multiply(
            tf.transformations.quaternion_multiply(q1, q2), 
            tf.transformations.quaternion_conjugate(q1)
        )[:3]

    def compute_inverse_kinematics(self, desired_pose_offset, silent=True):
        # desired_pose_offset = [offset_x, offset_y, offset_z]
        joint_state = None
        _, current_pose = self.get_pose()
        desired_pose = current_pose
        desired_pose.position.x += desired_pose_offset[0]
        desired_pose.position.y += desired_pose_offset[1]
        desired_pose.position.z += desired_pose_offset[2]
        rospy.wait_for_service("ur_control_wrapper/inverse_kinematics")
        compute_ik = rospy.ServiceProxy("ur_control_wrapper/inverse_kinematics", InverseKinematics)
        try:
            req = InverseKinematicsRequest()
            req.pose = desired_pose
            response = compute_ik(req)
            # response = compute_ik(desired_pose)
            solution_found, joint_state = response.solution_found, response.joint_state
            if solution_found:
                if not silent:
                    print("joint state: ")
                    print(joint_state)
            else:
                print("Solution is not found.")
        except rospy.ServiceException as exc:
            print("Service did not process request: " + str(exc))
        return joint_state

    def set_joint_velocity(self, jv_input):
        controller_state = self.check_controller_state(silent=True)
        if controller_state["joint_group_vel_controller"] != "running":
            print("joint_group_vel_controller is not started. Failed to set joint velocity.")
            return
        # pass
        if isinstance(jv_input, str):
            jv_float = [float(entry) for entry in jv_input.split(" ")] # [0., 0., 0., 0., 0., 0.] # 6 entries
        else:
            jv_float = jv_input
        data = Float64MultiArray()
        data.data = jv_float
        self.joint_velocity_pub.publish(data)
        return


    def check_controller_state(self, silent=False):
        rospy.wait_for_service("/controller_manager/list_controllers")
        list_controllers = rospy.ServiceProxy("/controller_manager/list_controllers", ListControllers)
        response = list_controllers()
        controller_state = {}
        for single_controller in response.controller:
            if single_controller.name == "joint_group_vel_controller" \
                or single_controller.name == "scaled_pos_joint_traj_controller":
                controller_state[single_controller.name] = single_controller.state
        if not silent:
            if controller_state["scaled_pos_joint_traj_controller"] == "running":
                print("Current controller is scaled_pos_joint_traj_controller.")
            elif controller_state["joint_group_vel_controller"] == "running":
                print("Current controller is joint_group_vel_controller.")
            else:
                print("Neither scaled_pos_joint_traj_controller nor joint_group_vel_controller is running.")
        return controller_state

    def switch_controller(self, desired_controller):
        # desired_controller = 'joint_group' or 'scaled_pos'
        controller_switched = False
        if desired_controller != 'joint_group' and desired_controller != 'scaled_pos':
            print("Wrong controller input.")
            return controller_switched
        rospy.wait_for_service("controller_manager/switch_controller")
        switch_controller = rospy.ServiceProxy("controller_manager/switch_controller", SwitchController)
        try:
            req = SwitchControllerRequest()
            if desired_controller == 'joint_group':
                req.start_controllers = ['joint_group_vel_controller']
                req.stop_controllers = ['scaled_pos_joint_traj_controller']
            else:
                req.start_controllers = ['scaled_pos_joint_traj_controller']
                req.stop_controllers = ['joint_group_vel_controller']
            req.strictness = 2
            req.start_asap = False
            req.timeout = 0.0
            response = switch_controller(req)
            if response.ok:
                controller_switched = True
                if desired_controller == 'joint_group':
                    print("Controller successfully switched to joint_group_vel_controller.")
                else:
                    print("Controller successfully switched to scaled_pos_joint_traj_controller.")
            else:
                print("Controller switch failed.")
        except rospy.ServiceException as exc:
            print("Service did not process request: " + str(exc))
        return controller_switched

    def set_default_angles(self):
        controller_state = self.check_controller_state(silent=True)
        if controller_state["scaled_pos_joint_traj_controller"] != "running":
            print("joint_group_vel_controller is not started.")
            self.switch_controller("scaled_pos")

        rospy.wait_for_service("ur_control_wrapper/set_joints")
        set_joints = rospy.ServiceProxy("ur_control_wrapper/set_joints", SetJoints)
        joints = JointState()
        #joints.name = ["elbow_joint", "shoulder_lift_joint", "shoulder_pan_joint", "wrist_1_joint", "wrist_2_joint", "wrist_3_joint"]
        #joints.position = [-np.pi / 3.0, -2.0 * np.pi / 3, 0.0, np.pi * 1.0 / 2.0, -np.pi / 2.0, 0.0]
        joints.name = ["shoulder_pan_joint", "shoulder_lift_joint", "elbow_joint", "wrist_1_joint", "wrist_2_joint", "wrist_3_joint"]
        joints.position = [0.0, -2.0 * np.pi / 3, -np.pi / 3.0, -np.pi / 2.0, np.pi / 2.0, 0.0]
        try:
            response = set_joints(joints)
        except rospy.ServiceException as exc:
            print("Service did not process request: " + str(exc))

    def zero_force_torque_sensor(self):
        rospy.wait_for_service("ur_hardware_interface/zero_ftsensor")
        zero_ft_sensor = rospy.ServiceProxy("ur_hardware_interface/zero_ftsensor", Trigger)
        response = zero_ft_sensor()
        if response.success:
            print("Force torque sensor is zeroed.")
        else:
            print("Failed to zero force torque sensor.")
        return response.success


def arg_parse():
    parser = argparse.ArgumentParser()
    parser.add_argument('--distance_lower_bound', default=0.01, type=float)
    parser.add_argument('--distance_upper_bound', default=0.2, type=float)
    parser.add_argument('--distance_velocity_scale', default=0.2, type=float)
    parser.add_argument('--pose_vel_scale', default=0.08, type=float) # * 0.8
    parser.add_argument('--attract', action='store_true')

    #new params
    parser.add_argument('--hand_vec_scale', default=1., type=float)
    parser.add_argument('--goal_vec_scale', default=2., type=float)
    parser.add_argument('--min_dist', default=0.01, type=float)
    parser.add_argument('--vel_dec_scale', default=0.15, type=float) # * 1./15

    parser.add_argument('--freq', default=500, type=int)


    return parser.parse_args()

if __name__ == '__main__':
    try:
        args = arg_parse()
        ccc = CoCoControl(args)
        ccc.reach_default_position()
        while not rospy.is_shutdown():
            if ccc.admittance_control_mode == "on":
                ccc.admittance_control()
            # ccc.coexistence_collaboration_control()
            # ccc.rate.sleep()
    except rospy.ROSInterruptException:
        pass
