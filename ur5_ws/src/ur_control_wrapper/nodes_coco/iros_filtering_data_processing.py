import pathhack
import rosbag
from os.path import join
import numpy as np
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import pickle

# plt.rcParams["font.family"] = "Times New Roman"
plt.rcParams['figure.figsize'] = (10,4)
plt.rc('xtick', labelsize=20)    # fontsize of the tick labels
plt.rc('ytick', labelsize=20)    # fontsize of the tick labels
plt.rc('legend',fontsize=20)

ax_y_lim = (-0.05, 1.05)
y_ticks = [0, 0.5, 1]
# # ax_x_lim = (-5, 105)
# x_ticks = [0,40,80]
x_ticks = range(0,80,5)

bag_path = join(pathhack.pkg_path, 'raw_datasets', '220224-repaired.bag')
bag = rosbag.Bag(bag_path)
# t_prev = 0.
# t_step = 0.
# time_traj = []
# time_step_traj = []
# wrist_traj = []
# ee_traj = []
# annotation_traj = []

# not_interact_annotations = \
#     [[1640559170.90, 1640559183.04],
#      [1640559187.50, 1640559209.05],
#      [1640559213.70, 1640559216.62],
#      [1640559227.18, 1640559257.51]]
# interact_annotations = \
#     [[1640559183.04, 1640559187.50],
#      [1640559209.05, 1640559213.70],
#      [1640559216.62, 1640559227.18],
#      [1640559257.51, 1640559267.61],
#      [1640559267.61, 1640559284.00]]

# annotated_timestamps = [1640559170.90, 1640559183.04, 1640559187.50, 1640559209.05, 1640559213.70, \
#     1640559216.62, 1640559227.18, 1640559257.51, 1640559267.61, 1640559284.00]
# annotated_interactive_intention = 0 # start with 0 and then switch
# annotated_i = 0

low_level_intention_prob_dist = []
times_low_level = []
for topic, msg, t in bag.read_messages(topics=['/hri/human_intention_prob_dist']):
    t_bag = t.secs+1e-9*t.nsecs
    raw_data = np.array(msg.data) # (5,)
    low_level_intention_prob_dist.append(raw_data)
    times_low_level.append(t_bag)
    # print(raw_data)
    # break
times_low_level = np.array(times_low_level)
times_low_level -= times_low_level[0]
low_level_intention_prob_dist = np.stack(low_level_intention_prob_dist,axis=0) # (t, 5)

fig, ax = plt.subplots()
fig.set_tight_layout(True)
low_labels = ["part 1", "part 2", "part 3", "part 4", "preparation area"]
for i in range(5):
    ax.plot(times_low_level, low_level_intention_prob_dist[:,i],c="C"+str(i+2), label=low_labels[i])
ax.set_ylim(ax_y_lim)
ax.legend()
plt.yticks(y_ticks)
plt.xticks(x_ticks)
fig.savefig("low_intention_filter.png")

most_likely_low_intention = np.argmax(low_level_intention_prob_dist, axis=1)
fig, ax = plt.subplots()
fig.set_tight_layout(True)
ax.axis('off')
for i in range(len(times_low_level)-1):
    color = "C"+str(most_likely_low_intention[i]+2)
    ax.plot(times_low_level[i:i+2], [0.5,0.5],c=color,linewidth=30)
fig.savefig("most_likely_low_intention.png")





high_level_intention_prob_dist = []
times_high_level = []
for topic, msg, t in bag.read_messages(topics=['/hri/high_intention_prob_dist']):
    t_bag = t.secs+1e-9*t.nsecs
    raw_data = np.array(msg.data) # (5,)
    high_level_intention_prob_dist.append(raw_data)
    times_high_level.append(t_bag)
    # print(raw_data)
    # break
times_high_level = np.array(times_high_level)
times_high_level -= times_high_level[0]
high_level_intention_prob_dist = np.stack(high_level_intention_prob_dist,axis=0) # (t, 5)




# plt.rcParams["font.family"] = "Times New Roman"
# plt.rcParams['figure.figsize'] = (10,4)
# plt.rc('xtick', labelsize=15)    # fontsize of the tick labels
# plt.rc('ytick', labelsize=15)    # fontsize of the tick labels

ax_y_lim = (-0.05, 1.05)
y_ticks = [0, 0.5, 1]
# ax_x_lim = (-5, 105)
# x_ticks = [0,40,80]
x_ticks = range(0,80,5)

fig, ax = plt.subplots()
fig.set_tight_layout(True)

# high_labels = ["collaboration interactive intention", "coexistence interactive intention"]
high_labels = ["collaboration", "coexistence"]

for i in [1,0]:
    ax.plot(times_high_level, high_level_intention_prob_dist[:,i],c="C"+str(1-i),label=high_labels[i])
# ax.set_aspect('equal',adjustable='box')
ax.set_ylim(ax_y_lim)
plt.yticks(y_ticks)
ax.legend()
# ax.set_xlim(ax_x_lim)
plt.xticks(x_ticks)
fig.savefig("high_intention_filter.png")

most_likely_high_intention = np.argmax(high_level_intention_prob_dist, axis=1)
fig, ax = plt.subplots()
fig.set_tight_layout(True)
ax.axis('off')
for i in range(len(times_high_level)-1):
    color = "C"+str(1-most_likely_high_intention[i])
    ax.plot(times_high_level[i:i+2], [0.5,0.5],c=color,linewidth=30)
fig.savefig("most_likely_high_intention.png")
