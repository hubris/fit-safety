#!/usr/bin/env python
import numpy as np
import rospy
from std_msgs.msg import Float64, Float64MultiArray, Bool
from geometry_msgs.msg import Pose
from ur_control_wrapper.srv import GetPose
from geometry_msgs.msg import WrenchStamped
        
class TrajectoryPlanner:
    def __init__(
        self,
        freq=100,
        vel=2.,
        distance_threshold=1e-2,
        force_z_threshold=50.,
    ):
        # * freq: frequency of checking if goal is reached
        # * vel: the velocity magnitude
        rospy.init_node('trajectory_planner', anonymous=True)
        self.initialize_low_intention_waypoints()
        self.rate = rospy.Rate(freq)
        self.vel = vel
        self.distance_threshold = distance_threshold
        self.force_z_threshold = force_z_threshold
        self.curr_plan_idx = 0 # default rest plan
        self.curr_plan_step = 0 # need to go to the 0th position
        self.force_z = 0. # newton
        self.traj_info_pub = rospy.Publisher('/planned_trajectory_info', Float64MultiArray, queue_size=10)
        self.task_goal_pub = rospy.Publisher('task_goal', Float64MultiArray, queue_size=1)
        self.task_vel_pub = rospy.Publisher('task_vel', Float64, queue_size=1)
        self.collision_avoidance = False
        self.goal_noise = np.zeros(3)
        rospy.Subscriber('/low_intention_command', Float64MultiArray, self.callback_low_intention_command)
        rospy.Subscriber("/wrench", WrenchStamped, self.callback_wrench)
        rospy.Subscriber('/collision_avoidance', Bool, self.callback_collision_avoidance)


    def initialize_low_intention_waypoints(self):
        # plan 1,2,3,4 -> low intention 1,2,3,4
        # plan_0 = [[-0.65, 0.175, 0.50]] # default plan, default robot rest position
        plan_0 = [[-0.57, 0.135, 0.50]] # default plan, default robot rest position
        # plan_1 = [[-0.74158094265, 0.0191916815452, 0.3], [-0.74158094265, 0.0191916815452, 0.17], [-0.74158094265, 0.0191916815452, 0.3]]
        # plan_2 = [[-0.386749305895, 0.0129789230209, 0.3], [-0.386749305895, 0.0129789230209, 0.17], [-0.386749305895, 0.0129789230209, 0.3]]
        # plan_3 = [[-0.392318172364, 0.259958125535, 0.3], [-0.392318172364, 0.259958125535, 0.17], [-0.392318172364, 0.259958125535, 0.3]]
        # plan_4 = [[-0.741527928414, 0.258061148477, 0.3], [-0.741527928414, 0.258061148477, 0.17], [-0.741527928414, 0.258061148477, 0.3]]
        # plan_1 = [[-0.74, 0.02, 0.3], [-0.74, 0.02, 0.17], [-0.74, 0.02, 0.3]]
        # plan_2 = [[-0.40, 0.02, 0.3], [-0.40, 0.02, 0.17], [-0.40, 0.02, 0.3]]
        # plan_3 = [[-0.40, 0.25, 0.3], [-0.40, 0.25, 0.17], [-0.40, 0.25, 0.3]]
        # plan_4 = [[-0.74, 0.25, 0.3], [-0.74, 0.25, 0.17], [-0.74, 0.25, 0.3]]

        # 220218
        # plan_1 = [[-0.74158094265, 0.0191916815452, 0.3], [-0.74158094265, 0.0191916815452, 0.17], [-0.74158094265, 0.0191916815452, 0.3]]
        # plan_2 = [[-0.386749305895, 0.0129789230209, 0.3], [-0.386749305895, 0.0129789230209, 0.17], [-0.386749305895, 0.0129789230209, 0.3]]
        # plan_3 = [[-0.392318172364, 0.259958125535, 0.3], [-0.392318172364, 0.259958125535, 0.17], [-0.392318172364, 0.259958125535, 0.3]]
        # plan_4 = [[-0.741527928414, 0.258061148477, 0.3], [-0.741527928414, 0.258061148477, 0.17], [-0.741527928414, 0.258061148477, 0.3]]

        # 220220
        plan_1 = [[-0.741855183581, 0.0165639596027, 0.3], [-0.741855183581, 0.0165639596027, 0.17], [-0.741855183581, 0.0165639596027, 0.3]]
        plan_2 = [[-0.386256242107, 0.0118721858587, 0.3], [-0.386256242107, 0.0118721858587, 0.17], [-0.386256242107, 0.0118721858587, 0.3]]
        plan_3 = [[-0.390034197481, 0.259512495914, 0.3], [-0.390034197481, 0.259512495914, 0.17], [-0.390034197481, 0.259512495914, 0.3]]
        plan_4 = [[-0.741735032571, 0.258321462196, 0.3], [-0.741735032571, 0.258321462196, 0.17], [-0.741735032571, 0.258321462196, 0.3]]

        # plan_1 = [[-0.741595347966, 0.0188166741001, 0.3], [-0.741595347966, 0.0188166741001, 0.17], [-0.741595347966, 0.0188166741001, 0.3]]
        # plan_2 = [[-0.396880886853, 0.0208367675825, 0.3], [-0.396880886853, 0.0208367675825, 0.17], [-0.396880886853, 0.0208367675825, 0.3]]
        # plan_3 = [[-0.389820968661, 0.249793106665, 0.3], [-0.389820968661, 0.249793106665, 0.17], [-0.389820968661, 0.249793106665, 0.3]]
        # plan_4 = [[-0.738169003388, 0.263045140345, 0.3], [-0.738169003388, 0.263045140345, 0.17], [-0.738169003388, 0.263045140345, 0.3]]
        # intention 1
        # position: 
        # x: -0.741595347966
        # y: 0.0188166741001
        # z: 0.137131506006
        # orientation: 
        # x: 0.687797605953
        # y: -0.025988687052
        # z: -0.725190716135
        # w: 0.0189067877464
        # intention 2
        # position: 
        # x: -0.396880886853
        # y: 0.0208367675825
        # z: 0.129209775555
        # orientation: 
        # x: 0.714678790534
        # y: 0.0232118944284
        # z: -0.696563222501
        # w: 0.0591194669883
        # intention 3
        # ep
        # position: 
        # x: -0.389820968661
        # y: 0.249793106665
        # z: 0.130675708751
        # orientation: 
        # x: -0.694205174924
        # y: 0.00202642951961
        # z: 0.718377085605
        # w: 0.0448266836774
        # intention 4
        # ep
        # position: 
        # x: -0.738169003388
        # y: 0.263045140345
        # z: 0.134673302207
        # orientation: 
        # x: 0.70665322684
        # y: -0.0128041252666
        # z: -0.707443083916
        # w: 0.00124675266062
        # plan_1 = [[-0.80, 0.05, 0.35], [-0.80, 0.05, 0.15], [-0.80, 0.05, 0.35]]
        # plan_2 = [[-0.50, 0.05, 0.35], [-0.50, 0.05, 0.15], [-0.50, 0.05, 0.35]]
        # plan_3 = [[-0.50, 0.30, 0.35], [-0.50, 0.30, 0.15], [-0.50, 0.30, 0.35]]
        # plan_4 = [[-0.80, 0.30, 0.35], [-0.80, 0.30, 0.15], [-0.80, 0.30, 0.35]]
        # plan_1 = [[-0.80, 0.05, 0.35], [-0.80, 0.05, 0.05]]
        # plan_2 = [[-0.50, 0.05, 0.35], [-0.50, 0.05, 0.05]]
        # plan_3 = [[-0.50, 0.30, 0.35], [-0.50, 0.30, 0.05]]
        # plan_4 = [[-0.80, 0.30, 0.35], [-0.80, 0.30, 0.05]]
        self.plans = [plan_0, plan_1, plan_2, plan_3, plan_4]
        return


    def callback_low_intention_command(self, data):
        # * data.data [low_intention_command]
        if self.curr_plan_idx == int(data.data[0]):
            return
        # self.goal_noise = np.random.rand(3)*np.array([1, 1, 0])*0.02
        self.goal_noise = (np.random.rand(3)-0.5)*np.array([2, 2, 0])*0.02 # still 0.02
        self.curr_plan_idx = int(data.data[0])
        self.curr_plan_step = 0 # * 0 means going to the 0th position of the plan
        self.publish_traj_info()
        return
    
    def callback_wrench(self, data):
        self.force_z = data.wrench.force.z
        # print(self.force_z)
        # force direction is reverse in wrench frame

    def callback_collision_avoidance(self, data):
        self.collision_avoidance = data.data

    def publish_traj_info(self):
        traj_info_msg = Float64MultiArray()
        traj_info_msg.data = [self.curr_plan_idx, self.curr_plan_step]
        self.traj_info_pub.publish(traj_info_msg)
        return
    


    def run(self):
        while not rospy.is_shutdown():
            if self.curr_plan_step == len(self.plans[self.curr_plan_idx]):
                # * if reached the last goal position. Stay there?
                goal_msg = Float64MultiArray()
                # goal_msg.data = self.plans[self.curr_plan_idx][-1]
                goal_msg.data = self.plans[self.curr_plan_idx][-1]+self.goal_noise # last goal
                self.task_goal_pub.publish(goal_msg)
                self.task_vel_pub.publish(0.) # have reached last goal, take rest
            else:
                current_pose = self.get_pose()
                current_pos = np.array([current_pose.position.x, current_pose.position.y, current_pose.position.z])
                pos_goal_distance = np.linalg.norm(self.plans[self.curr_plan_idx][self.curr_plan_step]+self.goal_noise - current_pos)
                # print("goal noise: ", self.goal_noise)
                if self.curr_plan_step != 1 and pos_goal_distance < 1e-2:
                    # * If reached goal, but the current step is 0 (above the pushing point)
                    # * let's go to the step 1, which is the pushing step
                    self.curr_plan_step += 1
                    # print("hello 0: ", self.curr_plan_step)
                    self.publish_traj_info()
                if self.curr_plan_idx > 0:
                    if self.curr_plan_step == 1:
                        # * now you are alreay at the pushing step
                        # * compute goal distance again and threshold force for the pushing action
                        pos_goal_distance = np.linalg.norm(self.plans[self.curr_plan_idx][self.curr_plan_step]+self.goal_noise - current_pos)
                        # print(self.force_z)
                        if pos_goal_distance < 1e-2 or self.force_z < -self.force_z_threshold:
                            # * finished the pushing action (reached the last goal)
                            self.curr_plan_step += 1
                            # print("force pause: ", self.curr_plan_step)
                            self.publish_traj_info()
                    

                if self.collision_avoidance and self.curr_plan_step <= 1:
                    # * if during reaching, human avoidance has the effect on joint velocity, then just let
                    # * robot go back to the above pushing goal.
                    # * reset the goal noise when affected by human
                    # self.goal_noise = np.random.rand(3)*np.array([1, 1, 0])*0.02
                    self.goal_noise = (np.random.rand(3)-0.5)*np.array([2, 2, 0])*0.02 # still 0.02
                    self.curr_plan_step = 0
                    # print("hello 2: ", self.curr_plan_step)
                    self.publish_traj_info()
                
                if self.curr_plan_step == len(self.plans[self.curr_plan_idx]):
                    # * if you reached the goal for 1,2,3,4,
                    # * now you should go back to the default position.
                    # print("hello 3:", self.curr_plan_step)
                    if self.curr_plan_step == 1:
                        # * default position already
                        goal_msg = Float64MultiArray()
                        # default position not reaching for parts
                        goal_msg.data = self.plans[self.curr_plan_idx][-1] # last goal
                        # goal_msg.data = self.plans[self.curr_plan_idx][-1]+self.goal_noise # last goal
                        self.task_goal_pub.publish(goal_msg)
                        self.task_vel_pub.publish(0.) # have reached last goal, take rest
                    # else:
                    #     # * shift to default position 
                    #     self.curr_plan_idx = 0
                    #     self.curr_plan_step = 0
                    #     self.publish_traj_info()
                    #     goal_msg = Float64MultiArray()
                    #     goal_msg.data = self.plans[self.curr_plan_idx][-1] # last goal
                    #     self.task_goal_pub.publish(goal_msg)
                    #     self.task_vel_pub.publish(self.vel) # have reached last goal, take rest
                else:
                    # * if you are still in the middle of execution, go to the goal position of the corresponding intention.
                    goal_msg = Float64MultiArray()
                    # goal_msg.data = self.plans[self.curr_plan_idx][self.curr_plan_step]
                    goal_msg.data = self.plans[self.curr_plan_idx][self.curr_plan_step]+self.goal_noise
                    self.task_goal_pub.publish(goal_msg)
                    # if self.curr_plan_step == 1:
                    #     self.task_vel_pub.publish(1.) # pushing
                    # else:
                    #     self.task_vel_pub.publish(3.)
                    if self.curr_plan_step == 1:
                        self.task_vel_pub.publish(1.) # pushing
                    else:
                        # self.task_vel_pub.publish(3.)
                        self.task_vel_pub.publish(2.)
                    # self.task_vel_pub.publish(self.vel)
            self.rate.sleep()

    def get_pose(self):
        rospy.wait_for_service("ur_control_wrapper/get_pose")
        get_current_pose = rospy.ServiceProxy("ur_control_wrapper/get_pose", GetPose)
        current_pose = None
        try:
            current_pose = get_current_pose().pose
        except rospy.ServiceException as exc:
            print("Service did not process request: " + str(exc))
        return current_pose


if __name__ == '__main__':
    try:
        tp = TrajectoryPlanner()
        tp.run()
    except rospy.ROSInterruptException:
        pass