import pathhack
import pickle
import numpy as np
import matplotlib.pyplot as plt
from os.path import join



def compute_path_length(traj_path):
    traj_path_disp = traj_path[1:]-traj_path[:-1]
    return traj_path_disp, np.linalg.norm(traj_path_disp, axis=1).sum()


baseline_name = "repeated"
subject_names = ["xiang", "yeji", "zhe"]

experiment_results = {}
experiment_results[baseline_name] = {}

for subject_name in subject_names:
    experiment_results[baseline_name][subject_name] = {}
    experiment_results[baseline_name][subject_name]['path'] = []
    experiment_results[baseline_name][subject_name]['force'] = []
    experiment_results[baseline_name][subject_name]['energy'] = []
    experiment_results[baseline_name][subject_name]['time'] = []
    for i in range(1,11):
        with open(join(pathhack.pkg_path, 'raw_datasets', baseline_name+'_'+subject_name,\
            str(i)+'.p'), 'rb') as f: # admittance
            data = pickle.load(f, encoding='latin1')
        robot_path = np.stack(data['robot_path'], axis=0) # (traj_len, 3)
        force = np.stack(data['force'], axis=0) # (traj_len, 3)
        admittance_on = np.array(data['admittance_control_mode']) # (traj_len)
        traj_len = len(force)
        assert len(robot_path)==len(force)==len(admittance_on)
        force_magnitudes = np.linalg.norm(force, axis=1)
        robot_path_disp = robot_path[1:]-robot_path[:-1] # (traj_len-1,3)
        robot_path_len = np.linalg.norm(robot_path_disp, axis=1).sum()
        force_average = np.mean(force_magnitudes)
        energy = (robot_path_disp*force[:-1]).sum()
        spent_time = data['end_time'] - data['start_time']
        experiment_results[baseline_name][subject_name]['path'].append(robot_path_len)
        experiment_results[baseline_name][subject_name]['force'].append(force_average)
        experiment_results[baseline_name][subject_name]['energy'].append(energy)
        experiment_results[baseline_name][subject_name]['time'].append(spent_time)
        # print("path length: {0:.2f} m.".format(robot_path_len))
        # print("average force: {0:.2f} N.".format(force_average))
        # print("human effort: {0:.2f} J.".format(energy))
        # print("spent time: {0:.2f} sec.".format(spent_time))

        # print("max force: {0:.2f} N.".format(np.max(force[:,2])))
experiment_result_path = join(pathhack.pkg_path, 'raw_datasets', baseline_name+'_10.p')
with open(experiment_result_path, 'wb') as f:
    pickle.dump(experiment_results, f)
    print(experiment_result_path+" is dumped.")

with open(experiment_result_path, 'rb') as f: # admittance
    data = pickle.load(f, encoding='latin1')