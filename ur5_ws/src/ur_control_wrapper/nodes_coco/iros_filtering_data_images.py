import pathhack
import rosbag
from os.path import join
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from cv_bridge import CvBridge, CvBridgeError
import cv2

# bag_path = join(pathhack.pkg_path, 'high_level_raw', '2021-12-26-16-52-44.bag')
# bag = rosbag.Bag(bag_path)

bag_path = join(pathhack.pkg_path, 'raw_datasets', '220224-repaired.bag')
bag = rosbag.Bag(bag_path)
started = False
time_traj = []
time_step_traj = []
wrist_traj = []
ee_traj = []
bridge = CvBridge()
t_start = 0.
time_checked = 38
for topic, msg, t in bag.read_messages(topics=['/cam_right/color/image_raw']):
    t_bag = t.secs+1e-9*t.nsecs
    if not started:
        t_start = t_bag
        started = True
    
    snap_time= t_bag
    if time_checked-1. < t_bag-t_start < time_checked+1.:
        img = bridge.imgmsg_to_cv2(msg,"bgr8")
        cv2.imwrite(join(pathhack.pkg_path, 'raw_datasets', 'images', '{0:.2f}.png'.format(snap_time-t_start)), img)

bag.close()