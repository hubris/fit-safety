#!/usr/bin/env python
import numpy as np
import rospy
from std_msgs.msg import Float64, Float64MultiArray, Float32MultiArray
from geometry_msgs.msg import Pose
from ur_control_wrapper.srv import GetPose
from collections import deque
from std_srvs.srv import Trigger

class TaskPlanner:
    def __init__(
        self,
        num_low_intentions=4,
        stay_time=1.5,
        intention_prob_upper_threshold=0.8,
        freq=100,
    ):
        rospy.init_node('task_planner', anonymous=True)
        self.num_low_intentions = num_low_intentions
        self.stay_time = stay_time
        self.intention_prob_upper_threshold = intention_prob_upper_threshold
        self.rate = rospy.Rate(freq)
        self.task_set = set(range(1, self.num_low_intentions+1+1)) # intention 1,2,3,4
        self.ongoing_task_queue = deque()
        self.ready_task_queue = deque()
        self.reset_most_likely_intention()
        rospy.Subscriber('/hri/human_intention_prob_dist', Float32MultiArray, self.callback_low_intention_prob)
        rospy.Subscriber('/planned_trajectory_info', Float64MultiArray, self.callback_planned_trajectory_info)
        self.low_intention_command_pub = rospy.Publisher("/low_intention_command", Float64MultiArray, queue_size=1)
        self.curr_plan_idx_from_traj_planner = -999
        self.curr_plan_step_from_traj_planner = -999
        self.zero_force_torque_sensor()

    def reset_most_likely_intention(self):
        self.most_likely_intention = 0 # invalid
        self.most_likely_intention_start_time = -999. # invalid

    def callback_planned_trajectory_info(self, data):
        self.curr_plan_idx_from_traj_planner, self.curr_plan_step_from_traj_planner = data.data

    def callback_low_intention_prob(self, data):
        # data.data: list of probabilities
        assert len(data.data)==1+self.num_low_intentions
        curr_most_likely_intention =  np.argmax(data.data)+1
        # if curr_most_likely_intention == 5:
        #     return
        if data.data[curr_most_likely_intention-1] > self.intention_prob_upper_threshold:
            if self.most_likely_intention != curr_most_likely_intention:
                if curr_most_likely_intention in self.task_set:
                    self.most_likely_intention = curr_most_likely_intention
                    self.most_likely_intention_start_time = rospy.get_time()
            else:
                if rospy.get_time()-self.most_likely_intention_start_time>self.stay_time:
                    if curr_most_likely_intention != 5:
                        print(self.most_likely_intention)
                        self.task_set.remove(self.most_likely_intention)
                        self.ongoing_task_queue.append(self.most_likely_intention)
                        self.reset_most_likely_intention()
        if self.ongoing_task_queue and data.data[self.ongoing_task_queue[0]-1] < 1./self.num_low_intentions:
            # 0.25 # * After human left, the task is then ready for robot to execute
            self.ready_task_queue.append(self.ongoing_task_queue.popleft())

    # ! PAUSE HERE will have sth to do with while loop with ready_task_queue. finish traj_info_pub. Send low_intention_command
    
    def run(self):
        while not rospy.is_shutdown():
            if not self.ready_task_queue:
                # no ready task, just go to default position
                low_intention_command = Float64MultiArray()
                low_intention_command.data = [0]
                self.low_intention_command_pub.publish(low_intention_command)
                continue
            low_intention_command = Float64MultiArray()
            low_intention_command.data = [self.ready_task_queue[0]]
            self.low_intention_command_pub.publish(low_intention_command)
            if self.ready_task_queue[0] == self.curr_plan_idx_from_traj_planner and self.curr_plan_step_from_traj_planner == 3:
                self.ready_task_queue.popleft()
            self.rate.sleep()

    def zero_force_torque_sensor(self):
        rospy.wait_for_service("ur_hardware_interface/zero_ftsensor")
        zero_ft_sensor = rospy.ServiceProxy("ur_hardware_interface/zero_ftsensor", Trigger)
        response = zero_ft_sensor()
        if response.success:
            print("Force torque sensor is zeroed.")
        else:
            print("Failed to zero force torque sensor.")
        return response.success

if __name__ == '__main__':
    try:
        tp = TaskPlanner()
        tp.run()
    except rospy.ROSInterruptException:
        pass
