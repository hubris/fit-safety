#!/usr/bin/env python
import rospy
from std_msgs.msg import String

def talker():
    pub = rospy.Publisher('/iros_data_clock', String, queue_size=10)
    rospy.init_node('talker_iros_data_clock', anonymous=True)
    rate = rospy.Rate(100) # 30hz
    while not rospy.is_shutdown():
        str_msg = 'Collect data.'
        pub.publish(str_msg)
        rate.sleep()

if __name__ == '__main__':
    try:
        talker()
    except rospy.ROSInterruptException:
        pass