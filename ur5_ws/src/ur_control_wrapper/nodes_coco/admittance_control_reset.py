#!/usr/bin/env python
import rospy
from std_msgs.msg import String

class AdmittanceControlReset:
    def __init__(self):
        rospy.init_node('admittance_control_reset')
        self.pub = rospy.Publisher("/admittance_control_reset", String, queue_size=1)
    
    def publish_reset(self):
        self.pub.publish("reset")

    def run(self):
        while not rospy.is_shutdown():
            command_input = raw_input("\
                \n=====================================================\
                \nReset admittance control for next round: r\
                \n=====================================================\
                \nCommand: ") # raw_input() for python 2, input() for python 3
            if command_input == 'r':
                self.publish_reset()
            else:
                print("Th command ["+command_input+"] is invalid.")

if __name__ == '__main__':
    try:
        acr = AdmittanceControlReset()
        acr.run()
    except rospy.ROSInterruptException:
        pass

