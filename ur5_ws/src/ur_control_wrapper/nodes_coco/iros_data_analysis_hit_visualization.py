import pathhack
import pickle
import numpy as np
import matplotlib.pyplot as plt
from os.path import join



def compute_path_length(traj_path):
    traj_path_disp = traj_path[1:]-traj_path[:-1]
    return traj_path_disp, np.linalg.norm(traj_path_disp, axis=1).sum()

# with open(join(pathhack.pkg_path, 'raw_datasets', 'zhe',\
#      '33.p'), 'rb') as f: # admittance
#     data = pickle.load(f, encoding='latin1')

with open(join(pathhack.pkg_path, 'raw_datasets', 'hit_zhe_2',\
     '8.p'), 'rb') as f: # admittance
    data = pickle.load(f, encoding='latin1')

robot_path = np.stack(data['robot_path'], axis=0) # (traj_len, 3)
force = np.stack(data['force'], axis=0) # (traj_len, 3)
admittance_on = np.array(data['admittance_control_mode']) # (traj_len)
traj_len = len(force)
assert len(robot_path)==len(force)
# assert len(robot_path)==len(force)==len(admittance_on)

force_cleaned = force * admittance_on[:,np.newaxis]

force_magnitudes_cleaned = np.linalg.norm(force_cleaned, axis=1)

force_magnitudes = np.linalg.norm(force, axis=1)

robot_path_disp = robot_path[1:]-robot_path[:-1] # (traj_len-1,3)


robot_path_len = np.linalg.norm(robot_path_disp, axis=1).sum()
force_average = np.mean(force_magnitudes_cleaned)
energy = (robot_path_disp*force[:-1]).sum()
spent_time = data['end_time'] - data['start_time']


print("path length: {0:.2f} m.".format(robot_path_len))
print("average force: {0:.2f} N.".format(force_average))
print("human effort: {0:.2f} J.".format(energy))
print("spent time: {0:.2f} sec.".format(spent_time))

print("max force: {0:.2f} N.".format(np.max(force_magnitudes)))

colors = ['C0', 'C1']
colors_bar = []
for admittance_on_tt in admittance_on:
    colors_bar.append(colors[int(admittance_on_tt)])

admittance_edges = [0]
for i in range(1, len(admittance_on)-1):
    if admittance_on[i] != admittance_on[i+1]:
        admittance_edges.append(i+1)
admittance_edges.append(len(admittance_on))


fig, ax = plt.subplots()
ax.plot(np.arange(traj_len)/100, admittance_on)
fig.savefig("iros_visualization_admittance_on.png")


fig, ax = plt.subplots()
xx = np.arange(traj_len)/100
for i in range(len(admittance_edges)-1):
    li, ri = admittance_edges[i], admittance_edges[i+1]
    color = colors[int(admittance_on[li])]
    if color == 'C1':
        ax.plot(xx[li-1:ri+1], force_magnitudes[li-1:ri+1], c=color)
    else:
        ax.plot(xx[li:ri+1], force_magnitudes[li:ri+1], c=color)

# ax.plot(np.arange(traj_len)/100, force_magnitudes,c=colors_bar)
# ax.scatter(np.arange(traj_len)/100, force_magnitudes,c=colors_bar)
fig.savefig("iros_visualization_force_hit_2.png")
# print("average force: ", np.mean(force_magnitudes))



import matplotlib
matplotlib.use('Agg')
from mpl_toolkits.mplot3d import Axes3D

fig = plt.figure()
ax = Axes3D(fig)
ax.axis('auto')
# ax.axis('equal')
# ax.scatter(robot_path[:,0],robot_path[:,1],robot_path[:,2],'C1') # plot the point (2,3,4) on the figure
# plt.show()
ax.scatter(robot_path[:,0],robot_path[:,1],robot_path[:,2],c=colors_bar)
fig.savefig("iros_visualization_robot_path_hit_2.png")
# print("robot trajectory length: ", compute_path_length(robot_path))


# fig = plt.figure()
# ax = Axes3D(fig)
# ax.axis('auto')
# # ax.axis('equal')
# ax.scatter(data['human_path'][:,0],data['human_path'][:,1],data['human_path'][:,2],'C0') # plot the point (2,3,4) on the figure
# # plt.show()
# fig.savefig("visualization_human_path.png")
# print("human trajectory length: ", compute_path_length(data['human_path']))


