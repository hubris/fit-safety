#!/usr/bin/env python
import argparse
import rospy
import numpy as np

from std_msgs.msg import Float32MultiArray, Float64MultiArray, Bool, Float64
from controller_manager_msgs.srv import SwitchController, SwitchControllerRequest, ListControllers
from ur_control_wrapper.srv import GetPose, GetJoints, InverseKinematics, InverseKinematicsRequest
from geometry_msgs.msg import WrenchStamped
from ur_control_wrapper.srv import ExecutePause
from ur_control_wrapper.srv import SetJoints
from std_srvs.srv import Trigger
from sensor_msgs.msg import JointState

class PotentialFieldStatic:
    def __init__(self, args, tracking_topic_name, goal_pos_topic_name):
        self.args = args
        rospy.init_node('potential_field_static', anonymous=True)
        self.set_default_angles()
        self.zero_force_torque_sensor()
        rospy.Subscriber(tracking_topic_name, Float32MultiArray, self.tracking_callback)
        rospy.Subscriber(goal_pos_topic_name, Float64MultiArray, self.goal_pos_callback)
        rospy.Subscriber('task_vel', Float64, self.goal_vel_callback)
        controller_state = self.check_controller_state(silent=True)
        self.joint_velocity_pub = rospy.Publisher('/joint_group_vel_controller/command', Float64MultiArray, queue_size=10)
        self.collision_avoidance_pub = rospy.Publisher('/collision_avoidance', Bool, queue_size=10)
        rospy.Subscriber("/wrench", WrenchStamped, self.wrench_callback)
        
        if controller_state["joint_group_vel_controller"] != "running":
            self.switch_controller("joint_group")
        self.last_vel = np.zeros(3)
        self.last_mag = 0
        self.hand_pos = np.zeros(3)
        self.goal_pos = np.zeros(3)
        self.goal_vec_scale = 0.
        self.force_z = 0.
        self.urgent_force_stop = False

    def tracking_callback(self, data):
        self.hand_pos = np.array(data.data)

    def goal_pos_callback(self, data):
        self.goal_pos = np.array(data.data)
    
    def goal_vel_callback(self, data):
        self.goal_vec_scale = data.data
    
    def wrench_callback(self, data):
        self.force_z = data.wrench.force.z
        # print(self.force_z)
        # force direction is reverse in wrench frame 
        if self.force_z < -50:
            if not self.urgent_force_stop:
                self.set_joint_velocity([0,0,0,0,0,0])
                # print("pause")
                self.urgent_force_stop = True
            # next_vel = np.array([0.,0.,2.])
            # self.last_vel = next_vel
            # self.joint_move(next_vel)
            # print("Go up to avoid safety protective stop")
        else:
            self.urgent_force_stop=False
    
    def potential_deviate(self, attract = False):

        # #transform the current command to a directional vector if it is not one
        # cur_command = self.cmd2vec(cur_command)
        # print(self.hand_pos, self.goal_pos)
        
        #calculate repulsive vector based on hand position and current direction
        hand_vec = self.hand_cal_vec(self.hand_pos, attract)

        #calculate attractive vector based on goal position and current direction
        goal_vec = self.goal_cal_vec(self.goal_pos)
        # print(self.goal_pos)
        if np.linalg.norm(hand_vec)>0.3:
            self.collision_avoidance_pub.publish(True)
        else:
            self.collision_avoidance_pub.publish(False)

        deviated_command = self.generate_deviated_command(goal_vec, hand_vec)
        
        # if self.force_z < -20:
        #     pass
        #     # goal_vec array([-0.8910798 ,  0.62194966,  1.67903407])
        #     # print("hello")
        #     # next_vel = np.array([0.,0.,2.])
        #     # self.last_vel = next_vel
        #     # self.joint_move(next_vel)
        #     # self.vel_arrange(np.array([0.,0.,10.]))
        #     # self.vel_arrange(self.generate_deviated_command(np.array([0.,0.,10.]), hand_vec))
        # else:
        #     self.vel_arrange(deviated_command)
        self.vel_arrange(deviated_command)

        return

    def hand_cal_vec(self, hand_pos, attract):
        current_pos, _ = self.get_pose()
        displacement = (hand_pos - current_pos) * (2*attract - 1)
        dist = np.linalg.norm(displacement)
        # if dist > self.args.distance_upper_bound or dist < self.args.distance_lower_bound:
        #     hand_vec = np.zeros(3)
        #     return hand_vec
        if dist < self.args.distance_lower_bound:
            hand_vec = np.zeros(3)
            return hand_vec
        if abs(displacement[0])<0.15 and abs(displacement[1])<0.15 and abs(displacement[2])<0.3:
            r = dist if dist > self.args.min_dist else self.args.min_dist
            unit_hand_vec = (displacement * 1.0) / r
            hand_vec = r*unit_hand_vec
            # propotional to 1/r^2
            hand_vec = hand_vec * (1/(r**2)) * self.args.hand_vec_scale
            return hand_vec
        else:
            hand_vec = np.zeros(3)
            return hand_vec

    def goal_cal_vec(self, goal_pos):

        current_pos, _ = self.get_pose()
        displacement = goal_pos - current_pos
        dist = np.linalg.norm(displacement)
        r = dist if dist > self.args.min_dist else self.args.min_dist
        unit_goal_vec = (displacement * 1.0) / r

        # goal_vec = r*unit_goal_vec

        # propotional to 1/r^2
        # goal_vec = goal_vec * (1/(r**2)) * self.args.goal_vec_scale
        # # propotional to 1/r
        # goal_vec = goal_vec * (1/r) * self.args.goal_vec_scale
        # # constant
        # goal_vec = unit_goal_vec * self.args.goal_vec_scale
        goal_vec = unit_goal_vec * self.goal_vec_scale

        #algorithm TBD, can be non-linear
        return goal_vec

    def generate_deviated_command(self, goal_vec, hand_vec):
        #algorithm TBD, can be non-linear
        devitaed_cmd = goal_vec + hand_vec
        return devitaed_cmd #or a joint space command

    def get_pose(self):
        rospy.wait_for_service("ur_control_wrapper/get_pose")
        get_current_pose = rospy.ServiceProxy("ur_control_wrapper/get_pose", GetPose)
        current_pose = None
        try:
            current_pose = get_current_pose().pose
        except rospy.ServiceException as exc:
            print ("Service did not process request: " + str(exc))
        return np.array([current_pose.position.x, current_pose.position.y, current_pose.position.z]), current_pose

    def get_angle(self):
        rospy.wait_for_service("ur_control_wrapper/get_joints")
        get_current_joints = rospy.ServiceProxy("ur_control_wrapper/get_joints", GetJoints)
        current_joints = None
        try:
            current_joints = get_current_joints().joints
        except rospy.ServiceException as exc:
            print ("Service did not process request: " + str(exc)) 
        return current_joints

    def check_controller_state(self, silent=False):
        rospy.wait_for_service("/controller_manager/list_controllers")
        list_controllers = rospy.ServiceProxy("/controller_manager/list_controllers", ListControllers)
        response = list_controllers()
        controller_state = {}
        for single_controller in response.controller:
            if single_controller.name == "joint_group_vel_controller" \
                or single_controller.name == "scaled_pos_joint_traj_controller":
                controller_state[single_controller.name] = single_controller.state
        if not silent:
            if controller_state["scaled_pos_joint_traj_controller"] == "running":
                print("Current controller is scaled_pos_joint_traj_controller.")
            elif controller_state["joint_group_vel_controller"] == "running":
                print("Current controller is joint_group_vel_controller.")
            else:
                print("Neither scaled_pos_joint_traj_controller nor joint_group_vel_controller is running.")
        return controller_state

    def switch_controller(self, desired_controller):
        # desired_controller = 'joint_group' or 'scaled_pos'
        controller_switched = False
        if desired_controller != 'joint_group' and desired_controller != 'scaled_pos':
            print("Wrong controller input.")
            return controller_switched
        rospy.wait_for_service("controller_manager/switch_controller")
        switch_controller = rospy.ServiceProxy("controller_manager/switch_controller", SwitchController)
        try:
            req = SwitchControllerRequest()
            if desired_controller == 'joint_group':
                req.start_controllers = ['joint_group_vel_controller']
                req.stop_controllers = ['scaled_pos_joint_traj_controller']
            else:
                req.start_controllers = ['scaled_pos_joint_traj_controller']
                req.stop_controllers = ['joint_group_vel_controller']
            req.strictness = 2
            req.start_asap = False
            req.timeout = 0.0
            response = switch_controller(req)
            if response.ok:
                controller_switched = True
                if desired_controller == 'joint_group':
                    print("Controller successfully switched to joint_group_vel_controller.")
                else:
                    print("Controller successfully switched to scaled_pos_joint_traj_controller.")
            else:
                print("Controller switch failed.")
        except rospy.ServiceException as exc:
            print ("Service did not process request: " + str(exc))
        return controller_switched

    def compute_inverse_kinematics(self, desired_pose_offset, silent=True):
        # desired_pose_offset = [offset_x, offset_y, offset_z]
        joint_state = None
        _, current_pose = self.get_pose()
        desired_pose = current_pose
        desired_pose.position.x += desired_pose_offset[0]
        desired_pose.position.y += desired_pose_offset[1]
        desired_pose.position.z += desired_pose_offset[2]
        rospy.wait_for_service("ur_control_wrapper/inverse_kinematics")
        compute_ik = rospy.ServiceProxy("ur_control_wrapper/inverse_kinematics", InverseKinematics)
        try:
            req = InverseKinematicsRequest()
            req.pose = desired_pose
            response = compute_ik(req)
            # response = compute_ik(desired_pose)
            solution_found, joint_state = response.solution_found, response.joint_state
            if solution_found:
                if not silent:
                    print("joint state: ")
                    print(joint_state)
            else:
                print("Solution is not found.")
        except rospy.ServiceException as exc:
            print ("Service did not process request: " + str(exc))
        return joint_state

    def set_joint_velocity(self, jv_input):
        controller_state = self.check_controller_state(silent=True)
        if controller_state["joint_group_vel_controller"] != "running":
            print("joint_group_vel_controller is not started. Failed to set joint velocity.")
            return
        # pass
        if isinstance(jv_input, str):
            jv_float = [float(entry) for entry in jv_input.split(" ")] # [0., 0., 0., 0., 0., 0.] # 6 entries
        else:
            jv_float = jv_input
        data = Float64MultiArray()
        data.data = jv_float
        self.joint_velocity_pub.publish(data)
        return


    def vel_arrange(self, new_vel):
        last_vel = self.last_vel
        next_vel = (1-self.args.vel_dec_scale) * last_vel + self.args.vel_dec_scale * new_vel
        self.last_vel = next_vel
        self.joint_move(next_vel)


    def joint_move(self, next_vel):
        controller_state = self.check_controller_state(silent=True)
        if controller_state["joint_group_vel_controller"] != "running":
            print("joint_group_vel_controller is not started.")
            self.switch_controller("joint_group")
        # current_pose = self.get_pose()
        # current_pos = np.array([current_pose.position.x, current_pose.position.y, current_pose.position.z])
        current_pos, _ = self.get_pose() 
        pose_diff = next_vel
        magnitude = np.linalg.norm(pose_diff)
        if magnitude < 1e-5:
            self.set_joint_velocity(np.zeros(6))
            # print("The desired pose is already reached.")
            # ! Let's test if this helps
            return
        pose_offset_vel = 0.08*pose_diff/magnitude
        # print(pose_offset_vel)
        desired_next_joint_state = self.compute_inverse_kinematics(pose_offset_vel)
        if len(desired_next_joint_state.position) == 0:
            self.set_joint_velocity(np.zeros(6))
            print("The desired pose cannot be reached.")
            return
        current_joint_state = self.get_angle()
        # print(velocity) # roughly 0-12
        jv_input = np.array(desired_next_joint_state.position)[:6] - np.array(current_joint_state.position)
        jv_input = jv_input * magnitude
        self.set_joint_velocity(jv_input)

    def set_default_angles(self):
        controller_state = self.check_controller_state(silent=True)
        if controller_state["scaled_pos_joint_traj_controller"] != "running":
            print("joint_group_vel_controller is not started.")
            self.switch_controller("scaled_pos")

        rospy.wait_for_service("ur_control_wrapper/set_joints")
        set_joints = rospy.ServiceProxy("ur_control_wrapper/set_joints", SetJoints)
        joints = JointState()
        #joints.name = ["elbow_joint", "shoulder_lift_joint", "shoulder_pan_joint", "wrist_1_joint", "wrist_2_joint", "wrist_3_joint"]
        #joints.position = [-np.pi / 3.0, -2.0 * np.pi / 3, 0.0, np.pi * 1.0 / 2.0, -np.pi / 2.0, 0.0]
        joints.name = ["shoulder_pan_joint", "shoulder_lift_joint", "elbow_joint", "wrist_1_joint", "wrist_2_joint", "wrist_3_joint"]
        joints.position = [0.0, -2.0 * np.pi / 3, -np.pi / 3.0, -np.pi / 2.0, np.pi / 2.0, 0.0]
        try:
            response = set_joints(joints)
        except rospy.ServiceException as exc:
            print("Service did not process request: " + str(exc))
    
    def zero_force_torque_sensor(self):
        rospy.wait_for_service("ur_hardware_interface/zero_ftsensor")
        zero_ft_sensor = rospy.ServiceProxy("ur_hardware_interface/zero_ftsensor", Trigger)
        response = zero_ft_sensor()
        if response.success:
            print("Force torque sensor is zeroed.")
        else:
            print("Failed to zero force torque sensor.")
        return response.success


def arg_parse():
    parser = argparse.ArgumentParser()
    parser.add_argument('--distance_lower_bound', default=0.01, type=float)
    parser.add_argument('--distance_upper_bound', default=0.2, type=float)
    parser.add_argument('--distance_velocity_scale', default=0.2, type=float)
    parser.add_argument('--pose_vel_scale', default=0.08, type=float)
    parser.add_argument('--attract', action='store_true')

    #new params
    parser.add_argument('--hand_vec_scale', default=1., type=float)
    parser.add_argument('--goal_vec_scale', default=2., type=float)
    parser.add_argument('--min_dist', default=0.01, type=float)
    parser.add_argument('--vel_dec_scale', default=0.15, type=float)

    return parser.parse_known_args()

if __name__ == '__main__':
    try:
        args, unknown = arg_parse()
        # tracking_topic_name = "hri/human_wrist_pos_in_base"
        tracking_topic_name = "hri/human_skeleton_position_tracking_3D_floatarray"    
        goal_pos_topic_name = "task_goal"
        pfs = PotentialFieldStatic(args, tracking_topic_name, goal_pos_topic_name)
        while(1):
            pfs.potential_deviate()
        rospy.spin()
    except rospy.ROSInterruptException:
        pass
