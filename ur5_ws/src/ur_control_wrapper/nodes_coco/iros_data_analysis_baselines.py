import pathhack
import pickle
import numpy as np
import matplotlib.pyplot as plt
from os.path import join

print("admittance")
baseline_name = "admittance"
subject_names = ["ninghan", "xiang", "yeji", "yiqing", "zhe"]
metrics = ['path', 'force', 'energy', 'time']

experiment_result_path = join(pathhack.pkg_path, 'raw_datasets', baseline_name+'_10.p')
# with open(experiment_result_path, 'wb') as f:
#     pickle.dump(experiment_results, f)
#     print(experiment_result_path+" is dumped.")

with open(experiment_result_path, 'rb') as f: # admittance
    experiment_results = pickle.load(f, encoding='latin1')
    
for subject_name in subject_names:
    for metric in metrics:
        result_mean = np.mean(experiment_results[baseline_name][subject_name][metric])
        result_std = np.std(experiment_results[baseline_name][subject_name][metric])
        # print(subject_name+" "+metric+": {0:.2f} ± {1:.2f}".format(result_mean, result_std))
        print(subject_name+" "+metric+": {0:.2f} {1:.2f}".format(result_mean, result_std))


print()
# print()

# baseline_name = "repeated"
# subject_names = ["xiang", "yeji", "zhe"]
# metrics = ['path', 'force', 'energy', 'time']

# experiment_result_path = join(pathhack.pkg_path, 'raw_datasets', baseline_name+'_10.p')
# # with open(experiment_result_path, 'wb') as f:
# #     pickle.dump(experiment_results, f)
# #     print(experiment_result_path+" is dumped.")

# with open(experiment_result_path, 'rb') as f: # admittance
#     experiment_results = pickle.load(f, encoding='latin1')
    
# for subject_name in subject_names:
#     for metric in metrics:
#         result_mean = np.mean(experiment_results[baseline_name][subject_name][metric])
#         result_std = np.std(experiment_results[baseline_name][subject_name][metric])
#         # print(subject_name+" "+metric+": {0:.2f} ± {1:.2f}".format(result_mean, result_std))
#         print(subject_name+" "+metric+": {0:.2f} {1:.2f}".format(result_mean, result_std))



print("hit_2")
baseline_name = "hit_2"
subject_names = ["ninghan", "xiang", "zhe", "yiqing", "yeji"]
metrics = ['path', 'force', 'energy', 'time']

experiment_result_path = join(pathhack.pkg_path, 'raw_datasets', baseline_name+'_10.p')
# with open(experiment_result_path, 'wb') as f:
#     pickle.dump(experiment_results, f)
#     print(experiment_result_path+" is dumped.")

with open(experiment_result_path, 'rb') as f: # admittance
    experiment_results = pickle.load(f, encoding='latin1')
    
for subject_name in subject_names:
    for metric in metrics:
        result_mean = np.mean(experiment_results[baseline_name][subject_name][metric])
        result_std = np.std(experiment_results[baseline_name][subject_name][metric])
        # print(subject_name+" "+metric+": {0:.2f} ± {1:.2f}".format(result_mean, result_std))
        print(subject_name+" "+metric+": {0:.2f} {1:.2f}".format(result_mean, result_std))


# print()
# print("random")
# baseline_name = "random"
# subject_names = ["yeji", "xiang", "zhe", "yiqing", "ninghan"]
# metrics = ['path', 'force', 'energy', 'time']

# experiment_result_path = join(pathhack.pkg_path, 'raw_datasets', baseline_name+'_10.p')
# # with open(experiment_result_path, 'wb') as f:
# #     pickle.dump(experiment_results, f)
# #     print(experiment_result_path+" is dumped.")

# with open(experiment_result_path, 'rb') as f: # admittance
#     experiment_results = pickle.load(f, encoding='latin1')
    
# for subject_name in subject_names:
#     for metric in metrics:
#         result_mean = np.mean(experiment_results[baseline_name][subject_name][metric])
#         result_std = np.std(experiment_results[baseline_name][subject_name][metric])
#         # print(subject_name+" "+metric+": {0:.2f} ± {1:.2f}".format(result_mean, result_std))
#         print(subject_name+" "+metric+": {0:.2f} {1:.2f}".format(result_mean, result_std))


# print()