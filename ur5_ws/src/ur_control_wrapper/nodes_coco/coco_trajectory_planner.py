#!/usr/bin/env python
import numpy as np
import rospy
from std_msgs.msg import Float64, Float64MultiArray, Bool
from geometry_msgs.msg import Pose
from ur_control_wrapper.srv import GetPose
from geometry_msgs.msg import WrenchStamped
        
class TrajectoryPlanner:
    def __init__(
        self,
        freq=100,
        vel=2.,
        distance_threshold=1e-2,
        force_z_threshold=50.,
    ):
        # * freq: frequency of checking if goal is reached
        # * vel: the velocity magnitude
        rospy.init_node('trajectory_planner', anonymous=True)
        self.initialize_low_intention_waypoints()
        self.rate = rospy.Rate(freq)
        self.vel = vel
        self.distance_threshold = distance_threshold
        self.force_z_threshold = force_z_threshold
        self.curr_plan_idx = 0 # default rest plan
        self.curr_plan_step = 0 # need to go to the 0th position
        self.force_z = 0. # newton
        self.traj_info_pub = rospy.Publisher('/planned_trajectory_info', Float64MultiArray, queue_size=10)
        self.task_goal_pub = rospy.Publisher('task_goal', Float64MultiArray, queue_size=1)
        self.task_vel_pub = rospy.Publisher('task_vel', Float64, queue_size=1)
        self.collision_avoidance = False
        self.goal_noise = np.zeros(3)
        rospy.Subscriber('/low_intention_command', Float64MultiArray, self.callback_low_intention_command)
        rospy.Subscriber("/wrench", WrenchStamped, self.callback_wrench)
        rospy.Subscriber('/collision_avoidance', Bool, self.callback_collision_avoidance)
        rospy.Subscriber('/collaboration_goal', Float64MultiArray, self.callback_collaboration_goal)


    def initialize_low_intention_waypoints(self):
        # plan 1,2,3,4 -> low intention 1,2,3,4
        # plan_0 = [[-0.65, 0.175, 0.50]] # default plan, default robot rest position
        # plan_0 = [[-0.57, 0.135, 0.50]] # default plan, default robot rest position
        # plan_1 = [[-0.74, 0.02, 0.3], [-0.74, 0.02, 0.17], [-0.74, 0.02, 0.3]]
        # plan_2 = [[-0.40, 0.02, 0.3], [-0.40, 0.02, 0.17], [-0.40, 0.02, 0.3]]
        # plan_3 = [[-0.40, 0.25, 0.3], [-0.40, 0.25, 0.17], [-0.40, 0.25, 0.3]]
        # plan_4 = [[-0.74, 0.25, 0.3], [-0.74, 0.25, 0.17], [-0.74, 0.25, 0.3]]
        # 220220
        plan_0 = [[-0.57, 0.135, 0.50]] # default plan, default robot rest position
        plan_1 = [[-0.741855183581, 0.0165639596027, 0.3], [-0.741855183581, 0.0165639596027, 0.17], [-0.741855183581, 0.0165639596027, 0.3]]
        plan_2 = [[-0.386256242107, 0.0118721858587, 0.3], [-0.386256242107, 0.0118721858587, 0.17], [-0.386256242107, 0.0118721858587, 0.3]]
        plan_3 = [[-0.390034197481, 0.259512495914, 0.3], [-0.390034197481, 0.259512495914, 0.17], [-0.390034197481, 0.259512495914, 0.3]]
        plan_4 = [[-0.741735032571, 0.258321462196, 0.3], [-0.741735032571, 0.258321462196, 0.17], [-0.741735032571, 0.258321462196, 0.3]]
        # ! We may want to change plan 1,2,3,4 positions to be more specific
        plan_5 = [[-0.57, 0.135, 0.3], [-0.57, 0.135, 0.17], [-0.57, 0.135, 0.3]]
        # * x,y of plan 5 same as default position, will be used by collaboration goal.
        self.plans = [plan_0, plan_1, plan_2, plan_3, plan_4, plan_5]
        self.plans = [np.array(plan_i) for plan_i in self.plans]
        return


    def callback_low_intention_command(self, data):
        # * data.data [low_intention_command]
        if self.curr_plan_idx == int(data.data[0]):
            return
        self.goal_noise = np.random.rand(3)*np.array([1, 1, 0])*0.02
        self.curr_plan_idx = int(data.data[0])
        self.curr_plan_step = 0 # * 0 means going to the 0th position of the plan
        self.publish_traj_info()
        return
    
    def callback_wrench(self, data):
        self.force_z = data.wrench.force.z
        # force direction is reverse in wrench frame

    def callback_collision_avoidance(self, data):
        self.collision_avoidance = data.data
    
    def callback_collaboration_goal(self, data):
        # After admittance control the position will be goal position
        gx, gy, gz = data.data
        # self.plans[5][:,:2] = gx, gy
        self.plans[5][0] = gx, gy, gz
        self.plans[5][:,:2] = gx, gy
        print(self.plans[5])

    def publish_traj_info(self):
        traj_info_msg = Float64MultiArray()
        traj_info_msg.data = [self.curr_plan_idx, self.curr_plan_step]
        self.traj_info_pub.publish(traj_info_msg)
        return

    def run(self):
        while not rospy.is_shutdown():
            if self.curr_plan_step == len(self.plans[self.curr_plan_idx]):
                # * if reached the last goal position. Stay there?
                goal_msg = Float64MultiArray()
                if self.curr_plan_idx == 5:
                    goal_msg.data = self.plans[self.curr_plan_idx][-1] # last goal
                else:
                    goal_msg.data = self.plans[self.curr_plan_idx][-1]+self.goal_noise # last goal
                self.task_goal_pub.publish(goal_msg)
                self.task_vel_pub.publish(0.) # have reached last goal, take rest
            else:
                current_pose = self.get_pose()
                current_pos = np.array([current_pose.position.x, current_pose.position.y, current_pose.position.z])

                if self.curr_plan_idx == 5:
                    pos_goal_distance = np.linalg.norm(self.plans[self.curr_plan_idx][self.curr_plan_step] - current_pos)
                else:
                    pos_goal_distance = np.linalg.norm(self.plans[self.curr_plan_idx][self.curr_plan_step]+self.goal_noise-current_pos)
    
                if self.curr_plan_step != 1 and pos_goal_distance < 1e-2:
                    # * If reached goal, but the current step is 0 (above the pushing point)
                    # * let's go to the step 1, which is the pushing step
                    self.curr_plan_step += 1
                    print("hello 0: ", self.curr_plan_step, self.curr_plan_idx)
                    self.publish_traj_info()
                if self.curr_plan_idx > 0:
                    if self.curr_plan_step == 1:
                        # * now you are alreay at the pushing step
                        # * compute goal distance again and threshold force for the pushing action
                        if self.curr_plan_idx == 5:
                            pos_goal_distance = np.linalg.norm(self.plans[self.curr_plan_idx][self.curr_plan_step] - current_pos)
                        else:
                            pos_goal_distance = np.linalg.norm(self.plans[self.curr_plan_idx][self.curr_plan_step]+self.goal_noise-current_pos)
                        # print(self.force_z)
                        if pos_goal_distance < 1e-2 or self.force_z < -self.force_z_threshold:
                            # * finished the pushing action (reached the last goal)
                            self.curr_plan_step += 1
                            print("hello 1: ", self.curr_plan_step, self.curr_plan_idx)
                            self.publish_traj_info()
                    

                if self.collision_avoidance and self.curr_plan_step <= 1:
                    # * if during reaching, human avoidance has the effect on joint velocity, then just let
                    # * robot go back to the above pushing goal.
                    self.curr_plan_step = 0
                    print("hello 2: ", self.curr_plan_step, self.curr_plan_idx)
                    self.publish_traj_info()
                
                if self.curr_plan_step == len(self.plans[self.curr_plan_idx]):
                    # * if you reached the goal for 1,2,3,4,
                    # * now you should go back to the default position.
                    print("hello 3:", self.curr_plan_step)
                    if self.curr_plan_step == 1:
                        # * default position already
                        goal_msg = Float64MultiArray()
                        # default position not reaching for parts
                        goal_msg.data = self.plans[self.curr_plan_idx][-1] # last goal
                        # if self.curr_plan_idx == 5:
                        #     goal_msg.data = self.plans[self.curr_plan_idx][-1] # last goal
                        # else:
                        #     goal_msg.data = self.plans[self.curr_plan_idx][-1]+self.goal_noise # last goal
                        self.task_goal_pub.publish(goal_msg)
                        self.task_vel_pub.publish(0.) # have reached last goal, take rest
                else:
                    # * if you are still in the middle of execution, go to the goal position of the corresponding intention.
                    goal_msg = Float64MultiArray()
                    if self.curr_plan_idx == 5:
                        goal_msg.data = self.plans[self.curr_plan_idx][self.curr_plan_step] # last goal
                    else:
                        goal_msg.data = self.plans[self.curr_plan_idx][self.curr_plan_step]+self.goal_noise # last goal
                    self.task_goal_pub.publish(goal_msg)
                    if self.curr_plan_step == 1:
                        self.task_vel_pub.publish(1.) # pushing
                    else:
                        self.task_vel_pub.publish(2.)
            self.rate.sleep()

    def get_pose(self):
        rospy.wait_for_service("ur_control_wrapper/get_pose")
        get_current_pose = rospy.ServiceProxy("ur_control_wrapper/get_pose", GetPose)
        current_pose = None
        try:
            current_pose = get_current_pose().pose
        except rospy.ServiceException as exc:
            print("Service did not process request: " + str(exc))
        return current_pose


if __name__ == '__main__':
    try:
        tp = TrajectoryPlanner()
        tp.run()
    except rospy.ROSInterruptException:
        pass