#!/usr/bin/env python
import rospy
import numpy as np
from std_msgs.msg import String, Bool
from sensor_msgs.msg import JointState, Image, CameraInfo
from geometry_msgs.msg import Pose, Point, Quaternion, Vector3
from ur_control_wrapper.srv import SetPose
from ur_control_wrapper.srv import GetPose
from ur_control_wrapper.srv import GetJoints
from ur_control_wrapper.srv import SetJoints
from ur_control_wrapper.srv import SetTrajectory
from ur_control_wrapper.srv import GetCartesianPlan
from ur_control_wrapper.srv import ExecuteCartesianPlan
from ur_control_wrapper.srv import AddObject, AddObjectRequest
from ur_control_wrapper.srv import AttachObject, AttachObjectRequest
from ur_control_wrapper.srv import DetachObject, DetachObjectRequest
from ur_control_wrapper.srv import RemoveObject, RemoveObjectRequest
from gripper_controller_jenny import GripperHande as gripper
from status_listener import StatusListener
from cv_bridge import CvBridge, CvBridgeError
import cv2
import time

from tf import transformations as tfs

class Demo:
    def __init__(self):
        self.bridge = CvBridge()
        self.free_drive_pub = rospy.Publisher("ur_control_wrapper/enable_freedrive", Bool, queue_size=10)
        self.gripper_pub = rospy.Publisher("ur_control_wrapper/gripper", String, queue_size=10)
        self.connect_pub = rospy.Publisher("ur_control_wrapper/connect", Bool, queue_size=10)
        self.status = StatusListener()
        self.gripper_position_status = self.status.gripper_status.gPO
        self.img = (np.zeros((480,640)))
        self.cam_image_sub = rospy.Subscriber('/pylon_camera_node/image_raw', Image, self.image_callback)
        # self.cam_info = rospy.Subscriber('/camera/color/camera_info', CameraInfo, self.image_callback)
        self.filename = './data/{}.txt'.format(time.time())
        self.default_joints = [0.0, -2.0 * np.pi / 3, -np.pi / 3.0, np.pi / 2.0, -np.pi / 2.0, 0.0]
        self.modele_pickup_joints = [0.08789491653442383, -2.3390451870360316, -1.4212942123413086, 2.1864258485981445, -1.5647161642657679, 0.09175634384155273]
        self.modele_ready_insert_joints =  [-0.15918237367738897, -2.37111820797109, -1.2513322830200195, 2.0525213915058593, -1.5655434767352503, -0.15237123170961553]
        # model e little-alien
        # end-effector position:
        # ep
        # position: 
        # x: -0.526026059961
        # y: 0.0896204497105
        # z: 0.142106602092
        # orientation: 
        # x: 0.00282814224489
        # y: 0.706146735972
        # z: 0.00542508097825
        # w: 0.708039093118
        # ja
        # position: [0.08789491653442383, -2.3390451870360316, -1.4212942123413086, 2.1864258485981445, -1.5647161642657679, 0.09175634384155273]
        
        # ready to insert joint angles
        # position: [-0.15918237367738897, -2.37111820797109, -1.2513322830200195, 2.0525213915058593, -1.5655434767352503, -0.15237123170961553]
        # ready to insert ep
        # position: 
        #   x: -0.525869435639
        #   y: 0.221474786868
        #   z: 0.178277420549
        # orientation: 
        #   x: 0.00386980093748
        #   y: 0.706571065929
        #   z: 0.0039170641081
        #   w: 0.707620668184
        self.nancy_demo_joints = [90.*np.pi/180., -90.*np.pi/180., 0.*np.pi/180., 180.*np.pi/180., -90.*np.pi/180., 0.*np.pi/180.]

    def get_pose(self):
        rospy.wait_for_service("ur_control_wrapper/get_pose")
        get_current_pose = rospy.ServiceProxy("ur_control_wrapper/get_pose", GetPose)
        current_pose = None
        try:
            current_pose = get_current_pose().pose
        except rospy.ServiceException as exc:
            print "Service did not process request: " + str(exc) 

        return current_pose
    
    def image_callback(self,img):
        try:
            self.img = self.bridge.imgmsg_to_cv2(img,"bgr8")
        except CvBridgeError as e:
            print("CvBridge could not convert images from realsense to opencv")

    # def camera_info_callback(self,msg):
    #     K = msg.K
    #     K = np.asarray(K).astype(float).reshape((3,3))
    #     self.K = np.linalg.inv(K)
    #     self.D = msg.D
    
    def image_snapshot(self):
        snap_time=time.time()
        # img = cv2.undistort(self.img, self.K, self.D)
        cv2.imwrite('./data/{}.png'.format(snap_time),self.img)
        position = [snap_time, self.get_pose().position.x, self.get_pose().position.y, self.get_pose().position.z,
                    self.get_pose().orientation.x, self.get_pose().orientation.y, self.get_pose().orientation.z, self.get_pose().orientation.w]+list(self.get_angle().position)
        with open(self.filename, 'a') as file:
            file.write(str(position)+'\n')
        
    def get_angle(self):
        rospy.wait_for_service("ur_control_wrapper/get_joints")
        get_current_joints = rospy.ServiceProxy("ur_control_wrapper/get_joints", GetJoints)
        current_joints = None
        try:
            current_joints = get_current_joints().joints
        except rospy.ServiceException as exc:
            print "Service did not process request: " + str(exc) 
        return current_joints
    
    def set_default_angles(self):
        rospy.wait_for_service("ur_control_wrapper/set_joints")
        set_joints = rospy.ServiceProxy("ur_control_wrapper/set_joints", SetJoints)
        joints = JointState()
        #joints.name = ["elbow_joint", "shoulder_lift_joint", "shoulder_pan_joint", "wrist_1_joint", "wrist_2_joint", "wrist_3_joint"]
        #joints.position = [-np.pi / 3.0, -2.0 * np.pi / 3, 0.0, np.pi * 1.0 / 2.0, -np.pi / 2.0, 0.0]
        joints.name = ["shoulder_pan_joint", "shoulder_lift_joint", "elbow_joint", "wrist_1_joint", "wrist_2_joint", "wrist_3_joint"]
        # joints.position = [0.023975849151611328, -2.173887868920797, -1.0581588745117188, 1.6445128160664062, -1.570674244557516, -2.2236500875294496e-05]
        joints.position = self.default_joints
        # joints.position = [0.02065563201904297, -1.4051645559123536, -1.2984609603881836, -0.5594208997539063, 1.641685962677002, 1.5597729682922363]
        try:
            response = set_joints(joints)
        except rospy.ServiceException as exc:
            print "Service did not process request: " + str(exc)  
    
    def set_nancy_angles(self):
        rospy.wait_for_service("ur_control_wrapper/set_joints")
        set_joints = rospy.ServiceProxy("ur_control_wrapper/set_joints", SetJoints)
        joints = JointState()
        joints.name = ["shoulder_pan_joint", "shoulder_lift_joint", "elbow_joint", "wrist_1_joint", "wrist_2_joint", "wrist_3_joint"]
        joints.position = self.nancy_demo_joints
        try:
            response = set_joints(joints)
        except rospy.ServiceException as exc:
            print "Service did not process request: " + str(exc)
    
    def set_modele_pickup_angles(self):
        rospy.wait_for_service("ur_control_wrapper/set_joints")
        set_joints = rospy.ServiceProxy("ur_control_wrapper/set_joints", SetJoints)
        joints = JointState()
        joints.name = ["shoulder_pan_joint", "shoulder_lift_joint", "elbow_joint", "wrist_1_joint", "wrist_2_joint", "wrist_3_joint"]
        joints.position = self.modele_pickup_joints
        try:
            response = set_joints(joints)
        except rospy.ServiceException as exc:
            print "Service did not process request: " + str(exc)
    
    def set_modele_ready_insert_angles(self):
        rospy.wait_for_service("ur_control_wrapper/set_joints")
        set_joints = rospy.ServiceProxy("ur_control_wrapper/set_joints", SetJoints)
        joints = JointState()
        joints.name = ["shoulder_pan_joint", "shoulder_lift_joint", "elbow_joint", "wrist_1_joint", "wrist_2_joint", "wrist_3_joint"]
        joints.position = self.modele_ready_insert_joints
        try:
            response = set_joints(joints)
        except rospy.ServiceException as exc:
            print "Service did not process request: " + str(exc)

    def set_align_angles(self):
        rospy.wait_for_service("ur_control_wrapper/set_joints")
        set_joints = rospy.ServiceProxy("ur_control_wrapper/set_joints", SetJoints)
        joints = JointState()
        joints.name = ["shoulder_pan_joint", "shoulder_lift_joint", "elbow_joint", "wrist_1_joint", "wrist_2_joint", "wrist_3_joint"]
        # joints.position = [0.055490970611572266, -2.9393607578673304, -0.7415943145751953, 2.0078815656849365, -0.04255134264101201, 1.6604723930358887]
        joints.position = [0.08219289779663086, -2.5706149540343226, -1.4981203079223633, 2.3872238832661132, -0.13479930559267217, 1.6408886909484863]
        try:
            response = set_joints(joints)
        except rospy.ServiceException as exc:
            print "Service did not process request: " + str(exc)

        joints.position = [0.08204889297485352, -2.588429113427633, -1.4929723739624023, 2.4056917864033203, -0.13541013399233037, 1.6348819732666016]
        
        try:
            response = set_joints(joints)
        except rospy.ServiceException as exc:
            print "Service did not process request: " + str(exc)

    def set_align_pick_angles(self):
        rospy.wait_for_service("ur_control_wrapper/set_joints")
        set_joints = rospy.ServiceProxy("ur_control_wrapper/set_joints", SetJoints)
        joints = JointState()
        joints.name = ["shoulder_pan_joint", "shoulder_lift_joint", "elbow_joint", "wrist_1_joint", "wrist_2_joint", "wrist_3_joint"]
        # joints.position = [-0.3096254507647913, -2.428889890710348, -1.0674715042114258, 1.9440323549458007, -1.5609400908099573, -0.4603341261493128]
        joints.position = [0.08197736740112305, -2.4068905315794886, -1.5188608169555664, 2.264228029842041, -0.13685924211610967, 1.6210031509399414]

        try:
            response = set_joints(joints)
        except rospy.ServiceException as exc:
            print "Service did not process request: " + str(exc)
        
        # Pre-pick
        joints.position = [-0.31004554430116826, -2.351328512231344, -0.9451770782470703, 1.77212040006604, -1.4952290693866175, -0.4409573713885706]
        try:
            response = set_joints(joints)
        except rospy.ServiceException as exc:
            print "Service did not process request: " + str(exc) 

        joints.position = [-0.3480761686908167, -2.461543699304098, -1.0304231643676758, 1.9592820841022949, -1.5363157431231897, -0.44428855577577764]
        try:
            response = set_joints(joints)
        except rospy.ServiceException as exc:
            print "Service did not process request: " + str(exc)    

        self.gripper_position_status = 233
        self.gripper_pub.publish('%f' % (self.gripper_position_status))

        rospy.sleep(2.5)

        self.set_default_angles()

        joints.position = [0.0009512901306152344, -2.3369862041869105, -1.467341423034668, 2.2005294996448974, -0.09877425829042608, 0.0332942008972168]
        try:
            response = set_joints(joints)
        except rospy.ServiceException as exc:
            print "Service did not process request: " + str(exc)  

        rospy.sleep(5)

        joints.position = [0.3011198043823242, -2.168755193749899, -1.4578790664672852, 2.0548733907886962, -1.5664904753314417, 0.30431556701660156]
        try:
            response = set_joints(joints)
        except rospy.ServiceException as exc:
            print "Service did not process request: " + str(exc)  

        self.gripper_position_status = 0
        self.gripper_pub.publish('%f' % (self.gripper_position_status))

    def assembly_angles(self):
        rospy.wait_for_service("ur_control_wrapper/set_joints")
        set_joints = rospy.ServiceProxy("ur_control_wrapper/set_joints", SetJoints)
        joints = JointState()
        joints.name = ["shoulder_pan_joint", "shoulder_lift_joint", "elbow_joint", "wrist_1_joint", "wrist_2_joint", "wrist_3_joint"]
        # joints.position = [-0.4385269323932093, -2.390142103234762, -1.150099754333496, 1.964780493373535, -1.5514214674579065, -0.4336350599872034]
        # joints.position = [-0.44341737428774053, -2.391255041162008, -1.1480522155761719, 1.9659308630177001, -1.5607121626483362, -0.4363797346698206]
        # Pre-assembly
        joints.position = [-0.46100122133363897, -2.294448514977926, -1.3070974349975586, 2.0990418630787353, -1.577531639729635, -0.5878313223468226]
        try:
            response = set_joints(joints)
        except rospy.ServiceException as exc:
            print "Service did not process request: " + str(exc) 

        # Assembly
        joints.position = [-0.459937874470846, -2.3062015972533167, -1.3138914108276367, 2.118670626277588, -1.5756376425372522, -0.5885861555682581]
        try:
            response = set_joints(joints)
        except rospy.ServiceException as exc:
            print "Service did not process request: " + str(exc)      

    def execute_joints(self, joints_wp):
        """
            Execuring a wp in joints angle format
        """
        rospy.wait_for_service("ur_control_wrapper/set_joints")
        set_joints = rospy.ServiceProxy("ur_control_wrapper/set_joints", SetJoints)
        joints = JointState()
        joints.name = ["shoulder_pan_joint", "shoulder_lift_joint", "elbow_joint", "wrist_1_joint", "wrist_2_joint", "wrist_3_joint"]
        joints.position = joints_wp
        try:
            response = set_joints(joints)
        except rospy.ServiceException as exc:
            print "Service did not process request: " + str(exc) 

    def set_joints(self, direction):
        amount = float(direction[2:])
        joint = direction[:2]
        current_joints = self.get_angle()
        rospy.wait_for_service("ur_control_wrapper/set_joints")
        set_joints = rospy.ServiceProxy("ur_control_wrapper/set_joints", SetJoints)
        joints = JointState()
        joints.name = current_joints.name
        joint_angle = list(current_joints.position)

        try:
            if joint == "1+":
                joint_angle[0] += np.deg2rad(amount)
                print("Current joint 1 angle: %f degrees" % (joint_angle[0]))
            elif joint == "1-":
                joint_angle[0] -= np.deg2rad(amount)
                print("Current joint 1 angle: %f degrees" % (joint_angle[0]))
            elif joint == "2+":
                joint_angle[1] += np.deg2rad(amount)
                print("Current joint 2 angle: %f degrees" % (joint_angle[1]))
            elif joint == "2-":
                joint_angle[1] -= np.deg2rad(amount)
                print("Current joint 2 angle: %f degrees" % (joint_angle[1]))
            elif joint == "3+":
                joint_angle[2] += np.deg2rad(amount)
                print("Current joint 3 angle: %f degrees" % (joint_angle[2]))
            elif joint == "3-":
                joint_angle[2] -= np.deg2rad(amount)
                print("Current joint 3 angle: %f degrees" % (joint_angle[2]))
            elif joint == "4+":
                joint_angle[3] += np.deg2rad(amount)
                print("Current joint 4 angle: %f degrees" % (joint_angle[3]))
            elif joint == "4-":
                joint_angle[3] -= np.deg2rad(amount)
                print("Current joint 4 angle: %f degrees" % (joint_angle[3]))
            elif joint == "5+":
                joint_angle[4] += np.deg2rad(amount)
                print("Current joint 5 angle: %f degrees" % (joint_angle[4]))
            elif joint == "5-":
                joint_angle[4] -= np.deg2rad(amount)
                print("Current joint 5 angle: %f degrees" % (joint_angle[4]))
            elif joint == "6+":
                joint_angle[5] += np.deg2rad(amount)
                print("Current joint 6 angle: %f degrees" % (joint_angle[5]))
            elif joint == "6-":
                joint_angle[5] -= np.deg2rad(amount)
                print("Current joint 6 angle: %f degrees" % (joint_angle[5]))
            joints.position = tuple(joint_angle)
            response = set_joints(joints)
        except rospy.ServiceException as exc:
            print "Service did not process request: " + str(exc)

    def control_gripper(self, command):
        # Adjust gripper position: gp+/-
        # Adjust gripper force: gf+/-
        # Adjust gripper speed: gs+/-
        # Open gripper: gop 
        # Close gripper: gcl
        # Activate gripper: gac
        # Reset gripper: grs
        # Note: gp+ closes the gripper (max: 255), gp- opens the gripper (min: 0)

        if len(command) > 3:
            amount = float(command[3:])
        direction = command[:3]

        if direction == "gp+":
            self.gripper_position_status += amount
            if self.gripper_position_status > 255:
                self.gripper_position_status = 255
            elif self.gripper_position_status < 0:
                self.gripper_position_status = 0
            self.gripper_pub.publish('%f' % (self.gripper_position_status))
            print("Current gripper position: %f" % (self.gripper_position_status))
        elif direction == "gp-":
            self.gripper_position_status -= amount
            if self.gripper_position_status > 255:
                self.gripper_position_status = 255
            elif self.gripper_position_status < 0:
                self.gripper_position_status = 0
            self.gripper_pub.publish('%f' % (self.gripper_position_status))
            print("Current gripper position: %f" % (self.gripper_position_status))
        elif direction == "gf+":
            self.gripper_pub.publish('gi')
        elif direction == "gf-":
            self.gripper_pub.publish('gd') 
        elif direction == "gs+":
            self.gripper_pub.publish('gf')
        elif direction == "gs-":
            self.gripper_pub.publish('gl')
        elif direction == "gop":
            self.gripper_pub.publish('go')
        elif direction == "gcl":
            self.gripper_pub.publish('gc')
        elif direction == "gac":
            self.gripper_pub.publish('ga')
        elif direction == "grs":
            self.gripper_pub.publish('gr') 
        elif direction == "see":
            print("Current gripper position: %f" % (self.gripper_position_status))

    def move_arm(self, command):
        amount = float(command[2:])
        direction = command[:2]
        current_pose = self.get_pose()
        current_euler = np.rad2deg(tfs.euler_from_quaternion(np.array([current_pose.orientation.x, 
                        current_pose.orientation.y, current_pose.orientation.z, current_pose.orientation.w])))
        rospy.wait_for_service("ur_control_wrapper/set_pose")
        set_current_pose = rospy.ServiceProxy("ur_control_wrapper/set_pose", SetPose)
        try:
            if direction == "x+":
                current_pose.position.x += (amount/100) # convert to centimeters
                print("Current x position: %f" % (current_pose.position.x))
            elif direction == "x-":
                current_pose.position.x -= (amount/100) # convert to centimeters
                print("Current x position: %f" % (current_pose.position.x))
            elif direction == "y+":
                current_pose.position.y += (amount/100) # convert to centimeters
                print("Current y position: %f" % (current_pose.position.y))
            elif direction == "y-":
                current_pose.position.y -= (amount/100) # convert to centimeters
                print("Current y position: %f" % (current_pose.position.y))
            elif direction == "z+":
                current_pose.position.z += (amount/100) # convert to centimeters
                print("Current z position: %f" % (current_pose.position.z))
            elif direction == "z-":
                current_pose.position.z -= (amount/100) # convert to centimeters
                print("Current z position: %f" % (current_pose.position.z))

            elif direction == "r+":
                current_euler[0] += amount
                print("Current end effector roll: %f" % (current_euler[0]))
            elif direction == "r-":
                current_euler[0] -= amount
                print("Current end effector roll: %f" % (current_euler[0]))
            elif direction == "p+":
                current_euler[1] += amount
                print("Current end effector pitch: %f" % (current_euler[1]))
            elif direction == "p-":
                current_euler[1] -= amount
                print("Current end effector pitch: %f" % (current_euler[1]))
            elif direction == "w+":
                current_euler[2] += amount
                print("Current end effector yaw: %f" % (current_euler[2]))
            elif direction == "w-":
                current_euler[2] -= amount
                print("Current end effector yaw: %f" % (current_euler[2]))

            ori = np.deg2rad(current_euler)
            quat = tfs.quaternion_from_euler(ori[0], ori[1], ori[2])
            current_pose.orientation.x = quat[0]
            current_pose.orientation.y = quat[1]
            current_pose.orientation.z = quat[2]
            current_pose.orientation.w = quat[3]

            response = set_current_pose(current_pose)
            pose = response.response_pose
            is_reached = response.is_reached
        except rospy.ServiceException as exc:
            print "Service did not process request: " + str(exc)
    
    def waypoint_following(self):
        current_pose = self.get_pose()

        rospy.wait_for_service("ur_control_wrapper/follow_trajectory")
        set_trajectory = rospy.ServiceProxy("ur_control_wrapper/follow_trajectory", SetTrajectory)

        waypoints = []
        delta = [[-0.1, 0.0, 0.0], [-0.1, 0.0, -0.1], [-0.1, 0.0, 0.0], [0.0, 0.0, 0.0]]
        for wp in delta:
            wpi = Pose()
            wpi.position.x = current_pose.position.x + wp[0]
            wpi.position.y = current_pose.position.y + wp[1]
            wpi.position.z = current_pose.position.z + wp[2]
            wpi.orientation.x = current_pose.orientation.x
            wpi.orientation.y = current_pose.orientation.y
            wpi.orientation.z = current_pose.orientation.z
            wpi.orientation.w = current_pose.orientation.w
            waypoints.append(wpi)
        try:
            response = set_trajectory(waypoints)
            final_pose = response.final_pose
            is_reached = response.is_reached
        except ROSInterruptException as exc:
            print "Service did not process request: " + str(exc)

    def test_desired_ee_wp(self, input):
        """
            This function trys to received the desired parts pose and compute the desired ee pose as the waypoint
            for controller waypoint following.
        """
        user_input = input.split(",")
        partsname = user_input[0]
        orientation = user_input[1]
        lg = 0.16
        # gripper pose relative to parts origin
        parts_c2o = np.array([4.25, (1.33+1.5), -4.5])*0.001
        parts_c2o = np.array([0.0, 0.0, 0.0])*0.001
        parts_pose = {'main_shell': {'top':[parts_c2o[0], lg + parts_c2o[1], parts_c2o[2], np.pi, 0.0, -np.pi/2.0], 
                                     'bottom':[parts_c2o[0], -lg +parts_c2o[1], parts_c2o[2], np.pi, 0.0, np.pi/2.0],
                                     'left':[parts_c2o[0], parts_c2o[1], lg + parts_c2o[2], 0.0, np.pi/2.0, 0.0]},
                      'top_shell': {'top':[parts_c2o[0], lg + parts_c2o[1], parts_c2o[2], 0.0, 0.0, -np.pi/2.0]}}
                      
        rospy.wait_for_service("ur_control_wrapper/follow_trajectory")
        set_trajectory = rospy.ServiceProxy("ur_control_wrapper/follow_trajectory", SetTrajectory)

        tfBuffer = tf2_ros.Buffer()
        listener = tf2_ros.TransformListener(tfBuffer)
        
        try:
            trans = tfBuffer.lookup_transform("world", partsname, rospy.Time(), rospy.Duration(1.0))
        except (tf2_ros.LookupException, tf2_ros.ConnectivityException, tf2_ros.ExtrapolationException):
            print("exception!! when getting parts transform")
            return
        print(trans)
        parts_p = np.array([trans.transform.translation.x, trans.transform.translation.y, trans.transform.translation.z])
        trans_ee2parts = np.array(parts_pose[partsname][orientation][:3])
        M_p2w = tfs.quaternion_matrix(np.array([trans.transform.rotation.x, 
                        trans.transform.rotation.y, trans.transform.rotation.z, trans.transform.rotation.w]))
        euler = parts_pose[partsname][orientation][3:6]
        M_e2p = tfs.euler_matrix(euler[0], euler[1], euler[2], 'sxyz')
        M_w2p = M_p2w.T
        M_p2e = M_e2p.T
        ee_p = parts_p + np.dot(M_p2w[:3,:3],trans_ee2parts)
        ee_r = np.dot(M_p2e[:3,:3], M_w2p[:3,:3])
        M_ee = np.identity(4)
        M_ee[:3,:3] = ee_r.T
        ee_quat = tfs.quaternion_from_matrix(M_ee)

        waypoints = []
        wpi = Pose()
        wpi.position.x = ee_p[0]
        wpi.position.y = ee_p[1]
        wpi.position.z = ee_p[2]

        wpi.orientation.x = ee_quat[0]
        wpi.orientation.y = ee_quat[1]
        wpi.orientation.z = ee_quat[2]
        wpi.orientation.w = ee_quat[3]
        waypoints.append(wpi)
        print(wpi.position)
        print(np.rad2deg(tfs.euler_from_quaternion(ee_quat)))

        try:
            response = set_trajectory(waypoints)
            final_pose = response.final_pose
            is_reached = response.is_reached
        except ROSInterruptException as exc:
            print "Service did not process request: " + str(exc)  

        grip_num = "227"
        if orientation == "left":
            grip_num = "234"
        self.gripper_pub.publish(grip_num)
        rospy.sleep(2.5)

        current_pose = self.get_pose()
        waypoints2 = []
        delta = [0, 0, 0.2]
        wpi2 = Pose()
        wpi2.position.x = current_pose.position.x + delta[0]
        wpi2.position.y = current_pose.position.y + delta[1]
        wpi2.position.z = current_pose.position.z + delta[2]
        wpi2.orientation.x = current_pose.orientation.x
        wpi2.orientation.y = current_pose.orientation.y
        wpi2.orientation.z = current_pose.orientation.z
        wpi2.orientation.w = current_pose.orientation.w
        waypoints2.append(wpi2)
        waypoints2.append(current_pose)

        try:
            response = set_trajectory(waypoints2)
            final_pose = response.final_pose
            is_reached = response.is_reached
        except ROSInterruptException as exc:
            print "Service did not process request: " + str(exc)
        self.gripper_pub.publish("go")
        rospy.sleep(2.5)
        self.set_default_angles()

    def pick_and_place(self):
        """
            This function trys to implement the picking and place task repeatedly with a simple vertical movement.
            Gripper motion is also involved into the loop.
        """
        current_pose = self.get_pose()

        rospy.wait_for_service("ur_control_wrapper/get_cartesian_plan")
        get_cartesian_plan = rospy.ServiceProxy("ur_control_wrapper/get_cartesian_plan", GetCartesianPlan)

        rospy.wait_for_service("ur_control_wrapper/execute_cartesian_plan")
        execute_cartesian_plan = rospy.ServiceProxy("ur_control_wrapper/execute_cartesian_plan", ExecuteCartesianPlan)            
        
        waypoints = []
        waypoints.append(current_pose)

        delta = [0, 0.1, -0.2]
        wpi = Pose()
        # wpi.position.x = current_pose.position.x + delta[0]
        # wpi.position.y = current_pose.position.y + delta[1]
        # wpi.position.z = current_pose.position.z + delta[2]
        # wpi.orientation.x = current_pose.orientation.x
        # wpi.orientation.y = current_pose.orientation.y
        # wpi.orientation.z = current_pose.orientation.z
        # wpi.orientation.w = current_pose.orientation.w

        wpi.position.x = -0.61851904044
        wpi.position.y = 0.169756620366
        wpi.position.z = 0.111796361428
        wpi.orientation.x = -0.000450528897233
        wpi.orientation.y = 0.696428380162
        wpi.orientation.z = 0.0177322790189
        wpi.orientation.w = 0.717407049456
        waypoints.append(wpi)

        next_path = [waypoints[1]]
        try:
            response = get_cartesian_plan(next_path)
            plan1 = response.plan
            is_solved = response.is_solved
        except ROSInterruptException as exc:
            print "Service did not process request: " + str(exc)

        try:
            response = execute_cartesian_plan(plan1)
            final_pose = response.final_pose
            is_reached = response.is_reached
        except ROSInterruptException as exc:
            print "Service did not process request: " + str(exc)   
        
        self.gripper_pub.publish("227")
        rospy.sleep(2.5)

        next_path = waypoints
        try:
            response = get_cartesian_plan(next_path)
            plan2 = response.plan
            is_solved = response.is_solved
        except ROSInterruptException as exc:
            print "Service did not process request: " + str(exc)

        for iter in range(3):
            try:
                response = execute_cartesian_plan(plan2)
                final_pose = response.final_pose
                is_reached = response.is_reached
            except ROSInterruptException as exc:
                print "Service did not process request: " + str(exc)
            
            if iter % 2 == 0:
                self.gripper_pub.publish("150")
            else:
                self.gripper_pub.publish("227")
            rospy.sleep(1.3)

        next_path = [waypoints[0]]
        try:
            response = get_cartesian_plan(next_path)
            plan3 = response.plan
            is_solved = response.is_solved
        except ROSInterruptException as exc:
            print "Service did not process request: " + str(exc)

        try:
            response = execute_cartesian_plan(plan3)
            final_pose = response.final_pose
            is_reached = response.is_reached
        except ROSInterruptException as exc:
            print "Service did not process request: " + str(exc)  
        self.gripper_pub.publish("go")

    def snapshot_waypoints(self):

        rospy.wait_for_service("ur_control_wrapper/get_cartesian_plan")
        get_cartesian_plan = rospy.ServiceProxy("ur_control_wrapper/get_cartesian_plan", GetCartesianPlan)

        rospy.wait_for_service("ur_control_wrapper/execute_cartesian_plan")
        execute_cartesian_plan = rospy.ServiceProxy("ur_control_wrapper/execute_cartesian_plan", ExecuteCartesianPlan)  

        def path(waypoint):
            # Generate plan
            try:
                response = get_cartesian_plan(waypoint)
                plan1 = response.plan
                is_solved = response.is_solved
            except ROSInterruptException as exc:
                print "Service did not process request: " + str(exc)

            # Execute plan
            try:
                response = execute_cartesian_plan(plan1)
                final_pose = response.final_pose
                is_reached = response.is_reached
                # rospy.sleep(0.5)
                self.image_snapshot()
            except ROSInterruptException as exc:
                print "Service did not process request: " + str(exc) 

        current_pose = self.get_pose()

        r = 0.1
        num_points = 75
        delta_theta = 2*np.pi/num_points
        n = np.arange(75)
        xlist = current_pose.position.x + r*np.cos(delta_theta*n)
        ylist = current_pose.position.y + r*np.sin(delta_theta*n)
        for i in range(num_points):
            wpi = Pose()
            wpi.position.x = xlist[i]
            wpi.position.y = ylist[i]
            wpi.position.z = current_pose.position.z
            wpi.orientation.x = current_pose.orientation.x
            wpi.orientation.y = current_pose.orientation.y
            wpi.orientation.z = current_pose.orientation.z
            wpi.orientation.w = current_pose.orientation.w
            next_waypoint = [wpi]
            path(next_waypoint)
            current_pose = self.get_pose()

    def add_box(self):
        rospy.wait_for_service("ur_control_wrapper/add_object")
        add_box = rospy.ServiceProxy("ur_control_wrapper/add_object", AddObject)
        try:
            name = "box_object"
            pose = Pose(Point(-0.54, 0.34, 0.025), Quaternion(0.0, 0.0, 0.0, 1.0))
            size = Vector3(0.4, 0.20, 0.05)
            object_type = AddObjectRequest.TYPE_BOX
            response = add_box(name, pose, size, AddObjectRequest.TYPE_BOX).is_success
        except rospy.ServiceException as exc:
            print "Service did not process request: " + str(exc)

        try:
            name = "box_object2"
            pose = Pose(Point(-0.54, 0.34, 0.07), Quaternion(0.0, 0.0, 0.0, 1.0))
            size = Vector3(0.4, 0.07, 0.04)
            object_type = AddObjectRequest.TYPE_BOX
            response = add_box(name, pose, size, AddObjectRequest.TYPE_BOX).is_success
        except rospy.ServiceException as exc:
            print "Service did not process request: " + str(exc)
    
    def attach_box(self):
        print "wait for service"
        rospy.wait_for_service("ur_control_wrapper/attach_object")
        print "service found"
        attach_box = rospy.ServiceProxy("ur_control_wrapper/attach_object", AttachObject)
        try:
            name = "box_tool"
            pose = Pose(Point(-0.34, -0.0075, -0.023), Quaternion(0.0, 0.0, 0.0, 1.0))
            size = Vector3(0.02, 0.02, 0.02)
            object_type = AttachObjectRequest.TYPE_BOX
            response = attach_box(name, pose, size, object_type).is_success
            if response:
                print "successfully attached!"
            else:
                print "did not attach successfully"
        except rospy.ServiceException as exc:
            print "Service did not process request: " + str(exc)
    
    def detach_box(self):
        rospy.wait_for_service("ur_control_wrapper/detach_object")
        detach_box = rospy.ServiceProxy("ur_control_wrapper/detach_object", DetachObject)
        try:
            name = "box_tool"
            response = detach_box(name, True).is_success
        except rospy.ServiceException as exc:
            print "Service did not process request: " + str(exc)
    
    def remove_box(self):
        rospy.wait_for_service("ur_control_wrapper/remove_object")
        remove_box = rospy.ServiceProxy("ur_control_wrapper/remove_object", RemoveObject)
        try:
            name = "box_object"
            response = remove_box(name).is_success
        except rospy.ServiceException as exc:
            print "Service did not process request: " + str(exc)
        
    def run(self):
        while not rospy.is_shutdown():
            print "====================================================="
            command_input = raw_input("Freedrive: fs(start), fe(end) \nGripper: grip \
            \nConnect: c(connect) \nGet End Effector Pose: ep \nGet joint angles: ja \
            \nGo to align position: da \nGo to aligned picking position: dp \nGo to assembly angles: aa \
            \nGo to Default Position: d \nGo to nancy Position: nancy\nWaypoints following: wp \nPick and place test: pp \nSet joints: sj \
            \nGet image snapshot: sa \
            \nGet dataset: data \
            \nMove arm: x+/-#, y+/-#, z+/-#, r(roll)+/-#, p(pitch)+/-#, w(yaw)+/-# \n")
            if command_input == "fs":
                self.free_drive_pub.publish(True)
            elif command_input == "fe":
                self.free_drive_pub.publish(False)
            elif command_input == "grip":
                sub_cmd = raw_input("Adjust gripper position: gp+/-# \
                                    \nAdjust gripper force: gf+/- \
                                    \nAdjust gripper speed: gs+/- \
                                    \nOpen gripper: gop \nClose gripper: gcl \
                                    \nActivate gripper: gac \
                                    \nReset gripper: grs \
                                    \nGet current gripper position: see \
                                    \nNote: gp+ closes the gripper (max: 255), gp- opens the gripper (min: 0);\n")
                direction = sub_cmd
                self.control_gripper(direction)
            elif command_input == "c":
                self.connect_pub.publish(True)
            elif command_input == "ep":
                print self.get_pose()
            elif command_input == "ja":
                print self.get_angle()
            elif command_input == "da":
                self.set_align_angles()
            elif command_input == "dp":
                self.set_align_pick_angles()
            elif command_input == "aa":
                self.assembly_angles()
            elif command_input == "d":
                self.set_default_angles()
            elif command_input == "nancy":
                self.set_nancy_angles()
            elif command_input == "modele-pickup":
                self.set_modele_pickup_angles()
            elif command_input == "modele-ready":
                self.set_modele_ready_insert_angles()
            elif command_input =="wp":
                self.waypoint_following()
            elif command_input == "pp":
                self.pick_and_place()
            elif command_input =="sj":
                sub_cmd = raw_input("Adjust joint by #degrees: 1+/-#; 2+/-#; 3+/-#; 4+/-#; 5+/-#; 6+/-# \n")
                direction = sub_cmd
                self.set_joints(direction)
            elif command_input == "tee":
                sub_cmd = "main_shell,left"
                self.test_desired_ee_wp(sub_cmd)
                sub_cmd = "top_shell,top"
                self.test_desired_ee_wp(sub_cmd)
            elif command_input == "ab":
                self.add_box()
            elif command_input == "atb":
                self.attach_box()
            elif command_input == "db":
                self.detach_box()
            elif command_input == "rb":
                self.remove_box()
            elif command_input == "sa":
                self.image_snapshot()
            elif command_input == "data":
                self.snapshot_waypoints()
            elif command_input == "gd":
                self.execute_joints([0.06331396102905273, -2.3033443890013636, -1.9767999649047852, 5.850466239243307, -1.5672815481769007, 0.0004458427429199219])
            else: # move arm
                direction = command_input
                self.move_arm(direction)

if __name__ == '__main__':
    try:
        rospy.init_node('ur_control_wrapper_demo_mode', anonymous=True)

        demo = Demo()

        demo.run()
    except rospy.ROSInterruptException:
        pass