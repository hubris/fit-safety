import rosbag
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.colors import Normalize
from matplotlib import cm
import matplotlib
from mpl_toolkits.mplot3d import Axes3D
from pylab import *
import transformations as tfs
import pdb
    
class Ur5eBagStates(object):
    """grabs all topics that are intereted or to be plotted"""
    def __init__(self, bag, robot_id):
        self.bag = rosbag.Bag(bag)
        self.get_states(robot_id)

    def get_states(self, robot_id):
        topic1 = "/joint_states"
        topic2 = "/ee_pose_jenny"
        topic3 = "/gripper_status_jenny"

        msgs1 = self.bag.read_messages(topic1)
        joint_time = []
        joint_angles = []
        joint_velocity = []
        joint_effort = []
        for msg in msgs1:
            joint_angles.append(msg[1].position)
            joint_velocity.append(msg[1].velocity)
            joint_effort.append(msg[1].effort)
            joint_time.append(msg[1].header.stamp.to_sec())
        # converting to np array
        self.joint_name = msg[1].name
        self.joint_angles = np.asarray(joint_angles)
        self.joint_velocity = np.asarray(joint_velocity)
        self.joint_effort = np.asarray(joint_effort)
        self.joint_time = np.asarray(joint_time)-joint_time[0]

        msgs2 = self.bag.read_messages(topic2)
        ee_time = []
        ee_x_position = []
        ee_y_position = []
        ee_z_position = []
        ee_orientation = []
        ee_euler = []

        for msg in msgs2:
            ee_x_position.append(msg[1].position.x)
            ee_y_position.append(msg[1].position.y)
            ee_z_position.append(msg[1].position.z)
            ee_orientation.append(msg[1].orientation)
            euler_temp = np.rad2deg(tfs.euler_from_quaternion(np.array([msg[1].orientation.x, 
                        msg[1].orientation.y, msg[1].orientation.z, msg[1].orientation.w])))
            ee_euler.append(euler_temp)

        # converting to np array
        self.ee_x_position = np.asarray(ee_x_position)
        self.ee_y_position = np.asarray(ee_y_position)
        self.ee_z_position = np.asarray(ee_z_position)
        self.ee_orientation = np.asarray(ee_orientation)
        self.ee_euler = np.asarray(ee_euler)


        msgs3 = self.bag.read_messages(topic3)
        gripper_time = []
        gripper_position = []

        for msg in msgs3:
            gripper_position.append(msg[1].array.data[0])
            gripper_time.append(msg[1].header.stamp.to_sec())
        # converting to np array
        self.gripper_position = np.asarray(gripper_position)
        self.gripper_time = np.asarray(gripper_time)-gripper_time[0]     

def get_states(text_filepath):
    data = []
    x = []
    y = []
    z = []
    j1 = []
    j2 = []
    j3 = []
    j4 = []
    j5 = []
    j6 = []
    textfile = open(text,"r")
    for line in textfile.readlines():
        list1 = line[1:-2].split(',')
        sublist = [float(i) for i in list1]
        data.append(sublist)
        x.append(sublist[1])
        y.append(sublist[2])
        z.append(sublist[3])
        j1.append(sublist[8])
        j2.append(sublist[9])
        j3.append(sublist[10])
        j4.append(sublist[11])
        j5.append(sublist[12])
        j6.append(sublist[13])
    return data, x, y, z, j1, j2, j3, j4, j5, j6

#*****************************************main function to plot*************************************************#

path = '/home/hollydinkel/Documents/CoBot/03-25-2021/'
bag = path + 'Misc/fit_2021-03-25-19-06-10.bag'
text = path + 'Data/fit_2021-03-25-19-06-10.txt'
vehi_id = "00"

real = Ur5eBagStates(bag, vehi_id)
data, x, y, z, j1, j2, j3, j4, j5, j6 = get_states(text)

#********************************************* Plot the Bag File************************************************#
f1 = plt.figure(1)
plt.plot(real.joint_time, real.joint_angles[:,0], real.joint_time, real.joint_angles[:,1], 
         real.joint_time, real.joint_angles[:,2], real.joint_time, real.joint_angles[:,3],
         real.joint_time, real.joint_angles[:,4], real.joint_time, real.joint_angles[:,5])
plt.xlabel('Time (s)')
plt.ylabel('Joint Angles (radians)')
plt.title('UR5e Joint Angle Full Trajectory')
plt.legend((real.joint_name))
plt.savefig(path + 'Misc/joint_angles_full_trajectory.png')

## Plot planar (2D) path of the end effector
corner_x = [-0.40, -0.7048, -0.7048, -0.40, -0.40]
corner_y = [0, 0, 0.3048, 0.3048, 0]
corner_z = [-0.03, -0.03, -0.03, -0.03, -0.03]

f2 = plt.figure(2)
plt.plot(real.ee_x_position, real.ee_y_position, color='magenta')
plt.plot(corner_x, corner_y, color='green')
plt.xlabel('x (m)')
plt.ylabel('y (m)')
plt.title('UR5e End-Effector Full Planar Path')
plt.savefig(path + 'Misc/ee_full_planar_path.png')

f3 = plt.figure(3)
ax3 = Axes3D(f3)
ax3.plot(real.ee_x_position, real.ee_y_position, real.ee_z_position, color='magenta', label='Full End-Effector C-Space Path')
ax3.plot(corner_x, corner_y, color='green', label='Tag Board Corners')
ax3.set_xlabel('x (m)')
ax3.set_ylabel('y (m)')
ax3.set_zlabel('z (m)')
ax3.legend()
plt.savefig(path + 'Misc/ee_full_3D_path.png')

z_mean = np.mean(real.ee_z_position)
z_stdev = np.std(real.ee_z_position)
z_med = np.median(real.ee_z_position)
f4 = plt.figure(4)
z_n, z_bins, z_patches = plt.hist(x=real.ee_z_position, bins='auto', color='#0504aa',alpha=0.7, rwidth=0.85)
plt.grid(axis='y', alpha = 0.75)
plt.xlabel('End-Effector Z Position (m)')
plt.ylabel('Frequency')
plt.title('End-Effector Height Along Full Trajectory')
a = '\n'
plt.text(0.4711,430, r'$\mu$={:.4f} (m) {}$\sigma$={:.4f} (m) {}median={:.4f} (m) {}mode={} (count)'.format(z_mean, a, z_stdev, a, z_med, a, int(z_n.max())), fontsize='small')
plt.savefig(path + 'Misc/ee_height_full_histogram.png')

#********************************************* Plot Dataset Data*******************************************#

# f5 = plt.figure(5)
# plt.plot(real.joint_time, real.joint_angles[:,0], real.joint_time, real.joint_angles[:,1], 
#          real.joint_time, real.joint_angles[:,2], real.joint_time, real.joint_angles[:,3],
#          real.joint_time, real.joint_angles[:,4], real.joint_time, real.joint_angles[:,5])
# plt.xlabel('Time (s)')
# plt.ylabel('Joint Angles (radians)')
# plt.title('UR5e Joint Angle Trajectory')
# plt.legend((real.joint_name))
# plt.savefig(path + 'Misc/joint_angles_full_trajectory.png')

# Plot planar (2D) path of the end effector
f6 = plt.figure(6)
plt.plot(x, y, '*-', color='magenta')
plt.plot(corner_x, corner_y, color='green', label='Tag Board Corners')
plt.xlabel('x (m)')
plt.ylabel('y (m)')
plt.title('UR5e End-Effector Dataset Planar Path')
plt.savefig(path + 'Misc/ee_full_dataset_path.png')

f7 = plt.figure(7)
ax3 = Axes3D(f7)
ax3.plot(x, y, z, '*-', color='magenta', label='End-Effector C-Space Dataset Path')
ax3.plot(corner_x, corner_y, color='green', label='Tag Board Corners')
ax3.set_xlabel('x (m)')
ax3.set_ylabel('y (m)')
ax3.set_zlabel('z (m)')
ax3.legend()
plt.savefig(path + 'Misc/ee_dataset_3D_path.png')

z_mean = np.mean(z)
z_stdev = np.std(z)
z_med = np.median(z)
f8 = plt.figure(8)
z_n, z_bins, z_patches = plt.hist(x=z, bins='auto', color='#0504aa',alpha=0.7, rwidth=0.85)
plt.grid(axis='y', alpha = 0.75)
plt.xlabel('Dataset End-Effector Z Position (m)')
plt.ylabel('Frequency')
plt.title('End-Effector Height for Image Dataset')
a = '\n'
plt.text(0.471089,15.83, r'$\mu$={:.4f} (m) {}$\sigma$={:.4f} (m) {}median={:.4f} (m) {}mode={} (count)'.format(z_mean, a, z_stdev, a, z_med, a, int(z_n.max())), fontsize='small')
plt.savefig(path + 'Misc/ee_height_dataset_histogram.png')

plt.show()