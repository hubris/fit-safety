#!/usr/bin/env python
import rospy
import numpy as np
from std_msgs.msg import String, Bool, Float64MultiArray
from std_srvs.srv import Trigger, SetBool
from sensor_msgs.msg import JointState, Image, CameraInfo
from geometry_msgs.msg import Pose, Point, Quaternion, Vector3, WrenchStamped
from controller_manager_msgs.srv import SwitchController, SwitchControllerRequest, ListControllers
from ur_control_wrapper.srv import SetPose
from ur_control_wrapper.srv import GetPose
from ur_control_wrapper.srv import GetJoints
from ur_control_wrapper.srv import SetJoints
from ur_control_wrapper.srv import SetTrajectory
from ur_control_wrapper.srv import GetCartesianPlan
from ur_control_wrapper.srv import ExecuteCartesianPlan
from ur_control_wrapper.srv import AddObject, AddObjectRequest
from ur_control_wrapper.srv import AttachObject, AttachObjectRequest
from ur_control_wrapper.srv import DetachObject, DetachObjectRequest
from ur_control_wrapper.srv import RemoveObject, RemoveObjectRequest
from ur_control_wrapper.srv import InverseKinematics, InverseKinematicsRequest
from gripper_controller_jenny import GripperHande as gripper
from status_listener import StatusListener
from cv_bridge import CvBridge, CvBridgeError
import cv2
import time

from tf import transformations as tfs

import tf
from tf import TransformListener

def qv_mult(q1, v1):
    # v1 = tf.transformations.unit_vector(v1)
    q2 = list(v1)
    q2.append(0.0)
    return tf.transformations.quaternion_multiply(
        tf.transformations.quaternion_multiply(q1, q2), 
        tf.transformations.quaternion_conjugate(q1)
    )[:3]


class PotentialFieldStatic:
    def __init__(self, args, tracking_topic_name, transformation_matrix):
        self.args = args
        self.transformation_matrix = transformation_matrix
        rospy.init_node('potential_field_static', anonymous=True)
        # self.intention_prob_dist_pub = rospy.Publisher("hri/human_intention_prob_dist", Float32MultiArray, queue_size=1)
        rospy.Subscriber(tracking_topic_name, HumanSkeletonPosition, self.tracking_callback)

    def tracking_callback(self, data):
        pos = np.array(data.data).reshape(data.n_humans, data.n_keypoints, data.n_dim) # (n_human, 25, 3)
        wrist_pos = pos[0,4] # (3,) right wrist
        wrist_pos_tf = np.dot(self.transformation_matrix, np.concatenate((wrist_pos,[1.]),axis=0))[:3] # (3,)
        

        # ep
        # position: 
        # x: -0.704394356791
        # y: 0.133268692128
        # z: 0.43097065771
        # orientation: 
        # x: -0.707088736263
        # y: -2.63796782179e-06
        # z: 0.707124824778
        # w: 3.50075593324e-05

        # self.trajectory = np.concatenate((self.trajectory, wrist_pos_tf[np.newaxis,:]*100), axis=0) # (t, 3)
        # if len(self.trajectory) < self.args.Tf+self.args.T_warmup:
        #     return
        # if (len(self.trajectory)-(self.args.Tf+self.args.T_warmup)) % self.args.step_per_update == 0:
        #     x_obs = self.trajectory
        #     self.intention_pf.predict(x_obs)
        #     self.intention_pf.update_weight(x_obs, tau=args.tau)
        #     self.intention_pf.resample()
        #     if self.args.mutable:
        #         self.intention_pf.mutate(mutation_prob=args.mutation_prob)
        #     intention_prob_dist = self.intention_pf.get_intention_probability() # (2,)
        #     predicted_trajectories = self.intention_api.predict_trajectories(
        #         x_obs,
        #         self.intention_pf.get_intention(),
        #         truncated=True, # does not matter for ilstm, does matter for ilm
        #     )
        #     self.current_filtering_results['intention_prob_dist'] = intention_prob_dist
        #     self.current_filtering_results['predicted_trajectories'] = predicted_trajectories
        #     self.current_filtering_results['particle_intentions'] = self.intention_pf.get_intention()
        #     intention_prob_dist_msg = Float32MultiArray()
        #     intention_prob_dist_msg.data = list(intention_prob_dist.astype(np.float32))
        #     self.intention_prob_dist_pub.publish(intention_prob_dist_msg)

 def __init__(self, args, device, tracking_topic_name, transformation_matrix):

    tracking_topic_name = "/hri/human_skeleton_position_tracking_3D"


    # T_cam_in_base = np.array(
    #     [[0.006224954603275489, 0.9982289837511288, -0.05916133912915617, -0.46120498770812773], 
    #     [0.9995933902634383, -0.004565496298061819, 0.028144059549524127, -0.005809570375797866], 
    #     [0.027824133478621935, -0.05931242834707712, -0.9978516626849133, 1.8369516707526776],
    #     [0.0, 0.0, 0.0, 1.0]]
    # )

    # 211202
    transformation_matrix = np.array(
        [[0.0021317922423761374, 0.9983572246475552, -0.05725579065951769, -0.4592949889034955],
        [0.9998419196667168, -0.0011175089383325249, 0.017741605160713724, -0.008644474966984353],
        [0.017648492498782677, -0.057284510011343534, -0.9982019349353066, 1.8398017828469182],
        [0.0, 0.0, 0.0, 1.0]]
    )
    # transformation_matrix = np.array([[ 0.00622504,  0.99822903, -0.05916131, -0.46120499],
    #                                 [ 0.99959345, -0.00456556,  0.02814404, -0.00580957],
    #                                 [ 0.02782409, -0.05931246, -0.99785162,  1.83695167],
    #                                 [ 0.,          0.,          0.,          1.        ],])
    args = arg_parse()
    args.Tf = args.pred_seq_len
    args.T_warmup = args.obs_seq_len
    torch.manual_seed(args.random_seed)
    np.random.seed(args.random_seed)
    device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
    print(device)
    try:
        online_mif = OnlineMIF(
            args,
            device,
            tracking_topic_name,
            transformation_matrix,
        )
        online_mif.run()
    except rospy.ROSInterruptException:
        pass






class AdmittanceControl:
    def __init__(self):
        rospy.Service('ur_control_wrapper/switch_admittance_control', SetBool, self.switch_admittance_control)
        rospy.Service('ur_control_wrapper/check_admittance_control', Trigger, self.check_admittance_control)
        self.admittance_control_mode = False
        self.joint_velocity_pub = rospy.Publisher('/joint_group_vel_controller/command', Float64MultiArray, queue_size=10)
        self.wrench_sub = rospy.Subscriber('/wrench', WrenchStamped, self.wrench_callback, queue_size=1)
        self.pose_vel_scale = 0.08
        self.force_vel_scale = 1./10
        self.force_threshold = 1.
        self.zero_force_torque_sensor()
        self.tf = TransformListener()

    def switch_admittance_control(self, data):
        self.admittance_control_mode = data.data
        if self.admittance_control_mode:
            message = "Admittance control mode is turned on."
            controller_state = self.check_controller_state()
            if controller_state["joint_group_vel_controller"] != "running":
                self.switch_controller("joint_group")
        else:
            message = "Admittance control mode is turned off."
            controller_state = self.check_controller_state()
            if controller_state["scaled_pos_joint_traj_controller"] != "running":
                self.switch_controller("scaled_pos")
        success = True
        return success, message

    def check_admittance_control(self, data):
        if self.admittance_control_mode:
            message = "Admittance control mode is on."
        else:
            message = "Admittance control mode is off."
        success = True
        return success, message

    def wrench_callback(self, data):
        if not self.admittance_control_mode:
            return
        controller_state = self.check_controller_state(silent=True)
        if controller_state["joint_group_vel_controller"] != "running":
            self.switch_controller("joint_group")
        force_tool = np.array([data.wrench.force.x, data.wrench.force.y, data.wrench.force.z])
        # print(force_tool)
        force_tool = (force_tool>self.force_threshold)*(force_tool-self.force_threshold)\
            +(force_tool<-self.force_threshold)*(force_tool+self.force_threshold)        
        try:
            (trans_tool_base, rot_tool_base) = self.tf.lookupTransform("/base_link", "/tool0_controller", rospy.Time(0))
            force_base = qv_mult(rot_tool_base, force_tool)
            force_magnitude = np.linalg.norm(force_base)
            if force_magnitude > 1e-6:
                pose_offset_vel = self.pose_vel_scale*force_base/force_magnitude
            else:
                pose_offset_vel = np.zeros(3)
            # print(pose_offset_vel)
            desired_next_joint_state = self.compute_inverse_kinematics(pose_offset_vel)
            if len(desired_next_joint_state.position) == 0:
                self.set_joint_velocity(np.zeros(6))
                print("The desired pose cannot be reached.")
                self.switch_controller("scaled_pos")
                return
            current_joint_state = self.get_angle()
            # print(force_magnitude) # roughly 0-12
            # print(np.array(desired_next_joint_state.position)) # if you are using hande launch file, then the joint state is length of 8.
            # print(np.array(current_joint_state.position))
            jv_input = np.array(desired_next_joint_state.position)[:6] - np.array(current_joint_state.position)
            jv_input = jv_input * force_magnitude*self.force_vel_scale
            self.set_joint_velocity(jv_input)
            
        except (tf.LookupException, tf.ConnectivityException, tf.ExtrapolationException):
            print("Error on tf.")

    def wrench_callback_copy(self, data):
        if not self.admittance_control_mode:
            return
        controller_state = self.check_controller_state(silent=True)
        if controller_state["joint_group_vel_controller"] != "running":
            self.switch_controller("joint_group")
        force = np.array([data.wrench.force.x, data.wrench.force.y, data.wrench.force.z])
        
        force = (force>self.force_threshold)*(force-self.force_threshold)+(force<-self.force_threshold)*(force+self.force_threshold)
        # print(force)
        try:
            (trans_ee, rot_ee) = self.tf.lookupTransform("/base_link", "/tool0_controller", rospy.Time(0))
            force_rotated = qv_mult(rot_ee, force)
            print(force_rotated)
            pose_offset_vel = self.pose_vel_scale*force_rotated
            # print(pose_offset_vel)
            desired_next_joint_state = self.compute_inverse_kinematics(pose_offset_vel)
            if len(desired_next_joint_state.position) == 0:
                self.set_joint_velocity(np.zeros(6))
                print("The desired pose cannot be reached.")
                self.switch_controller("scaled_pos")
                return
            current_joint_state = self.get_angle()
            jv_input = np.array(desired_next_joint_state.position) - np.array(current_joint_state.position)
            # self.set_joint_velocity(jv_input)
        except (tf.LookupException, tf.ConnectivityException, tf.ExtrapolationException):
            print("Error on tf.")

    def get_pose(self):
        rospy.wait_for_service("ur_control_wrapper/get_pose")
        get_current_pose = rospy.ServiceProxy("ur_control_wrapper/get_pose", GetPose)
        current_pose = None
        try:
            current_pose = get_current_pose().pose
        except rospy.ServiceException as exc:
            print "Service did not process request: " + str(exc)
        return current_pose

        
    def get_angle(self):
        rospy.wait_for_service("ur_control_wrapper/get_joints")
        get_current_joints = rospy.ServiceProxy("ur_control_wrapper/get_joints", GetJoints)
        current_joints = None
        try:
            current_joints = get_current_joints().joints
        except rospy.ServiceException as exc:
            print "Service did not process request: " + str(exc) 
        return current_joints

    def compute_inverse_kinematics(self, desired_pose_offset, silent=True):
        # desired_pose_offset = [offset_x, offset_y, offset_z]
        joint_state = None
        current_pose = self.get_pose()
        desired_pose = current_pose
        desired_pose.position.x += desired_pose_offset[0]
        desired_pose.position.y += desired_pose_offset[1]
        desired_pose.position.z += desired_pose_offset[2]
        rospy.wait_for_service("ur_control_wrapper/inverse_kinematics")
        compute_ik = rospy.ServiceProxy("ur_control_wrapper/inverse_kinematics", InverseKinematics)
        try:
            req = InverseKinematicsRequest()
            req.pose = desired_pose
            response = compute_ik(req)
            # response = compute_ik(desired_pose)
            solution_found, joint_state = response.solution_found, response.joint_state
            if solution_found:
                if not silent:
                    print("joint state: ")
                    print(joint_state)
            else:
                print("Solution is not found.")
        except rospy.ServiceException as exc:
            print "Service did not process request: " + str(exc)
        return joint_state

    def set_joint_velocity(self, jv_input):
        controller_state = self.check_controller_state(silent=True)
        if controller_state["joint_group_vel_controller"] != "running":
            print("joint_group_vel_controller is not started. Failed to set joint velocity.")
            return
        # pass
        if isinstance(jv_input, str):
            jv_float = [float(entry) for entry in jv_input.split(" ")] # [0., 0., 0., 0., 0., 0.] # 6 entries
        else:
            jv_float = jv_input
        data = Float64MultiArray()
        data.data = jv_float
        self.joint_velocity_pub.publish(data)
        return
    
    def check_controller_state(self, silent=False):
        rospy.wait_for_service("/controller_manager/list_controllers")
        list_controllers = rospy.ServiceProxy("/controller_manager/list_controllers", ListControllers)
        response = list_controllers()
        controller_state = {}
        for single_controller in response.controller:
            if single_controller.name == "joint_group_vel_controller" \
                or single_controller.name == "scaled_pos_joint_traj_controller":
                controller_state[single_controller.name] = single_controller.state
        if not silent:
            if controller_state["scaled_pos_joint_traj_controller"] == "running":
                print("Current controller is scaled_pos_joint_traj_controller.")
            elif controller_state["joint_group_vel_controller"] == "running":
                print("Current controller is joint_group_vel_controller.")
            else:
                print("Neither scaled_pos_joint_traj_controller nor joint_group_vel_controller is running.")
        return controller_state

    
    def switch_controller(self, desired_controller):
        # desired_controller = 'joint_group' or 'scaled_pos'
        controller_switched = False
        if desired_controller != 'joint_group' and desired_controller != 'scaled_pos':
            print("Wrong controller input.")
            return controller_switched
        rospy.wait_for_service("controller_manager/switch_controller")
        switch_controller = rospy.ServiceProxy("controller_manager/switch_controller", SwitchController)
        try:
            req = SwitchControllerRequest()
            if desired_controller == 'joint_group':
                req.start_controllers = ['joint_group_vel_controller']
                req.stop_controllers = ['scaled_pos_joint_traj_controller']
            else:
                req.start_controllers = ['scaled_pos_joint_traj_controller']
                req.stop_controllers = ['joint_group_vel_controller']
            req.strictness = 2
            req.start_asap = False
            req.timeout = 0.0
            response = switch_controller(req)
            if response.ok:
                controller_switched = True
                if desired_controller == 'joint_group':
                    print("Controller successfully switched to joint_group_vel_controller.")
                else:
                    print("Controller successfully switched to scaled_pos_joint_traj_controller.")
            else:
                print("Controller switch failed.")
        except rospy.ServiceException as exc:
            print "Service did not process request: " + str(exc)
        return controller_switched
    
    def zero_force_torque_sensor(self):
        rospy.wait_for_service("ur_hardware_interface/zero_ftsensor")
        zero_ft_sensor = rospy.ServiceProxy("ur_hardware_interface/zero_ftsensor", Trigger)
        response = zero_ft_sensor()
        if response.success:
            print("Force torque sensor is zeroed.")
        else:
            print("Failed to zero force torque sensor.")
        return response.success

if __name__ == '__main__':
    try:
        rospy.init_node('ur_admittance_control_node', anonymous=True)
        admittance_control = AdmittanceControl()
        rospy.spin()
    except rospy.ROSInterruptException:
        pass