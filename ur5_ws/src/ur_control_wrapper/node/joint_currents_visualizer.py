#!/usr/bin/env python
r"""Visualize actual and target currents of one joint on ur robot in real time."""
import sys
import argparse
import rospy
import matplotlib.pyplot as plt
from ur_control_wrapper.msg import JointCurrents


class JointCurrentsVisualizer: 
    def __init__(self, joint_index=0):
        r"""Visualize the currents on one joint.
        inputs:
            - joint_index: 
                # integer.
                # index of the joint to be visualized. 0-5.
                # default: 0. Means the base joint.
        """
        assert joint_index in range(6) # ur has only 6 joints
        self.joint_index = joint_index
        _, self.ax = plt.subplots(3,1)
        ylabels = ['ac', 'tc', 'dc'] # actual current, target current, difference current (dc=ac-tc) 
        y_limit = [[-6,6], [-6, 6], [-1, 1]]
        self.gCU_plt = []
        for ax, ylabel, ylim in zip(self.ax, ylabels, y_limit):
            ax.set_xlim([0,200])
            ax.set_ylim(ylim)
            ax.set_xlabel('time')
            ax.set_ylabel(ylabel)
            self.gCU_plt.append(ax.plot([], [], 'b-')[0])
        self.ac = []
        self.tc = []
        self.dc = []

    def callback_realtime_plot(self, data):
        self.ac.append(data.actual_current[self.joint_index])
        self.tc.append(data.target_current[self.joint_index])
        self.dc.append(data.actual_current[self.joint_index]-data.target_current[self.joint_index])
        current = [self.ac, self.tc, self.dc]
        for i, (c_plt, c) in enumerate(zip(self.gCU_plt, current)):
            if len(c) > 200:
                c = c[-200:]
            c_plt.set_data(range(len(c)), c)
        plt.draw()


def arg_parse():
    parser = argparse.ArgumentParser(description='Visualize joint currents.')
    parser.add_argument('--joint_index', type=int, default=0, \
        help='index of the joint to be visualized.')
    return parser.parse_args(rospy.myargv(argv=sys.argv)[1:])


def listener(args):
    r"""Subscribes to the topic /joint_currents and plot values in real time."""
    jcv = JointCurrentsVisualizer(joint_index=args.joint_index)
    rospy.init_node('ur_control_wrapper_joint_currents_listener', anonymous=True)
    rospy.Subscriber("/joint_currents", JointCurrents, jcv.callback_realtime_plot, queue_size=10)
    plt.show()
    rospy.spin()


if __name__ == '__main__':
    args = arg_parse()
    listener(args)