#!/usr/bin/env python

import rospy
import numpy as np
from std_msgs.msg import String, Bool
from sensor_msgs.msg import JointState
from geometry_msgs.msg import Pose, Point, Quaternion, Vector3
from ur_control_wrapper.srv import SetPose
from ur_control_wrapper.srv import GetPose
from ur_control_wrapper.srv import GetJoints
from ur_control_wrapper.srv import SetJoints
from ur_control_wrapper.srv import SetTrajectory
from ur_control_wrapper.srv import GetCartesianPlan
from ur_control_wrapper.srv import ExecuteCartesianPlan
from ur_control_wrapper.srv import AddObject, AddObjectRequest
from ur_control_wrapper.srv import AttachObject, AttachObjectRequest
from ur_control_wrapper.srv import DetachObject, DetachObjectRequest
from ur_control_wrapper.srv import RemoveObject, RemoveObjectRequest
from gripper_controller_jenny import GripperHande as gripper
from status_listener import StatusListener
import matplotlib.pyplot as plt
from tf import transformations as tfs
import tf2_ros
from tf2_msgs.msg import TFMessage
class Demo:
    def __init__(self):
        self.free_drive_pub = rospy.Publisher("ur_control_wrapper/enable_freedrive", Bool, queue_size=10)
        self.gripper_pub = rospy.Publisher("ur_control_wrapper/gripper", String, queue_size=10)
        self.connect_pub = rospy.Publisher("ur_control_wrapper/connect", Bool, queue_size=10)
        self.status = StatusListener()
        self.gripper_position_status = self.status.gripper_status.gPO
        rospy.Subscriber("/tf", TFMessage, self.GetPartsPose)
        self.parts_detect = set()
        self.reset_vision_pub = rospy.Publisher("/reset_vision", Bool, queue_size=10)
        rospy.Subscriber("/reset_robot", Bool, self.reset_robot)

        self.reset_robot_flag = False

    def reset_robot(self, msg):
        self.reset_robot_flag = msg.data

    def get_pose(self):
        rospy.wait_for_service("ur_control_wrapper/get_pose")
        get_current_pose = rospy.ServiceProxy("ur_control_wrapper/get_pose", GetPose)
        current_pose = None
        try:
            current_pose = get_current_pose().pose
            current_euler = np.rad2deg(tfs.euler_from_quaternion(np.array([current_pose.orientation.x, 
                        current_pose.orientation.y, current_pose.orientation.z, current_pose.orientation.w])))
            # print("euler_angle = ", current_euler)
        except rospy.ServiceException as exc:
            print "Service did not process request: " + str(exc) 

        return current_pose
    
    def get_angle(self):
        rospy.wait_for_service("ur_control_wrapper/get_joints")
        get_current_joints = rospy.ServiceProxy("ur_control_wrapper/get_joints", GetJoints)
        current_joints = None
        try:
            current_joints = get_current_joints().joints
        except rospy.ServiceException as exc:
            print "Service did not process request: " + str(exc) 
        return current_joints
    # Make sure to modify the default joint angle for different robots. Robot 2: the 1st entry should be np.pi
    def set_default_angles(self, joints_angles = [0.0, -2.0 * np.pi / 3, -np.pi / 3.0, np.pi / 2.0, -np.pi / 2.0, 0.0]):
        rospy.wait_for_service("ur_control_wrapper/set_joints")
        set_joints = rospy.ServiceProxy("ur_control_wrapper/set_joints", SetJoints)
        joints = JointState()
        #joints.name = ["elbow_joint", "shoulder_lift_joint", "shoulder_pan_joint", "wrist_1_joint", "wrist_2_joint", "wrist_3_joint"]
        #joints.position = [-np.pi / 3.0, -2.0 * np.pi / 3, 0.0, np.pi * 1.0 / 2.0, -np.pi / 2.0, 0.0]
        joints.name = ["shoulder_pan_joint", "shoulder_lift_joint", "elbow_joint", "wrist_1_joint", "wrist_2_joint", "wrist_3_joint"]
        #joints.position = [0.023975849151611328, -2.173887868920797, -1.0581588745117188, 1.6445128160664062, -1.570674244557516, 0.0]
        joints.position = joints_angles  # [0.0, -2.0 * np.pi / 3, -np.pi / 3.0, np.pi / 2.0, -np.pi / 2.0, 0.0]
#        [0.0011186599731445312, -2.344504495660299, -1.4683351516723633, 2.2413531976887207, -1.5681212584124964, 0.004898548126220703]
#        [0.0010228157043457031, -2.343271394769186, -1.4688987731933594, 2.242299719447754, -1.5686605612384241, 0.004286289215087891]
        
        try:
            response = set_joints(joints)
        except rospy.ServiceException as exc:
            print "Service did not process request: " + str(exc)      

        self.gripper_pub.publish("100")
    
    def set_align_angles(self):
        rospy.wait_for_service("ur_control_wrapper/set_joints")
        set_joints = rospy.ServiceProxy("ur_control_wrapper/set_joints", SetJoints)
        joints = JointState()
        joints.name = ["shoulder_pan_joint", "shoulder_lift_joint", "elbow_joint", "wrist_1_joint", "wrist_2_joint", "wrist_3_joint"]
        joints.position = [0.1562185287475586, -2.5559779606261195, -1.586348533630371, 2.5811564165302734, -0.03694612184633428, 1.570235252380371]
        try:
            response = set_joints(joints)
        except rospy.ServiceException as exc:
            print "Service did not process request: " + str(exc)

    def set_align_pick_angles(self):
        rospy.wait_for_service("ur_control_wrapper/set_joints")
        set_joints = rospy.ServiceProxy("ur_control_wrapper/set_joints", SetJoints)
        joints = JointState()
        joints.name = ["shoulder_pan_joint", "shoulder_lift_joint", "elbow_joint", "wrist_1_joint", "wrist_2_joint", "wrist_3_joint"]
        joints.position = [-0.31185847917665654, -2.4204427204527796, -1.0834665298461914, 1.9517590242573242, -1.5629780928241175, -0.46342593828310186]


        try:
            response = set_joints(joints)
        except rospy.ServiceException as exc:
            print "Service did not process request: " + str(exc)
     
        
    def set_joints(self, direction):
        amount = float(direction[2:])
        joint = direction[:2]
        current_joints = self.get_angle()
        rospy.wait_for_service("ur_control_wrapper/set_joints")
        set_joints = rospy.ServiceProxy("ur_control_wrapper/set_joints", SetJoints)
        joints = JointState()
        joints.name = current_joints.name
        joint_angle = list(current_joints.position)

        try:
            if joint == "1+":
                joint_angle[0] += np.deg2rad(amount)
                print("Current joint 1 angle: %f degrees" % (np.rad2deg(joint_angle[0])))
            elif joint == "1-":
                joint_angle[0] -= np.deg2rad(amount)
                print("Current joint 1 angle: %f degrees" % (np.rad2deg(joint_angle[0])))
            elif joint == "2+":
                joint_angle[1] += np.deg2rad(amount)
                print("Current joint 2 angle: %f degrees" % (np.rad2deg(joint_angle[1])))
            elif joint == "2-":
                joint_angle[1] -= np.deg2rad(amount)
                print("Current joint 2 angle: %f degrees" % (np.rad2deg(joint_angle[1])))
            elif joint == "3+":
                joint_angle[2] += np.deg2rad(amount)
                print("Current joint 3 angle: %f degrees" % (np.rad2deg(joint_angle[2])))
            elif joint == "3-":
                joint_angle[2] -= np.deg2rad(amount)
                print("Current joint 3 angle: %f degrees" % (np.rad2deg(joint_angle[2])))
            elif joint == "4+":
                joint_angle[3] += np.deg2rad(amount)
                print("Current joint 4 angle: %f degrees" % (np.rad2deg(joint_angle[3])))
            elif joint == "4-":
                joint_angle[3] -= np.deg2rad(amount)
                print("Current joint 4 angle: %f degrees" % (np.rad2deg(joint_angle[3])))
            elif joint == "5+":
                joint_angle[4] += np.deg2rad(amount)
                print("Current joint 5 angle: %f degrees" % (np.rad2deg(joint_angle[4])))
            elif joint == "5-":
                joint_angle[4] -= np.deg2rad(amount)
                print("Current joint 5 angle: %f degrees" % (np.rad2deg(joint_angle[4])))
            elif joint == "6+":
                joint_angle[5] += np.deg2rad(amount)
                print("Current joint 6 angle: %f degrees" % (np.rad2deg(joint_angle[5])))
            elif joint == "6-":
                joint_angle[5] -= np.deg2rad(amount)
                print("Current joint 6 angle: %f degrees" % (np.rad2deg(joint_angle[5])))
            joints.position = tuple(joint_angle)
            response = set_joints(joints)
        except rospy.ServiceException as exc:
            print "Service did not process request: " + str(exc)
        
    def control_gripper(self, command):
        if len(command) > 3:
            amount = float(command[3:])
        direction = command[:3]
        # print("Current gripper position: %f" % (self.gripper_position_status))
        if direction == "gp+":
            self.gripper_position_status += amount
            if self.gripper_position_status > 255:
                self.gripper_position_status = 255
            elif self.gripper_position_status < 0:
                self.gripper_position_status = 0
            self.gripper_pub.publish('%f' % (self.gripper_position_status))
            # print("Current gripper position: %f" % (self.gripper_position_status))
        elif direction == "gp-":
            self.gripper_position_status -= amount
            if self.gripper_position_status > 255:
                self.gripper_position_status = 255
            elif self.gripper_position_status < 0:
                self.gripper_position_status = 0
            self.gripper_pub.publish('%f' % (self.gripper_position_status))
            # print("Current gripper position: %f" % (self.gripper_position_status))
        elif direction == "gf+":
            self.gripper_pub.publish('gi')
        elif direction == "gf-":
            self.gripper_pub.publish('gd') 
        elif direction == "gs+":
            self.gripper_pub.publish('gf')
        elif direction == "gs-":
            self.gripper_pub.publish('gl')
        elif direction == "gop":
            self.gripper_pub.publish('go')
        elif direction == "gcl":
            self.gripper_pub.publish('gc')
        elif direction == "gac":
            self.gripper_pub.publish('ga')
        elif direction == "grs":
            self.gripper_pub.publish('gr') 

    def move_arm(self, command):
        amount = float(command[2:])
        direction = command[:2]
        current_pose = self.get_pose()
        current_euler = np.rad2deg(tfs.euler_from_quaternion(np.array([current_pose.orientation.x, 
                        current_pose.orientation.y, current_pose.orientation.z, current_pose.orientation.w])))
        print("euler_before = ", current_euler)                
        rospy.wait_for_service("ur_control_wrapper/set_pose")
        set_current_pose = rospy.ServiceProxy("ur_control_wrapper/set_pose", SetPose)
        try:
            if direction == "x+":
                current_pose.position.x += (amount/100) # convert to centimeters
                print("Current x position: %f" % (current_pose.position.x))
            elif direction == "x-":
                current_pose.position.x -= (amount/100) # convert to centimeters
                print("Current x position: %f" % (current_pose.position.x))
            elif direction == "y+":
                current_pose.position.y += (amount/100) # convert to centimeters
                print("Current y position: %f" % (current_pose.position.y))
            elif direction == "y-":
                current_pose.position.y -= (amount/100) # convert to centimeters
                print("Current y position: %f" % (current_pose.position.y))
            elif direction == "z+":
                current_pose.position.z += (amount/100) # convert to centimeters
                print("Current z position: %f" % (current_pose.position.z))
            elif direction == "z-":
                current_pose.position.z -= (amount/100) # convert to centimeters
                print("Current z position: %f" % (current_pose.position.z))

            elif direction == "r+":
                current_euler[0] += amount
                print("Current end effector roll: %f" % (current_euler[0]))
            elif direction == "r-":
                current_euler[0] -= amount
                print("Current end effector roll: %f" % (current_euler[0]))
            elif direction == "p+":
                current_euler[1] += amount
                print("Current end effector pitch: %f" % (current_euler[1]))
            elif direction == "p-":
                current_euler[1] -= amount
                print("Current end effector pitch: %f" % (current_euler[1]))
            elif direction == "w+":
                current_euler[2] += amount
                print("Current end effector yaw: %f" % (current_euler[2]))
            elif direction == "w-":
                current_euler[2] -= amount
                print("Current end effector yaw: %f" % (current_euler[2]))

            print("euler_after = ", current_euler)
            ori = np.deg2rad(current_euler)
            quat = tfs.quaternion_from_euler(ori[0], ori[1], ori[2])
            current_pose.orientation.x = quat[0]
            current_pose.orientation.y = quat[1]
            current_pose.orientation.z = quat[2]
            current_pose.orientation.w = quat[3]

            response = set_current_pose(current_pose)
            pose = response.response_pose
            is_reached = response.is_reached
        except rospy.ServiceException as exc:
            print "Service did not process request: " + str(exc)
    
    def waypoint_following(self):
        current_pose = self.get_pose()

        rospy.wait_for_service("ur_control_wrapper/follow_trajectory")
        set_trajectory = rospy.ServiceProxy("ur_control_wrapper/follow_trajectory", SetTrajectory)

        waypoints = []
        delta = [[-0.1, 0.0, 0.0], [-0.1, 0.0, -0.1], [-0.1, 0.0, 0.0], [0.0, 0.0, 0.0]]
        for wp in delta:
            wpi = Pose()
            wpi.position.x = current_pose.position.x + wp[0]
            wpi.position.y = current_pose.position.y + wp[1]
            wpi.position.z = current_pose.position.z + wp[2]
            wpi.orientation.x = current_pose.orientation.x
            wpi.orientation.y = current_pose.orientation.y
            wpi.orientation.z = current_pose.orientation.z
            wpi.orientation.w = current_pose.orientation.w
            waypoints.append(wpi)
        try:
            response = set_trajectory(waypoints)
            final_pose = response.final_pose
            is_reached = response.is_reached
        except ROSInterruptException as exc:
            print "Service did not process request: " + str(exc)

    def test_desired_ee_wp(self, input):
        self.enter_and_close(input)

        current_pose = self.get_pose()

        self.lift_down_open(current_pose)


        wp = self.get_delta_wp(self.get_pose(), [0.0, 0.0, 0.012])
        self.execute_trajectory([wp])

        # self.set_default_angles(joints_angles = [0.12123775482177734, -2.2965408764281214, -1.5038652420043945, 2.22770540296521, -1.5678570906268519, 0.12182283401489258])
        # wp = self.get_delta_wp(self.get_pose(), [0.0, 0.0, 0.012])
        # self.execute_trajectory([wp])

    def grasping_q4(self):
        self.execute_joints([-0.0012305418597620132, -2.2915073833861292, -1.532109260559082, 2.2528082567402343, -1.5699074904071253, -0.0018828550921838882])
        self.enter_and_close("top_shell,top")
        # rospy.sleep(1.7)
        # self.move_arm("z+6")
        self.execute_joints([-0.0011823813067834976, -2.222851415673727, -1.504103660583496, 2.156036062831543, -1.569895092641012, -0.00045425096620732575])

        self.execute_joints([0.06083393096923828, -2.3768049679198207, -1.6749563217163086, 4.017343206996582, 1.5954289436340332, -3.147741142903463])
        self.execute_joints([0.06073808670043945, -2.408879896203512, -1.6721744537353516, 4.046029253596924, 1.59541654586792, -3.1458595434771937])

        for gi in range(5):
            self.control_gripper("gs-")
        self.gripper_pub.publish("180")
        rospy.sleep(1.0)
        for gi in range(5):
            self.control_gripper("gs+")

        # self.move_arm("z+9")
        self.execute_joints([0.06085777282714844, -2.244667192498678, -1.6646947860717773, 3.875077410335205, 1.595716953277588, -3.1463516394244593])
        #rospy.sleep(0.7)

        self.execute_joints([0.28427934646606445, -2.26662602047109, -1.583786964416504, 2.2798995214649658, -1.5713098684894007, 1.8560338020324707])
        self.enter_and_close("main_shell,top")
        # rospy.sleep(1.7)
        
        # self.move_arm("z+6")
        self.execute_joints([0.28444719314575195, -2.1945530376830042, -1.5542497634887695, 2.1772515016743164, -1.570338551198141, 1.8549542427062988])
        
        self.execute_joints([0.06083393096923828, -2.3768049679198207, -1.6749563217163086, 4.017343206996582, 1.5954289436340332, -3.147741142903463])
        self.execute_joints([0.06073808670043945, -2.408879896203512, -1.6721744537353516, 4.046029253596924, 1.59541654586792, -3.1458595434771937])

        for gi in range(5):
            self.control_gripper("gs-")
        self.gripper_pub.publish("180")
        rospy.sleep(1.0)
        for gi in range(5):
            self.control_gripper("gs+")

        # self.move_arm("z+10")
        self.execute_joints([0.06092977523803711, -2.2286130390562953, -1.6606788635253906, 3.8540398317524414, 1.5955843925476074, -3.1466873327838343])
        # rospy.sleep(0.7)

    def assembly_tutorial(self):
        self.enter_and_close("insert_mold,bottom")
        # self.execute_joints([-0.24863177934755498, -2.392189165154928, -1.3664064407348633, 2.1867617803760986, -1.5650761763202112, -0.24759465852846319])

        # for gi in range(5):
        #     self.control_gripper("gs-")
        # self.gripper_pub.publish("210")
        # rospy.sleep(1.0)
        # for gi in range(5):
        #     self.control_gripper("gs+")

        # self.move_arm("z+9")
        self.execute_joints([-0.24858409563173467, -2.1962796650328578, -0.719700813293457, 1.3440982538410644, -1.5651000181781214, -0.24710303941835576])

        self.execute_joints([0.4980440139770508, -2.3510529003539027, -0.6792440414428711, -3.0582734547057093, 0.4973173141479492, 2.981031894683838])
        
        # self.move_arm("z+6")
        self.execute_joints([0.5036492347717285, -2.341822763482565, -0.8528871536254883, -2.8846017322936, 0.5022025108337402, 2.9755940437316895])
        
        # self.execute_joints([0.5037574768066406, -2.3398591480650843, -0.8719091415405273, -2.857859273950094, 0.5003466606140137, 2.9672369956970215])
        self.execute_joints([0.5035772323608398, -2.3399907551207484, -0.8706750869750977, -2.8610435924925746, 0.5036749839782715, 2.9679818153381348])

        for gi in range(5):
            self.control_gripper("gs-")
        self.gripper_pub.publish("100")
        rospy.sleep(2.0)
        for gi in range(5):
            self.control_gripper("gs+")

        self.move_arm("y-2")    # side move

        for gi in range(5):
            self.control_gripper("gs-")
        self.gripper_pub.publish("gc")
        rospy.sleep(2.0)
        for gi in range(5):
            self.control_gripper("gs+")
        
        self.move_arm("y+1.6")  # side push
        self.move_arm("y-2")    # side leave
        self.move_arm("z+2")    # lift
        self.move_arm("y+2.5")  # approach
        self.move_arm("z-1.62") # downward push
        self.move_arm("z+5")    # leave

    def enter_and_close(self, input):
        # wp = self.get_wp_from_part(input)
        wp = input
        wp_p = self.get_delta_wp(wp, [0.0, 0.0, 0.01])
        self.execute_trajectory([wp_p, wp])

        for gi in range(1):
            self.control_gripper("gs-")

        grip_num = "247"
        self.gripper_pub.publish(grip_num)
        rospy.sleep(1.0)

        for gi in range(1):
            self.control_gripper("gs+")

    def lift_down_open(self, current_pose, lift_delta = 0.02):
        delta = [0, 0, lift_delta]
        wpi2 = self.get_delta_wp(current_pose, delta)
        self.execute_trajectory([wpi2, current_pose])

        for gi in range(1):
            self.control_gripper("gs-")

        self.gripper_pub.publish("100")
        rospy.sleep(1.0)

        for gi in range(1):
            self.control_gripper("gs+")

    def get_delta_wp(self, current_pose, delta):
        wp = Pose()
        wp.position.x = current_pose.position.x + delta[0]
        wp.position.y = current_pose.position.y + delta[1]
        wp.position.z = current_pose.position.z + delta[2]
        wp.orientation.x = current_pose.orientation.x
        wp.orientation.y = current_pose.orientation.y
        wp.orientation.z = current_pose.orientation.z
        wp.orientation.w = current_pose.orientation.w
        return wp

    def get_wp_from_part(self, input):
        user_input = input.split("/")
        partsname = user_input[2]
        orientation = user_input[3]
        lg = 0.16
        grasp_offset = 0.0
        grasp_offset2 = -0.011 #-0.014
        # gripper pose relative to parts origin
        parts_c2o = np.array([0.0, 0.0, 0.0])*0.001                      
        ee_in_parts = {'main_shell': {'top':[parts_c2o[0], parts_c2o[1] + grasp_offset, parts_c2o[2] - lg, 0.0, -np.pi/2.0, np.pi/2.0], 
                                     'bottom':[parts_c2o[0], parts_c2o[1]+ grasp_offset, parts_c2o[2] + lg, 0.0, np.pi/2.0, -np.pi/2.0],
                                     'left':[parts_c2o[0], parts_c2o[1]-lg, parts_c2o[2], -np.pi/2.0, 0.0, np.pi/2.0]},
                       'top_shell': {'top':[parts_c2o[0], parts_c2o[1]+ grasp_offset, parts_c2o[2] - lg, 0.0, -np.pi/2.0, np.pi/2.0],
                                     'bottom':[parts_c2o[0], parts_c2o[1]+ grasp_offset, parts_c2o[2] + lg, 0.0, np.pi/2.0, np.pi/2.0]},
                       'insert_mold': {'bottom':[parts_c2o[0], parts_c2o[1] - grasp_offset2, parts_c2o[2] + lg, 0.0, np.pi/2.0, np.pi/2.0]},
                       'goal': {'top':[parts_c2o[0], parts_c2o[1]+ grasp_offset, parts_c2o[2] - lg, 0.0, -np.pi/2.0, -np.pi/2.0]}}
        tfBuffer = tf2_ros.Buffer()
        listener = tf2_ros.TransformListener(tfBuffer)
        try:
            trans = tfBuffer.lookup_transform("world", input, rospy.Time(), rospy.Duration(2.0))
        except (tf2_ros.LookupException, tf2_ros.ConnectivityException, tf2_ros.ExtrapolationException):
            print("exception!! when getting parts transform")
            return None
        #print(trans)
        parts_p = np.array([trans.transform.translation.x, trans.transform.translation.y, trans.transform.translation.z])
        trans_ee2parts = np.array(ee_in_parts[partsname][orientation][:3])
        M_p2w = tfs.quaternion_matrix(np.array([trans.transform.rotation.x, 
                        trans.transform.rotation.y, trans.transform.rotation.z, trans.transform.rotation.w]))
        euler = ee_in_parts[partsname][orientation][3:6]
        M_e2p = tfs.euler_matrix(euler[0], euler[1], euler[2], 'sxyz')
        M_w2p = M_p2w.T
        M_p2e = M_e2p.T
        ee_p = parts_p + np.dot(M_p2w[:3,:3],trans_ee2parts)
        ee_r = np.dot(M_p2e[:3,:3], M_w2p[:3,:3])
        M_ee = np.identity(4)
        M_ee[:3,:3] = ee_r.T
        ee_quat = tfs.quaternion_from_matrix(M_ee)

        wp = Pose()
        wp.position.x = ee_p[0]
        wp.position.y = ee_p[1]
        wp.position.z = ee_p[2]
        wp.orientation.x = ee_quat[0]
        wp.orientation.y = ee_quat[1]
        wp.orientation.z = ee_quat[2]
        wp.orientation.w = ee_quat[3]

        return wp

    def GetPartsPose(self, tfs):
        for tf in tfs.transforms:
            if tf.header.frame_id == "world" and tf.child_frame_id.split('/')[0]=='poserbpf':
                self.parts_detect.add(tf.child_frame_id)

    def execute_trajectory(self, wps):
        """
            Executing a list of wp
        """
        waypoints = []
        for wp in wps:
            waypoints.append(wp)
        rospy.wait_for_service("ur_control_wrapper/follow_trajectory")
        set_trajectory = rospy.ServiceProxy("ur_control_wrapper/follow_trajectory", SetTrajectory)
        try:
            response = set_trajectory(waypoints)
            final_pose = response.final_pose
            is_reached = response.is_reached
        except ROSInterruptException as exc:
            print "Service did not process request: " + str(exc)  

    def execute_joints(self, joints_wp):
        """
            Execuring a wp in joints angle format
        """
        rospy.wait_for_service("ur_control_wrapper/set_joints")
        set_joints = rospy.ServiceProxy("ur_control_wrapper/set_joints", SetJoints)
        joints = JointState()
        joints.name = ["shoulder_pan_joint", "shoulder_lift_joint", "elbow_joint", "wrist_1_joint", "wrist_2_joint", "wrist_3_joint"]
        joints.position = joints_wp
        try:
            response = set_joints(joints)
        except rospy.ServiceException as exc:
            print "Service did not process request: " + str(exc)

    def pick_and_place(self):
        """
            This function trys to implement the picking and place task repeatedly with a simple vertical movement.
            Gripper motion is also involved into the loop.
        """
        current_pose = self.get_pose()

        rospy.wait_for_service("ur_control_wrapper/get_cartesian_plan")
        get_cartesian_plan = rospy.ServiceProxy("ur_control_wrapper/get_cartesian_plan", GetCartesianPlan)

        rospy.wait_for_service("ur_control_wrapper/execute_cartesian_plan")
        execute_cartesian_plan = rospy.ServiceProxy("ur_control_wrapper/execute_cartesian_plan", ExecuteCartesianPlan)            
        
        waypoints = []
        waypoints.append(current_pose)
        delta = [0, 0, -0.2]
        wpi = Pose()
        # wpi.position.x = current_pose.position.x + delta[0]
        # wpi.position.y = current_pose.position.y + delta[1]
        # wpi.position.z = current_pose.position.z + delta[2]
        # wpi.orientation.x = current_pose.orientation.x
        # wpi.orientation.y = current_pose.orientation.y
        # wpi.orientation.z = current_pose.orientation.z
        # wpi.orientation.w = current_pose.orientation.w

        # wpi.position.x = -0.616089436615
        # wpi.position.y = 0.109872737942
        # wpi.position.z = 0.003066135493649
        # wpi.orientation.x = -0.00944544691936
        # wpi.orientation.y = 0.726030962968
        # wpi.orientation.z = 0.0267542016121
        # wpi.orientation.w = 0.687076441919
        # waypoints.append(wpi)
        quat = tfs.quaternion_from_euler(0.0, np.pi/2.0, 0.0)
        wpi.position.x = -0.6
        wpi.position.y = 0.12
        wpi.position.z = 0.005
        wpi.orientation.x = quat[0]
        wpi.orientation.y = quat[1]
        wpi.orientation.z = quat[2]
        wpi.orientation.w = quat[3]
        waypoints.append(wpi)

        next_path = [waypoints[1]]
        try:
            response = get_cartesian_plan(next_path)
            plan1 = response.plan
            is_solved = response.is_solved
        except ROSInterruptException as exc:
            print "Service did not process request: " + str(exc)

        try:
            response = execute_cartesian_plan(plan1)
            final_pose = response.final_pose
            is_reached = response.is_reached
        except ROSInterruptException as exc:
            print "Service did not process request: " + str(exc)   
        
        self.gripper_pub.publish("227")
        rospy.sleep(2.5)
        
        next_path = waypoints
        try:
            response = get_cartesian_plan(next_path)
            plan2 = response.plan
            is_solved = response.is_solved
        except ROSInterruptException as exc:
            print "Service did not process request: " + str(exc)

        for iter in range(3):
            try:
                response = execute_cartesian_plan(plan2)
                final_pose = response.final_pose
                is_reached = response.is_reached
            except ROSInterruptException as exc:
                print "Service did not process request: " + str(exc)
            
            if iter % 2 == 0:
                self.gripper_pub.publish("150")
            else:
                self.gripper_pub.publish("227")
            rospy.sleep(1.3)

        next_path = [waypoints[0]]
        try:
            response = get_cartesian_plan(next_path)
            plan3 = response.plan
            is_solved = response.is_solved
        except ROSInterruptException as exc:
            print "Service did not process request: " + str(exc)

        try:
            response = execute_cartesian_plan(plan3)
            final_pose = response.final_pose
            is_reached = response.is_reached
        except ROSInterruptException as exc:
            print "Service did not process request: " + str(exc)  
        self.gripper_pub.publish("go")

    def add_box(self):
        rospy.wait_for_service("ur_control_wrapper/add_object")
        add_box = rospy.ServiceProxy("ur_control_wrapper/add_object", AddObject)
        try:
            name = "box_object"
            pose = Pose(Point(-0.54, 0.34, 0.025), Quaternion(0.0, 0.0, 0.0, 1.0))
            size = Vector3(0.4, 0.20, 0.05)
            object_type = AddObjectRequest.TYPE_BOX
            response = add_box(name, pose, size, AddObjectRequest.TYPE_BOX).is_success
        except rospy.ServiceException as exc:
            print "Service did not process request: " + str(exc)

        try:
            name = "box_object2"
            pose = Pose(Point(-0.54, 0.34, 0.07), Quaternion(0.0, 0.0, 0.0, 1.0))
            size = Vector3(0.4, 0.07, 0.04)
            object_type = AddObjectRequest.TYPE_BOX
            response = add_box(name, pose, size, AddObjectRequest.TYPE_BOX).is_success
        except rospy.ServiceException as exc:
            print "Service did not process request: " + str(exc)

    def add_parts(self, part_name, part_pose, part_size):
        rospy.wait_for_service("ur_control_wrapper/add_object")
        add_box = rospy.ServiceProxy("ur_control_wrapper/add_object", AddObject)
        try:
            name = part_name
            pose = Pose(Point(part_pose[0],part_pose[1], part_pose[2]), Quaternion(part_pose[3], part_pose[4], part_pose[5], part_pose[6]))
            size = Vector3(part_size[0], part_size[1], part_size[2])
            object_type = AddObjectRequest.TYPE_BOX
            response = add_box(name, pose, size, AddObjectRequest.TYPE_BOX).is_success
        except rospy.ServiceException as exc:
            print "Service did not process request: " + str(exc)
    
    def attach_box(self):
        print "wait for service"
        rospy.wait_for_service("ur_control_wrapper/attach_object")
        print "service found"
        attach_box = rospy.ServiceProxy("ur_control_wrapper/attach_object", AttachObject)
        try:
            name = "box_tool"
            pose = Pose(Point(-0.34, -0.0075, -0.023), Quaternion(0.0, 0.0, 0.0, 1.0))
            size = Vector3(0.02, 0.02, 0.02)
            object_type = AttachObjectRequest.TYPE_BOX
            response = attach_box(name, pose, size, object_type).is_success
            if response:
                print "successfully attached!"
            else:
                print "did not attach successfully"
        except rospy.ServiceException as exc:
            print "Service did not process request: " + str(exc)
    
    def detach_box(self):
        rospy.wait_for_service("ur_control_wrapper/detach_object")
        detach_box = rospy.ServiceProxy("ur_control_wrapper/detach_object", DetachObject)
        try:
            name = "box_tool"
            response = detach_box(name, True).is_success
        except rospy.ServiceException as exc:
            print "Service did not process request: " + str(exc)
    
    def remove_box(self):
        rospy.wait_for_service("ur_control_wrapper/remove_object")
        remove_box = rospy.ServiceProxy("ur_control_wrapper/remove_object", RemoveObject)
        try:
            name = "box_object"
            response = remove_box(name).is_success
        except rospy.ServiceException as exc:
            print "Service did not process request: " + str(exc)

    def remove_parts(self, part_name):
        rospy.wait_for_service("ur_control_wrapper/remove_object")
        remove_box = rospy.ServiceProxy("ur_control_wrapper/remove_object", RemoveObject)
        try:
            name = part_name
            response = remove_box(name).is_success
        except rospy.ServiceException as exc:
            print "Service did not process request: " + str(exc)
        
    def run(self):
        while not rospy.is_shutdown():
            print "====================================================="
            command_input = raw_input("Freedrive: fs(start), fe(end) \nGripper: grip \
            \nConnect: c(connect) \nGet End Effector Pose: ep \nGet joint angles: ja \
            \nGo to Default Position: d \nWaypoints following: wp \nPick and place test: pp \nSet joints: sj \
            \nTest fake parts pose estimation: tee;\nQ4 grasping: gq4; \nassembly tutorial: at; \n reset fixture robot: rf; \nGo to align Position: da \nGo to align pick Position: dp \n  \
            \nMove arm: x+/-#, y+/-#, z+/-#, r(roll)+/-#, p(pitch)+/-#, w(yaw)+/-# \n")
            if command_input == "fs":
                self.free_drive_pub.publish(True)
            elif command_input == "fe":
                self.free_drive_pub.publish(False)
            elif command_input == "grip":
                sub_cmd = raw_input("Adjust gripper position: gp+/-# \
                                    \nAdjust gripper force: gf+/- \
                                    \nAdjust gripper speed: gs+/- \
                                    \nOpen gripper: gop \nClose gripper: gcl \
                                    \nActivate gripper: gac \
                                    \nReset gripper: grs \
                                    \nNote: gp+ closes the gripper (max: 255), gp- opens the gripper (min: 0);\n")
                direction = sub_cmd
                self.control_gripper(direction)
            elif command_input == "c":
                self.connect_pub.publish(True)
            elif command_input == "ep":
                print self.get_pose()
            elif command_input == "ja":
                print self.get_angle()
            elif command_input == "d":
                self.set_default_angles()
                #self.set_default_angles(joints_angles = [0.12123775482177734, -2.2965408764281214, -1.5038652420043945, 2.22770540296521, -1.5678570906268519, 0.12182283401489258])
            elif command_input == "da":
                self.set_align_angles()
            elif command_input == "dp":
                self.set_align_pick_angles()
            elif command_input =="wp":
                self.waypoint_following()
            elif command_input == "pp":
                self.pick_and_place()
            elif command_input =="sj":
                sub_cmd = raw_input("Adjust joint by #degrees: 1+/-#; 2+/-#; 3+/-#; 4+/-#; 5+/-#; 6+/-# \n")
                direction = sub_cmd
                self.set_joints(direction)
            elif command_input == "tee":
                # sub_cmd = raw_input("parts name, orientation (top, bottom for main_shell, only top for top_shell) (comma as separator):")
                # self.test_desired_ee_wp(sub_cmd)
                for gi in range(5):
                    self.control_gripper("gs+")
                for gf in range(5):
                    self.control_gripper("gf-")

                # self.add_parts("floor", [0.0, 0.0, -0.036-0.015, 0.000, 1.000, 0.000, 0.000], [3.00, 3.00, 0.03])
                # self.add_parts("main_shell", [-0.493, 0.011, -0.035, 0.000, 1.000, 0.000, 0.000], [0.015, 0.015, 0.01])
                # self.add_parts("top_shell", [-0.493, 0.143, -0.034, 0.707, 0.707, -0.000, 0.000], [0.015, 0.015, 0.01])
                
                # Mwp.orientation.w = current_pose.orientation.w
                start = rospy.Time.now().to_sec()
                for ixd in range(50):
                    print("Run {}".format(ixd+1))
                    sub_cmd = "top_shell,top"
                    # self.remove_parts("top_shell")
                    # self.add_parts("main_shell", [-0.493, 0.011, -0.035, 0.000, 1.000, 0.000, 0.000], [0.015, 0.015, 0.01])
                    self.test_desired_ee_wp(sub_cmd)
                    
                    sub_cmd = "main_shell,top"
                    # self.remove_parts("main_shell")
                    # self.add_parts("top_shell", [-0.493, 0.143, -0.034, 0.707, 0.707, -0.000, 0.000], [0.015, 0.015, 0.01])
                    self.test_desired_ee_wp(sub_cmd)
                    end = rospy.Time.now().to_sec()
                    print("cycle time = ", (end - start)/((ixd+1)*2.0))
                    print("grasping time = ", (end - start)/((ixd+1)*2.0)/2)
                # self.remove_parts("main_shell")
                # self.remove_parts("top_shell")
                # self.remove_parts("floor")

            elif command_input == "gq4":
                for gi in range(5):
                    self.control_gripper("gs+")
                # self.control_gripper("gf-")
                # self.control_gripper("gf-")
                self.set_default_angles(joints_angles = [0.12123775482177734, -2.2965408764281214, -1.5038652420043945, 2.22770540296521, -1.5678570906268519, 0.12182283401489258])
                rospy.sleep(0.7)
                start = rospy.Time.now().to_sec()
                for id in range(50):
                    print("Run {}".format(id+1))
                    self.grasping_q4()
                    end = rospy.Time.now().to_sec()
                    print("cycle time = ", (end - start)/(2.0*(id+1)))
                    print("grasping time = ", (end - start)/(2.0*(id+1))/2)

            elif command_input == "at":
                for gi in range(5):
                    self.control_gripper("gs+")
                self.set_default_angles()
                rospy.sleep(0.7)
                start = rospy.Time.now().to_sec()
                for id in range(1):
                    print("Run {}".format(id+1))
                    self.assembly_tutorial()
                    end = rospy.Time.now().to_sec()
                    print("cycle time = ", (end - start)/(2.0*(id+1)))
                    print("grasping time = ", (end - start)/(2.0*(id+1))/2)
            # two robots jointly assemble, reset robot
            elif command_input == "rf":
                for gi in range(5):
                    self.control_gripper("gs+")
                self.set_default_angles()
                rospy.sleep(0.7)
                start = rospy.Time.now().to_sec()
                for iter in range(1):
                    print("Run {}".format(iter+1))
                    loop_start = rospy.Time.now().to_sec()

                    self.enter_and_close("main_shell,left")
                    # self.execute_joints([2.8517298698425293, -2.3931437931456507, -1.299189567565918, 2.121654911632202, -1.5737088362323206, -0.2911914030658167])
                    # self.execute_joints([2.8518857955932617, -2.4190155468382777, -1.3091959953308105, 2.1583944994160156, -1.5730732123004358, -0.29132301012148076])
                    
                    # for gi in range(5):
                    #     self.control_gripper("gs-")
                    # grip_num = "246"
                    # self.gripper_pub.publish(grip_num)
                    # rospy.sleep(2.0)
                    # for gi in range(3):
                    #     self.control_gripper("gs+")

                    self.execute_joints([0.0 + np.pi, -2.0 * np.pi / 3, -np.pi / 3.0, np.pi / 2.0, -np.pi / 2.0, 0.0])
                    self.execute_joints([3.325911521911621, -2.8223968944945277, -1.0041155815124512, 5.396614300995626, -1.5681422392474573, 2.90586519241333])
                    half_time = rospy.Time.now().to_sec()
                    print("arrive fixture time = ", half_time - loop_start)
                    # rospy.sleep(45.0)
            # two robots jointly assemble, place the assembled part        
            elif command_input == "rfp":
                self.execute_joints([0.0 + np.pi, -2.0 * np.pi / 3, -np.pi / 3.0, np.pi / 2.0, -np.pi / 2.0, 0.0])
                self.execute_joints([3.8037590980529785, -2.3307811222472132, -1.3764166831970215, 2.1317543226429443, -1.566247288380758, 0.6586318016052246])
                for gi in range(5):
                    self.control_gripper("gs+")
                self.gripper_pub.publish("200")
                rospy.sleep(1.0)
                for gi in range(5):
                    self.control_gripper("gs-")
                end = rospy.Time.now().to_sec()
                print("cycle time = ", (end - start)/(2.0*(iter+1)))
                print("grasping time = ", (end - start)/(2.0*(iter+1))/2)
                self.set_default_angles()
            # vision based grasping test
            elif command_input == "tf":
                self.execute_joints([0.7852187156677246, -2.094323297540182, -1.047079086303711, 1.570835753078125, -1.5708306471454065, 0.00015783309936523438])
                for gi in range(5):
                    self.control_gripper("gs+")
                for gf in range(10):
                    self.control_gripper("gf-")

                # print(self.parts_detect)
                # temp = self.parts_detect.copy()
                time_all = []
                grasp_num_all = []
                run_round = 2
                max_parts_num = 5
                for iter in range(run_round):
                    while not self.reset_robot_flag:
                        print('Waiting for perception ...')
                        rospy.sleep(0.2)
                    rospy.sleep(2.0)
                    print('Perception is ready, start grasping ...')
                    print('iter = ', iter + 1)
                    grasp_num = 0
                    temp = self.parts_detect.copy()
                    parts_record = self.parts_detect.copy()
                    temp_L = len(parts_record)
                    print('parts number =', temp_L)
                    if temp_L > max_parts_num:
                        max_parts_num = temp_L
                    print(parts_record)
                    start = rospy.Time.now().to_sec()
                    for part in sorted(temp):
                        # print(part)
                        # if part.split("/")[1] == '0':
                        wp = self.get_wp_from_part(part)
                        if wp is not None:
                            self.test_desired_ee_wp(wp)
                            grasp_num += 1
                            parts_record.remove(part)
                            rospy.sleep(0.2)
                    end = rospy.Time.now().to_sec()
                    time_iter = (end - start)/5.0
                    time_all.append(time_iter)
                    grasp_num_all.append(grasp_num)
                    self.execute_joints([0.7818083763122559, -2.1127873859801234, -1.3214406967163086, 1.863279028529785, -1.5707820097552698, 0.432767391204834])
                    
                    self.reset_robot_flag = False
                    reset_vision_flag = Bool()
                    reset_vision_flag.data = True
                    self.reset_vision_pub.publish(reset_vision_flag)
                    self.parts_detect.clear()
                    print('remaining parts number =', len(parts_record))
                    print('parts number in container =', len(self.parts_detect))
                print('grasp time cycle = ', np.mean(np.array(time_all))/2.0)
                grasp_num_all_np = np.array(grasp_num_all)
                print('average grasp = ', np.mean(grasp_num_all_np))
                self.set_default_angles()

                bins = np.arange(5+1)
                fig = plt.figure()
                plt.hist(grasp_num_all_np, bins)
                plt.xlabel('Grasp times')
                plt.ylabel('Repeat')
                plt.title('Grasp times distribution')
                plt.savefig('grasp_times_{}.png'.format(rospy.Time.now().to_sec()))

            elif command_input == "ab":
                self.add_box()
            elif command_input == "atb":
                self.attach_box()
            elif command_input == "db":
                self.detach_box()
            elif command_input == "rb":
                self.remove_box()
            else: # move arm
                direction = command_input
                self.move_arm(direction)

if __name__ == '__main__':
    try:
        rospy.init_node('ur_control_wrapper_demo_mode', anonymous=True)

        demo = Demo()

        demo.run()
    except rospy.ROSInterruptException:
        pass
