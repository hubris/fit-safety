#!/usr/bin/env python

import rospy
import numpy as np
from std_srvs.srv import Trigger
from std_msgs.msg import String, Bool, Float32MultiArray
from sensor_msgs.msg import JointState
from geometry_msgs.msg import Pose, Point, Quaternion, Vector3
from ur_control_wrapper.srv import SetPose
from ur_control_wrapper.srv import GetPose
from ur_control_wrapper.srv import GetJoints
from ur_control_wrapper.srv import SetJoints
from ur_control_wrapper.srv import SetTrajectory
from ur_control_wrapper.srv import GetCartesianPlan
from ur_control_wrapper.srv import ExecuteCartesianPlan
from ur_control_wrapper.srv import AddObject, AddObjectRequest
from ur_control_wrapper.srv import AttachObject, AttachObjectRequest
from ur_control_wrapper.srv import DetachObject, DetachObjectRequest
from ur_control_wrapper.srv import RemoveObject, RemoveObjectRequest

from tf import transformations as tfs

from ur_control_wrapper.srv import ExecutePause ### zhe Q3 ###
from ur_kinematics import all_close
from copy import deepcopy
from ur_control_wrapper.msg import StringStamped, HumanIntervention
import time

class Demo:
    def __init__(self):
        self.free_drive_pub = rospy.Publisher("ur_control_wrapper/enable_freedrive", Bool, queue_size=10)
        self.gripper_pub = rospy.Publisher("ur_control_wrapper/gripper", String, queue_size=10)
        self.connect_pub = rospy.Publisher("ur_control_wrapper/connect", Bool, queue_size=10)
        ### zhe Q3 starts ###
        self.create_pose_goals()
        self.machine_state = 999 # 'idle', or initial state
        self.vision_pause_flag = False # when paused, state remains the same. State refers to the goal you want to reach.
        self.contact_pause_flag = False
        self.log_pub = rospy.Publisher('hri/log', StringStamped, queue_size=10)
        ### zhe Q3 ends ###

        ### zhe Q6 starts ###
        self.human_intention = 2 # valid 1 or 2
        self.intention_machine_states = [1,4] # correspond to pose_goals
        # self.prob_low_threshold = 0.85
        # self.prob_high_threshold = 0.9
        self.prob_low_threshold = 0.7
        self.prob_high_threshold = 0.8
        rospy.Subscriber("hri/human_intention_prob_dist", Float32MultiArray, self.callback_human_intention, queue_size=1)
        # rospy.Subscriber("hri/human_intention_prob_dist", Float32MultiArray, self.callback_human_intention_conservative, queue_size=1)
        
        ### zhe Q6 ends ###

    def create_pose_goals(self):
        # to read pose, rostopic echo /ee_pose_jenny
        ### zhe Q4 ###
        self.default_pose = Pose()
        self.default_pose.position.x = -0.704400473749
        self.default_pose.position.y = 0.133289038688
        self.default_pose.position.z = 0.3#0.430970688155
        self.default_pose.orientation.x = 0.707096830336
        self.default_pose.orientation.y = 1.52220433182e-05
        self.default_pose.orientation.z = -0.707116731632
        self.default_pose.orientation.w = 1.19655829861e-05

        self.prepared_pose_part_0 = deepcopy(self.default_pose)
        self.prepared_pose_part_0.position.x = -0.499307140242
        self.prepared_pose_part_0.position.y = 0.128263724425

        self.pose_part_0 = deepcopy(self.prepared_pose_part_0)
        self.pose_part_0.position.z = 0.17

        self.prepared_pose_part_1 = deepcopy(self.default_pose)
        self.prepared_pose_part_1.position.x = -0.794354203331
        self.prepared_pose_part_1.position.y = 0.11333854571

        self.pose_part_1 = deepcopy(self.prepared_pose_part_1)
        self.pose_part_1.position.z = 0.17
        
        self.pose_goals = []
        # self.pose_goals.append(self.default_pose)
        # self.pose_goals.append(self.below_default_pose)
        self.pose_goals.append(self.prepared_pose_part_0)
        self.pose_goals.append(self.pose_part_0)
        self.pose_goals.append(self.prepared_pose_part_0)
        self.pose_goals.append(self.prepared_pose_part_1)
        self.pose_goals.append(self.pose_part_1)
        self.pose_goals.append(self.prepared_pose_part_1)
        return
    
    def zero_force_torque_sensor(self):
        rospy.wait_for_service("ur_hardware_interface/zero_ftsensor")
        zero_ft_sensor = rospy.ServiceProxy("ur_hardware_interface/zero_ftsensor", Trigger)
        response = zero_ft_sensor()
        if response.success:
            print("Force torque sensor is zeroed.")
        else:
            print("Failed to zero force torque sensor.")
        return response.success

    def execute_state_task(self, machine_state):
        ### zhe Q3 ###
        self.machine_state = machine_state # 0-1
        curr_pose_goal = self.pose_goals[self.machine_state]
        if isinstance(curr_pose_goal, Pose):
            
            # rospy.wait_for_service("ur_control_wrapper/set_pose_asynchronous")
            # set_pose = rospy.ServiceProxy("ur_control_wrapper/set_pose_asynchronous", SetPose)
            # try:
            #     response = set_pose(curr_pose_goal)
            #     pose = response.response_pose
            #     is_reached = response.is_reached
            # except rospy.ServiceException as exc:
            #     print "Service did not process request: " + str(exc)

            rospy.wait_for_service("ur_control_wrapper/get_cartesian_plan")
            get_cartesian_plan = rospy.ServiceProxy("ur_control_wrapper/get_cartesian_plan", GetCartesianPlan)

            rospy.wait_for_service("ur_control_wrapper/execute_cartesian_plan_asynchronous")
            execute_cartesian_plan_asynchronous = rospy.ServiceProxy("ur_control_wrapper/execute_cartesian_plan_asynchronous", ExecuteCartesianPlan)            
            
            next_path = [curr_pose_goal]
            try:
                response = get_cartesian_plan(next_path)
                plan1 = response.plan
                is_solved = response.is_solved
            except rospy.ServiceException as exc:
                print "Service did not process request: " + str(exc)

            try:
                response = execute_cartesian_plan_asynchronous(plan1)
                # dummy outputs for asynchronous execution
                _ = response.final_pose
                _ = response.is_reached
            except rospy.ServiceException as exc:
                print "Service did not process request: " + str(exc)   
            return
        elif isinstance(curr_pose_goal, str):
            # gripper
            self.gripper_pub.publish(curr_pose_goal)
            rospy.sleep(2.0)
            return
        else:
            raise RuntimeError("curr_pose_goal type is not supported.")

    def callback_state_task_completion(self, data):
        ### zhe Q3 ###
        # data is useless. This is like a clock.
        pause_flag = self.vision_pause_flag or self.contact_pause_flag
        if pause_flag:
            rospy.wait_for_service("ur_control_wrapper/execute_pause")
            execute_pause = rospy.ServiceProxy("ur_control_wrapper/execute_pause", ExecutePause)
            try:
                execute_pause()
            except rospy.ServiceException as exc:
                print "Service did not process request: " + str(exc)
            return
        if 0 <= self.machine_state < len(self.pose_goals): # not invalid initial state 999
            curr_pose_goal = self.pose_goals[self.machine_state]
            if isinstance(curr_pose_goal, Pose):
                rospy.wait_for_service("ur_control_wrapper/get_pose")
                get_current_pose = rospy.ServiceProxy("ur_control_wrapper/get_pose", GetPose)
                try:
                    current_pose = get_current_pose().pose
                except rospy.ServiceException as exc:
                    print "Service did not process request: " + str(exc)
                # print(all_close(curr_pose_goal, current_pose, 0.005))
                # if all_close(curr_pose_goal, current_pose, 0.005) and not pause_flag: # reached the goal
                if all_close(curr_pose_goal, current_pose, 0.01) and not pause_flag: # reached the goal
                    next_state = list(range(1,len(self.pose_goals)))+[0]  # [1,2,...,state_n, 0]
                    machine_state = next_state[self.machine_state]
                    if self.human_intention != 2 and machine_state == self.intention_machine_states[self.human_intention]:
                        machine_state = next_state[machine_state]
                    print("go to state "+str(machine_state))
                    self.execute_state_task(machine_state)
            elif isinstance(curr_pose_goal, str):
                next_state = list(range(1,len(self.pose_goals)))+[0]  # [1,2,...,state_n, 0]
                machine_state = next_state[self.machine_state]
                if self.human_intention != 2 and machine_state == self.intention_machine_states[self.human_intention]:
                        machine_state = next_state[machine_state]
                print("go to state "+str(machine_state))
                self.execute_state_task(machine_state)
            else:
                raise RuntimeError("curr_pose_goal type is not supported.")
        return

    def callback_human_intention_conservative(self, data):
        intention_prob_dist = data.data
        max_prob_intention = np.argmax(intention_prob_dist)
        # if intention_prob_dist[max_prob_intention] < self.prob_low_threshold: # < 0.85
        #     self.human_intention = 2
        # if intention_prob_dist[max_prob_intention] > self.prob_high_threshold: # > 0.9
        self.human_intention = max_prob_intention
        
        if self.human_intention != 2 and self.machine_state == self.intention_machine_states[self.human_intention]:
            rospy.wait_for_service("ur_control_wrapper/execute_pause")
            execute_pause = rospy.ServiceProxy("ur_control_wrapper/execute_pause", ExecutePause)
            try:
                execute_pause()
            except rospy.ServiceException as exc:
                print "Service did not process request: " + str(exc)
            next_state = list(range(1,len(self.pose_goals)))+[0]  # [1,2,...,state_n, 0]
            machine_state = next_state[self.machine_state]
            self.execute_state_task(machine_state)


    def callback_human_intention(self, data):
        start_reaction_time = time.time()
        intention_prob_dist = data.data
        max_prob_intention = np.argmax(intention_prob_dist)
        if intention_prob_dist[max_prob_intention] < self.prob_low_threshold: # < 0.85
            self.human_intention = 2
        if intention_prob_dist[max_prob_intention] > self.prob_high_threshold: # > 0.9
            self.human_intention = max_prob_intention
        
        if self.human_intention != 2 and self.machine_state == self.intention_machine_states[self.human_intention]:
            rospy.wait_for_service("ur_control_wrapper/execute_pause")
            execute_pause = rospy.ServiceProxy("ur_control_wrapper/execute_pause", ExecutePause)
            try:
                execute_pause()
            except rospy.ServiceException as exc:
                print "Service did not process request: " + str(exc)
            reaction_time = time.time()-start_reaction_time
            print("reaction time: {0:.6f} sec".format(reaction_time))
            next_state = list(range(1,len(self.pose_goals)))+[0]  # [1,2,...,state_n, 0]
            machine_state = next_state[self.machine_state]
            self.execute_state_task(machine_state)

        # if data.data and data.source == "wrench":
        #     print('Wrench warning.')
        #     rospy.wait_for_service("ur_control_wrapper/execute_pause")
        #     execute_pause = rospy.ServiceProxy("ur_control_wrapper/execute_pause", ExecutePause)
        #     try:
        #         execute_pause()
        #     except rospy.ServiceException as exc:
        #         print "Service did not process request: " + str(exc)
        #     # self.machine_state = 0 # default pose
        #     next_state = list(range(1,len(self.pose_goals)))+[0]  # [1,2,...,state_n, 0]
        #     machine_state = next_state[self.machine_state]
        #     if self.human_intention != 2 and machine_state == self.intention_machine_states[self.human_intention]:
        #         machine_state = next_state[machine_state]
        #     self.execute_state_task(machine_state)


    def callback_human_intervention(self, data):
        ### zhe Q3 ###
        # data is used as a trigger.
        # True means that human intruded and keeps in.
        # False means that human left and keeps out.
        # print('Here is data', data.data)
        # if data.data and not self.pause_flag:
        if data.data and data.source == "wrench":
            print('Wrench warning.')
            rospy.wait_for_service("ur_control_wrapper/execute_pause")
            execute_pause = rospy.ServiceProxy("ur_control_wrapper/execute_pause", ExecutePause)
            try:
                execute_pause()
            except rospy.ServiceException as exc:
                print "Service did not process request: " + str(exc)
            # self.machine_state = 0 # default pose
            next_state = list(range(1,len(self.pose_goals)))+[0]  # [1,2,...,state_n, 0]
            machine_state = next_state[self.machine_state]
            if self.human_intention != 2 and machine_state == self.intention_machine_states[self.human_intention]:
                machine_state = next_state[machine_state]
            self.execute_state_task(machine_state)
            # self.machine_state = next_state[self.machine_state]
            # if self.human_intention != 2 and machine_state == self.intention_machine_states[self.human_intention]:
            #     machine_state = next_state[machine_state]
            # self.execute_state_task(self.machine_state)
            return
            #! or go to next state
            #! next_state = list(range(1,len(self.pose_goals)))+[0]  # [1,2,...,state_n, 0]
            #! self.machine_state = next_state[self.machine_state]
            #! self.execute_state_task(self.machine_state)

        prev_pause_flag = self.vision_pause_flag or self.contact_pause_flag
        if data.source == "vision":
            self.vision_pause_flag = data.data
        if data.source == "contact":
            self.contact_pause_flag = data.data
        curr_pause_flag = self.vision_pause_flag or self.contact_pause_flag
        
        if curr_pause_flag and not prev_pause_flag:
            print('Pause.')
            rospy.wait_for_service("ur_control_wrapper/execute_pause")
            execute_pause = rospy.ServiceProxy("ur_control_wrapper/execute_pause", ExecutePause)
            try:
                execute_pause()
            except rospy.ServiceException as exc:
                print "Service did not process request: " + str(exc)
            log_msg = StringStamped()
            log_msg.header.stamp = rospy.Time.now()
            log_msg.data = "Robot pause executed"
            self.log_pub.publish(log_msg)
        
        elif not curr_pause_flag and prev_pause_flag:
            print('Resume.')
            curr_pose_goal = self.pose_goals[self.machine_state]

            rospy.wait_for_service("ur_control_wrapper/get_pose")
            get_current_pose = rospy.ServiceProxy("ur_control_wrapper/get_pose", GetPose)
            try:
                current_pose = get_current_pose().pose
            except rospy.ServiceException as exc:
                print "Service did not process request: " + str(exc)
            if not all_close(curr_pose_goal, current_pose, 0.005): # reached the goal
                if self.human_intention != 2 and self.machine_state == self.intention_machine_states[self.human_intention]:
                    next_state = list(range(1,len(self.pose_goals)))+[0]  # [1,2,...,state_n, 0]
                    machine_state = next_state[self.machine_state]
                    self.execute_state_task(machine_state)
                else:
                    # resume
                    self.execute_state_task(self.machine_state)
            
                log_msg = StringStamped()
                log_msg.header.stamp = rospy.Time.now()
                log_msg.data = "Robot resume executed"
                self.log_pub.publish(log_msg)
            # ! zhe debug intervention does not consider gripper yet.


    def get_pose(self):
        rospy.wait_for_service("ur_control_wrapper/get_pose")
        get_current_pose = rospy.ServiceProxy("ur_control_wrapper/get_pose", GetPose)
        current_pose = None
        try:
            current_pose = get_current_pose().pose
        except rospy.ServiceException as exc:
            print "Service did not process request: " + str(exc) 

        return current_pose
    
    def get_angle(self):
        rospy.wait_for_service("ur_control_wrapper/get_joints")
        get_current_joints = rospy.ServiceProxy("ur_control_wrapper/get_joints", GetJoints)
        current_joints = None
        try:
            current_joints = get_current_joints().joints
        except rospy.ServiceException as exc:
            print "Service did not process request: " + str(exc) 
        return current_joints
    
    def set_default_angles(self):
        rospy.wait_for_service("ur_control_wrapper/set_joints")
        set_joints = rospy.ServiceProxy("ur_control_wrapper/set_joints", SetJoints)
        joints = JointState()
        #joints.name = ["elbow_joint", "shoulder_lift_joint", "shoulder_pan_joint", "wrist_1_joint", "wrist_2_joint", "wrist_3_joint"]
        #joints.position = [-np.pi / 3.0, -2.0 * np.pi / 3, 0.0, np.pi * 1.0 / 2.0, -np.pi / 2.0, 0.0]
        joints.name = ["shoulder_pan_joint", "shoulder_lift_joint", "elbow_joint", "wrist_1_joint", "wrist_2_joint", "wrist_3_joint"]
        #joints.position = [0.023975849151611328, -2.173887868920797, -1.0581588745117188, 1.6445128160664062, -1.570674244557516, -2.2236500875294496e-05]
        # joints.position = [0.0, -2.0 * np.pi / 3, -np.pi / 3.0, np.pi / 2.0, -np.pi / 2.0, 0.0]
        joints.position = [0.0, -2.0 * np.pi / 3, -np.pi / 3.0, -np.pi / 2.0, np.pi / 2.0, 0.0]
        try:
            response = set_joints(joints)
        except rospy.ServiceException as exc:
            print "Service did not process request: " + str(exc)      

        self.gripper_pub.publish("go")

    def set_joints(self, direction):
        current_joints = self.get_angle()
        rospy.wait_for_service("ur_control_wrapper/set_joints")
        set_joints = rospy.ServiceProxy("ur_control_wrapper/set_joints", SetJoints)
        joints = JointState()
        joints.name = current_joints.name
        temp = list(current_joints.position)

        try:
            if direction == "1+":
                temp[0] += np.deg2rad(10.0)
            elif direction == "1-":
                temp[0] -= np.deg2rad(10.0)
            elif direction == "2+":
                temp[1] += np.deg2rad(10.0)
            elif direction == "2-":
                temp[1] -= np.deg2rad(10.0)
            elif direction == "3+":
                temp[2] += np.deg2rad(10.0)
            elif direction == "3-":
                temp[2] -= np.deg2rad(10.0)
            elif direction == "4+":
                temp[3] += np.deg2rad(10.0)
            elif direction == "4-":
                temp[3] -= np.deg2rad(10.0)
            elif direction == "5+":
                temp[4] += np.deg2rad(10.0)
            elif direction == "5-":
                temp[4] -= np.deg2rad(10.0)
            elif direction == "6+":
                temp[5] += np.deg2rad(45.0)
            elif direction == "6-":
                temp[5] -= np.deg2rad(45.0)
            joints.position = tuple(temp)
            response = set_joints(joints)
        except rospy.ServiceException as exc:
            print "Service did not process request: " + str(exc)

    def move_arm(self, direction):
        current_pose = self.get_pose()
        current_euler = np.rad2deg(tfs.euler_from_quaternion(np.array([current_pose.orientation.x, 
                        current_pose.orientation.y, current_pose.orientation.z, current_pose.orientation.w])))
        print("euler_before = ", current_euler)
        rospy.wait_for_service("ur_control_wrapper/set_pose")
        set_current_pose = rospy.ServiceProxy("ur_control_wrapper/set_pose", SetPose)
        try:
            if direction == "x+":
                current_pose.position.x += 0.05
            elif direction == "x-":
                current_pose.position.x -= 0.05
            elif direction == "y+":
                current_pose.position.y += 0.05
            elif direction == "y-":
                current_pose.position.y -= 0.05
            elif direction == "z+":
                current_pose.position.z += 0.05
            elif direction == "z-":
                current_pose.position.z -= 0.05

            elif direction == "roll+":
                current_euler[0] += 20.0
            elif direction == "roll-":
                current_euler[0] -= 20.0
            elif direction == "pitch+":
                current_euler[1] += 20.0
            elif direction == "pitch-":
                current_euler[1] -= 20.0
            elif direction == "yaw+":
                current_euler[2] += 20.0
            elif direction == "yaw-":
                current_euler[2] -= 20.0

            print("euler_after = ", current_euler)
            ori = np.deg2rad(current_euler)
            quat = tfs.quaternion_from_euler(ori[0], ori[1], ori[2])
            current_pose.orientation.x = quat[0]
            current_pose.orientation.y = quat[1]
            current_pose.orientation.z = quat[2]
            current_pose.orientation.w = quat[3]

            response = set_current_pose(current_pose)
            pose = response.response_pose
            is_reached = response.is_reached
        except rospy.ServiceException as exc:
            print "Service did not process request: " + str(exc)
    
    def waypoint_following(self):
        current_pose = self.get_pose()

        rospy.wait_for_service("ur_control_wrapper/follow_trajectory")
        set_trajectory = rospy.ServiceProxy("ur_control_wrapper/follow_trajectory", SetTrajectory)

        waypoints = []
        delta = [[-0.1, 0.0, 0.0], [-0.1, 0.0, -0.1], [-0.1, 0.0, 0.0], [0.0, 0.0, 0.0]]
        for wp in delta:
            wpi = Pose()
            wpi.position.x = current_pose.position.x + wp[0]
            wpi.position.y = current_pose.position.y + wp[1]
            wpi.position.z = current_pose.position.z + wp[2]
            wpi.orientation.x = current_pose.orientation.x
            wpi.orientation.y = current_pose.orientation.y
            wpi.orientation.z = current_pose.orientation.z
            wpi.orientation.w = current_pose.orientation.w
            waypoints.append(wpi)
        try:
            response = set_trajectory(waypoints)
            final_pose = response.final_pose
            is_reached = response.is_reached
        except ROSInterruptException as exc:
            print "Service did not process request: " + str(exc)

    def pick_and_place(self):
        """
            This function trys to implement the picking and place task repeatedly with a simple vertical movement.
            Gripper motion is also involved into the loop.
        """
        current_pose = self.get_pose()

        rospy.wait_for_service("ur_control_wrapper/get_cartesian_plan")
        get_cartesian_plan = rospy.ServiceProxy("ur_control_wrapper/get_cartesian_plan", GetCartesianPlan)

        rospy.wait_for_service("ur_control_wrapper/execute_cartesian_plan")
        execute_cartesian_plan = rospy.ServiceProxy("ur_control_wrapper/execute_cartesian_plan", ExecuteCartesianPlan)            
        
        waypoints = []
        waypoints.append(current_pose)
        delta = [0, 0, -0.2]
        wpi = Pose()
        # wpi.position.x = current_pose.position.x + delta[0]
        # wpi.position.y = current_pose.position.y + delta[1]
        # wpi.position.z = current_pose.position.z + delta[2]
        # wpi.orientation.x = current_pose.orientation.x
        # wpi.orientation.y = current_pose.orientation.y
        # wpi.orientation.z = current_pose.orientation.z
        # wpi.orientation.w = current_pose.orientation.w

        wpi.position.x = -0.616089436615
        wpi.position.y = 0.109872737942
        wpi.position.z = 0.000766135493649
        wpi.orientation.x = -0.00944544691936
        wpi.orientation.y = 0.726030962968
        wpi.orientation.z = 0.0267542016121
        wpi.orientation.w = 0.687076441919
        waypoints.append(wpi)

        next_path = [waypoints[1]]
        try:
            response = get_cartesian_plan(next_path)
            plan1 = response.plan
            is_solved = response.is_solved
        except ROSInterruptException as exc:
            print "Service did not process request: " + str(exc)

        try:
            response = execute_cartesian_plan(plan1)
            final_pose = response.final_pose
            is_reached = response.is_reached
        except ROSInterruptException as exc:
            print "Service did not process request: " + str(exc)   
        
        self.gripper_pub.publish("227")
        rospy.sleep(2.5)

        next_path = waypoints
        try:
            response = get_cartesian_plan(next_path)
            plan2 = response.plan
            is_solved = response.is_solved
        except ROSInterruptException as exc:
            print "Service did not process request: " + str(exc)

        for iter in range(3):
            try:
                response = execute_cartesian_plan(plan2)
                final_pose = response.final_pose
                is_reached = response.is_reached
            except ROSInterruptException as exc:
                print "Service did not process request: " + str(exc)
            
            if iter % 2 == 0:
                self.gripper_pub.publish("150")
            else:
                self.gripper_pub.publish("227")
            rospy.sleep(1.3)

        next_path = [waypoints[0]]
        try:
            response = get_cartesian_plan(next_path)
            plan3 = response.plan
            is_solved = response.is_solved
        except ROSInterruptException as exc:
            print "Service did not process request: " + str(exc)

        try:
            response = execute_cartesian_plan(plan3)
            final_pose = response.final_pose
            is_reached = response.is_reached
        except ROSInterruptException as exc:
            print "Service did not process request: " + str(exc)  
        self.gripper_pub.publish("go")
        
        
    def run(self):
        rospy.Subscriber('hri/check_task_clock', String, self.callback_state_task_completion)
        rospy.Subscriber('hri/human_intervention', HumanIntervention, self.callback_human_intervention)
        self.set_default_angles()
        self.gripper_pub.publish("gc")
        rospy.sleep(2.0)
        self.zero_force_torque_sensor()
        machine_state = 0
        print('Went to the default position. Start FSM.')
        self.execute_state_task(machine_state)
        rospy.spin()


if __name__ == '__main__':
    try:
        rospy.init_node('ur_control_wrapper_demo_mode', anonymous=True)

        demo = Demo()
        demo.run()
    except rospy.ROSInterruptException:
        pass
