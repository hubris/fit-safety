#!/usr/bin/env python

import rospy
from moveit_msgs.msg import CollisionObject
from shape_msgs.msg import SolidPrimitive
from geometry_msgs.msg import Pose
import moveit_commander

def move_box(exist, x,y,z, objName):
    collision_object_msg = CollisionObject()

    
    collision_object_msg.id = objName
    box_pose = Pose()
    box_pose.position.x = x
    box_pose.position.y = y
    box_pose.position.z = z
    box_pose.orientation.w = 1.0

    collision_object_msg.primitive_poses = [box_pose]
    collision_object_msg.header.stamp = rospy.Time.now()
    collision_object_msg.header.frame_id = 'base_link'


    if not exist:
        # define the box
        # add a box into the scene
        collision_object_msg.operation = collision_object_msg.ADD
        primitive = SolidPrimitive()
        primitive.type = primitive.BOX
        primitive.dimensions = [0.03, 0.03, 0.03]
        collision_object_msg.primitives = [primitive]

        while objName not in scene.get_known_object_names():
            collision_object_pub.publish(collision_object_msg)
    else:
        collision_object_msg.operation = collision_object_msg.MOVE
        collision_object_pub.publish(collision_object_msg)

if __name__ == "__main__":
    rospy.init_node('move_box_node',anonymous=True)
    collision_object_pub = rospy.Publisher('/collision_object', CollisionObject, queue_size=5)
    scene = moveit_commander.PlanningSceneInterface()
    start_point=0.0 # -0.4
    end_point=0.4
    # insert the box
    # xyz_box = [0.2, 0.8500000000000001, 0.6] #[0.2, 0.3, 0.6]
    # xyz_box = [0.57, -0.15, 0.23] #[0.2, 0.3, 0.6]
    # ('box position', [-0.53, 0.2, 0.18])
    # xyz_box = [-0.53, 0.25, 0.38]
    xyz_box = [-0.63, 0.15, 0.33]
    # [0.0004353523254394531, -2.149865289727682, -0.9598836898803711, 1.5396653848835449, -1.5693438688861292, 0.0007815361022949219]
    # xyz_box = [-0.53, 0.2, 0.18] # [-0.48000000000000004, 0.2, 0.27999999999999997]
    # [-0.48000000000000004, 0.25, 0.27999999999999997]
    # x: 0.578462627859
    # #   y: 0.000624385599296
    # #   z: 0.231755151796
    # move_box(exist=False, x=0.6, y=start_point, z=1.1, objName='box')
    move_box(exist=False, x=xyz_box[0], y=xyz_box[1], z=xyz_box[2], objName='box')
    # rospy.sleep(2)

    # while not rospy.is_shutdown():
    #         print "====================================================="
    #         command_input = raw_input("Freedrive: fs(start), fe(end) \nGripper: grip \
    #         \nConnect: c(connect) \nGet End Effector Pose: ep \nGet joint angles: ja \
    #         \nGo to align position: da \nGo to aligned picking position: dp \nGo to assembly angles: aa \
    #         \nGo to Default Position: d \nWaypoints following: wp \nPick and place test: pp \nSet joints: sj \
    #         \nMove arm: x+/-#, y+/-#, z+/-#, r(roll)+/-#, p(pitch)+/-#, w(yaw)+/-# \n")
    #         if command_input == "fs":
    #             self.free_drive_pub.publish(True)
    #         elif command_input == "fe":
    #             self.free_drive_pub.publish(False)

    while not rospy.is_shutdown():
        command_input = raw_input('w: up; s: down; a: left; d: right; z: forward, x: backward\n')
        if command_input == "w":
            xyz_box[2] = xyz_box[2] + 0.05
            move_box(exist=True, x=xyz_box[0], y=xyz_box[1], z=xyz_box[2], objName='box')
        elif command_input == "s":
            xyz_box[2] = xyz_box[2] - 0.05
            move_box(exist=True, x=xyz_box[0], y=xyz_box[1], z=xyz_box[2], objName='box')
        elif command_input == "a":
            xyz_box[0] = xyz_box[0] + 0.05
            move_box(exist=True, x=xyz_box[0], y=xyz_box[1], z=xyz_box[2], objName='box')
        elif command_input == "d":
            xyz_box[0] = xyz_box[0] - 0.05
            move_box(exist=True, x=xyz_box[0], y=xyz_box[1], z=xyz_box[2], objName='box')
        elif command_input == "z":
            xyz_box[1] = xyz_box[1] + 0.05
            move_box(exist=True, x=xyz_box[0], y=xyz_box[1], z=xyz_box[2], objName='box')
        elif command_input == "x":
            xyz_box[1] = xyz_box[1] - 0.05
            move_box(exist=True, x=xyz_box[0], y=xyz_box[1], z=xyz_box[2], objName='box')
        print('box position', xyz_box)
    # move the box
    # time_interval=0.1
    # counter=0
    # positions=[-0.4, -0.3, -0.2, -0.1,  0. ,  0.1,  0.2,  0.3,  0.4]
    # while not rospy.is_shutdown():
    #     for i in range(len(positions)):
    #         rospy.sleep(time_interval)
    #         move_box(exist=True, x=0.0, y=positions[i], z=1.0, objName='box') #x=0.6, y=positions[i], z=0.9 
    #     positions.reverse()

    # 2.8133392333984375e-05, -2.150069376031393, -0.9599428176879883, 1.5392810541340332, -1.5708063284503382, 4.9591064453125e-05