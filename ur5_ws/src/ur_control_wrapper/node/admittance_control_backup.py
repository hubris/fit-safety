#!/usr/bin/env python
import rospy
import numpy as np
from std_msgs.msg import String, Bool, Float64MultiArray
from std_srvs.srv import Trigger, SetBool
from sensor_msgs.msg import JointState, Image, CameraInfo
from geometry_msgs.msg import Pose, Point, Quaternion, Vector3, WrenchStamped
from controller_manager_msgs.srv import SwitchController, SwitchControllerRequest, ListControllers
from ur_control_wrapper.srv import SetPose
from ur_control_wrapper.srv import GetPose
from ur_control_wrapper.srv import GetJoints
from ur_control_wrapper.srv import SetJoints
from ur_control_wrapper.srv import SetTrajectory
from ur_control_wrapper.srv import GetCartesianPlan
from ur_control_wrapper.srv import ExecuteCartesianPlan
from ur_control_wrapper.srv import AddObject, AddObjectRequest
from ur_control_wrapper.srv import AttachObject, AttachObjectRequest
from ur_control_wrapper.srv import DetachObject, DetachObjectRequest
from ur_control_wrapper.srv import RemoveObject, RemoveObjectRequest
from ur_control_wrapper.srv import InverseKinematics, InverseKinematicsRequest
from gripper_controller_jenny import GripperHande as gripper
from status_listener import StatusListener
from cv_bridge import CvBridge, CvBridgeError
import cv2
import time

from collections import deque

from tf import transformations as tfs

import tf
from tf import TransformListener

def qv_mult(q1, v1):
    # v1 = tf.transformations.unit_vector(v1)
    q2 = list(v1)
    q2.append(0.0)
    return tf.transformations.quaternion_multiply(
        tf.transformations.quaternion_multiply(q1, q2), 
        tf.transformations.quaternion_conjugate(q1)
    )[:3]

class AdmittanceControl:
    def __init__(self):
        rospy.Service('ur_control_wrapper/switch_admittance_control', SetBool, self.switch_admittance_control)
        rospy.Service('ur_control_wrapper/check_admittance_control', Trigger, self.check_admittance_control)
        self.admittance_control_mode = False
        self.joint_velocity_pub = rospy.Publisher('/joint_group_vel_controller/command', Float64MultiArray, queue_size=10)
        self.wrench_sub = rospy.Subscriber('/wrench', WrenchStamped, self.wrench_callback, queue_size=1)
        self.pose_vel_scale = 0.08
        self.force_vel_scale = 1./10
        self.force_threshold = 1.
        self.zero_force_torque_sensor()
        self.tf = TransformListener()

        self.smooth_force_mem = deque()
        self.smooth_force_mem_size = 20
        self.smoothed_force = np.zeros(3)
        self.smooth_decay = 0.95


    def switch_admittance_control(self, data):
        self.admittance_control_mode = data.data
        if self.admittance_control_mode:
            message = "Admittance control mode is turned on."
            controller_state = self.check_controller_state()
            if controller_state["joint_group_vel_controller"] != "running":
                self.switch_controller("joint_group")
        else:
            message = "Admittance control mode is turned off."
            controller_state = self.check_controller_state()
            if controller_state["scaled_pos_joint_traj_controller"] != "running":
                self.switch_controller("scaled_pos")
        success = True
        return success, message

    def check_admittance_control(self, data):
        if self.admittance_control_mode:
            message = "Admittance control mode is on."
        else:
            message = "Admittance control mode is off."
        success = True
        return success, message

    def wrench_callback(self, data):
        if not self.admittance_control_mode:
            return
        controller_state = self.check_controller_state(silent=True)
        if controller_state["joint_group_vel_controller"] != "running":
            self.switch_controller("joint_group")
        force_tool = np.array([data.wrench.force.x, data.wrench.force.y, data.wrench.force.z])
        # print(force_tool)
        force_tool = (force_tool>self.force_threshold)*(force_tool-self.force_threshold)\
            +(force_tool<-self.force_threshold)*(force_tool+self.force_threshold)    

        #smooth the force direction and magnitude based on a series of consecutive previous forces
        # force_tool = self.smooth_force(force_tool)    

        try:
            (trans_tool_base, rot_tool_base) = self.tf.lookupTransform("/base_link", "/tool0_controller", rospy.Time(0))
            force_base = qv_mult(rot_tool_base, force_tool)
            force_magnitude = np.linalg.norm(force_base)
            if force_magnitude > 1e-6:
                pose_offset_vel = self.pose_vel_scale*force_base/force_magnitude
            else:
                pose_offset_vel = np.zeros(3)
            # print(pose_offset_vel)
            desired_next_joint_state = self.compute_inverse_kinematics(pose_offset_vel)
            if len(desired_next_joint_state.position) == 0:
                self.set_joint_velocity(np.zeros(6))
                print("The desired pose cannot be reached.")
                self.switch_controller("scaled_pos")
                return
            current_joint_state = self.get_angle()
            # print(force_magnitude) # roughly 0-12
            # print(np.array(desired_next_joint_state.position)) # if you are using hande launch file, then the joint state is length of 8.
            # print(np.array(current_joint_state.position))
            jv_input = np.array(desired_next_joint_state.position)[:6] - np.array(current_joint_state.position)
            jv_input = jv_input * force_magnitude*self.force_vel_scale
            self.set_joint_velocity(jv_input)
            
        except (tf.LookupException, tf.ConnectivityException, tf.ExtrapolationException):
            print("Error on tf.")

    def wrench_callback_copy(self, data):
        if not self.admittance_control_mode:
            return
        controller_state = self.check_controller_state(silent=True)
        if controller_state["joint_group_vel_controller"] != "running":
            self.switch_controller("joint_group")
        force = np.array([data.wrench.force.x, data.wrench.force.y, data.wrench.force.z])
        
        force = (force>self.force_threshold)*(force-self.force_threshold)+(force<-self.force_threshold)*(force+self.force_threshold)
        # print(force)
        
        #smooth the force direction and magnitude based on a series of consecutive previous forces
        force = self.smooth_force(force)    

        try:
            (trans_ee, rot_ee) = self.tf.lookupTransform("/base_link", "/tool0_controller", rospy.Time(0))
            force_rotated = qv_mult(rot_ee, force)
            print(force_rotated)
            pose_offset_vel = self.pose_vel_scale*force_rotated
            # print(pose_offset_vel)
            desired_next_joint_state = self.compute_inverse_kinematics(pose_offset_vel)
            if len(desired_next_joint_state.position) == 0:
                self.set_joint_velocity(np.zeros(6))
                print("The desired pose cannot be reached.")
                self.switch_controller("scaled_pos")
                return
            current_joint_state = self.get_angle()
            jv_input = np.array(desired_next_joint_state.position) - np.array(current_joint_state.position)
            # self.set_joint_velocity(jv_input)
        except (tf.LookupException, tf.ConnectivityException, tf.ExtrapolationException):
            print("Error on tf.")

    def smooth_force(self, force):
        self.smooth_force_mem.append(force)
        if len(self.smooth_force_mem) > self.smooth_force_mem_size:
            self.smooth_force_mem.pop()
        smoothed = np.zeros(3)
        buf_size = len(self.smooth_force_mem)
        for i in range(buf_size):
            smoothed += self.smooth_force_mem[i] * self.smooth_decay**(buf_size-i)

        smoothed = (smoothed / np.linalg.norm(smoothed)) * np.linalg.norm(force)
        #only direction got smoothed now
        self.smoothed_force = smoothed
        
        return self.smoothed_force
        



    def get_pose(self):
        rospy.wait_for_service("ur_control_wrapper/get_pose")
        get_current_pose = rospy.ServiceProxy("ur_control_wrapper/get_pose", GetPose)
        current_pose = None
        try:
            current_pose = get_current_pose().pose
        except rospy.ServiceException as exc:
            print "Service did not process request: " + str(exc)
        return current_pose

        
    def get_angle(self):
        rospy.wait_for_service("ur_control_wrapper/get_joints")
        get_current_joints = rospy.ServiceProxy("ur_control_wrapper/get_joints", GetJoints)
        current_joints = None
        try:
            current_joints = get_current_joints().joints
        except rospy.ServiceException as exc:
            print "Service did not process request: " + str(exc) 
        return current_joints

    def compute_inverse_kinematics(self, desired_pose_offset, silent=True):
        # desired_pose_offset = [offset_x, offset_y, offset_z]
        joint_state = None
        current_pose = self.get_pose()
        desired_pose = current_pose
        desired_pose.position.x += desired_pose_offset[0]
        desired_pose.position.y += desired_pose_offset[1]
        desired_pose.position.z += desired_pose_offset[2]
        rospy.wait_for_service("ur_control_wrapper/inverse_kinematics")
        compute_ik = rospy.ServiceProxy("ur_control_wrapper/inverse_kinematics", InverseKinematics)
        try:
            req = InverseKinematicsRequest()
            req.pose = desired_pose
            response = compute_ik(req)
            # response = compute_ik(desired_pose)
            solution_found, joint_state = response.solution_found, response.joint_state
            if solution_found:
                if not silent:
                    print("joint state: ")
                    print(joint_state)
            else:
                print("Solution is not found.")
        except rospy.ServiceException as exc:
            print "Service did not process request: " + str(exc)
        return joint_state

    def set_joint_velocity(self, jv_input):
        controller_state = self.check_controller_state(silent=True)
        if controller_state["joint_group_vel_controller"] != "running":
            print("joint_group_vel_controller is not started. Failed to set joint velocity.")
            return
        # pass
        if isinstance(jv_input, str):
            jv_float = [float(entry) for entry in jv_input.split(" ")] # [0., 0., 0., 0., 0., 0.] # 6 entries
        else:
            jv_float = jv_input
        data = Float64MultiArray()
        data.data = jv_float
        self.joint_velocity_pub.publish(data)
        return
    
    def check_controller_state(self, silent=False):
        rospy.wait_for_service("/controller_manager/list_controllers")
        list_controllers = rospy.ServiceProxy("/controller_manager/list_controllers", ListControllers)
        response = list_controllers()
        controller_state = {}
        for single_controller in response.controller:
            if single_controller.name == "joint_group_vel_controller" \
                or single_controller.name == "scaled_pos_joint_traj_controller":
                controller_state[single_controller.name] = single_controller.state
        if not silent:
            if controller_state["scaled_pos_joint_traj_controller"] == "running":
                print("Current controller is scaled_pos_joint_traj_controller.")
            elif controller_state["joint_group_vel_controller"] == "running":
                print("Current controller is joint_group_vel_controller.")
            else:
                print("Neither scaled_pos_joint_traj_controller nor joint_group_vel_controller is running.")
        return controller_state

    
    def switch_controller(self, desired_controller):
        # desired_controller = 'joint_group' or 'scaled_pos'
        controller_switched = False
        if desired_controller != 'joint_group' and desired_controller != 'scaled_pos':
            print("Wrong controller input.")
            return controller_switched
        rospy.wait_for_service("controller_manager/switch_controller")
        switch_controller = rospy.ServiceProxy("controller_manager/switch_controller", SwitchController)
        try:
            req = SwitchControllerRequest()
            if desired_controller == 'joint_group':
                req.start_controllers = ['joint_group_vel_controller']
                req.stop_controllers = ['scaled_pos_joint_traj_controller']
            else:
                req.start_controllers = ['scaled_pos_joint_traj_controller']
                req.stop_controllers = ['joint_group_vel_controller']
            req.strictness = 2
            req.start_asap = False
            req.timeout = 0.0
            response = switch_controller(req)
            if response.ok:
                controller_switched = True
                if desired_controller == 'joint_group':
                    print("Controller successfully switched to joint_group_vel_controller.")
                else:
                    print("Controller successfully switched to scaled_pos_joint_traj_controller.")
            else:
                print("Controller switch failed.")
        except rospy.ServiceException as exc:
            print "Service did not process request: " + str(exc)
        return controller_switched
    
    def zero_force_torque_sensor(self):
        rospy.wait_for_service("ur_hardware_interface/zero_ftsensor")
        zero_ft_sensor = rospy.ServiceProxy("ur_hardware_interface/zero_ftsensor", Trigger)
        response = zero_ft_sensor()
        if response.success:
            print("Force torque sensor is zeroed.")
        else:
            print("Failed to zero force torque sensor.")
        return response.success

if __name__ == '__main__':
    try:
        rospy.init_node('ur_admittance_control_node', anonymous=True)
        admittance_control = AdmittanceControl()
        rospy.spin()
    except rospy.ROSInterruptException:
        pass