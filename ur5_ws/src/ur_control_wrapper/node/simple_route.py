#!/usr/bin/env python
import numpy as np
import rospy
from std_msgs.msg import Float64, Float64MultiArray
from geometry_msgs.msg import Pose
from ur_control_wrapper.srv import GetPose

class route_planer:
    def __init__(self):
        self.task_goal_pub = rospy.Publisher('task_goal', Float64MultiArray, queue_size=1)
        self.task_vel_pub = rospy.Publisher('task_vel', Float64, queue_size=1)
        # self.right_goals = [[-0.705052806614, 0.234387694165, 0.232087357934], [-0.505052806614, 0.234387694165, 0.232087357934], [-0.505052806614, 0.034387694165, 0.232087357934], [-0.705052806614, 0.034387694165, 0.232087357934]]
        # self.left_goals = [[-0.705052806614, 0.234387694165, 0.232087357934], [-0.505052806614, 0.234387694165, 0.232087357934], [-0.505052806614, 0.034387694165, 0.232087357934], [-0.705052806614, 0.034387694165, 0.232087357934]]
        self.right_goals = [[-0.75, 0.03, 0.3], [-0.45, 0.03, 0.3], [-0.45, 0.25, 0.3], [-0.75, 0.25, 0.3]]
        # self.left_goals = [[-0.80, 0.05, 0.35], [-0.50, 0.05, 0.35], [-0.50, 0.30, 0.35], [-0.80, 0.30, 0.35], [-0.80, 0.30, 0.05]]
        self.vel = 4
        self.robot_idx = 0 # 0 represent robot on the right. 1 represent robot on the left

    def get_pose(self):
        rospy.wait_for_service("ur_control_wrapper/get_pose")
        get_current_pose = rospy.ServiceProxy("ur_control_wrapper/get_pose", GetPose)
        current_pose = None
        try:
            current_pose = get_current_pose().pose
        except rospy.ServiceException as exc:
            print("Service did not process request: " + str(exc))

        return current_pose

    def planer(self):
        # set the publish rate in Hz
        rate = rospy.Rate(50)
        goal_idx = 0
        # print("Publish goals at 50 Hz")
        print('publish goal')
        if(self.robot_idx == 0):
            goals = self.right_goals
            goal_num = len(goals)
        else:
            goals = self.left_goals
            goal_num = len(goals)

        while(not rospy.is_shutdown()):
            current_pose = self.get_pose()
            current_pos = np.array([current_pose.position.x, current_pose.position.y, current_pose.position.z])
            pose_diff = goals[goal_idx] - current_pos
            distance = np.linalg.norm(pose_diff)
            if(distance**2 < 2e-4):
                goal_idx += 1
                goal_idx = goal_idx % goal_num
            my_msg = Float64MultiArray()
            my_msg.data = goals[goal_idx]
            self.task_goal_pub.publish(my_msg)
            self.task_vel_pub.publish(self.vel)

if __name__ == '__main__':
    try:
        rospy.init_node('planer', anonymous=True)
        planer = route_planer()
        planer.planer()

    except rospy.ROSInterruptException:
        pass