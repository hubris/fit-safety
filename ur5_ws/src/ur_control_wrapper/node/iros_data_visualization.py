import pathhack
import pickle
import numpy as np
import matplotlib.pyplot as plt
from os.path import join



def compute_path_length(traj_path):
    traj_path_disp = traj_path[1:]-traj_path[:-1]
    return np.linalg.norm(traj_path_disp, axis=1).sum()


# with open(join(pathhack.pkg_path, 'raw_datasets', 'zhe', '5.p'), 'rb') as f:
#     data = pickle.load(f, encoding='latin1')
with open(join(pathhack.pkg_path, 'raw_datasets', 'yeji', '1.p'), 'rb') as f: # coexistence
    data = pickle.load(f, encoding='latin1')

# with open(join(pathhack.pkg_path, 'raw_datasets', 'yeji', '7.p'), 'rb') as f: # admittance
#     data = pickle.load(f, encoding='latin1')

# (traj_len, 3) # admittance_control on or off bool array
data['human_path'] = np.stack(data['human_path'], axis=0)
data['robot_path'] = np.stack(data['robot_path'], axis=0)
# print(data['force'])
data['force'] = np.stack(data['force'], axis=0)
traj_len = len(data['force'])
force_magnitudes = np.linalg.norm(data['force'], axis=1)
fig, ax = plt.subplots()
ax.plot(np.arange(traj_len)/30, force_magnitudes)
fig.savefig("visualization_force_6.png")
print("average force: ", np.mean(force_magnitudes))


import matplotlib
matplotlib.use('Agg')
from mpl_toolkits.mplot3d import Axes3D

fig = plt.figure()
ax = Axes3D(fig)
ax.axis('auto')
# ax.axis('equal')
ax.scatter(data['robot_path'][:,0],data['robot_path'][:,1],data['robot_path'][:,2],'C1') # plot the point (2,3,4) on the figure
# plt.show()
fig.savefig("visualization_robot_path.png")
print("robot trajectory length: ", compute_path_length(data['robot_path']))


fig = plt.figure()
ax = Axes3D(fig)
ax.axis('auto')
# ax.axis('equal')
ax.scatter(data['human_path'][:,0],data['human_path'][:,1],data['human_path'][:,2],'C0') # plot the point (2,3,4) on the figure
# plt.show()
fig.savefig("visualization_human_path.png")
print("human trajectory length: ", compute_path_length(data['human_path']))


