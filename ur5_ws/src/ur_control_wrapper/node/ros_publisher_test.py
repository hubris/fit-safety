#!/usr/bin/env python
import rospy
from std_msgs.msg import Float64

from geometry_msgs.msg import Pose, Point, Quaternion, Vector3, WrenchStamped


class talker:
    def __init__(self):
        self.pub1 = rospy.Publisher('chatter1', Float64, queue_size=1)
        self.pub2 = rospy.Publisher('chatter2', Float64, queue_size=1)
        self.pub3 = rospy.Publisher('chatter3', Float64, queue_size=1)

    def talk(self):
        rate = rospy.Rate(4)
        num = 0
        while not rospy.is_shutdown():
            num += 1
            self.pub1.publish(num)
            if num%2 == 0:
                self.pub2.publish(num)
                if num%4 == 0:
                    self.pub3.publish(num)
            rate.sleep()

if __name__ == '__main__':
    try:
        rospy.init_node('talker', anonymous=True)
        talker = talker()
        talker.talk()
    except rospy.ROSInterruptException:

        pass