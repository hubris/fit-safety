#!/usr/bin/env python
import rospy
from ur_msgs.srv import SetSpeedSliderFraction

if __name__ == '__main__':

    try:
        rospy.wait_for_service("ur_hardware_interface/set_speed_slider")
        set_speed = rospy.ServiceProxy("ur_hardware_interface/set_speed_slider", SetSpeedSliderFraction)

        print(set_speed(0.3))
    except rospy.ROSInterruptException:
        pass