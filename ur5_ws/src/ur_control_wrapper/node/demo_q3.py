#!/usr/bin/env python

import rospy
import numpy as np
from std_msgs.msg import String, Bool
from sensor_msgs.msg import JointState
from geometry_msgs.msg import Pose, Point, Quaternion, Vector3
from ur_control_wrapper.msg import HumanIntervention
from ur_control_wrapper.srv import SetPose
from ur_control_wrapper.srv import GetPose
from ur_control_wrapper.srv import GetJoints
from ur_control_wrapper.srv import SetJoints
from ur_control_wrapper.srv import SetTrajectory
from ur_control_wrapper.srv import GetCartesianPlan
from ur_control_wrapper.srv import ExecuteCartesianPlan
from ur_control_wrapper.srv import AddObject, AddObjectRequest
from ur_control_wrapper.srv import AttachObject, AttachObjectRequest
from ur_control_wrapper.srv import DetachObject, DetachObjectRequest
from ur_control_wrapper.srv import RemoveObject, RemoveObjectRequest

from tf import transformations as tfs

from ur_control_wrapper.srv import ExecutePause ### zhe Q3 ###
from ur_kinematics import all_close

class Demo:
    def __init__(self):
        self.free_drive_pub = rospy.Publisher("/ur_control_wrapper/enable_freedrive", Bool, queue_size=10)
        self.gripper_pub = rospy.Publisher("/ur_control_wrapper/gripper", String, queue_size=10)
        self.connect_pub = rospy.Publisher("/ur_control_wrapper/connect", Bool, queue_size=10)
        ### zhe Q3 starts ###
        self.create_pose_goals()
        self.machine_state = 999 # 'idle', or initial state
        self.pause_flag = False # when paused, state remains the same. State refers to the goal you want to reach.
        self.time_pub = rospy.Publisher('/q3/time_stamp', String, queue_size=10)
        ### zhe Q3 ends ###

    def create_pose_goals(self):
        ### zhe Q3 ###
        self.pose_goals = []
        # another pose
        pose_goal = Pose()
        pose_goal.position.x = -0.396676200582
        pose_goal.position.y = -0.334979194148
        pose_goal.position.z = 0.381549084187
        pose_goal.orientation.x = -0.491344068718
        pose_goal.orientation.y = 0.490623269772
        pose_goal.orientation.z = 0.521610007854
        pose_goal.orientation.w = 0.495774962055
        self.pose_goals.append(pose_goal)
        # default pose
        pose_goal = Pose()
        pose_goal.position.x = -0.534276193493
        pose_goal.position.y = 0.121855217532
        pose_goal.position.z = 0.380308184416
        pose_goal.orientation.x = -0.00739091691942
        pose_goal.orientation.y = 0.70087753693
        pose_goal.orientation.z = 0.0126824360759
        pose_goal.orientation.w = 0.713130568963
        self.pose_goals.append(pose_goal)
        return

    def execute_state_task(self, machine_state):
        ### zhe Q3 ###
        self.machine_state = machine_state # 0-1
        curr_pose_goal = self.pose_goals[self.machine_state]

        rospy.wait_for_service("/ur_control_wrapper/get_cartesian_plan")
        get_cartesian_plan = rospy.ServiceProxy("/ur_control_wrapper/get_cartesian_plan", GetCartesianPlan)

        rospy.wait_for_service("/ur_control_wrapper/execute_cartesian_plan_asynchronous")
        execute_cartesian_plan_asynchronous = rospy.ServiceProxy("/ur_control_wrapper/execute_cartesian_plan_asynchronous", ExecuteCartesianPlan)            
        
        next_path = [curr_pose_goal]
        try:
            response = get_cartesian_plan(next_path)
            plan1 = response.plan
            is_solved = response.is_solved
        except rospy.ServiceException as exc:
            print "Service did not process request: " + str(exc)

        try:
            response = execute_cartesian_plan_asynchronous(plan1)
            # dummy outputs for asynchronous execution
            _ = response.final_pose
            _ = response.is_reached
        except rospy.ServiceException as exc:
            print "Service did not process request: " + str(exc)   
        return

    def callback_state_task_completion(self, data):
        ### zhe Q3 ###
        # data is useless. This is like a clock.
        if self.pause_flag:
            rospy.wait_for_service("/ur_control_wrapper/execute_pause")
            execute_pause = rospy.ServiceProxy("/ur_control_wrapper/execute_pause", ExecutePause)
            try:
                execute_pause()
            except rospy.ServiceException as exc:
                print "Service did not process request: " + str(exc)
            str_pause = "Pause recurring %s" % rospy.get_time()
            self.time_pub.publish(str_pause)
            return
        if self.machine_state <= 1 and self.machine_state >= 0:
            rospy.wait_for_service("/ur_control_wrapper/get_pose")
            get_current_pose = rospy.ServiceProxy("/ur_control_wrapper/get_pose", GetPose)
            try:
                current_pose = get_current_pose().pose
            except rospy.ServiceException as exc:
                print "Service did not process request: " + str(exc)
            curr_pose_goal = self.pose_goals[self.machine_state]
            if all_close(curr_pose_goal, current_pose, 0.01) and not self.pause_flag: # reached the goal
                next_state = [1,0]
                #curr_state= [0,1]
                machine_state = next_state[self.machine_state]
                self.execute_state_task(machine_state)
        return

    def callback_human_intervention(self, data):
        ### zhe Q3 ###
        # data is used as a trigger.
        # True means that human intruded and keeps in.
        # False means that human left and keeps out.
        # print('Here is data', data.data)
        # if data.data and not self.pause_flag:
        if data.data and not self.pause_flag:
            print('Pause.')
            self.pause_flag = True
            rospy.wait_for_service("/ur_control_wrapper/execute_pause")
            execute_pause = rospy.ServiceProxy("/ur_control_wrapper/execute_pause", ExecutePause)
            try:
                execute_pause()
            except rospy.ServiceException as exc:
                print "Service did not process request: " + str(exc)
            str_pause = "Pause starts %s" % rospy.get_time()
            self.time_pub.publish(str_pause)
        
        elif not data.data and self.pause_flag:
            print('Resume.')
            self.pause_flag = False
            curr_pose_goal = self.pose_goals[self.machine_state]

            rospy.wait_for_service("/ur_control_wrapper/get_pose")
            get_current_pose = rospy.ServiceProxy("/ur_control_wrapper/get_pose", GetPose)
            try:
                current_pose = get_current_pose().pose
            except rospy.ServiceException as exc:
                print "Service did not process request: " + str(exc)
            if not all_close(curr_pose_goal, current_pose, 0.01): # reached the goal
                self.execute_state_task(self.machine_state)
                str_resume = "Resume %s" % rospy.get_time()
                self.time_pub.publish(str_resume)


    def get_pose(self):
        rospy.wait_for_service("/ur_control_wrapper/get_pose")
        get_current_pose = rospy.ServiceProxy("/ur_control_wrapper/get_pose", GetPose)
        current_pose = None
        try:
            current_pose = get_current_pose().pose
        except rospy.ServiceException as exc:
            print "Service did not process request: " + str(exc) 

        return current_pose
    
    def get_angle(self):
        rospy.wait_for_service("/ur_control_wrapper/get_joints")
        get_current_joints = rospy.ServiceProxy("/ur_control_wrapper/get_joints", GetJoints)
        current_joints = None
        try:
            current_joints = get_current_joints().joints
        except rospy.ServiceException as exc:
            print "Service did not process request: " + str(exc) 
        return current_joints
    
    def set_default_angles(self):
        rospy.wait_for_service("/ur_control_wrapper/set_joints")
        set_joints = rospy.ServiceProxy("/ur_control_wrapper/set_joints", SetJoints)
        joints = JointState()
        #joints.name = ["elbow_joint", "shoulder_lift_joint", "shoulder_pan_joint", "wrist_1_joint", "wrist_2_joint", "wrist_3_joint"]
        #joints.position = [-np.pi / 3.0, -2.0 * np.pi / 3, 0.0, np.pi * 1.0 / 2.0, -np.pi / 2.0, 0.0]
        joints.name = ["shoulder_pan_joint", "shoulder_lift_joint", "elbow_joint", "wrist_1_joint", "wrist_2_joint", "wrist_3_joint"]
        joints.position = [0.023975849151611328, -2.173887868920797, -1.0581588745117188, 1.6445128160664062, -1.570674244557516, -2.2236500875294496e-05]
        try:
            response = set_joints(joints)
        except rospy.ServiceException as exc:
            print "Service did not process request: " + str(exc)      

        self.gripper_pub.publish("go")

    def set_joints(self, direction):
        current_joints = self.get_angle()
        rospy.wait_for_service("/ur_control_wrapper/set_joints")
        set_joints = rospy.ServiceProxy("/ur_control_wrapper/set_joints", SetJoints)
        joints = JointState()
        joints.name = current_joints.name
        temp = list(current_joints.position)

        try:
            if direction == "1+":
                temp[0] += np.deg2rad(10.0)
            elif direction == "1-":
                temp[0] -= np.deg2rad(10.0)
            elif direction == "2+":
                temp[1] += np.deg2rad(10.0)
            elif direction == "2-":
                temp[1] -= np.deg2rad(10.0)
            elif direction == "3+":
                temp[2] += np.deg2rad(10.0)
            elif direction == "3-":
                temp[2] -= np.deg2rad(10.0)
            elif direction == "4+":
                temp[3] += np.deg2rad(10.0)
            elif direction == "4-":
                temp[3] -= np.deg2rad(10.0)
            elif direction == "5+":
                temp[4] += np.deg2rad(10.0)
            elif direction == "5-":
                temp[4] -= np.deg2rad(10.0)
            elif direction == "6+":
                temp[5] += np.deg2rad(45.0)
            elif direction == "6-":
                temp[5] -= np.deg2rad(45.0)
            joints.position = tuple(temp)
            response = set_joints(joints)
        except rospy.ServiceException as exc:
            print "Service did not process request: " + str(exc)

    def move_arm(self, direction):
        current_pose = self.get_pose()
        current_euler = np.rad2deg(tfs.euler_from_quaternion(np.array([current_pose.orientation.x, 
                        current_pose.orientation.y, current_pose.orientation.z, current_pose.orientation.w])))
        print("euler_before = ", current_euler)
        rospy.wait_for_service("/ur_control_wrapper/set_pose")
        set_current_pose = rospy.ServiceProxy("/ur_control_wrapper/set_pose", SetPose)
        try:
            if direction == "x+":
                current_pose.position.x += 0.05
            elif direction == "x-":
                current_pose.position.x -= 0.05
            elif direction == "y+":
                current_pose.position.y += 0.05
            elif direction == "y-":
                current_pose.position.y -= 0.05
            elif direction == "z+":
                current_pose.position.z += 0.05
            elif direction == "z-":
                current_pose.position.z -= 0.05

            elif direction == "roll+":
                current_euler[0] += 20.0
            elif direction == "roll-":
                current_euler[0] -= 20.0
            elif direction == "pitch+":
                current_euler[1] += 20.0
            elif direction == "pitch-":
                current_euler[1] -= 20.0
            elif direction == "yaw+":
                current_euler[2] += 20.0
            elif direction == "yaw-":
                current_euler[2] -= 20.0

            print("euler_after = ", current_euler)
            ori = np.deg2rad(current_euler)
            quat = tfs.quaternion_from_euler(ori[0], ori[1], ori[2])
            current_pose.orientation.x = quat[0]
            current_pose.orientation.y = quat[1]
            current_pose.orientation.z = quat[2]
            current_pose.orientation.w = quat[3]

            response = set_current_pose(current_pose)
            pose = response.response_pose
            is_reached = response.is_reached
        except rospy.ServiceException as exc:
            print "Service did not process request: " + str(exc)
    
    def waypoint_following(self):
        current_pose = self.get_pose()

        rospy.wait_for_service("/ur_control_wrapper/follow_trajectory")
        set_trajectory = rospy.ServiceProxy("/ur_control_wrapper/follow_trajectory", SetTrajectory)

        waypoints = []
        delta = [[-0.1, 0.0, 0.0], [-0.1, 0.0, -0.1], [-0.1, 0.0, 0.0], [0.0, 0.0, 0.0]]
        for wp in delta:
            wpi = Pose()
            wpi.position.x = current_pose.position.x + wp[0]
            wpi.position.y = current_pose.position.y + wp[1]
            wpi.position.z = current_pose.position.z + wp[2]
            wpi.orientation.x = current_pose.orientation.x
            wpi.orientation.y = current_pose.orientation.y
            wpi.orientation.z = current_pose.orientation.z
            wpi.orientation.w = current_pose.orientation.w
            waypoints.append(wpi)
        try:
            response = set_trajectory(waypoints)
            final_pose = response.final_pose
            is_reached = response.is_reached
        except ROSInterruptException as exc:
            print "Service did not process request: " + str(exc)

    def pick_and_place(self):
        """
            This function trys to implement the picking and place task repeatedly with a simple vertical movement.
            Gripper motion is also involved into the loop.
        """
        current_pose = self.get_pose()

        rospy.wait_for_service("/ur_control_wrapper/get_cartesian_plan")
        get_cartesian_plan = rospy.ServiceProxy("/ur_control_wrapper/get_cartesian_plan", GetCartesianPlan)

        rospy.wait_for_service("/ur_control_wrapper/execute_cartesian_plan")
        execute_cartesian_plan = rospy.ServiceProxy("/ur_control_wrapper/execute_cartesian_plan", ExecuteCartesianPlan)            
        
        waypoints = []
        waypoints.append(current_pose)
        delta = [0, 0, -0.2]
        wpi = Pose()
        # wpi.position.x = current_pose.position.x + delta[0]
        # wpi.position.y = current_pose.position.y + delta[1]
        # wpi.position.z = current_pose.position.z + delta[2]
        # wpi.orientation.x = current_pose.orientation.x
        # wpi.orientation.y = current_pose.orientation.y
        # wpi.orientation.z = current_pose.orientation.z
        # wpi.orientation.w = current_pose.orientation.w

        wpi.position.x = -0.616089436615
        wpi.position.y = 0.109872737942
        wpi.position.z = 0.000766135493649
        wpi.orientation.x = -0.00944544691936
        wpi.orientation.y = 0.726030962968
        wpi.orientation.z = 0.0267542016121
        wpi.orientation.w = 0.687076441919
        waypoints.append(wpi)

        next_path = [waypoints[1]]
        try:
            response = get_cartesian_plan(next_path)
            plan1 = response.plan
            is_solved = response.is_solved
        except ROSInterruptException as exc:
            print "Service did not process request: " + str(exc)

        try:
            response = execute_cartesian_plan(plan1)
            final_pose = response.final_pose
            is_reached = response.is_reached
        except ROSInterruptException as exc:
            print "Service did not process request: " + str(exc)   
        
        self.gripper_pub.publish("227")
        rospy.sleep(2.5)

        next_path = waypoints
        try:
            response = get_cartesian_plan(next_path)
            plan2 = response.plan
            is_solved = response.is_solved
        except ROSInterruptException as exc:
            print "Service did not process request: " + str(exc)

        for iter in range(3):
            try:
                response = execute_cartesian_plan(plan2)
                final_pose = response.final_pose
                is_reached = response.is_reached
            except ROSInterruptException as exc:
                print "Service did not process request: " + str(exc)
            
            if iter % 2 == 0:
                self.gripper_pub.publish("150")
            else:
                self.gripper_pub.publish("227")
            rospy.sleep(1.3)

        next_path = [waypoints[0]]
        try:
            response = get_cartesian_plan(next_path)
            plan3 = response.plan
            is_solved = response.is_solved
        except ROSInterruptException as exc:
            print "Service did not process request: " + str(exc)

        try:
            response = execute_cartesian_plan(plan3)
            final_pose = response.final_pose
            is_reached = response.is_reached
        except ROSInterruptException as exc:
            print "Service did not process request: " + str(exc)  
        self.gripper_pub.publish("go")
        
        
    def run(self):
        while not rospy.is_shutdown():
            print "====================================================="
            command_input = raw_input("Freedrive: fs(start);fe-end: \nGripper: go(open); gc(close);\
            \nConnect: c(connect);\nGet End Effector Pose: ep; \nGet joint angles: ja;\
            \nGo to Default Position: d; \nWaypoints following: wp; \nPick and place test: pp; \nSet joints: sj;\
            \nQ3 demo: q3;\
            \nMove arm: x+(x direction move up 5 cm); x-; y+; y-; z+; z-; roll+-, pitch+-, yaw+-(20 deg): \n")
            if command_input == "fs":
                self.free_drive_pub.publish(True)
            elif command_input == "fe":
                self.free_drive_pub.publish(False)
            elif command_input == "go":
                self.gripper_pub.publish(command_input)
            elif command_input == "gc":
                self.gripper_pub.publish(command_input)
            elif command_input == "c":
                self.connect_pub.publish(True)
            elif command_input == "ep":
                print self.get_pose()
            elif command_input == "ja":
                print self.get_angle()
            elif command_input == "d":
                self.set_default_angles()
            elif command_input =="wp":
                self.waypoint_following()
            elif command_input == "pp":
                self.pick_and_place()
            elif command_input =="sj":
                sub_cmd = raw_input("elbow_joint(1)-> wrist_3_joint(6) move 10 degrees: 1+; 1-; 2+; 2-; 3+; 3-; 4+; 4-; 5+; 5-; 6+; 6-: \n")
                direction = sub_cmd
                self.set_joints(direction)
            elif command_input == "q3": # q3 demo
                rospy.Subscriber('/q3/check_task_clock', String, self.callback_state_task_completion)
                rospy.Subscriber('/q3/human_intervention', Bool, self.callback_human_intervention)
                self.set_default_angles()
                machine_state = 0
                print('Went to the default position. Start FSM.')
                self.execute_state_task(machine_state)

            else: # move arm
                direction = command_input
                self.move_arm(direction)

if __name__ == '__main__':
    try:
        rospy.init_node('ur_control_wrapper_demo_mode', anonymous=True)

        demo = Demo()

        demo.run()
    except rospy.ROSInterruptException:
        pass
