#!/usr/bin/env python3
import numpy as np
import rospy
from std_msgs.msg import Bool
import matplotlib.pyplot as plt
from ur_control_wrapper.msg import JointCurrents, StringStamped, HumanIntervention

class HumanTouchJoints:
    def __init__(self, time_window=20):
        self.human_intervention = False
        self.human_intervention_time = -999.
        self.bool_pub = rospy.Publisher('hri/human_intervention', HumanIntervention, queue_size=10)
        self.log_pub = rospy.Publisher('hri/log', StringStamped, queue_size=10)
        rospy.init_node('human_touch_joints')
        self.time_window = time_window
        self.d_current_window = np.empty((0,6)) # 0 means initially empty. 6 means 6 joints
        self.std = np.empty((0,6)) # 0 means initially empty. 6 means 6 joints
        # self.std_thresholds = [[0.25, 0.3], [0.25, 0.3]]
        self.std_thresholds = [[0.56, 0.6], [0.55, 0.6]]
        
        _, self.axes = plt.subplots(2,1) # for now we only plot and use joint 0 and joint 1 for human intervention.
        self.std_plots = []
        ylimits = [[-1,1], [-1,1]]
        ylabels = ['joint 0', 'joint 1']
        for plt_idx, (ax, ylabel, ylimit, std_threshold) in enumerate(zip(self.axes, ylabels, ylimits, self.std_thresholds)):
            ax.set_xlim([0,2])
            ax.set_ylim(ylimit)
            if plt_idx == len(self.axes)-1:
                ax.set_xlabel('time (sec)')
            ax.set_ylabel(ylabel)
            ax.plot(np.arange(200)/100., np.ones(200)*std_threshold[0], 'r--')
            ax.plot(np.arange(200)/100., np.ones(200)*std_threshold[1], 'r--')
            self.std_plots.append(ax.plot([], [], 'b-')[0])
            


    def callback(self, data):
        r"""
        inputs:
            - data
                # tuple: (a_current, t_current)
                - a_current
                    # actual current of each joint. # list of len 6, float values.
                - t_current
                    # target current of each joint. # list of len 6, float values.    
        """
        a_current, t_current = np.array(data.actual_current).reshape(1,-1), np.array(data.target_current).reshape(1,-1)
        d_current = a_current - t_current # (1,6)
        self.d_current_window = np.concatenate((self.d_current_window, d_current), axis=0) # (<=time_window+1, 6)
        if len(self.d_current_window) > self.time_window:
            self.d_current_window = self.d_current_window[-self.time_window:]
        
        if len(self.d_current_window) == self.time_window:
            self.std = np.concatenate((self.std, self.d_current_window.std(axis=0)[np.newaxis,:]), axis=0) # (<=201, 6)
            if len(self.std) > 200:
                self.std = self.std[-200:]
            self.check_human_intervention()
            for joint_plot_idx, std_plot in enumerate(self.std_plots):
                std_plot.set_data(np.arange(len(self.std))/100., self.std[:,joint_plot_idx])
            plt.draw()

    def check_human_intervention(self):
        # two thresholds for human touch detection
        if not self.human_intervention and \
            (self.std[-1,0] > self.std_thresholds[0][1] or self.std[-1,1] > self.std_thresholds[1][1]):
            # not true in the state and found intervention
            self.human_intervention = True
            human_intervention_msg = HumanIntervention()
            human_intervention_msg.header.stamp = rospy.Time.now()
            human_intervention_msg.source = "contact"
            human_intervention_msg.data = self.human_intervention
            self.bool_pub.publish(human_intervention_msg)
            self.human_intervention_time = rospy.get_time()
            log_msg = StringStamped()
            log_msg.header.stamp = rospy.Time.now()
            log_msg.data = "Touch intervention is detected"
            self.log_pub.publish(log_msg)            
            return
        elif self.human_intervention and \
            (self.std[-1,0] < self.std_thresholds[0][0] or self.std[-1,1] < self.std_thresholds[1][0]):
            # true in the state and found no intervention
            if rospy.get_time() - self.human_intervention_time > 5.:
                # more than 5 sec
                self.human_intervention = False
                human_intervention_msg = HumanIntervention()
                human_intervention_msg.header.stamp = rospy.Time.now()
                human_intervention_msg.source = "contact"
                human_intervention_msg.data = self.human_intervention
                self.bool_pub.publish(human_intervention_msg)
                log_msg = StringStamped()
                log_msg.header.stamp = rospy.Time.now()
                log_msg.data = "Touch intervention is over"
                self.log_pub.publish(log_msg)
            return
        elif self.human_intervention and \
            (self.std[-1,0] > self.std_thresholds[0][1] or self.std[-1,1] > self.std_thresholds[1][1]):
            # true in the state, and when intervention goes on, update the intervention time
            self.human_intervention_time = rospy.get_time()
            return
        else:
            return


def main():
    htj = HumanTouchJoints()
    rospy.Subscriber("/joint_currents", JointCurrents, htj.callback, queue_size=10)
    plt.show()
    rospy.spin()


if __name__ == '__main__':
    try:
        main()
    except rospy.ROSInterruptException:
        pass