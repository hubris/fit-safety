#!/usr/bin/env python3
r"""Talker that publishes joint currents. We use python3 as interpreter for now 
because rtde_receive is installed within python3 with a low version of pip."""
import sys
import argparse
import rospy
import rtde_receive
from ur_control_wrapper.msg import JointCurrents 

def JointCurrentsTalker(robot_ip, publish_rate):
    r"""Publish joint currents. Average rate can reach 70,000 Hz without the publish_rate constraint.
    inputs:
        - robot_ip: str. default: "192.168.0.2"
        - publish_rate: float. default: 100
    """
    rtde_r = rtde_receive.RTDEReceiveInterface(robot_ip)
    pub = rospy.Publisher('joint_currents', JointCurrents, queue_size=10)
    rospy.init_node('ur_control_wrapper_joint_currents_talker', anonymous=True)
    r = rospy.Rate(publish_rate)
    while not rospy.is_shutdown():
        JointCurrents_msg = JointCurrents()
        JointCurrents_msg.header.stamp = rospy.Time.now()
        JointCurrents_msg.actual_current = rtde_r.getActualCurrent()
        JointCurrents_msg.target_current = rtde_r.getTargetCurrent()
        pub.publish(JointCurrents_msg)
        r.sleep()

def arg_parse():
    parser = argparse.ArgumentParser(description='Visualize joint currents.')
    parser.add_argument('--robot_ip', type=str)
    parser.add_argument('--publish_rate', type=float, default=100.)
    return parser.parse_args(rospy.myargv(argv=sys.argv)[1:]) # remove additional args from ros. [1:] is to remove the filename itself.

if __name__ == '__main__':
    try:
        args = arg_parse()
        print(args)
        print("ur_control_wrapper_joint_currents_talker connecting to :" + str(args.robot_ip))
        JointCurrentsTalker(args.robot_ip, args.publish_rate)
    except rospy.ROSInterruptException:
        pass