#!/usr/bin/env python
r"""Visualize actual and target currents of one joint on ur robot in real time."""
import sys
import argparse
import rospy
import matplotlib.pyplot as plt
from geometry_msgs.msg import WrenchStamped


class WrenchVisualizer: 
    def __init__(self, visualize_torque=False):
        r"""Visualize the wrench.
        inputs:
            - joint_index: 
                # integer.
                # index of the joint to be visualized. 0-5.
                # default: 0. Means the base joint.
        """
        assert not visualize_torque # for now let's not visualize torque
        _, self.ax = plt.subplots(3,1)
        ylabels = ['fx', 'fy', 'fz'] # actual current, target current, difference current (dc=ac-tc) 
        y_limit = [[-8,8], [-8, 8], [-30, 30]]
        self.wrench_plt = []
        for ax, ylabel, ylim in zip(self.ax, ylabels, y_limit):
            ax.set_xlim([0,200])
            ax.set_ylim(ylim)
            ax.set_xlabel('time')
            ax.set_ylabel(ylabel)
            self.wrench_plt.append(ax.plot([], [], 'b-')[0])
        self.fx = []
        self.fy = []
        self.fz = []

    def callback_realtime_plot(self, data):
        self.fx.append(data.wrench.force.x)
        self.fy.append(data.wrench.force.y)
        self.fz.append(data.wrench.force.z)
        wrench = [self.fx, self.fy, self.fz]
        for i, (w_plt, w) in enumerate(zip(self.wrench_plt, wrench)):
            if len(w) > 200:
                w = w[-200:]
            w_plt.set_data(range(len(w)), w)
        plt.draw()


def arg_parse():
    parser = argparse.ArgumentParser(description='Visualize wrench.')
    parser.add_argument('--visualize_torque', action='store_true', \
        help='visualize torque. Now our code does not support torque visualization yet.')
    return parser.parse_args(rospy.myargv(argv=sys.argv)[1:])


def listener(args):
    r"""Subscribes to the topic /joint_currents and plot values in real time."""
    wv = WrenchVisualizer(visualize_torque=args.visualize_torque)
    rospy.init_node('ur_control_wrapper_wrench_listener', anonymous=True)
    rospy.Subscriber("/wrench", WrenchStamped, wv.callback_realtime_plot, queue_size=1)
    # self.wrench_sub = rospy.Subscriber('/wrench', WrenchStamped, self.wrench_callback, queue_size=1)
    plt.show()
    rospy.spin()


if __name__ == '__main__':
    args = arg_parse()
    listener(args)