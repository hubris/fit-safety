#!/usr/bin/env python
r"""Visualize actual and target currents of one joint on ur robot in real time."""
import sys
import argparse
import rospy
import numpy as np
import matplotlib.pyplot as plt
from geometry_msgs.msg import WrenchStamped


class ForceMagnitudeVisualizer: 
    def __init__(self):
        r"""Visualize the wrench.
        inputs:
            - joint_index: 
                # integer.
                # index of the joint to be visualized. 0-5.
                # default: 0. Means the base joint.
        """
        self.fig, self.ax = plt.subplots(figsize=(6, 6))
        self.fig.canvas.set_window_title('force_magnitude')
        # plt.tight_layout()
        self.prob_threshold = 0.9
        # set up probability distribution plot
        self.ax.set_xlim([0, 0.4]) # look symmetric
        self.ax.set_ylim([0, 50])
        self.ax.set_yticks([0, 10, 20, 30, 40, 50])
        self.ax.set_xticks([0,0.1,0.2,0.3,0.4])
        # self.ax.set_xticklabels(["cooperation", "coexistence"])
        # self.ax.set_yticklabels([0, 0.5, "thre-\nshold", 1])
        # self.ax.axhline(y=0.9, color='k', linestyle='--')
        self.ax.set_xlabel('time (sec)')
        self.ax.set_ylabel('force (N)')
        self.force_magnitude_plt = self.ax.plot([], [], c='C1')[0]
        self.fm = []


    def callback_realtime_plot(self, data):
        fm = (data.wrench.force.x**2.+data.wrench.force.y**2.+data.wrench.force.z**2.)**(1/2.)
        self.fm.append(fm)
        if len(self.fm)>200:
            self.fm = self.fm[-200:]
        self.force_magnitude_plt.set_data(np.arange(len(self.fm))/500., self.fm)
        plt.draw()


def arg_parse():
    parser = argparse.ArgumentParser(description='Visualize wrench.')
    parser.add_argument('--visualize_torque', action='store_true', \
        help='visualize torque. Now our code does not support torque visualization yet.')
    return parser.parse_args(rospy.myargv(argv=sys.argv)[1:])


def listener(args):
    r"""Subscribes to the topic /joint_currents and plot values in real time."""
    fmv = ForceMagnitudeVisualizer()
    rospy.init_node('ur_control_wrapper_wrench_listener', anonymous=True)
    rospy.Subscriber("/wrench", WrenchStamped, fmv.callback_realtime_plot, queue_size=1)
    # self.wrench_sub = rospy.Subscriber('/wrench', WrenchStamped, self.wrench_callback, queue_size=1)
    plt.show()
    rospy.spin()


if __name__ == '__main__':
    args = arg_parse()
    listener(args)