#!/usr/bin/env python3
import numpy as np
import rospy
from std_msgs.msg import Bool
import matplotlib.pyplot as plt
from ur_control_wrapper.msg import JointCurrents, StringStamped, HumanIntervention

import sys
import argparse
import rospy
import matplotlib.pyplot as plt
from geometry_msgs.msg import WrenchStamped

class WrenchSupervisor:
    def __init__(self, force_z_threshold=(5., 50.)): # (5,40)
        self.intervention = False
        self.force_z_threshold = force_z_threshold
        self.wrench_intervention = False
        self.bool_pub = rospy.Publisher('hri/human_intervention', HumanIntervention, queue_size=10)
        rospy.init_node('wrench_supervisor')
        rospy.Subscriber("/wrench", WrenchStamped, self.callback, queue_size=1)

    def callback(self, data):
        r"""
        inputs:
            - data
                # tuple: (a_current, t_current)
                - a_current
                    # actual current of each joint. # list of len 6, float values.
                - t_current
                    # target current of each joint. # list of len 6, float values.    
        """
        if abs(data.wrench.force.z) > self.force_z_threshold[1] and not self.intervention:
            self.intervention = True
            intervention_msg = HumanIntervention() # now just borrow the structure in the past.
            intervention_msg.header.stamp = rospy.Time.now()
            intervention_msg.source = "wrench"
            intervention_msg.data = self.intervention
            self.bool_pub.publish(intervention_msg)
            print(intervention_msg)
        elif abs(data.wrench.force.z) < self.force_z_threshold[0] and self.intervention:
            self.intervention = False
            intervention_msg = HumanIntervention() # now just borrow the structure in the past.
            intervention_msg.header.stamp = rospy.Time.now()
            intervention_msg.source = "wrench"
            intervention_msg.data = self.intervention
            self.bool_pub.publish(intervention_msg)
            print(intervention_msg)
            # # send true and false just to switch to the next state machine
            # self.intervention = False
            # intervention_msg = HumanIntervention() # now just borrow the structure in the past.
            # intervention_msg.header.stamp = rospy.Time.now()
            # intervention_msg.source = "wrench"
            # intervention_msg.data = self.intervention
            # self.bool_pub.publish(intervention_msg)

        # a_current, t_current = np.array(data.actual_current).reshape(1,-1), np.array(data.target_current).reshape(1,-1)
        # d_current = a_current - t_current # (1,6)
        # self.d_current_window = np.concatenate((self.d_current_window, d_current), axis=0) # (<=time_window+1, 6)
        # if len(self.d_current_window) > self.time_window:
        #     self.d_current_window = self.d_current_window[-self.time_window:]
        
        # if len(self.d_current_window) == self.time_window:
        #     self.std = np.concatenate((self.std, self.d_current_window.std(axis=0)[np.newaxis,:]), axis=0) # (<=201, 6)
        #     if len(self.std) > 200:
        #         self.std = self.std[-200:]
        #     self.check_human_intervention()
        #     for joint_plot_idx, std_plot in enumerate(self.std_plots):
        #         std_plot.set_data(np.arange(len(self.std))/100., self.std[:,joint_plot_idx])
        #     plt.draw()


    # def callback_realtime_plot(self, data):
    #     self.fx.append(data.wrench.force.x)
    #     self.fy.append(data.wrench.force.y)
    #     self.fz.append(data.wrench.force.z)
    #     wrench = [self.fx, self.fy, self.fz]
    #     for i, (w_plt, w) in enumerate(zip(self.wrench_plt, wrench)):
    #         if len(w) > 200:
    #             w = w[-200:]
    #         w_plt.set_data(range(len(w)), w)
    #     plt.draw()



    # def check_human_intervention(self):
    #     # two thresholds for human touch detection
    #     if not self.human_intervention and \
    #         (self.std[-1,0] > self.std_thresholds[0][1] or self.std[-1,1] > self.std_thresholds[1][1]):
    #         # not true in the state and found intervention
    #         self.human_intervention = True
    #         intervention_msg = HumanIntervention()
    #         intervention_msg.header.stamp = rospy.Time.now()
    #         intervention_msg.source = "contact"
    #         intervention_msg.data = self.human_intervention
    #         self.bool_pub.publish(intervention_msg)
    #         self.human_intervention_time = rospy.get_time()
    #         log_msg = StringStamped()
    #         log_msg.header.stamp = rospy.Time.now()
    #         log_msg.data = "Touch intervention is detected"
    #         self.log_pub.publish(log_msg)            
    #         return
    #     elif self.human_intervention and \
    #         (self.std[-1,0] < self.std_thresholds[0][0] or self.std[-1,1] < self.std_thresholds[1][0]):
    #         # true in the state and found no intervention
    #         if rospy.get_time() - self.human_intervention_time > 5.:
    #             # more than 5 sec
    #             self.human_intervention = False
    #             intervention_msg = HumanIntervention()
    #             intervention_msg.header.stamp = rospy.Time.now()
    #             intervention_msg.source = "contact"
    #             intervention_msg.data = self.human_intervention
    #             self.bool_pub.publish(intervention_msg)
    #             log_msg = StringStamped()
    #             log_msg.header.stamp = rospy.Time.now()
    #             log_msg.data = "Touch intervention is over"
    #             self.log_pub.publish(log_msg)
    #         return
    #     elif self.human_intervention and \
    #         (self.std[-1,0] > self.std_thresholds[0][1] or self.std[-1,1] > self.std_thresholds[1][1]):
    #         # true in the state, and when intervention goes on, update the intervention time
    #         self.human_intervention_time = rospy.get_time()
    #         return
    #     else:
    #         return


def main():
    ws = WrenchSupervisor()
    # htj = HumanTouchJoints()
    # rospy.Subscriber("/joint_currents", JointCurrents, htj.callback, queue_size=10)
    # plt.show()
    rospy.spin()


if __name__ == '__main__':
    try:
        main()
    except rospy.ROSInterruptException:
        pass