#!/usr/bin/env python
import rospy
import pathhack
from os.path import join, isdir
from os import makedirs
import pickle
# import numpy as np
from os.path import exists
import tf
from std_msgs.msg import Float32MultiArray
from geometry_msgs.msg import WrenchStamped
import numpy as np

class DataCollectionPipeline:
    def __init__(
        self,
        path_topic_name="/hri/human_robot_pos",
        force_topic_name="/wrench",
        ):
        self.raw_datasets_folderpath = join(pathhack.pkg_path, "raw_datasets")
        if not isdir(self.raw_datasets_folderpath):
            makedirs(self.raw_datasets_folderpath)
        self.subject_name = ""
        self.subject_folderpath = ""
        self.collection_status = "off"
        self.reset_data()
        self.tf = tf.TransformListener()
        rospy.Subscriber(path_topic_name, Float32MultiArray, self.path_callback) # slower frequency
        rospy.Subscriber(force_topic_name, WrenchStamped, self.force_callback, queue_size=1)

    def reset_data(self):
        self.data_tt = {}
        self.data_tt['human_path'] = np.zeros(3)
        self.data_tt['robot_path'] = np.zeros(3)
        self.data_tt['force'] = np.zeros(3)
        self.data = {}
        self.data['human_path'] = []
        self.data['robot_path'] = []
        self.data['force'] = []

    def change_subject_name(self, subject_name):
        self.subject_name = subject_name
        if self.subject_name != "":
            self.subject_folderpath = join(self.raw_datasets_folderpath, subject_name)
            if not isdir(self.subject_folderpath):
                makedirs(self.subject_folderpath)
            if self.subject_name not in self.trajectory_index.keys():
                self.trajectory_index[self.subject_name] = 0
                self.save_trajectory_index()
    
    def change_collection_status(self, command_input):
        if command_input == "s":
            self.collection_status = "on"
        elif command_input == "e" and self.collection_status == "on":
            self.collection_status = "off"
            self.trajectory_index[self.subject_name] += 1
            self.save_trajectory_index()
            # save data
            trajectory_data_filename = str(self.trajectory_index[self.subject_name])+".p"
            trajectory_data_filepath = join(self.subject_folderpath, trajectory_data_filename)
            with open(trajectory_data_filepath, 'wb') as f:
                pickle.dump(self.data, f)
                print(trajectory_data_filepath+" is dumped.")
            self.reset_data()
        else:
            raise RuntimeError("The command input is wrong for changing collection status.")

    def path_callback(self, data):
        self.data_tt['human_path'] = np.array(data.data[1:4])
        self.data_tt['robot_path'] = np.array(data.data[4:7])
        if self.collection_status != "on":
            return
        if self.subject_name == "":
            self.collection_status = "failed"
            print("The subject name is empty. Collection failed.")
            return
        self.data['human_path'].append(self.data_tt['human_path'])
        self.data['robot_path'].append(self.data_tt['robot_path'])
        self.data['force'].append(self.data_tt['force'])
        return
    
    def print_state(self):
        print("Subject name: ", self.subject_name)
        if self.subject_name in self.trajectory_index.keys():
            print("Trajectory index: ", self.trajectory_index[self.subject_name])
        else:
            print("The trajectory index is not initialized for current subject and intention.")
        print("Collection status: ", self.collection_status)

    def save_trajectory_index(self):
        with open(self.trajectory_index_filepath, 'wb') as f:
            pickle.dump(self.trajectory_index, f)
            print(self.trajectory_index_filepath+" is updated.")
    
    def initialize_trajectory_index(self, file_id_str):
        self.trajectory_index_filepath = join(self.raw_datasets_folderpath, \
            "trajectory_index_"+file_id_str+".p")
        if not exists(self.trajectory_index_filepath):
            self.trajectory_index = {} # {'zhe': 2} means zhe has 2 trajectories.
            self.save_trajectory_index()
        else:
            with open(self.trajectory_index_filepath, 'rb') as f:
                self.trajectory_index = pickle.load(f)

    def force_callback(self, data):
        self.data_tt['force'] = np.array([data.wrench.force.x, data.wrench.force.y, data.wrench.force.z])

    def run(self):
        command_input = raw_input("\
                \n=====================================================\
                \nPlease input the id for trajectory index file. E.g. trajectory_index_ID.p \
                \n=====================================================\
                \nCommand: ")
        self.initialize_trajectory_index(command_input)
        while not rospy.is_shutdown():
            command_input = raw_input("\
                \n=====================================================\
                \nSubject name: sn NAME\
                \nStart collection: s\
                \nEnd collection: e\
                \nPrint current state: state\
                \n=====================================================\
                \nCommand: ")
            if command_input[:2] == "sn":
                self.change_subject_name(command_input[3:])
            elif command_input == "s" or command_input == "e":
                self.change_collection_status(command_input)
            elif command_input == "state":
                self.print_state()
            else:
                print("Th command ["+command_input+"] is invalid.")

if __name__ == '__main__':
    try:
        rospy.init_node('data_collection_pipeline', anonymous=True)
        dcp = DataCollectionPipeline()
        dcp.run()
    except rospy.ROSInterruptException:
        pass
