import numpy as np
import sys
import cv2
from glob import glob
import PIL.Image
import matplotlib.pyplot as plt
import os
from pupil_apriltags import Detector
import json

def main():
	
	data = {}
	scenes = [1,2]

	for scene in scenes:
		image_paths = glob(f'/home/hollydinkel/Documents/CoBot/20210329/Data/scene_{scene}/*.jpeg')
		data[f'scene_{scene}'] = []
		with open(f'/home/hollydinkel/Documents/CoBot/20210329/Data/scene_{scene}/scene_{scene}.json','w') as outfile:
			for i in range(len(image_paths)):
				image_path = image_paths[i]
				image_name = image_paths[i].split("/")[-1][:-5]

				imgBGR = cv2.imread(image_path)
				imgRGB = cv2.cvtColor(imgBGR,cv2.COLOR_BGR2RGB)

				#AR tag detection
				image = plt.imread(image_path,cv2.IMREAD_GRAYSCALE)
				image = image[:,:,0].copy()
				tag_detector = Detector(families='tag36h11',nthreads=1,quad_decimate=1.0,quad_sigma=0.0,refine_edges=1,decode_sharpening=0.25,debug=0)
				tags = tag_detector.detect(image, estimate_tag_pose=False,camera_params=None, tag_size=0.044)

				ii=image.copy()
				width=25
				for i in range(len(tags)):
					center = tags[i].center[:]
					bound = tags[i].corners[:]
					tag_id = tags[i].tag_id
					data[f'scene_{scene}'].append({'image_id': image_name, 'tag': tag_id, 'center': center.tolist(), 'corners': bound.flatten().tolist()})
					
				########################## Generate image coordinates of center and bounding box for debugging and verification ###########################
					ii[int(center[1])-width:int(center[1])+width,int(center[0])-width:int(center[0])+width] = 1
					for bb in bound:
						# Generate image coordinates of apriltag corners
						ii[int(bb[1])-width:int(bb[1])+width,int(bb[0])-width:int(bb[0])+width] = 1

					print("scene:", scene, "image:", i, "tag id:", tag_id)
				
				# Plot original image
				plt.imshow(image)	

				# Plot apriltag corners and center
				plt.figure()
				plt.imshow(ii)
				plt.savefig('detected_tag2.png')
				# plt.show()

			json.dump(data,outfile,indent=4)

if __name__ == '__main__':
	main()