#!/usr/bin/env python

import rospy
from moveit_msgs.msg import CollisionObject
from shape_msgs.msg import SolidPrimitive
from geometry_msgs.msg import Pose
import moveit_commander



import math
import rospy
import numpy as np
# from ros_openpose.msg import Frame
from std_msgs.msg import ColorRGBA
from geometry_msgs.msg import Vector3, Point
from visualization_msgs.msg import Marker, MarkerArray
from rosop.msg import HumanSkeletonPosition

class HumanWristObstacle:
    def __init__(self, considered_wrist="right"):
        rospy.init_node("human_wrist_obstacle")
        self.scene = moveit_commander.PlanningSceneInterface()
        rospy.Subscriber("hri/human_skeleton_position_tracking_3D", HumanSkeletonPosition, self.wrist_callback)
        self.collision_object_pub = rospy.Publisher('/collision_object', CollisionObject, queue_size=5)
        if considered_wrist=="left":
            self.wrist_idx = 7
        elif considered_wrist=="right":
            self.wrist_idx = 4
        else:
            raise RuntimeError("Wrist can only be left or right.")
        self.collision_object_msg = None

    
    def wrist_callback(self, data):
        pos_3d = np.array(data.data).reshape(data.n_humans, data.n_keypoints, data.n_dim)
        # print(pos_3d)
        if sum(abs(pos_3d[0, self.wrist_idx])) != 0.: 
            # marker_size = np.mean(pos_3d[human_idx, 0])*5 # std mean over xyz
            # marker_size = 0.1
            # print(marker_size)
            # wrist_marker = self.create_marker(marker_id, marker_color, Marker.SPHERE, marker_size, curr_stamp)
            # wrist_point = Point()
            # wrist_point.x, wrist_point.y, wrist_point.z = pos_3d[human_idx, self.wrist_idx]
            # wrist_marker.pose.position = wrist_point
            # marker_array.markers.append(wrist_marker)
            box_pose = Pose()
            # print("3d")
            box_pose.position.x, box_pose.position.y, box_pose.position.z = pos_3d[0, self.wrist_idx]
            box_pose.orientation.w = 1.0
            # print(box_pose)
        else:
            # print("zero")
            return
        # box_pose = Pose()
        # box_pose.position.x = x
        # box_pose.position.y = y
        # box_pose.position.z = z
        # box_pose.orientation.w = 1.0

        if self.collision_object_msg is None:
            self.collision_object_msg = CollisionObject()
            self.collision_object_msg.id = "wrist"
            self.collision_object_msg.operation = self.collision_object_msg.ADD
            primitive = SolidPrimitive()
            primitive.type = primitive.BOX
            primitive.dimensions = [0.1, 0.1, 0.1]
            self.collision_object_msg.primitives = [primitive]
            self.collision_object_msg.primitive_poses = [box_pose]
            self.collision_object_msg.header.stamp = rospy.Time.now()
            self.collision_object_msg.header.frame_id = 'camera_link'
            while self.collision_object_msg.id not in self.scene.get_known_object_names():
                self.collision_object_pub.publish(self.collision_object_msg)
            # print("added.")
        else:
            self.collision_object_msg.primitive_poses = [box_pose]
            self.collision_object_msg.header.stamp = rospy.Time.now()
            self.collision_object_msg.operation = self.collision_object_msg.MOVE
            # print("published")
            self.collision_object_pub.publish(self.collision_object_msg)


if __name__ == "__main__":
    hwo = HumanWristObstacle()
    rospy.spin()