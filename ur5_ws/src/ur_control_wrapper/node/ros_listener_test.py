#!/usr/bin/env python
import rospy
import message_filters
from std_msgs.msg import Float64

from geometry_msgs.msg import Pose, Point, Quaternion, Vector3, WrenchStamped


class listener():
    def __init__(self):
        rospy.init_node('listener', anonymous=True)
        self.sub1 = message_filters.Subscriber('chatter1', Float64)
        self.sub2 = message_filters.Subscriber('chatter2', Float64)
        self.sub3 = message_filters.Subscriber('chatter3', Float64)
        
        ts = message_filters.ApproximateTimeSynchronizer([self.sub1, self.sub2, self.sub3], queue_size=1, slop=2, allow_headerless=True)
        ts.registerCallback(self.callback)

    def callback(self, data1, data2, data3):
        print(data1, data2, data3)
        print(type(listener))

if __name__ == '__main__':
    try:
        listener()
        rospy.spin()
    except rospy.ROSInterruptException:

        pass