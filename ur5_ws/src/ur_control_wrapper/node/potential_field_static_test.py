#!/usr/bin/env python
import argparse
import rospy
import numpy as np

from std_msgs.msg import Float32MultiArray, Float64MultiArray
from controller_manager_msgs.srv import SwitchController, SwitchControllerRequest, ListControllers
from ur_control_wrapper.srv import GetPose, GetJoints, InverseKinematics, InverseKinematicsRequest


# from std_msgs.msg import String, Bool, Float64MultiArray, 
# from std_srvs.srv import Trigger, SetBool
# from sensor_msgs.msg import JointState, Image, CameraInfo
# from geometry_msgs.msg import Pose, Point, Quaternion, Vector3, WrenchStamped
# from controller_manager_msgs.srv import SwitchController, SwitchControllerRequest, ListControllers
# from ur_control_wrapper.srv import SetPose
# from ur_control_wrapper.srv import GetPose
# from ur_control_wrapper.srv import GetJoints
# from ur_control_wrapper.srv import SetJoints
# from ur_control_wrapper.srv import SetTrajectory
# from ur_control_wrapper.srv import GetCartesianPlan
# from ur_control_wrapper.srv import ExecuteCartesianPlan
# from ur_control_wrapper.srv import AddObject, AddObjectRequest
# from ur_control_wrapper.srv import AttachObject, AttachObjectRequest
# from ur_control_wrapper.srv import DetachObject, DetachObjectRequest
# from ur_control_wrapper.srv import RemoveObject, RemoveObjectRequest
# from ur_control_wrapper.srv import InverseKinematics, InverseKinematicsRequest
# from gripper_controller_jenny import GripperHande as gripper
# from status_listener import StatusListener
# from cv_bridge import CvBridge, CvBridgeError
# import cv2
# import time

# from tf import transformations as tfs

# import tf
# from tf import TransformListener

# def qv_mult(q1, v1):
#     # v1 = tf.transformations.unit_vector(v1)
#     q2 = list(v1)
#     q2.append(0.0)
#     return tf.transformations.quaternion_multiply(
#         tf.transformations.quaternion_multiply(q1, q2), 
#         tf.transformations.quaternion_conjugate(q1)
#     )[:3]


class PotentialFieldStatic:
    def __init__(self, args, tracking_topic_name):
        self.args = args
        rospy.init_node('potential_field_static', anonymous=True)
        rospy.Subscriber(tracking_topic_name, Float32MultiArray, self.tracking_callback)
        # rospy.Subscriber(routing_planning,
        controller_state = self.check_controller_state(silent=True)
        self.joint_velocity_pub = rospy.Publisher('/joint_group_vel_controller/command', Float64MultiArray, queue_size=10)
        if controller_state["joint_group_vel_controller"] != "running":
            self.switch_controller("joint_group")
        self.last_vel = np.zeros(3)
        self.last_mag = 0

    def get_pose(self):
        rospy.wait_for_service("ur_control_wrapper/get_pose")
        get_current_pose = rospy.ServiceProxy("ur_control_wrapper/get_pose", GetPose)
        current_pose = None
        try:
            current_pose = get_current_pose().pose
        except rospy.ServiceException as exc:
            print "Service did not process request: " + str(exc)
        return current_pose

    def get_angle(self):
        rospy.wait_for_service("ur_control_wrapper/get_joints")
        get_current_joints = rospy.ServiceProxy("ur_control_wrapper/get_joints", GetJoints)
        current_joints = None
        try:
            current_joints = get_current_joints().joints
        except rospy.ServiceException as exc:
            print "Service did not process request: " + str(exc) 
        return current_joints

    def check_controller_state(self, silent=False):
        rospy.wait_for_service("/controller_manager/list_controllers")
        list_controllers = rospy.ServiceProxy("/controller_manager/list_controllers", ListControllers)
        response = list_controllers()
        controller_state = {}
        for single_controller in response.controller:
            if single_controller.name == "joint_group_vel_controller" \
                or single_controller.name == "scaled_pos_joint_traj_controller":
                controller_state[single_controller.name] = single_controller.state
        if not silent:
            if controller_state["scaled_pos_joint_traj_controller"] == "running":
                print("Current controller is scaled_pos_joint_traj_controller.")
            elif controller_state["joint_group_vel_controller"] == "running":
                print("Current controller is joint_group_vel_controller.")
            else:
                print("Neither scaled_pos_joint_traj_controller nor joint_group_vel_controller is running.")
        return controller_state

    def switch_controller(self, desired_controller):
        # desired_controller = 'joint_group' or 'scaled_pos'
        controller_switched = False
        if desired_controller != 'joint_group' and desired_controller != 'scaled_pos':
            print("Wrong controller input.")
            return controller_switched
        rospy.wait_for_service("controller_manager/switch_controller")
        switch_controller = rospy.ServiceProxy("controller_manager/switch_controller", SwitchController)
        try:
            req = SwitchControllerRequest()
            if desired_controller == 'joint_group':
                req.start_controllers = ['joint_group_vel_controller']
                req.stop_controllers = ['scaled_pos_joint_traj_controller']
            else:
                req.start_controllers = ['scaled_pos_joint_traj_controller']
                req.stop_controllers = ['joint_group_vel_controller']
            req.strictness = 2
            req.start_asap = False
            req.timeout = 0.0
            response = switch_controller(req)
            if response.ok:
                controller_switched = True
                if desired_controller == 'joint_group':
                    print("Controller successfully switched to joint_group_vel_controller.")
                else:
                    print("Controller successfully switched to scaled_pos_joint_traj_controller.")
            else:
                print("Controller switch failed.")
        except rospy.ServiceException as exc:
            print "Service did not process request: " + str(exc)
        return controller_switched

    def compute_inverse_kinematics(self, desired_pose_offset, silent=True):
        # desired_pose_offset = [offset_x, offset_y, offset_z]
        joint_state = None
        current_pose = self.get_pose()
        desired_pose = current_pose
        desired_pose.position.x += desired_pose_offset[0]
        desired_pose.position.y += desired_pose_offset[1]
        desired_pose.position.z += desired_pose_offset[2]
        rospy.wait_for_service("ur_control_wrapper/inverse_kinematics")
        compute_ik = rospy.ServiceProxy("ur_control_wrapper/inverse_kinematics", InverseKinematics)
        try:
            req = InverseKinematicsRequest()
            req.pose = desired_pose
            response = compute_ik(req)
            # response = compute_ik(desired_pose)
            solution_found, joint_state = response.solution_found, response.joint_state
            if solution_found:
                if not silent:
                    print("joint state: ")
                    print(joint_state)
            else:
                print("Solution is not found.")
        except rospy.ServiceException as exc:
            print "Service did not process request: " + str(exc)
        return joint_state

    def set_joint_velocity(self, jv_input):
        controller_state = self.check_controller_state(silent=True)
        if controller_state["joint_group_vel_controller"] != "running":
            print("joint_group_vel_controller is not started. Failed to set joint velocity.")
            return
        # pass
        if isinstance(jv_input, str):
            jv_float = [float(entry) for entry in jv_input.split(" ")] # [0., 0., 0., 0., 0., 0.] # 6 entries
        else:
            jv_float = jv_input
        data = Float64MultiArray()
        data.data = jv_float
        self.joint_velocity_pub.publish(data)
        return

    def tracking_callback(self, data):
        human_wrist_pos_in_base = np.array(data.data)
        # print(human_wrist_pos_in_base)
        current_pose = self.get_pose()
        ee_pos = np.array([current_pose.position.x, current_pose.position.y, current_pose.position.z])
        repulsive_vec = ee_pos - human_wrist_pos_in_base
        # print(np.linalg.norm(repulsive_vec))
        last_vel = self.last_vel
        last_mag = self.last_mag
        hand_ee_dist = np.linalg.norm(repulsive_vec)
        
        if hand_ee_dist > self.args.distance_upper_bound or hand_ee_dist < self.args.distance_lower_bound:
            # lower than lower bound is usually mismeasurement
            self.set_joint_velocity(np.zeros(6))
            return
        print(np.linalg.norm(repulsive_vec))
        repulsive_vec_norm = repulsive_vec/hand_ee_dist
        if self.args.attract:
            pose_offset_vel = -self.args.pose_vel_scale*repulsive_vec_norm
        else:
            pose_offset_vel = self.args.pose_vel_scale*repulsive_vec_norm
        desired_next_joint_state = self.compute_inverse_kinematics(pose_offset_vel)
        if len(desired_next_joint_state.position) == 0:
            self.set_joint_velocity(np.zeros(6))
            print("The desired pose cannot be reached.")
            return
        current_joint_state = self.get_angle()
        jv_input = np.array(desired_next_joint_state.position)[:6] - np.array(current_joint_state.position)
        # ! closer, repulsive larger makes sense. But closer, approach faster does not.
        if self.args.attract:
            # make the robot move slower when it approaches human
            jv_input = jv_input*2.*(2/(1+np.exp(-10*hand_ee_dist))-1)
        else:
            jv_input = jv_input*(1./hand_ee_dist)*self.args.distance_velocity_scale
        self.set_joint_velocity(jv_input)


def arg_parse():
    parser = argparse.ArgumentParser()
    parser.add_argument('--distance_lower_bound', default=0.05, type=float)
    parser.add_argument('--distance_upper_bound', default=0.3, type=float)
    parser.add_argument('--distance_velocity_scale', default=0.2, type=float)
    parser.add_argument('--pose_vel_scale', default=0.08, type=float)
    parser.add_argument('--attract', action='store_true')
    return parser.parse_args()

try:
    args = arg_parse()
    # tracking_topic_name = "hri/human_wrist_pos_in_base"
    tracking_topic_name = "hri/human_skeleton_position_tracking_3D_floatarray"     
    pfs = PotentialFieldStatic(args, tracking_topic_name)
    rospy.spin()
except rospy.ROSInterruptException:
    pass
