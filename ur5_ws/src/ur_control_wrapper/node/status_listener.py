#!/usr/bin/env python

import rospy
import rospkg
import numpy as np
import roslib; roslib.load_manifest('robotiq_2f_gripper_control')
import rospy
from robotiq_2f_gripper_control.msg import _Robotiq2FGripper_robot_input  as inputMsg
from geometry_msgs.msg import Pose
from sensor_msgs.msg import JointState
from ur_control_wrapper.srv import GetPose
from ur_control_wrapper.srv import GetJoints
from ur_control_wrapper.msg import Float32MultiArrayStamped


min_gap_ = 0.0
max_gap_ = 0.052    # (meter)
min_effort_ = 0.0
max_effort_ = 100.0 # (N)

class StatusListener:
    def __init__(self):
        # self.is_simulator = rospy.get_param("sim")

        self.gripper_status = inputMsg.Robotiq2FGripper_robot_input()
        self.gripper_status_msg = Float32MultiArrayStamped()
        self.ee_pose_pub = rospy.Publisher('ee_pose_jenny', Pose, queue_size=1)
        self.gripper_status_pub = rospy.Publisher('gripper_status_jenny', Float32MultiArrayStamped, queue_size=1)
        # self.robot_status_pub = rospy.Publisher('/joint_states', JointState, queue_size=1)
        rospy.Subscriber("Robotiq2FGripperRobotInput", inputMsg.Robotiq2FGripper_robot_input, self.GripperStatusInterpreter)
        # rospy.Subscriber("/joint_states_ur5", JointState, self.ur_joint_cb)
        rospy.Timer(rospy.Duration(0.002), self.data_log)

    def GripperStatusInterpreter(self, status):
        """Generate a string according to the current value of the status variables."""

        output = '\n-----\n2F gripper status interpreter\n-----\n'

        #gACT
        output += 'gACT = ' + str(status.gACT) + ': '
        if(status.gACT == 0):
            output += 'Gripper reset\n'
        if(status.gACT == 1):
            output += 'Gripper activation\n'

        #gGTO
        output += 'gGTO = ' + str(status.gGTO) + ': '
        if(status.gGTO == 0):
            output += 'Standby (or performing activation/automatic release)\n'
        if(status.gGTO == 1):
            output += 'Go to Position Request\n'

        #gSTA
        output += 'gSTA = ' + str(status.gSTA) + ': '
        if(status.gSTA == 0):
            output += 'Gripper is in reset ( or automatic release ) state. see Fault Status if Gripper is activated\n'
        if(status.gSTA == 1):
            output += 'Activation in progress\n'
        if(status.gSTA == 2):
            output += 'Not used\n'
        if(status.gSTA == 3):
            output += 'Activation is completed\n'

        #gOBJ
        output += 'gOBJ = ' + str(status.gOBJ) + ': '
        if(status.gOBJ == 0):
            output += 'Fingers are in motion (only meaningful if gGTO = 1)\n'
        if(status.gOBJ == 1):
            output += 'Fingers have stopped due to a contact while opening\n'
        if(status.gOBJ == 2):
            output += 'Fingers have stopped due to a contact while closing \n'
        if(status.gOBJ == 3):
            output += 'Fingers are at requested position\n'
    
        #gFLT
        output += 'gFLT = ' + str(status.gFLT) + ': '
        if(status.gFLT == 0x00):
            output += 'No Fault\n'
        if(status.gFLT == 0x05):
            output += 'Priority Fault: Action delayed, initialization must be completed prior to action\n'
        if(status.gFLT == 0x07):
            output += 'Priority Fault: The activation bit must be set prior to action\n'
        if(status.gFLT == 0x09):
            output += 'Minor Fault: The communication chip is not ready (may be booting)\n'   
        if(status.gFLT == 0x0B):
            output += 'Minor Fault: Automatic release in progress\n'
        if(status.gFLT == 0x0E):
            output += 'Major Fault: Overcurrent protection triggered\n'
        if(status.gFLT == 0x0F):
            output += 'Major Fault: Automatic release completed\n'

        #gPR
        output += 'gPR = ' + str(status.gPR) + ': '
        output += 'Echo of the requested position for the Gripper: ' + str(status.gPR) + '/255\n'

        #gPO
        output += 'gPO = ' + str(status.gPO) + ': '
        output += 'Position of Fingers: ' + str(status.gPO) + '/255\n'

        #gCU
        output += 'gCU = ' + str(status.gCU) + ': '
        output += 'Current of Fingers: ' + str(status.gCU * 10) + ' mA\n'   

        self.gripper_status = status

    # def ur_joint_cb(self, msg):
    #     finger_position = self.gripper_status.gPO * (max_gap_ - min_gap_)/255.0 + min_gap_
    #     gripper_effort = self.gripper_status.gCU *(max_effort_ - min_effort_)/255.0 + min_effort_
    #     msg.name += ['joint_finger', 'right_finger_joint']
    #     msg.position = tuple(list(msg.position) + [finger_position/2.0, finger_position/2.0])
    #     msg.velocity = tuple(list(msg.velocity) + [0.0, 0.0])
    #     msg.effort = tuple(list(msg.effort) + [gripper_effort/2.0, gripper_effort/2.0])
        
    #     self.robot_status_pub.publish(msg)

    def data_log(self, event):
        # get current ee pose
        rospy.wait_for_service("ur_control_wrapper/get_pose")
        get_current_pose = rospy.ServiceProxy("ur_control_wrapper/get_pose", GetPose)
        current_pose = None
        try:
            current_pose = get_current_pose().pose
        except rospy.ServiceException as exc:
            print "Service did not process request: " + str(exc) 

        # compute the gripper position
        gripper_position = -1.0*self.gripper_status.gPO*52.0/255.0 + 52.0

        # publish current ee pose and joints values. 
        if current_pose is not None:
            self.ee_pose_pub.publish(current_pose)

        # publish gripper position
        self.gripper_status_msg.header.stamp = rospy.Time.now()
        self.gripper_status_msg.array.data = np.array([gripper_position])
        self.gripper_status_pub.publish(self.gripper_status_msg)

 

if __name__ == '__main__':
    try:
        rospy.init_node('ur_control_wrapper_status_listener', anonymous=True)

        status_listener = StatusListener()

        rospy.spin()

    except rospy.ROSInterruptException:
        pass	





