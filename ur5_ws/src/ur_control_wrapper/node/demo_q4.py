#!/usr/bin/env python

import rospy
import numpy as np
from std_msgs.msg import String, Bool
from sensor_msgs.msg import JointState
from geometry_msgs.msg import Pose, Point, Quaternion, Vector3
from ur_control_wrapper.srv import SetPose
from ur_control_wrapper.srv import GetPose
from ur_control_wrapper.srv import GetJoints
from ur_control_wrapper.srv import SetJoints
from ur_control_wrapper.srv import SetTrajectory
from ur_control_wrapper.srv import GetCartesianPlan
from ur_control_wrapper.srv import ExecuteCartesianPlan
from ur_control_wrapper.srv import AddObject, AddObjectRequest
from ur_control_wrapper.srv import AttachObject, AttachObjectRequest
from ur_control_wrapper.srv import DetachObject, DetachObjectRequest
from ur_control_wrapper.srv import RemoveObject, RemoveObjectRequest

from tf import transformations as tfs

from ur_control_wrapper.srv import ExecutePause ### zhe Q3 ###
from ur_kinematics import all_close
from copy import deepcopy
from ur_control_wrapper.msg import StringStamped, HumanIntervention

class Demo:
    def __init__(self):
        self.free_drive_pub = rospy.Publisher("ur_control_wrapper/enable_freedrive", Bool, queue_size=10)
        self.gripper_pub = rospy.Publisher("ur_control_wrapper/gripper", String, queue_size=10)
        self.connect_pub = rospy.Publisher("ur_control_wrapper/connect", Bool, queue_size=10)
        ### zhe Q3 starts ###
        self.create_pose_goals()
        self.machine_state = 999 # 'idle', or initial state
        self.pause_flag = False # when paused, state remains the same. State refers to the goal you want to reach.
        self.log_pub = rospy.Publisher('hri/log', StringStamped, queue_size=10)
        ### zhe Q3 ends ###

    def create_pose_goals(self):
        # to read pose, rostopic echo /ee_pose_jenny
        ### zhe Q4 ###
        self.default_pose = Pose()
        self.default_pose.position.x = -0.534276193493
        self.default_pose.position.y = 0.121855217532
        self.default_pose.position.z = 0.380308184416
        self.default_pose.orientation.x = -0.00739091691942
        self.default_pose.orientation.y = 0.70087753693
        self.default_pose.orientation.z = 0.0126824360759
        self.default_pose.orientation.w = 0.713130568963

        self.below_default_pose = deepcopy(self.default_pose) # need deepcopy. copy is not enough.
        self.below_default_pose.position.z = 0.14

        self.pose_1 = deepcopy(self.default_pose)
        self.pose_1.position.x = -0.396676200582
        self.pose_1.position.y = -0.334979194148

        # we use both table and below to help deal with the unlevel table problem.
        self.table_pose_1 = deepcopy(self.pose_1)
        self.table_pose_1.position.z = 0.136

        self.below_pose_1 = deepcopy(self.pose_1)
        self.below_pose_1.position.z = 0.14
        
        self.pose_goals = []
        self.pose_goals.append(self.below_default_pose)
        self.pose_goals.append("go")
        self.pose_goals.append("gc")
        self.pose_goals.append(self.default_pose)
        self.pose_goals.append(self.pose_1)
        self.pose_goals.append(self.table_pose_1)
        self.pose_goals.append("go")
        self.pose_goals.append(self.below_pose_1)
        self.pose_goals.append("gc")
        self.pose_goals.append(self.pose_1)
        self.pose_goals.append(self.default_pose)
        return

    def execute_state_task(self, machine_state):
        ### zhe Q3 ###
        self.machine_state = machine_state # 0-1
        curr_pose_goal = self.pose_goals[self.machine_state]
        if isinstance(curr_pose_goal, Pose):
            rospy.wait_for_service("ur_control_wrapper/get_cartesian_plan")
            get_cartesian_plan = rospy.ServiceProxy("ur_control_wrapper/get_cartesian_plan", GetCartesianPlan)

            rospy.wait_for_service("ur_control_wrapper/execute_cartesian_plan_asynchronous")
            execute_cartesian_plan_asynchronous = rospy.ServiceProxy("ur_control_wrapper/execute_cartesian_plan_asynchronous", ExecuteCartesianPlan)            
            
            next_path = [curr_pose_goal]
            try:
                response = get_cartesian_plan(next_path)
                plan1 = response.plan
                is_solved = response.is_solved
            except rospy.ServiceException as exc:
                print "Service did not process request: " + str(exc)

            try:
                response = execute_cartesian_plan_asynchronous(plan1)
                # dummy outputs for asynchronous execution
                _ = response.final_pose
                _ = response.is_reached
            except rospy.ServiceException as exc:
                print "Service did not process request: " + str(exc)   
            return
        elif isinstance(curr_pose_goal, str):
            # gripper
            self.gripper_pub.publish(curr_pose_goal)
            rospy.sleep(2.0)
            return
        else:
            raise RuntimeError("curr_pose_goal type is not supported.")

    def callback_state_task_completion(self, data):
        ### zhe Q3 ###
        # data is useless. This is like a clock.
        if self.pause_flag:
            rospy.wait_for_service("ur_control_wrapper/execute_pause")
            execute_pause = rospy.ServiceProxy("ur_control_wrapper/execute_pause", ExecutePause)
            try:
                execute_pause()
            except rospy.ServiceException as exc:
                print "Service did not process request: " + str(exc)
            return
        if 0 <= self.machine_state < len(self.pose_goals): # not invalid initial state 999
            curr_pose_goal = self.pose_goals[self.machine_state]
            if isinstance(curr_pose_goal, Pose):
                rospy.wait_for_service("ur_control_wrapper/get_pose")
                get_current_pose = rospy.ServiceProxy("ur_control_wrapper/get_pose", GetPose)
                try:
                    current_pose = get_current_pose().pose
                except rospy.ServiceException as exc:
                    print "Service did not process request: " + str(exc)
                if all_close(curr_pose_goal, current_pose, 0.005) and not self.pause_flag: # reached the goal
                    next_state = list(range(1,len(self.pose_goals)))+[0]  # [1,2,...,state_n, 0]
                    machine_state = next_state[self.machine_state]
                    print("go to state "+str(machine_state))
                    self.execute_state_task(machine_state)
            elif isinstance(curr_pose_goal, str):
                next_state = list(range(1,len(self.pose_goals)))+[0]  # [1,2,...,state_n, 0]
                machine_state = next_state[self.machine_state]
                print("go to state "+str(machine_state))
                self.execute_state_task(machine_state)
            else:
                raise RuntimeError("curr_pose_goal type is not supported.")
        return

    def callback_human_intervention(self, data):
        ### zhe Q3 ###
        # data is used as a trigger.
        # True means that human intruded and keeps in.
        # False means that human left and keeps out.
        # print('Here is data', data.data)
        # if data.data and not self.pause_flag:
        if data.data and not self.pause_flag:
            print('Pause.')
            self.pause_flag = True
            rospy.wait_for_service("ur_control_wrapper/execute_pause")
            execute_pause = rospy.ServiceProxy("ur_control_wrapper/execute_pause", ExecutePause)
            try:
                execute_pause()
            except rospy.ServiceException as exc:
                print "Service did not process request: " + str(exc)
            log_msg = StringStamped()
            log_msg.header.stamp = rospy.Time.now()
            log_msg.data = "Robot pause executed"
            print('Robot pause executed')
            self.log_pub.publish(log_msg)
        
        elif not data.data and self.pause_flag:
            print('Resume.')
            self.pause_flag = False
            curr_pose_goal = self.pose_goals[self.machine_state]

            rospy.wait_for_service("ur_control_wrapper/get_pose")
            get_current_pose = rospy.ServiceProxy("ur_control_wrapper/get_pose", GetPose)
            try:
                current_pose = get_current_pose().pose
            except rospy.ServiceException as exc:
                print "Service did not process request: " + str(exc)
            if not all_close(curr_pose_goal, current_pose, 0.005): # reached the goal
                self.execute_state_task(self.machine_state)
            
                log_msg = StringStamped()
                log_msg.header.stamp = rospy.Time.now()
                log_msg.data = "Robot resume executed"
                self.log_pub.publish(log_msg)
            # ! zhe debug intervention does not consider gripper yet.


    def get_pose(self):
        rospy.wait_for_service("ur_control_wrapper/get_pose")
        get_current_pose = rospy.ServiceProxy("ur_control_wrapper/get_pose", GetPose)
        current_pose = None
        try:
            current_pose = get_current_pose().pose
        except rospy.ServiceException as exc:
            print "Service did not process request: " + str(exc) 

        return current_pose
    
    def get_angle(self):
        rospy.wait_for_service("ur_control_wrapper/get_joints")
        get_current_joints = rospy.ServiceProxy("ur_control_wrapper/get_joints", GetJoints)
        current_joints = None
        try:
            current_joints = get_current_joints().joints
        except rospy.ServiceException as exc:
            print "Service did not process request: " + str(exc) 
        return current_joints
    
    def set_default_angles(self):
        rospy.wait_for_service("ur_control_wrapper/set_joints")
        set_joints = rospy.ServiceProxy("ur_control_wrapper/set_joints", SetJoints)
        joints = JointState()
        #joints.name = ["elbow_joint", "shoulder_lift_joint", "shoulder_pan_joint", "wrist_1_joint", "wrist_2_joint", "wrist_3_joint"]
        #joints.position = [-np.pi / 3.0, -2.0 * np.pi / 3, 0.0, np.pi * 1.0 / 2.0, -np.pi / 2.0, 0.0]
        joints.name = ["shoulder_pan_joint", "shoulder_lift_joint", "elbow_joint", "wrist_1_joint", "wrist_2_joint", "wrist_3_joint"]
        joints.position = [0.023975849151611328, -2.173887868920797, -1.0581588745117188, 1.6445128160664062, -1.570674244557516, -2.2236500875294496e-05]
        try:
            response = set_joints(joints)
        except rospy.ServiceException as exc:
            print "Service did not process request: " + str(exc)      

        self.gripper_pub.publish("go")

    def set_joints(self, direction):
        current_joints = self.get_angle()
        rospy.wait_for_service("ur_control_wrapper/set_joints")
        set_joints = rospy.ServiceProxy("ur_control_wrapper/set_joints", SetJoints)
        joints = JointState()
        joints.name = current_joints.name
        temp = list(current_joints.position)

        try:
            if direction == "1+":
                temp[0] += np.deg2rad(10.0)
            elif direction == "1-":
                temp[0] -= np.deg2rad(10.0)
            elif direction == "2+":
                temp[1] += np.deg2rad(10.0)
            elif direction == "2-":
                temp[1] -= np.deg2rad(10.0)
            elif direction == "3+":
                temp[2] += np.deg2rad(10.0)
            elif direction == "3-":
                temp[2] -= np.deg2rad(10.0)
            elif direction == "4+":
                temp[3] += np.deg2rad(10.0)
            elif direction == "4-":
                temp[3] -= np.deg2rad(10.0)
            elif direction == "5+":
                temp[4] += np.deg2rad(10.0)
            elif direction == "5-":
                temp[4] -= np.deg2rad(10.0)
            elif direction == "6+":
                temp[5] += np.deg2rad(45.0)
            elif direction == "6-":
                temp[5] -= np.deg2rad(45.0)
            joints.position = tuple(temp)
            response = set_joints(joints)
        except rospy.ServiceException as exc:
            print "Service did not process request: " + str(exc)

    def move_arm(self, direction):
        current_pose = self.get_pose()
        current_euler = np.rad2deg(tfs.euler_from_quaternion(np.array([current_pose.orientation.x, 
                        current_pose.orientation.y, current_pose.orientation.z, current_pose.orientation.w])))
        print("euler_before = ", current_euler)
        rospy.wait_for_service("ur_control_wrapper/set_pose")
        set_current_pose = rospy.ServiceProxy("ur_control_wrapper/set_pose", SetPose)
        try:
            if direction == "x+":
                current_pose.position.x += 0.05
            elif direction == "x-":
                current_pose.position.x -= 0.05
            elif direction == "y+":
                current_pose.position.y += 0.05
            elif direction == "y-":
                current_pose.position.y -= 0.05
            elif direction == "z+":
                current_pose.position.z += 0.05
            elif direction == "z-":
                current_pose.position.z -= 0.05

            elif direction == "roll+":
                current_euler[0] += 20.0
            elif direction == "roll-":
                current_euler[0] -= 20.0
            elif direction == "pitch+":
                current_euler[1] += 20.0
            elif direction == "pitch-":
                current_euler[1] -= 20.0
            elif direction == "yaw+":
                current_euler[2] += 20.0
            elif direction == "yaw-":
                current_euler[2] -= 20.0

            print("euler_after = ", current_euler)
            ori = np.deg2rad(current_euler)
            quat = tfs.quaternion_from_euler(ori[0], ori[1], ori[2])
            current_pose.orientation.x = quat[0]
            current_pose.orientation.y = quat[1]
            current_pose.orientation.z = quat[2]
            current_pose.orientation.w = quat[3]

            response = set_current_pose(current_pose)
            pose = response.response_pose
            is_reached = response.is_reached
        except rospy.ServiceException as exc:
            print "Service did not process request: " + str(exc)
    
    def waypoint_following(self):
        current_pose = self.get_pose()

        rospy.wait_for_service("ur_control_wrapper/follow_trajectory")
        set_trajectory = rospy.ServiceProxy("ur_control_wrapper/follow_trajectory", SetTrajectory)

        waypoints = []
        delta = [[-0.1, 0.0, 0.0], [-0.1, 0.0, -0.1], [-0.1, 0.0, 0.0], [0.0, 0.0, 0.0]]
        for wp in delta:
            wpi = Pose()
            wpi.position.x = current_pose.position.x + wp[0]
            wpi.position.y = current_pose.position.y + wp[1]
            wpi.position.z = current_pose.position.z + wp[2]
            wpi.orientation.x = current_pose.orientation.x
            wpi.orientation.y = current_pose.orientation.y
            wpi.orientation.z = current_pose.orientation.z
            wpi.orientation.w = current_pose.orientation.w
            waypoints.append(wpi)
        try:
            response = set_trajectory(waypoints)
            final_pose = response.final_pose
            is_reached = response.is_reached
        except ROSInterruptException as exc:
            print "Service did not process request: " + str(exc)

    def pick_and_place(self):
        """
            This function trys to implement the picking and place task repeatedly with a simple vertical movement.
            Gripper motion is also involved into the loop.
        """
        current_pose = self.get_pose()

        rospy.wait_for_service("ur_control_wrapper/get_cartesian_plan")
        get_cartesian_plan = rospy.ServiceProxy("ur_control_wrapper/get_cartesian_plan", GetCartesianPlan)

        rospy.wait_for_service("ur_control_wrapper/execute_cartesian_plan")
        execute_cartesian_plan = rospy.ServiceProxy("ur_control_wrapper/execute_cartesian_plan", ExecuteCartesianPlan)            
        
        waypoints = []
        waypoints.append(current_pose)
        delta = [0, 0, -0.2]
        wpi = Pose()
        # wpi.position.x = current_pose.position.x + delta[0]
        # wpi.position.y = current_pose.position.y + delta[1]
        # wpi.position.z = current_pose.position.z + delta[2]
        # wpi.orientation.x = current_pose.orientation.x
        # wpi.orientation.y = current_pose.orientation.y
        # wpi.orientation.z = current_pose.orientation.z
        # wpi.orientation.w = current_pose.orientation.w

        wpi.position.x = -0.616089436615
        wpi.position.y = 0.109872737942
        wpi.position.z = 0.000766135493649
        wpi.orientation.x = -0.00944544691936
        wpi.orientation.y = 0.726030962968
        wpi.orientation.z = 0.0267542016121
        wpi.orientation.w = 0.687076441919
        waypoints.append(wpi)

        next_path = [waypoints[1]]
        try:
            response = get_cartesian_plan(next_path)
            plan1 = response.plan
            is_solved = response.is_solved
        except ROSInterruptException as exc:
            print "Service did not process request: " + str(exc)

        try:
            response = execute_cartesian_plan(plan1)
            final_pose = response.final_pose
            is_reached = response.is_reached
        except ROSInterruptException as exc:
            print "Service did not process request: " + str(exc)   
        
        self.gripper_pub.publish("227")
        rospy.sleep(2.5)

        next_path = waypoints
        try:
            response = get_cartesian_plan(next_path)
            plan2 = response.plan
            is_solved = response.is_solved
        except ROSInterruptException as exc:
            print "Service did not process request: " + str(exc)

        for iter in range(3):
            try:
                response = execute_cartesian_plan(plan2)
                final_pose = response.final_pose
                is_reached = response.is_reached
            except ROSInterruptException as exc:
                print "Service did not process request: " + str(exc)
            
            if iter % 2 == 0:
                self.gripper_pub.publish("150")
            else:
                self.gripper_pub.publish("227")
            rospy.sleep(1.3)

        next_path = [waypoints[0]]
        try:
            response = get_cartesian_plan(next_path)
            plan3 = response.plan
            is_solved = response.is_solved
        except ROSInterruptException as exc:
            print "Service did not process request: " + str(exc)

        try:
            response = execute_cartesian_plan(plan3)
            final_pose = response.final_pose
            is_reached = response.is_reached
        except ROSInterruptException as exc:
            print "Service did not process request: " + str(exc)  
        self.gripper_pub.publish("go")
        
        
    def run(self):
        rospy.Subscriber('hri/check_task_clock', String, self.callback_state_task_completion)
        rospy.Subscriber('hri/human_intervention', HumanIntervention, self.callback_human_intervention)
        self.set_default_angles()
        machine_state = 0
        print('Went to the default position. Start FSM.')
        self.execute_state_task(machine_state)
        rospy.spin()


if __name__ == '__main__':
    try:
        rospy.init_node('ur_control_wrapper_demo_mode', anonymous=True)

        demo = Demo()

        demo.run()
    except rospy.ROSInterruptException:
        pass
