#!/usr/bin/env python
"""
Created on Thur Nov 12 13:44:48 2020
A bag data reader and plotter for ur5e. This script pulls out the bag data into python data structure. 
As a replacement for the rqt_bag, which pull out all the data and plot them.
Since the rqt_bag uses a python API.
It's too slow to play back dense bag files well as some situations.
@author: junyi
"""

import rosbag
import numpy as np
import matplotlib.pyplot as plt
#from mpl_toolkits.mplot3d import Axes3D
from matplotlib.colors import Normalize
from matplotlib import cm
import matplotlib
from pylab import *
from tf import transformations as tfs
    
class Ur5eStates(object):
    """grabs all topics that are intereted or to be plotted"""
    def __init__(self, bag, robot_id):
        self.bag = rosbag.Bag(bag)
        self.get_states(robot_id)

    def get_states(self, robot_id):
        topic1 = "/" + robot_id + "/joint_states"
        topic2 = "/" + robot_id + "/ee_pose_jenny"
        topic3 = "/" + robot_id + "/gripper_status_jenny"

        msgs1 = self.bag.read_messages(topic1)
        joint_time = []
        joint_angles = []
        joint_velocity = []
        joint_effort = []
        for msg in msgs1:
            joint_angles.append(msg[1].position)
            joint_velocity.append(msg[1].velocity)
            joint_effort.append(msg[1].effort)
            joint_time.append(msg[1].header.stamp.to_sec())
        # converting to np array
        self.joint_name = msg[1].name
        self.joint_angles = np.asarray(joint_angles)
        self.joint_velocity = np.asarray(joint_velocity)
        self.joint_effort = np.asarray(joint_effort)
        self.joint_time = np.asarray(joint_time)-joint_time[0]

        msgs2 = self.bag.read_messages(topic2)
        ee_time = []
        ee_position = []
        ee_orientation = []
        ee_euler = []

        for msg in msgs2:
            ee_position.append([msg[1].position.x, msg[1].position.y, msg[1].position.z])
            ee_orientation.append([msg[1].orientation.x, msg[1].orientation.y,msg[1].orientation.z,msg[1].orientation.w])
            euler_temp = np.rad2deg(tfs.euler_from_quaternion(np.array([msg[1].orientation.x, 
                        msg[1].orientation.y, msg[1].orientation.z, msg[1].orientation.w])))
            ee_euler.append(euler_temp)
            ee_time.append(msg[2].to_sec())
        # converting to np array
        self.ee_position = np.asarray(ee_position)
        self.ee_orientation = np.asarray(ee_orientation)
        self.ee_euler = np.asarray(ee_euler)
        self.ee_time = np.asarray(ee_time)-ee_time[0]

        msgs3 = self.bag.read_messages(topic3)
        gripper_time = []
        gripper_position = []

        for msg in msgs3:
            gripper_position.append(msg[1].array.data[0])
            gripper_time.append(msg[1].header.stamp.to_sec())
        # converting to np array
        self.gripper_position = np.asarray(gripper_position)
        self.gripper_time = np.asarray(gripper_time)-gripper_time[0]        


#*****************************************main function to plot*************************************************#
plt.close('all')


path = '/home/jenny/Desktop/ur5e_bags/'
bag1 = path + 'gcs_multi_2021-07-16-17-49-10_two_success.bag' #'gcs_multi_2021-05-10-17-26-02.bag'   #'gcs_multi_2021-05-10-20-30-39.bag'
font = 15
font_legend = 13

# for iter in range(2):
#     vehi_id = "v" + str(iter + 1)
#     real = Ur5eStates(bag1, vehi_id)
#     # #### Plot the 6 joints angles of the arm
#     f1 = plt.figure(3.0*iter + 1)
#     plt.plot(real.joint_time, real.joint_angles[:,0], real.joint_time, real.joint_angles[:,1], 
#              real.joint_time, real.joint_angles[:,2], real.joint_time, real.joint_angles[:,3],
#              real.joint_time, real.joint_angles[:,4], real.joint_time, real.joint_angles[:,5])
#     plt.tick_params(labelsize = font)
#     plt.xlabel('time (s)', fontsize = font)
#     plt.ylabel('Joint Angles (radians)', fontsize = font)
#     plt.legend(real.joint_name, fontsize = font_legend, loc = 'upper right', ncol = 6)

#     ### Plot the end effector pose of the arm
#     # print(real.ee_position.shape)
#     # print(real.ee_time.shape)
#     f2 = plt.figure(3.0*iter + 2)
#     # Consider using subplot if you want
#     plt.tick_params(labelsize = font)
#     plt.plot(real.ee_time, real.ee_position[:,0], real.ee_time, real.ee_position[:,1], real.ee_time, real.ee_position[:,2])
#     plt.xlabel('time (s)', fontsize = font)
#     plt.ylabel('end-effector position (m)', fontsize = font)
#     plt.legend(['x (m)', 'y (m)', 'z (m)'], fontsize = font_legend)

#     f3 = plt.figure(3.0*iter + 3)
#     # Consider using subplot if you want
#     plt.plot(real.gripper_time, real.gripper_position)
#     plt.tick_params(labelsize = font)
#     plt.xlabel('time (s)', fontsize = font)
#     plt.ylabel('finger distance (m)', fontsize = font)

real1 = Ur5eStates(bag1, "v1")
real2 = Ur5eStates(bag1, "v2")
f2 = plt.figure(2)
plt.tick_params(labelsize = font)
plt.subplot(3,1,1)
plt.plot(real1.ee_time, real1.ee_position[:,0], real2.ee_time, real2.ee_position[:,0])
plt.ylabel('ee pos x (m)', fontsize = font)
plt.legend(['Robot 1','Robot 2'], fontsize = font_legend)
plt.subplot(3,1,2)
plt.plot(real1.ee_time, real1.ee_position[:,1], real2.ee_time, real2.ee_position[:,1])
plt.ylabel('ee pos y (m)', fontsize = font)
plt.subplot(3,1,3)
plt.plot(real1.ee_time, real1.ee_position[:,2], real2.ee_time, real2.ee_position[:,2])
plt.ylabel('ee pos z (m)', fontsize = font)
plt.xlabel('time (s)', fontsize = font)

f3 = plt.figure(3)
plt.tick_params(labelsize = font)

plt.plot(real1.gripper_time, real1.gripper_position, real2.gripper_time, real2.gripper_position)
plt.xlabel('time (s)', fontsize = font)
plt.ylabel('finger distance (m)', fontsize = font)
plt.legend(['Robot 1','Robot 2'], fontsize = font_legend)

plt.show()

            
            
            
            