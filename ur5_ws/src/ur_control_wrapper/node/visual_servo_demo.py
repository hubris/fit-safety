#!/usr/bin/env python

import rospy
import numpy as np
from std_msgs.msg import String, Bool
from sensor_msgs.msg import JointState, Image, CameraInfo
from geometry_msgs.msg import Pose, Point, Quaternion, Vector3
from ur_control_wrapper.srv import SetPose
from ur_control_wrapper.srv import GetPose
from ur_control_wrapper.srv import GetJoints
from ur_control_wrapper.srv import SetJoints
from ur_control_wrapper.srv import SetTrajectory
from ur_control_wrapper.srv import GetCartesianPlan
from ur_control_wrapper.srv import ExecuteCartesianPlan
from ur_control_wrapper.srv import AddObject, AddObjectRequest
from ur_control_wrapper.srv import AttachObject, AttachObjectRequest
from ur_control_wrapper.srv import DetachObject, DetachObjectRequest
from ur_control_wrapper.srv import RemoveObject, RemoveObjectRequest
from gripper_controller_jenny import GripperHande as gripper
from status_listener import StatusListener
from cv_bridge import CvBridge, CvBridgeError
import cv2
import apriltag
import time
from tf import transformations as tfs
import tf2_ros
from tf2_msgs.msg import TFMessage
from ur_control_wrapper.msg import Float32MultiArrayStamped

class VisualServoDemo:
    
    def __init__(self):
        # import pdb; pdb.set_trace()
        self.vs_force_msg = Float32MultiArrayStamped()
        self.corner_pixel_msg = Float32MultiArrayStamped()
        self.track_error_msg = Float32MultiArrayStamped()
        self.vs_force_pub = rospy.Publisher("vs_force", Float32MultiArrayStamped, queue_size=1)
        self.corner_pixel_pub = rospy.Publisher("corner_pixel", Float32MultiArrayStamped, queue_size=1)
        self.track_error_pub = rospy.Publisher("track_error", Float32MultiArrayStamped, queue_size=1)
        self.free_drive_pub = rospy.Publisher("ur_control_wrapper/enable_freedrive", Bool, queue_size=10)
        self.gripper_pub = rospy.Publisher("ur_control_wrapper/gripper", String, queue_size=10)
        self.connect_pub = rospy.Publisher("ur_control_wrapper/connect", Bool, queue_size=10)
        self.status = StatusListener() # this publishes several topics
        self.gripper_position_status = self.status.gripper_status.gPO
        # rospy.Subscriber("/tf", TFMessage, self.GetPartsPose)
        # self.parts_detect = set()
        self.cam_image_sub = rospy.Subscriber('/pylon_camera_node/image_raw', Image, self.image_callback)
        # self.tag_to_cam = rospy.Subscriber('/pylon_camera_node/image_raw', Image, self.image_callback2)
        self.cv_bridge = CvBridge()
        msg = rospy.wait_for_message('/pylon_camera_node/camera_info', CameraInfo)
        K = np.array(msg.K).reshape(3, 3)
        self.intrinsic_matrix = K
        print(self.intrinsic_matrix)
        # expression of base in cam
        self.extrinsic_matrix = np.array([[-0.040524961066899304, -0.9986312055640854, 0.03306914142199944, -0.052754060341385474], 
                                            [-0.9991719723340917, 0.04038221760954298, -0.0049724592629000735, -0.5256772148970205], 
                                            [0.0036303063724650564, -0.033243256035516, -0.9994406531911695, 1.7684893025670605], 
                                            [0.0, 0.0, 0.0, 1.0]])
        # options = apriltag.DetectorOptions(families='tag36h11',
        #                          border=1,
        #                          nthreads=4,
        #                          quad_decimate=1.0,
        #                          quad_blur=0.0,
        #                          refine_edges=True,
        #                          refine_decode=False,
        #                          refine_pose=False,
        #                          debug=False,
        #                          quad_contours=True)
        options = apriltag.DetectorOptions(quad_blur=2.0, refine_decode=True, refine_pose=True)
        self.tag_detector = apriltag.Detector(options)
        # self.tag_detector = Detector(families='tag36h11', nthreads=1, quad_decimate=1.0, quad_sigma=0.0, refine_edges=1, decode_sharpening=0.25, debug=0)
        self.track_start = False

    def get_pose(self):
        rospy.wait_for_service("ur_control_wrapper/get_pose")
        get_current_pose = rospy.ServiceProxy("ur_control_wrapper/get_pose", GetPose)
        current_pose = None
        try:
            current_pose = get_current_pose().pose
            current_euler = np.rad2deg(tfs.euler_from_quaternion(np.array([current_pose.orientation.x, 
                        current_pose.orientation.y, current_pose.orientation.z, current_pose.orientation.w])))
            # print("euler_angle = ", current_euler)
        except rospy.ServiceException as exc:
            print("Service did not process request: " + str(exc))
        return current_pose

    def image_callback(self, img):
        try:
            self.img = self.cv_bridge.imgmsg_to_cv2(img, 'rgb8')
        except CvBridgeError as e:
            print("CvBridge could not convert images from realsense to opencv")
        self.gray = cv2.cvtColor(self.img, cv2.COLOR_BGR2GRAY)
        if self.track_start:
            self.track_tag_3d()
        
    def detect_tag(self):
        # snap_time=time.time()
        # cv2.imwrite('./data/{}_color.png'.format(snap_time), self.img)
        # cv2.imwrite('./data/{}_gray.png'.format(snap_time), self.gray)
        tag_detections = self.tag_detector.detect(self.gray)
        # print('tag= ')
        # print(tag_detections)
        # for tag in tag_detections:
        #     print(tag.tag_id)
        #     print(tag.center)
        #     print(tag.corners)
        return tag_detections

    def track_tag(self):
        tag_detections = self.detect_tag()
        if not tag_detections:
            print('no tag!!!')
            return
        # current_corners = np.array([tag_detections[0].corners[3], tag_detections[1].corners[2], 
        #                             tag_detections[2].corners[0], tag_detections[3].corners[1]]).flatten('F')
        current_corners = np.array(tag_detections[0].corners).flatten('F')      
        self.corner_pixel_msg.header.stamp = rospy.Time.now()
        self.corner_pixel_msg.array.data = np.array(current_corners)
        self.corner_pixel_pub.publish(self.corner_pixel_msg)       
        # this is the pixel coordinates after vd command
        desired_corners = np.array([[2908.87158203, 2380.71313476],
                                    [2913.43261719, 1808.5111084 ],
                                    [3486.73999023, 1813.12414551],
                                    [3481.86279297, 2385.45117188]]).flatten('F')
        track_error = np.linalg.norm(current_corners - desired_corners)
        self.track_error_msg.header.stamp = rospy.Time.now()
        self.track_error_msg.array.data = np.array([track_error])
        self.track_error_pub.publish(self.track_error_msg)
        rot_mat = self.extrinsic_matrix[:3, :3]
        z_init = 1.7684893025670605 - 0.23365
        delta_t = 0.05
        tol = 0
        if track_error > tol:
            print(track_error)
            vel = self.visual_servo(current_corners, desired_corners, rot_mat, z_init)
            current_pose = self.get_pose()
            delta = vel*delta_t 
            wp_next = self.get_delta_wp(current_pose, delta)
            # print(type(wp_next))
            self.execute_trajectory([wp_next])
        else:
            print('tracking is done')
            self.track_start = False

    def track_tag_3d(self):
        tag_detections = self.detect_tag()
        # for 3d visual servo, this should be in the form [x1 y1 x2 y2 ...]
        if not tag_detections:
            print('no tag!!!')
            return
        current_corners = np.array([tag_detections[0].corners[3], tag_detections[1].corners[2], 
                                    tag_detections[2].corners[0], tag_detections[3].corners[1]]).flatten('C')
        # current_corners = np.array(tag_detections[0].corners).flatten('C')
        self.corner_pixel_msg.header.stamp = rospy.Time.now()
        self.corner_pixel_msg.array.data = np.array(current_corners)
        self.corner_pixel_pub.publish(self.corner_pixel_msg)   
        # this is the pixel coordinates after vd command
        desired_corners = np.array([[3480.51464843, 2384.36401367],
                                    [3491.21948242, 1168.19921875],
                                    [2264.71411133, 2374.41479492],
                                    [2273.84033203, 1158.47546387]]).flatten('C')
        # desired_corners = np.array([[3478.58227539, 2388.00146484],
        #                             [3489.81665039, 1171.60827637],
        #                             [2262.60791015, 2377.51464844],
        #                             [2272.34887695, 1161.44177246]]).flatten('C')
        # desired_corners = np.array([[2906.37548828, 2383.19970703],
        #                             [2911.25317383, 1810.83898926],
        #                             [3484.70727539, 1815.66479492],
        #                             [3479.55444336, 2388.10571289]]).flatten('C')
        # rot_mat = self.extrinsic_matrix[:3, :3]
        track_error = np.linalg.norm(current_corners - desired_corners)
        self.track_error_msg.header.stamp = rospy.Time.now()
        self.track_error_msg.array.data = np.array([track_error])
        self.track_error_pub.publish(self.track_error_msg)
        z0_des = 1.53416354
        z1_des = 1.53365012
        z2_des = 1.53350265
        z3_des = 1.53295036
        # z0_des = 1.53385253
        # z1_des = 1.53359263
        # z2_des = 1.53390363
        # z3_des = 1.53416354
        z_des = np.array([z0_des, z1_des, z2_des, z3_des])
        delta_t = 1e-4
        tol = 1
        if track_error > tol:
            print('track error: ', track_error)
            current_pose = self.get_pose()
            vel = self.visual_servo_3d(current_corners, desired_corners, current_pose, z_des)
            print('command vel: ', vel)
            # print('norm of vel: ', np.linalg.norm(vel))
            # normalize the vel command
            # vel = vel / np.linalg.norm(vel)/2
            vel = np.log10(track_error) * vel / np.linalg.norm(vel)
            print('command vel - scale: ', vel)

            delta = vel[0:3]*delta_t
            current_position = np.array([current_pose.position.x, current_pose.position.y, current_pose.position.z])
            position_next = current_position + delta
            # print(position_next)

            rot_x_delta = tfs.rotation_matrix(vel[3]*delta_t, [1,0,0])
            rot_y_delta = tfs.rotation_matrix(vel[4]*delta_t, [0,1,0])
            rot_z_delta = tfs.rotation_matrix(vel[5]*delta_t, [0,0,1])
            # the order of multiplication
            rot_delta = np.matmul(rot_z_delta, np.matmul(rot_x_delta, rot_y_delta))
            rot_delta_2 = np.matmul(rot_x_delta, np.matmul(rot_y_delta, rot_z_delta))
            rot_delta_3 = np.matmul(rot_y_delta, np.matmul(rot_x_delta, rot_z_delta))
            rot_err_1 = rot_delta - rot_delta_2
            rot_err_2 = rot_delta_3 - rot_delta_2
            rot_err_3 = rot_delta - rot_delta_3
            # rot_delta = (rot_delta + rot_delta_2 + rot_delta_3)/3
            print('norm of error: ', np.linalg.norm(rot_err_1), np.linalg.norm(rot_err_2), np.linalg.norm(rot_err_3))
            rot_e2w = tfs.quaternion_matrix([current_pose.orientation.x, current_pose.orientation.y, current_pose.orientation.z,
                current_pose.orientation.w])
            rot_next = np.matmul(rot_e2w, rot_delta)
            quaternion_next = tfs.quaternion_from_matrix(rot_next)
            # print(quaternion_next)

            wp_next = Pose()
            wp_next.position.x = position_next[0]
            wp_next.position.y = position_next[1]
            wp_next.position.z = position_next[2]
            wp_next.orientation.x = quaternion_next[0]
            wp_next.orientation.y = quaternion_next[1]
            wp_next.orientation.z = quaternion_next[2]
            wp_next.orientation.w = quaternion_next[3]
            # print(wp_next)

            # delta_pose = self.delta_pose_in_ee(vel, current_pose)
            # delta = delta_pose*delta_t 
            # wp_next = self.get_delta_wp(current_pose, delta)
            self.execute_trajectory([wp_next])
        else:
            print('tracking is done')
            self.track_start = False

    # def track_tag_3d(self):
    #     tag_detections = self.detect_tag()
    #     # for 3d visual servo, this should be in the form [x1 y1 x2 y2 ...]
    #     if not tag_detections:
    #         print('no tag!!!')
    #         return
    #     current_corners = np.array(tag_detections[0].corners).flatten('C')

    #     desired_corners = np.array([[2943.06054688, 2310.5961914],
    #                                 [2948.40332031, 1736.93774414],
    #                                 [3523.1875,     1742.47192383],
    #                                 [3517.32836914, 2316.26342774]]).flatten('C')
    #     # rot_mat = self.extrinsic_matrix[:3, :3]
    #     z_des = 1.53860
    #     delta_t = 0.1
    #     tol = 20
    #     if np.linalg.norm(current_corners - desired_corners) > tol:
    #         current_pose = self.get_pose()
    #         vel = self.visual_servo_3d(current_corners, desired_corners, current_pose, z_des)
    #         delta_pose = self.delta_pose_in_ee(vel, current_pose)
    #         delta = delta_pose*delta_t 
    #         wp_next = self.get_delta_wp(current_pose, delta)
    #         self.execute_trajectory([wp_next])
    #     else:
    #         print('tracking is done')
    #         self.track_start = False

    def visual_servo(self, current, desired, rot_mat, z_init):
        # intrinsic params
        cu = self.intrinsic_matrix[0, 2]
        cv = self.intrinsic_matrix[1, 2]
        fx = self.intrinsic_matrix[0, 0]
        fy = self.intrinsic_matrix[1, 1]
        arr_len = len(current)
        # height of the camera over the tag
        z = z_init
        # convert pixel coordinates to coordinates in image frame (in mm)
        x = (current[0:arr_len//2] - cu) / fx
        y = (current[arr_len//2:] - cv) / fy
        x_d = (desired[0:arr_len//2] - cu) / fx
        y_d = (desired[arr_len//2:] - cv) / fy
        e = np.append(x - x_d, y - y_d)
        Le = np.array([[1/z, 0, -x[0]/z], [1/z, 0, -x[1]/z], [1/z, 0, -x[2]/z], [1/z, 0, -x[3]/z], \
        [0, 1/z, -y[0]/z], [0, 1/z, -y[1]/z], [0, 1/z, -y[2]/z], [0, 1/z, -y[3]/z]])
        # position gain
        gain_p = 1
        Le_inv = np.matmul(np.linalg.inv(np.matmul(np.transpose(Le), Le)), np.transpose(Le))
        vc = - gain_p * np.matmul(Le_inv, e)
        # project vc from camera frame to the world frame
        vc = np.matmul(rot_mat, vc)
        # print(vc.shape)
        self.vs_force_msg.header.stamp = rospy.Time.now()
        self.vs_force_msg.array.data = np.array(vc)
        self.vs_force_pub.publish(self.vs_force_msg)
        return vc

    def visual_servo_3d(self, current, desired, ee_pose, z_des):
        # current/desired data format: [x1, y1, x2, y2, x3, y3, x4, y4]
        # intrinsic params
        cu = self.intrinsic_matrix[0, 2]
        cv = self.intrinsic_matrix[1, 2]
        fx = self.intrinsic_matrix[0, 0]
        fy = self.intrinsic_matrix[1, 1]       
        # convert pixel coordinates to coordinates in image frame
        x = (current[[0, 2, 4, 6]] - cu) / fx
        y = (current[[1, 3, 5, 7]] - cv) / fy
        x_d = (desired[[0, 2, 4, 6]] - cu) / fx
        y_d = (desired[[1, 3, 5, 7]] - cv) / fy
        e = current - desired
        # position of 4 object points wrt the origin of end-effector (ee) frame (constant, we need to measure it)
        r0_1 = 0.00279874
        r0_2 = -0.03644232
        r0_3 = 0.04746394
        r1_1 = 0.00310047
        r1_2 = -0.0394452
        r1_3 = -0.03748247
        r2_1 = 0.00274798
        r2_2 = 0.04850459
        r2_3 = 0.04446086
        r3_1 = 0.00304971
        r3_2 = 0.04550172
        r3_3 = -0.04048554
        # r0_1 = 0.00277485
        # r0_2 = 0.0035327
        # r0_3 = 0.04605072
        # r1_1 = 0.00291684
        # r1_2 = 0.00211958
        # r1_3 = 0.00607594
        # r2_1 = 0.00294073
        # r2_2 = -0.03785544
        # r2_3 = 0.00748916
        # r3_1 = 0.00279874
        # r3_2 = -0.03644232
        # r3_3 = 0.04746394       
        # obtain the rotation matrix from ee frame to the camera frame
        rot_e2w = tfs.quaternion_matrix([ee_pose.orientation.x, ee_pose.orientation.y, ee_pose.orientation.z,
                ee_pose.orientation.w])
        rot_e2c = np.matmul(self.extrinsic_matrix[:3,:3].transpose(), rot_e2w[:3,:3])
        # interaction matrix
        Le_temp1 = np.array([[1/z_des[0], 0, -x[0]/z_des[0]], [0, 1/z_des[0], -y[0]/z_des[0]]]) # 2 by 3 matrix
        Le_temp2 = np.matmul(Le_temp1, rot_e2c)
        Le_temp3 = np.array([[1, 0, 0, 0, r0_3, -r0_2], [0, 1, 0, -r0_3, 0, r0_1], [0, 0, 1, r0_2, -r0_1, 0]]) # 3 by 6 matrix
        Le0 = np.matmul(Le_temp2, Le_temp3) # 2 by 6 matrix
        # do the same for the rest 3 points
        Le_temp1 = np.array([[1/z_des[1], 0, -x[1]/z_des[1]], [0, 1/z_des[1], -y[1]/z_des[1]]]) # 2 by 3 matrix
        Le_temp2 = np.matmul(Le_temp1, rot_e2c)
        Le_temp3 = np.array([[1, 0, 0, 0, r1_3, -r1_2], [0, 1, 0, -r1_3, 0, r1_1], [0, 0, 1, r1_2, -r1_1, 0]]) # 3 by 6 matrix
        Le1 = np.matmul(Le_temp2, Le_temp3) # 2 by 6 matrix

        Le_temp1 = np.array([[1/z_des[2], 0, -x[2]/z_des[2]], [0, 1/z_des[2], -y[2]/z_des[2]]]) # 2 by 3 matrix
        Le_temp2 = np.matmul(Le_temp1, rot_e2c)
        Le_temp3 = np.array([[1, 0, 0, 0, r2_3, -r2_2], [0, 1, 0, -r2_3, 0, r2_1], [0, 0, 1, r2_2, -r2_1, 0]]) # 3 by 6 matrix
        Le2 = np.matmul(Le_temp2, Le_temp3) # 2 by 6 matrix

        Le_temp1 = np.array([[1/z_des[3], 0, -x[3]/z_des[3]], [0, 1/z_des[3], -y[3]/z_des[3]]]) # 2 by 3 matrix
        Le_temp2 = np.matmul(Le_temp1, rot_e2c)
        Le_temp3 = np.array([[1, 0, 0, 0, r3_3, -r3_2], [0, 1, 0, -r3_3, 0, r3_1], [0, 0, 1, r3_2, -r3_1, 0]]) # 3 by 6 matrix
        Le3 = np.matmul(Le_temp2, Le_temp3) # 2 by 6 matrix
        Le = np.vstack((Le0, Le1, Le2, Le3))
        # position gain
        gain_p = 0.01
        Le_inv = np.matmul(np.linalg.inv(np.matmul(np.transpose(Le), Le)), np.transpose(Le))
        vc = - gain_p * np.matmul(Le_inv, e)
        # print('here vc: ', vc)
        self.vs_force_msg.header.stamp = rospy.Time.now()
        self.vs_force_msg.array.data = np.array(vc)
        self.vs_force_pub.publish(self.vs_force_msg)
        return vc

    def delta_pose_in_ee(self, vc, current_pose):
        trans = np.array([current_pose.position.x, current_pose.position.y, current_pose.position.z])
        quat = np.array([current_pose.orientation.x, current_pose.orientation.y, current_pose.orientation.z, current_pose.orientation.w])
        rot_homo = tfs.quaternion_matrix(quat)
        rot = rot_homo[:3, :3]

        p_dot = np.matmul(rot, vc[:3])
        w_aug = np.append(0.0, vc[-3:])
        q_dot = 0.5*quat*w_aug

        vw = np.append(p_dot, q_dot)
        print('vw = ', vw)
        return vw

    def get_angle(self):
        rospy.wait_for_service("ur_control_wrapper/get_joints")
        get_current_joints = rospy.ServiceProxy("ur_control_wrapper/get_joints", GetJoints)
        current_joints = None
        try:
            current_joints = get_current_joints().joints
        except rospy.ServiceException as exc:
            print("Service did not process request: " + str(exc))
        return current_joints
    
    def set_default_angles(self, joints_angles = [0.0, -2.0 * np.pi / 3, -np.pi / 3.0, np.pi / 2.0, -np.pi / 2.0, 0.0]):
        rospy.wait_for_service("ur_control_wrapper/set_joints")
        set_joints = rospy.ServiceProxy("ur_control_wrapper/set_joints", SetJoints)
        joints = JointState()
        #joints.name = ["elbow_joint", "shoulder_lift_joint", "shoulder_pan_joint", "wrist_1_joint", "wrist_2_joint", "wrist_3_joint"]
        #joints.position = [-np.pi / 3.0, -2.0 * np.pi / 3, 0.0, np.pi * 1.0 / 2.0, -np.pi / 2.0, 0.0]
        joints.name = ["shoulder_pan_joint", "shoulder_lift_joint", "elbow_joint", "wrist_1_joint", "wrist_2_joint", "wrist_3_joint"]
        #joints.position = [0.023975849151611328, -2.173887868920797, -1.0581588745117188, 1.6445128160664062, -1.570674244557516, 0.0]
        joints.position = joints_angles  # [0.0, -2.0 * np.pi / 3, -np.pi / 3.0, np.pi / 2.0, -np.pi / 2.0, 0.0]
#        [0.0011186599731445312, -2.344504495660299, -1.4683351516723633, 2.2413531976887207, -1.5681212584124964, 0.004898548126220703]
#        [0.0010228157043457031, -2.343271394769186, -1.4688987731933594, 2.242299719447754, -1.5686605612384241, 0.004286289215087891]
        
        try:
            response = set_joints(joints)
        except rospy.ServiceException as exc:
            print("Service did not process request: " + str(exc))

        self.gripper_pub.publish("100")
    
    def set_align_angles(self):
        rospy.wait_for_service("ur_control_wrapper/set_joints")
        set_joints = rospy.ServiceProxy("ur_control_wrapper/set_joints", SetJoints)
        joints = JointState()
        joints.name = ["shoulder_pan_joint", "shoulder_lift_joint", "elbow_joint", "wrist_1_joint", "wrist_2_joint", "wrist_3_joint"]
        joints.position = [0.1562185287475586, -2.5559779606261195, -1.586348533630371, 2.5811564165302734, -0.03694612184633428, 1.570235252380371]
        try:
            response = set_joints(joints)
        except rospy.ServiceException as exc:
            print("Service did not process request: " + str(exc))

    def set_align_pick_angles(self):
        rospy.wait_for_service("ur_control_wrapper/set_joints")
        set_joints = rospy.ServiceProxy("ur_control_wrapper/set_joints", SetJoints)
        joints = JointState()
        joints.name = ["shoulder_pan_joint", "shoulder_lift_joint", "elbow_joint", "wrist_1_joint", "wrist_2_joint", "wrist_3_joint"]
        joints.position = [-0.31185847917665654, -2.4204427204527796, -1.0834665298461914, 1.9517590242573242, -1.5629780928241175, -0.46342593828310186]
        try:
            response = set_joints(joints)
        except rospy.ServiceException as exc:
            print("Service did not process request: " + str(exc))
     
        
    def set_joints(self, direction):
        amount = float(direction[2:])
        joint = direction[:2]
        current_joints = self.get_angle()
        rospy.wait_for_service("ur_control_wrapper/set_joints")
        set_joints = rospy.ServiceProxy("ur_control_wrapper/set_joints", SetJoints)
        joints = JointState()
        joints.name = current_joints.name
        joint_angle = list(current_joints.position)
        try:
            if joint == "1+":
                joint_angle[0] += np.deg2rad(amount)
                print("Current joint 1 angle: %f degrees" % (np.rad2deg(joint_angle[0])))
            elif joint == "1-":
                joint_angle[0] -= np.deg2rad(amount)
                print("Current joint 1 angle: %f degrees" % (np.rad2deg(joint_angle[0])))
            elif joint == "2+":
                joint_angle[1] += np.deg2rad(amount)
                print("Current joint 2 angle: %f degrees" % (np.rad2deg(joint_angle[1])))
            elif joint == "2-":
                joint_angle[1] -= np.deg2rad(amount)
                print("Current joint 2 angle: %f degrees" % (np.rad2deg(joint_angle[1])))
            elif joint == "3+":
                joint_angle[2] += np.deg2rad(amount)
                print("Current joint 3 angle: %f degrees" % (np.rad2deg(joint_angle[2])))
            elif joint == "3-":
                joint_angle[2] -= np.deg2rad(amount)
                print("Current joint 3 angle: %f degrees" % (np.rad2deg(joint_angle[2])))
            elif joint == "4+":
                joint_angle[3] += np.deg2rad(amount)
                print("Current joint 4 angle: %f degrees" % (np.rad2deg(joint_angle[3])))
            elif joint == "4-":
                joint_angle[3] -= np.deg2rad(amount)
                print("Current joint 4 angle: %f degrees" % (np.rad2deg(joint_angle[3])))
            elif joint == "5+":
                joint_angle[4] += np.deg2rad(amount)
                print("Current joint 5 angle: %f degrees" % (np.rad2deg(joint_angle[4])))
            elif joint == "5-":
                joint_angle[4] -= np.deg2rad(amount)
                print("Current joint 5 angle: %f degrees" % (np.rad2deg(joint_angle[4])))
            elif joint == "6+":
                joint_angle[5] += np.deg2rad(amount)
                print("Current joint 6 angle: %f degrees" % (np.rad2deg(joint_angle[5])))
            elif joint == "6-":
                joint_angle[5] -= np.deg2rad(amount)
                print("Current joint 6 angle: %f degrees" % (np.rad2deg(joint_angle[5])))
            joints.position = tuple(joint_angle)
            response = set_joints(joints)
        except rospy.ServiceException as exc:
            print("Service did not process request: " + str(exc))
        
    def control_gripper(self, command):
        if len(command) > 3:
            amount = float(command[3:])
        direction = command[:3]
        # print("Current gripper position: %f" % (self.gripper_position_status))
        if direction == "gp+":
            self.gripper_position_status += amount
            if self.gripper_position_status > 255:
                self.gripper_position_status = 255
            elif self.gripper_position_status < 0:
                self.gripper_position_status = 0
            self.gripper_pub.publish('%f' % (self.gripper_position_status))
            # print("Current gripper position: %f" % (self.gripper_position_status))
        elif direction == "gp-":
            self.gripper_position_status -= amount
            if self.gripper_position_status > 255:
                self.gripper_position_status = 255
            elif self.gripper_position_status < 0:
                self.gripper_position_status = 0
            self.gripper_pub.publish('%f' % (self.gripper_position_status))
            # print("Current gripper position: %f" % (self.gripper_position_status))
        elif direction == "gf+":
            self.gripper_pub.publish('gi')
        elif direction == "gf-":
            self.gripper_pub.publish('gd') 
        elif direction == "gs+":
            self.gripper_pub.publish('gf')
        elif direction == "gs-":
            self.gripper_pub.publish('gl')
        elif direction == "gop":
            self.gripper_pub.publish('go')
        elif direction == "gcl":
            self.gripper_pub.publish('gc')
        elif direction == "gac":
            self.gripper_pub.publish('ga')
        elif direction == "grs":
            self.gripper_pub.publish('gr') 

    def move_arm(self, command):
        amount = float(command[2:])
        direction = command[:2]
        current_pose = self.get_pose()
        current_euler = np.rad2deg(tfs.euler_from_quaternion(np.array([current_pose.orientation.x, 
                        current_pose.orientation.y, current_pose.orientation.z, current_pose.orientation.w])))
        print("euler_before = ", current_euler)                
        rospy.wait_for_service("ur_control_wrapper/set_pose")
        set_current_pose = rospy.ServiceProxy("ur_control_wrapper/set_pose", SetPose)
        try:
            if direction == "x+":
                current_pose.position.x += (amount/100) # convert to centimeters
                print("Current x position: %f" % (current_pose.position.x))
            elif direction == "x-":
                current_pose.position.x -= (amount/100) # convert to centimeters
                print("Current x position: %f" % (current_pose.position.x))
            elif direction == "y+":
                current_pose.position.y += (amount/100) # convert to centimeters
                print("Current y position: %f" % (current_pose.position.y))
            elif direction == "y-":
                current_pose.position.y -= (amount/100) # convert to centimeters
                print("Current y position: %f" % (current_pose.position.y))
            elif direction == "z+":
                current_pose.position.z += (amount/100) # convert to centimeters
                print("Current z position: %f" % (current_pose.position.z))
            elif direction == "z-":
                current_pose.position.z -= (amount/100) # convert to centimeters
                print("Current z position: %f" % (current_pose.position.z))

            elif direction == "r+":
                current_euler[0] += amount
                print("Current end effector roll: %f" % (current_euler[0]))
            elif direction == "r-":
                current_euler[0] -= amount
                print("Current end effector roll: %f" % (current_euler[0]))
            elif direction == "p+":
                current_euler[1] += amount
                print("Current end effector pitch: %f" % (current_euler[1]))
            elif direction == "p-":
                current_euler[1] -= amount
                print("Current end effector pitch: %f" % (current_euler[1]))
            elif direction == "w+":
                current_euler[2] += amount
                print("Current end effector yaw: %f" % (current_euler[2]))
            elif direction == "w-":
                current_euler[2] -= amount
                print("Current end effector yaw: %f" % (current_euler[2]))

            print("euler_after = ", current_euler)
            ori = np.deg2rad(current_euler)
            quat = tfs.quaternion_from_euler(ori[0], ori[1], ori[2])
            current_pose.orientation.x = quat[0]
            current_pose.orientation.y = quat[1]
            current_pose.orientation.z = quat[2]
            current_pose.orientation.w = quat[3]

            response = set_current_pose(current_pose)
            pose = response.response_pose
            is_reached = response.is_reached
        except rospy.ServiceException as exc:
            print("Service did not process request: " + str(exc))
    
    def waypoint_following(self):
        current_pose = self.get_pose()

        rospy.wait_for_service("ur_control_wrapper/follow_trajectory")
        set_trajectory = rospy.ServiceProxy("ur_control_wrapper/follow_trajectory", SetTrajectory)

        waypoints = []
        delta = [[-0.1, 0.0, 0.0], [-0.1, 0.0, -0.1], [-0.1, 0.0, 0.0], [0.0, 0.0, 0.0]]
        for wp in delta:
            wpi = Pose()
            wpi.position.x = current_pose.position.x + wp[0]
            wpi.position.y = current_pose.position.y + wp[1]
            wpi.position.z = current_pose.position.z + wp[2]
            wpi.orientation.x = current_pose.orientation.x
            wpi.orientation.y = current_pose.orientation.y
            wpi.orientation.z = current_pose.orientation.z
            wpi.orientation.w = current_pose.orientation.w
            waypoints.append(wpi)
        try:
            response = set_trajectory(waypoints)
            final_pose = response.final_pose
            is_reached = response.is_reached
        except ROSInterruptException as exc:
            print("Service did not process request: " + str(exc))

    def test_desired_ee_wp(self, input):
        self.enter_and_close(input)
        current_pose = self.get_pose()
        self.lift_down_open(current_pose)
        wp = self.get_delta_wp(self.get_pose(), [0.0, 0.0, 0.012])
        self.execute_trajectory([wp])

        # self.set_default_angles(joints_angles = [0.12123775482177734, -2.2965408764281214, -1.5038652420043945, 2.22770540296521, -1.5678570906268519, 0.12182283401489258])
        # wp = self.get_delta_wp(self.get_pose(), [0.0, 0.0, 0.012])
        # self.execute_trajectory([wp])

    def enter_and_close(self, input):
        wp = self.get_wp_from_part(input)

        wp_p = self.get_delta_wp(wp, [0.0, 0.0, 0.01])
        self.execute_trajectory([wp_p, wp])

        for gi in range(5):
            self.control_gripper("gs-")

        grip_num = "247"
        self.gripper_pub.publish(grip_num)
        rospy.sleep(2.5)

        for gi in range(5):
            self.control_gripper("gs+")

    def lift_down_open(self, current_pose, lift_delta = 0.02):
        delta = [0, 0, lift_delta]
        wpi2 = self.get_delta_wp(current_pose, delta)
        self.execute_trajectory([wpi2, current_pose])

        for gi in range(5):
            self.control_gripper("gs-")

        self.gripper_pub.publish("100")
        rospy.sleep(2.5)

        for gi in range(5):
            self.control_gripper("gs+")

    def get_delta_wp(self, current_pose, delta):
        qx = 0
        qy = 0
        qz = 0
        qw = 0
        if len(delta) > 3:
            qx = delta[3]
            qy = delta[4]
            qz = delta[5]
            qw = delta[6]
        wp = Pose()
        wp.position.x = current_pose.position.x + delta[0]
        wp.position.y = current_pose.position.y + delta[1]
        wp.position.z = current_pose.position.z + delta[2]
        wp.orientation.x = current_pose.orientation.x + qx
        wp.orientation.y = current_pose.orientation.y + qy
        wp.orientation.z = current_pose.orientation.z + qz
        wp.orientation.w = current_pose.orientation.w + qw
        return wp

    def get_wp_from_part(self, input):
        user_input = input.split("/")
        partsname = user_input[2]
        orientation = user_input[3]
        lg = 0.16
        grasp_offset = 0.0
        grasp_offset2 = -0.011 #-0.014
        # gripper pose relative to parts origin
        parts_c2o = np.array([0.0, 0.0, 0.0])*0.001                      
        ee_in_parts = {'main_shell': {'top':[parts_c2o[0], parts_c2o[1] + grasp_offset, parts_c2o[2] - lg, 0.0, -np.pi/2.0, np.pi/2.0], 
                                     'bottom':[parts_c2o[0], parts_c2o[1]+ grasp_offset, parts_c2o[2] + lg, 0.0, np.pi/2.0, -np.pi/2.0],
                                     'left':[parts_c2o[0], parts_c2o[1]-lg, parts_c2o[2], -np.pi/2.0, 0.0, np.pi/2.0]},
                       'top_shell': {'top':[parts_c2o[0], parts_c2o[1]+ grasp_offset, parts_c2o[2] - lg, 0.0, -np.pi/2.0, np.pi/2.0],
                                     'bottom':[parts_c2o[0], parts_c2o[1]+ grasp_offset, parts_c2o[2] + lg, 0.0, np.pi/2.0, np.pi/2.0]},
                       'insert_mold': {'bottom':[parts_c2o[0], parts_c2o[1] - grasp_offset2, parts_c2o[2] + lg, 0.0, np.pi/2.0, np.pi/2.0]},
                       'goal': {'top':[parts_c2o[0], parts_c2o[1]+ grasp_offset, parts_c2o[2] - lg, 0.0, -np.pi/2.0, -np.pi/2.0]}}
        tfBuffer = tf2_ros.Buffer()
        listener = tf2_ros.TransformListener(tfBuffer)
        try:
            trans = tfBuffer.lookup_transform("world", input, rospy.Time(), rospy.Duration(1.0))
        except (tf2_ros.LookupException, tf2_ros.ConnectivityException, tf2_ros.ExtrapolationException):
            print("exception!! when getting parts transform")
            return
        #print(trans)
        parts_p = np.array([trans.transform.translation.x, trans.transform.translation.y, trans.transform.translation.z])
        trans_ee2parts = np.array(ee_in_parts[partsname][orientation][:3])
        M_p2w = tfs.quaternion_matrix(np.array([trans.transform.rotation.x, 
                        trans.transform.rotation.y, trans.transform.rotation.z, trans.transform.rotation.w]))
        euler = ee_in_parts[partsname][orientation][3:6]
        M_e2p = tfs.euler_matrix(euler[0], euler[1], euler[2], 'sxyz')
        M_w2p = M_p2w.T
        M_p2e = M_e2p.T
        ee_p = parts_p + np.dot(M_p2w[:3,:3],trans_ee2parts)
        ee_r = np.dot(M_p2e[:3,:3], M_w2p[:3,:3])
        M_ee = np.identity(4)
        M_ee[:3,:3] = ee_r.T
        ee_quat = tfs.quaternion_from_matrix(M_ee)

        wp = Pose()
        wp.position.x = ee_p[0]
        wp.position.y = ee_p[1]
        wp.position.z = ee_p[2]
        wp.orientation.x = ee_quat[0]
        wp.orientation.y = ee_quat[1]
        wp.orientation.z = ee_quat[2]
        wp.orientation.w = ee_quat[3]

        return wp

    # def GetPartsPose(self, tfs):
    #     for tf in tfs.transforms:
    #         if tf.header.frame_id == "world" and tf.child_frame_id.split('/')[0]=='poserbpf':
    #             self.parts_detect.add(tf.child_frame_id)

    def execute_trajectory(self, wps):
        """
            Executing a list of wp
        """
        waypoints = []
        for wp in wps:
            waypoints.append(wp)
        rospy.wait_for_service("ur_control_wrapper/follow_trajectory")
        set_trajectory = rospy.ServiceProxy("ur_control_wrapper/follow_trajectory", SetTrajectory)
        try:
            response = set_trajectory(waypoints)
            final_pose = response.final_pose
            is_reached = response.is_reached
        except ROSInterruptException as exc:
            print("Service did not process request: " + str(exc))

    def execute_joints(self, joints_wp):
        """
            Execuring a wp in joints angle format
        """
        rospy.wait_for_service("ur_control_wrapper/set_joints")
        set_joints = rospy.ServiceProxy("ur_control_wrapper/set_joints", SetJoints)
        joints = JointState()
        joints.name = ["shoulder_pan_joint", "shoulder_lift_joint", "elbow_joint", "wrist_1_joint", "wrist_2_joint", "wrist_3_joint"]
        joints.position = joints_wp
        try:
            response = set_joints(joints)
        except rospy.ServiceException as exc:
            print("Service did not process request: " + str(exc))

    def add_box(self):
        rospy.wait_for_service("ur_control_wrapper/add_object")
        add_box = rospy.ServiceProxy("ur_control_wrapper/add_object", AddObject)
        try:
            name = "box_object"
            pose = Pose(Point(-0.54, 0.34, 0.025), Quaternion(0.0, 0.0, 0.0, 1.0))
            size = Vector3(0.4, 0.20, 0.05)
            object_type = AddObjectRequest.TYPE_BOX
            response = add_box(name, pose, size, AddObjectRequest.TYPE_BOX).is_success
        except rospy.ServiceException as exc:
            print("Service did not process request: " + str(exc))

        try:
            name = "box_object2"
            pose = Pose(Point(-0.54, 0.34, 0.07), Quaternion(0.0, 0.0, 0.0, 1.0))
            size = Vector3(0.4, 0.07, 0.04)
            object_type = AddObjectRequest.TYPE_BOX
            response = add_box(name, pose, size, AddObjectRequest.TYPE_BOX).is_success
        except rospy.ServiceException as exc:
            print("Service did not process request: " + str(exc))

    def add_parts(self, part_name, part_pose, part_size):
        rospy.wait_for_service("ur_control_wrapper/add_object")
        add_box = rospy.ServiceProxy("ur_control_wrapper/add_object", AddObject)
        try:
            name = part_name
            pose = Pose(Point(part_pose[0],part_pose[1], part_pose[2]), Quaternion(part_pose[3], part_pose[4], part_pose[5], part_pose[6]))
            size = Vector3(part_size[0], part_size[1], part_size[2])
            object_type = AddObjectRequest.TYPE_BOX
            response = add_box(name, pose, size, AddObjectRequest.TYPE_BOX).is_success
        except rospy.ServiceException as exc:
            print("Service did not process request: " + str(exc))
    
    def attach_box(self):
        print("wait for service")
        rospy.wait_for_service("ur_control_wrapper/attach_object")
        print("service found")
        attach_box = rospy.ServiceProxy("ur_control_wrapper/attach_object", AttachObject)
        try:
            name = "box_tool"
            pose = Pose(Point(-0.34, -0.0075, -0.023), Quaternion(0.0, 0.0, 0.0, 1.0))
            size = Vector3(0.02, 0.02, 0.02)
            object_type = AttachObjectRequest.TYPE_BOX
            response = attach_box(name, pose, size, object_type).is_success
            if response:
                print("successfully attached!")
            else:
                print("did not attach successfully")
        except rospy.ServiceException as exc:
            print("Service did not process request: " + str(exc))
    
    def detach_box(self):
        rospy.wait_for_service("ur_control_wrapper/detach_object")
        detach_box = rospy.ServiceProxy("ur_control_wrapper/detach_object", DetachObject)
        try:
            name = "box_tool"
            response = detach_box(name, True).is_success
        except rospy.ServiceException as exc:
            print("Service did not process request: " + str(exc))
    
    def remove_box(self):
        rospy.wait_for_service("ur_control_wrapper/remove_object")
        remove_box = rospy.ServiceProxy("ur_control_wrapper/remove_object", RemoveObject)
        try:
            name = "box_object"
            response = remove_box(name).is_success
        except rospy.ServiceException as exc:
            print("Service did not process request: " + str(exc))

    def remove_parts(self, part_name):
        rospy.wait_for_service("ur_control_wrapper/remove_object")
        remove_box = rospy.ServiceProxy("ur_control_wrapper/remove_object", RemoveObject)
        try:
            name = part_name
            response = remove_box(name).is_success
        except rospy.ServiceException as exc:
            print("Service did not process request: " + str(exc))
        
    def run(self):
        while not rospy.is_shutdown():
            print("=====================================================")
            command_input = raw_input("Freedrive: fs(start), fe(end) \nGripper: grip \
            \nConnect: c(connect) \nGet End Effector Pose: ep \nGet joint angles: ja \
            \nGo to Default Position: d \nWaypoints following: wp \nPick and place test: pp \nSet joints: sj \
            \nTest fake parts pose estimation: tee;\nQ4 grasping: gq4; \nassembly tutorial: at; \n reset fixture robot: rf; \nGo to align Position: da \nGo to align pick Position: dp \n  \
            \nMove arm: x+/-#, y+/-#, z+/-#, r(roll)+/-#, p(pitch)+/-#, w(yaw)+/-# \n")
            if command_input == "fs":
                self.free_drive_pub.publish(True)
            elif command_input == "fe":
                self.free_drive_pub.publish(False)
            elif command_input == "grip":
                sub_cmd = raw_input("Adjust gripper position: gp+/-# \
                                    \nAdjust gripper force: gf+/- \
                                    \nAdjust gripper speed: gs+/- \
                                    \nOpen gripper: gop \nClose gripper: gcl \
                                    \nActivate gripper: gac \
                                    \nReset gripper: grs \
                                    \nNote: gp+ closes the gripper (max: 255), gp- opens the gripper (min: 0);\n")
                direction = sub_cmd
                self.control_gripper(direction)
            elif command_input == "c":
                self.connect_pub.publish(True)
            elif command_input == "ep":
                print(self.get_pose())
            elif command_input == "ja":
                print(self.get_angle())
            elif command_input == "d":
                self.set_default_angles()
                #self.set_default_angles(joints_angles = [0.12123775482177734, -2.2965408764281214, -1.5038652420043945, 2.22770540296521, -1.5678570906268519, 0.12182283401489258])
            elif command_input == "da":
                self.set_align_angles()
            elif command_input == "dp":
                self.set_align_pick_angles()
            elif command_input =="wp":
                self.waypoint_following()
            elif command_input == "pp":
                self.pick_and_place()
            elif command_input =="sj":
                sub_cmd = raw_input("Adjust joint by #degrees: 1+/-#; 2+/-#; 3+/-#; 4+/-#; 5+/-#; 6+/-# \n")
                direction = sub_cmd
                self.set_joints(direction)
            elif command_input == "tag":
                self.track_start = True
                # self.track_tag_3d()
            elif command_input == "ab":
                self.add_box()
            elif command_input == "atb":
                self.attach_box()
            elif command_input == "db":
                self.detach_box()
            elif command_input == "rb":
                self.remove_box()
            elif command_input == 'vd':
                self.execute_joints([0.27866458892822266, -2.2618566952147425, -2.0358381271362305, 5.868762957840719, -1.5448516050921839, -0.2911284605609339])
            elif command_input == 'tag_detect':
                print(self.detect_tag())
            else: # move arm
                direction = command_input
                self.move_arm(direction)

if __name__ == '__main__':
    try:

        rospy.init_node('visual_servo_demo_mode', anonymous=True)

        demo = VisualServoDemo()

        demo.run()
    except rospy.ROSInterruptException:
        pass
