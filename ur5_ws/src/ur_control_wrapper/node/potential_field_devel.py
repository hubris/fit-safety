#!/usr/bin/env python
from asyncio.windows_events import NULL
from pandas import array
import rospy
import numpy as np
from std_msgs.msg import String, Bool, Float64MultiArray
from std_srvs.srv import Trigger, SetBool
from sensor_msgs.msg import JointState, Image, CameraInfo
from geometry_msgs.msg import Pose, Point, Quaternion, Vector3, WrenchStamped
from controller_manager_msgs.srv import SwitchController, SwitchControllerRequest, ListControllers
from ur_control_wrapper.srv import SetPose
from ur_control_wrapper.srv import GetPose
from ur_control_wrapper.srv import GetJoints
from ur_control_wrapper.srv import SetJoints
from ur_control_wrapper.srv import SetTrajectory
from ur_control_wrapper.srv import GetCartesianPlan
from ur_control_wrapper.srv import ExecuteCartesianPlan
from ur_control_wrapper.srv import AddObject, AddObjectRequest
from ur_control_wrapper.srv import AttachObject, AttachObjectRequest
from ur_control_wrapper.srv import DetachObject, DetachObjectRequest
from ur_control_wrapper.srv import RemoveObject, RemoveObjectRequest
from ur_control_wrapper.srv import InverseKinematics, InverseKinematicsRequest
from gripper_controller_jenny import GripperHande as gripper
from status_listener import StatusListener
from cv_bridge import CvBridge, CvBridgeError
import cv2
import time

from tf import transformations as tfs


class Potential_Field:
    def __init__(self, args, tracking_topic_name, goal_topic_name, transformation_matrix):
        self.transformation_matrix = transformation_matrix
        self.args = args
        
        rospy.init_node('potential_field_dev', anonymous=True)

        #hand position
        rospy.Subscriber(tracking_topic_name, HumanSkeletonPosition, self.tracking_callback)
        #goal position
        #TODO
        # rospy.Subscriber(goal_topic_name, GoalPosition, self.goal_callback)

        #hyper parameters to be tuned 
        self.hand_vec_scale = 1.
        self.goal_vec_scale = 1.
        self.min_dist = 5.

    def tracking_callback(self, data):
        pos = np.array(data.data).reshape(data.n_humans, data.n_keypoints, data.n_dim) # (n_human, 25, 3)
        wrist_pos = pos[0,4] # (3,) right wrist
        self.hand_pos = np.dot(self.transformation_matrix, np.concatenate((wrist_pos,[1.]),axis=0))[:3] # (3,)

    def goal_callback(self,data):

        pass
        #TODO
        self.goal_pos = np.zeros(3)
    
    def potential_deviate(self, cur_command, attract = False):

        # #transform the current command to a directional vector if it is not one
        # cur_command = self.cmd2vec(cur_command)

        #calculate repulsive vector based on hand position and current direction
        hand_vec = self.hand_cal_vec(self.hand_pos, cur_command, attract)

        #calculate attractive vector based on goal position and current direction
        goal_vec = self.goal_cal_vec(self.goal_pos, cur_command)

        deviated_command = self.generate_deviated_command(cur_command, goal_vec, hand_vec)

        #TODO
        return deviated_command # or publish this 

    def hand_cal_vec(self, hand_pos, cur_command, attract):

        cur_position = self.get_cur_position()
        dispalcement = (hand_pos - cur_position) * (2*attract - 1)
        dist = np.linalg.norm(dispalcement)
        unit_hand_vec = (dispalcement * 1.0) / dist

        r = dist if dist > self.min_dist else self.min_dist
        hand_vec = r*unit_hand_vec

        # propotional to 1/r^2
        hand_vec = hand_vec * (1/(r**2)) * self.hand_vec_scale
        # # propotional to 1/r
        # hand_vec = hand_vec * (1/r) * self.hand_vec_scale
        # # constant
        # hand_vec = hand_vec * self.hand_vec_scale

        #algorithm TBD, can be non-linear
        return hand_vec

    def goal_cal_vec(self, goal_pos, cur_command):

        cur_position = self.get_cur_position()
        dispalcement = goal_pos - cur_position
        dist = np.linalg.norm(dispalcement)
        unit_goal_vec = (dispalcement * 1.0) / dist

        r = dist if dist > self.min_dist else self.min_dist
        goal_vec = r*unit_goal_vec

        # propotional to 1/r^2
        goal_vec = goal_vec * (1/(r**2)) * self.goal_vec_scale
        # # propotional to 1/r
        # goal_vec = goal_vec * (1/r) * self.goal_vec_scale
        # # constant
        # goal_vec = goal_vec * self.goal_vec_scale

        #algorithm TBD, can be non-linear
        return goal_vec

    def generate_deviated_command(self, cur_command, goal_vec, hand_vec):
        #algorithm TBD, can be non-linear
        devitaed_cmd = cur_command + goal_vec + hand_vec
        return devitaed_cmd #or a joint space command

    #return currenct position of the end effector
    def get_cur_position(self):
        #TODO
        pass
    
    #transform the current command to a directional vector if it is not one
    def cmd2vec(self, cur_cmd):
        #TODO

        cur_vec = None 
        return cur_vec