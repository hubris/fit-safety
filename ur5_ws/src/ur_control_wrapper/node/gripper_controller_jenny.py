#!/usr/bin/env python

import rospy
import rospkg

from std_msgs.msg import String, Bool

import roslib; roslib.load_manifest('robotiq_2f_gripper_control')
import rospy
from robotiq_2f_gripper_control.msg import _Robotiq2FGripper_robot_output  as outputMsg
from time import sleep

class GripperHande:
    def __init__(self):
        self.is_simulator = rospy.get_param("sim")

        self.gripper_pub = rospy.Publisher('Robotiq2FGripperRobotOutput', outputMsg.Robotiq2FGripper_robot_output)
        
        self.command = outputMsg.Robotiq2FGripper_robot_output();
        
        rospy.Subscriber("ur_control_wrapper/gripper", String, self.control)

    def control(self, data):
        if not self.is_simulator:
            if data.data == "go":
                self.open_gripper()
            elif data.data == "gc":
                self.close_gripper()
            elif data.data == "ga":
                self.activate_gripper()
            elif data.data == "gr":
                self.reset_gripper()
            elif data.data == "gf":
                self.faster_gripper()
            elif data.data == "gl":
                self.slower_gripper()
            elif data.data == "gi":
                self.increase_force()
            elif data.data == "gd":
                self.decrease_force()
            else:
                cmd = int(float(data.data))
                self.move_gripper(cmd)

            self.gripper_pub.publish(self.command)
            rospy.sleep(0.2)

    def activate_gripper(self):
        if not self.is_simulator:
            self.command = outputMsg.Robotiq2FGripper_robot_output();
            self.command.rACT = 1
            self.command.rGTO = 1
            self.command.rSP  = 255
            self.command.rFR  = 150
    
    def reset_gripper(self):
        if not self.is_simulator:
            self.command = outputMsg.Robotiq2FGripper_robot_output();
            self.command.rACT = 0

    def open_gripper(self):
        if not self.is_simulator:
            self.command.rPR = 0 

    def close_gripper(self):
        if not self.is_simulator:
            self.command.rPR = 255
    
    def faster_gripper(self):
        if not self.is_simulator:
            self.command.rSP += 25
            if self.command.rSP > 255:
                self.command.rSP = 255

    def slower_gripper(self):
        if not self.is_simulator:
            self.command.rSP -= 25
            if self.command.rSP < 0:
                self.command.rSP = 0

    def increase_force(self):
        if not self.is_simulator:
            self.command.rFR += 25
            if self.command.rFR > 255:
                self.command.rFR = 255

    def decrease_force(self):
        if not self.is_simulator:
            self.command.rFR -= 25
            if self.command.rFR < 0:
                self.command.rFR = 0

    def move_gripper(self, cmd):
        if not self.is_simulator:
            try: 
                self.command.rPR = cmd
                if self.command.rPR > 255:
                    self.command.rPR = 255
                if self.command.rPR < 0:
                    self.command.rPR = 0
            except ValueError:
                pass       

if __name__ == '__main__':
    try:
        rospy.init_node('ur_control_wrapper_gripper_control_jenny', anonymous=True)

        gripper_control = GripperHande()

        rospy.spin()

    except rospy.ROSInterruptException:
        pass	





