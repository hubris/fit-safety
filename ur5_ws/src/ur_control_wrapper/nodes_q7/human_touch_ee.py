#!/usr/bin/env python3
import numpy as np
import rospy
from std_msgs.msg import Bool, Float64
import matplotlib.pyplot as plt
from ur_control_wrapper.msg import StringStamped, HumanIntervention
from geometry_msgs.msg import WrenchStamped
import pdb 
class HumanTouchEE:
    def __init__(
        self,
        smoothing_window_length=20,
        plot_window_length=200,
        f_thresholds=[3., 5.],
        f_limits = [0., 40.],
        suspension_time=3.,
    ):
        rospy.init_node('human_touch_ee')
        self.human_intervention = False
        self.human_intervention_time = -999.
        self.bool_pub = rospy.Publisher('hri/human_intervention', HumanIntervention, queue_size=10)
        self.log_pub = rospy.Publisher('hri/log', StringStamped, queue_size=10)
        self.force_pub = rospy.Publisher('hri/ee_force', Float64, queue_size=10)
        self.smoothing_window_length = smoothing_window_length
        self.plot_window_length = plot_window_length
        self.f_current_window = [] # [[fx_t, fy_t, fz_t], [fx_{t+1}, fy_{t+1}, fz_{t+1}], ...]
        self.smoothed_force = [] # [ft, f_{t+1}, ...]
        self.f_thresholds = f_thresholds # unit: Newton
        self.suspension_time = suspension_time # sec
        
        _, self.ax = plt.subplots() # for now we only plot and use joint 0 and joint 1 for human intervention.
        self.ax.set_xlim([0,2])
        self.ax.set_ylim(f_limits)
        self.ax.set_xlabel('time (sec)')
        self.ax.set_ylabel('force (N)')
        self.ax.plot(np.arange(self.plot_window_length)/100., np.ones(self.plot_window_length)*self.f_thresholds[0], 'r--')
        self.ax.plot(np.arange(self.plot_window_length)/100., np.ones(self.plot_window_length)*self.f_thresholds[1], 'r--')
        self.force_plot = self.ax.plot([], [], 'b-')[0]
        # print(type(self.force_plot))
        rospy.Subscriber("/wrench", WrenchStamped, self.callback)


    def callback(self, data):
        self.f_current_window.append([data.wrench.force.x, data.wrench.force.y, data.wrench.force.z])
        if len(self.f_current_window) > self.smoothing_window_length:
            self.f_current_window = self.f_current_window[-self.smoothing_window_length:]
        if len(self.f_current_window) == self.smoothing_window_length:
            force_tt = np.linalg.norm(np.array(self.f_current_window),axis=1).mean()
            self.force_pub.publish(force_tt)
            self.smoothed_force.append(force_tt)
            if len(self.smoothed_force) > self.plot_window_length:
                self.smoothed_force = self.smoothed_force[-self.plot_window_length:]
            self.check_human_intervention()
            # if len(self.smoothed_force) == self.plot_window_length:
            self.force_plot.set_data(np.arange(len(self.smoothed_force))/100., np.array(self.smoothed_force))

            # for joint_plot_idx, std_plot in enumerate(self.std_plots):
            #     std_plot.set_data(np.arange(len(self.std))/100., self.std[:,joint_plot_idx])
            plt.draw()

    def check_human_intervention(self):
        # two thresholds for human touch detection
        if not self.human_intervention and self.smoothed_force[-1] > self.f_thresholds[1]:
            # not true in the state and found intervention
            self.human_intervention = True
            human_intervention_msg = HumanIntervention()
            human_intervention_msg.header.stamp = rospy.Time.now()
            human_intervention_msg.source = "ee_contact"
            human_intervention_msg.data = self.human_intervention
            self.bool_pub.publish(human_intervention_msg)
            self.human_intervention_time = rospy.get_time()
            log_msg = StringStamped()
            log_msg.header.stamp = rospy.Time.now()
            log_msg.data = "EE touch intervention is detected"
            self.log_pub.publish(log_msg)            
            return
        elif self.human_intervention and self.smoothed_force[-1] < self.f_thresholds[0]:
            # true in the state and found no intervention
            if rospy.get_time() - self.human_intervention_time > self.suspension_time:
                self.human_intervention = False
                human_intervention_msg = HumanIntervention()
                human_intervention_msg.header.stamp = rospy.Time.now()
                human_intervention_msg.source = "ee_contact"
                human_intervention_msg.data = self.human_intervention
                self.bool_pub.publish(human_intervention_msg)
                log_msg = StringStamped()
                log_msg.header.stamp = rospy.Time.now()
                log_msg.data = "EE touch intervention is over"
                self.log_pub.publish(log_msg)
            return
        elif self.human_intervention and self.smoothed_force[-1] > self.f_thresholds[1]:
            # true in the state, and when intervention goes on, update the intervention time
            self.human_intervention_time = rospy.get_time()
            return
        else:
            return


def main():
    hte = HumanTouchEE()
    plt.show()
    rospy.spin()


if __name__ == '__main__':
    try:
        main()
    except rospy.ROSInterruptException:
        pass