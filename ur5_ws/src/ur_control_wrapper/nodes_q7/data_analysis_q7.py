import pathhack
import pickle
import numpy as np
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
from os.path import join
import rosbag

"""
header: 
  seq: 15
  stamp: 
    secs: 1644444300
    nsecs: 356555223
  frame_id: ''
data: "EE touch intervention is detected"
---
header: 
  seq: 15
  stamp: 
    secs: 1644444300
    nsecs: 356673002
  frame_id: ''
data: "Switched to compliant mode."
---
header: 
  seq: 16
  stamp: 
    secs: 1644444303
    nsecs: 849892139
  frame_id: ''
data: "EE touch intervention is over"
---
header: 
  seq: 16
  stamp: 
    secs: 1644444303
    nsecs: 850049972
  frame_id: ''
data: "Switched to non-compliant mode."
"""

def process_detect_to_switch_time():
    bag_filenames = {}
    bag_filenames['ninghan'] = ['ninghan_1.bag', 'ninghan_2.bag']
    bag_filenames['xiang'] = ['xiang_1.bag', 'xiang_2.bag']
    bag_filenames['yeji'] = ['yeji_1.bag']
    bag_filenames['yiqing'] = ['yiqing_1.bag', 'yiqing_2.bag']
    bag_filenames['zhe'] = ['zhe_1.bag']

    subject_names = ['ninghan', 'xiang', 'yeji', 'yiqing', 'zhe']

    process_time_results = {}

    for subject_name in subject_names:
        process_time_results[subject_name] = []
        for bag_filename in bag_filenames[subject_name]:
            # load bag
            bag = rosbag.Bag(join(pathhack.pkg_path, 'raw_datasets', bag_filename))
            forces = []
            logs = []
            tt_init = None
            times = []
            for topic, msg, t in bag.read_messages(topics=['/hri/ee_force']):
                if not tt_init:
                    tt_init = t.to_sec()
                force_tt = msg.data
                forces.append(force_tt)
                times.append(t.to_sec()-tt_init)
            for topic, msg, t in bag.read_messages(topics=['/hri/log']):
                log_tt = msg.data
                logs.append([log_tt, t.to_sec()-tt_init])
            bag.close()
            # process reaction time
            times = np.array(times)
            forces = np.array(forces)
            detect_time = []
            switch_time = []
            for log_msg, tt in logs:
                if log_msg == "EE touch intervention is detected":
                  detect_time.append(tt)
                if log_msg == "Switched to compliant mode.":
                  switch_time.append(tt)
            process_time = np.array(switch_time)-np.array(detect_time)
            process_time_results[subject_name] += list(process_time)
        print(subject_name + " is processed.")
    with open(join(pathhack.pkg_path, 'processed_datasets', 'process_time_results.p'), 'wb') as f:
        pickle.dump(process_time_results, f)
        print('process_time_results.p is created.')

def process_reaction_time():
    bag_filenames = {}
    bag_filenames['ninghan'] = ['ninghan_1.bag', 'ninghan_2.bag']
    bag_filenames['xiang'] = ['xiang_1.bag', 'xiang_2.bag']
    bag_filenames['yeji'] = ['yeji_1.bag']
    bag_filenames['yiqing'] = ['yiqing_1.bag', 'yiqing_2.bag']
    bag_filenames['zhe'] = ['zhe_1.bag']

    subject_names = ['ninghan', 'xiang', 'yeji', 'yiqing', 'zhe']

    reaction_time_results = {}

    for subject_name in subject_names:
        reaction_time_results[subject_name] = []
        for bag_filename in bag_filenames[subject_name]:
            # load bag
            bag = rosbag.Bag(join(pathhack.pkg_path, 'raw_datasets', bag_filename))
            forces = []
            logs = []
            tt_init = None
            times = []
            for topic, msg, t in bag.read_messages(topics=['/hri/ee_force']):
                if not tt_init:
                    tt_init = t.to_sec()
                force_tt = msg.data
                forces.append(force_tt)
                times.append(t.to_sec()-tt_init)
            for topic, msg, t in bag.read_messages(topics=['/hri/log']):
                log_tt = msg.data
                logs.append([log_tt, t.to_sec()-tt_init])
            bag.close()
            # process reaction time
            times = np.array(times)
            forces = np.array(forces)
            count = 0
            for log_msg, detect_t in logs:
                if log_msg == "Switched to compliant mode.":
                    times_range = np.where((detect_t-2<times)*(times<detect_t))[0]
                    start_time = times[times_range[np.where(forces[times_range] < 2.5)[0][-1]]] # ! 2.5 Newton
                    reaction_time = (detect_t - start_time)*1000
                    # print("Reaction time: {:.1f} milliseconds".format(reaction_time))
                    count += 1
                    reaction_time_results[subject_name].append(reaction_time)
        print(subject_name + " is processed.")
    
    with open(join(pathhack.pkg_path, 'processed_datasets', 'reaction_time_results.p'), 'wb') as f:
        pickle.dump(reaction_time_results, f)
        print('reaction_time_results.p is created.')





def analyze_reaction_time():
    with open(join(pathhack.pkg_path, 'processed_datasets', 'reaction_time_results.p'), 'rb') as f:
        reaction_time_results = pickle.load(f)
        print('reaction_time_results.p is loaded.')
    subject_names = ['ninghan', 'xiang', 'yeji', 'yiqing', 'zhe']
    reaction_time_results_total = []
    for subject_name in subject_names:
      data_len = len(reaction_time_results[subject_name])
      print("data len for subject "+subject_name+": ", str(data_len))
      print("reaction time < 50ms: ",  (np.array(reaction_time_results[subject_name])<50).sum())
      print("50ms < reaction time < 75 ms: ", ((50<np.array(reaction_time_results[subject_name]))*(np.array(reaction_time_results[subject_name])<75)).sum())
      print("75ms < reaction time < 100 ms: ", ((75<np.array(reaction_time_results[subject_name]))*(np.array(reaction_time_results[subject_name])<100)).sum())
      print("100ms < reaction time: ", (100<np.array(reaction_time_results[subject_name])).sum())
      print()
      reaction_time_results_total += reaction_time_results[subject_name]
    
    print("overall data len: ", len(reaction_time_results_total))
    # print("reaction time < 30ms: ", )
    # print("30ms < reaction time < 50 ms: ", )
    # print("50ms < reaction time < 100 ms: ", )
    # print("100ms < reaction time: ", )
    print("reaction time < 50ms: ",  (np.array(reaction_time_results_total)<50).sum())
    print("50 ms < reaction time < 75 ms: ", ((50<np.array(reaction_time_results_total))*(np.array(reaction_time_results_total)<75)).sum())
    print("75 ms < reaction time < 100 ms: ", ((75<np.array(reaction_time_results_total))*(np.array(reaction_time_results_total)<100)).sum())
    print("100 ms < reaction time: ", (100<np.array(reaction_time_results_total)).sum())


def analyze_detect_to_switch_time():
    with open(join(pathhack.pkg_path, 'processed_datasets', 'process_time_results.p'), 'rb') as f:
        process_time_results = pickle.load(f)
        print('process_time_results.p is loaded.')
    subject_names = ['ninghan', 'xiang', 'yeji', 'yiqing', 'zhe']
    process_time_results_total = []
    for subject_name in subject_names:
      process_time_results_total += process_time_results[subject_name]
    print("process_time_results_total mean: ", np.mean(process_time_results_total))
    print("process_time_results_total std: ", np.std(process_time_results_total))

# process_reaction_time()
# analyze_reaction_time()
# process_detect_to_switch_time()
analyze_detect_to_switch_time()