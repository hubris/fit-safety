#!/usr/bin/env python
import rospy
import numpy as np
from std_msgs.msg import Bool
import termios, sys
from ur_control_wrapper.msg import StringStamped

def talker():
    fd = sys.stdin.fileno()
    newattr = termios.tcgetattr(fd)
    newattr[3] = newattr[3] & ~termios.ICANON & ~termios.ECHO
    termios.tcsetattr(fd, termios.TCSANOW, newattr)
    pub = rospy.Publisher('hri/log', StringStamped, queue_size=10)
    rospy.init_node('talker_keyboard', anonymous=True)
    intervention_state = False
    while not rospy.is_shutdown():
        c = sys.stdin.read(1)
        if c != "\n": # not Enter key
            str_msg = c
            rospy.loginfo(str_msg)
            if c == 'd': # detected
                log_msg = StringStamped()
                log_msg.header.stamp = rospy.Time.now()
                log_msg.data = "Human detected touch."
                pub.publish(log_msg)

if __name__ == '__main__':
    try:
        talker()
    except rospy.ROSInterruptException:
        pass