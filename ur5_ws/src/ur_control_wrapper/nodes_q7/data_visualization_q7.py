import pathhack
import pickle
import numpy as np
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
from os.path import join
import rosbag



# bag = rosbag.Bag(join(pathhack.pkg_path, 'raw_datasets', '1.bag'))
# forces = []
# logs = []
# tt_init = None
# times = []
# for topic, msg, t in bag.read_messages(topics=['/hri/ee_force']):
#     if not tt_init:
#         tt_init = t.to_sec()
#     force_tt = msg.data
#     forces.append(force_tt)
#     times.append(t.to_sec()-tt_init)
# for topic, msg, t in bag.read_messages(topics=['/hri/log']):
#     log_tt = msg.data
#     logs.append([log_tt, t.to_sec()-tt_init])
# bag.close()


# # print(logs)
# print(times[0])
# print(type(times[0]))
# fig, ax = plt.subplots()
# ax.plot(times, forces)

# times = np.array(times)
# forces = np.array(forces)

# for log_msg, detect_t in logs:
#     if log_msg == "EE touch intervention is detected":
#         times_range = np.where((detect_t-0.1<times)*(times<detect_t))[0]
#         start_time = times[times_range[np.where(forces[times_range] < 2.5)[0][-1]]]
#         print("Reaction time: {:.1f} milliseconds".format((detect_t - start_time)*1000))
#         plt.axvline(x=start_time, color='g', linestyle='--')
#         plt.axvline(x=detect_t, color='r', linestyle='--')
#         # break


# # times_range = np.flip(times_range)

# # print(forces[times_range])
# # print(times_range)
# # print(detect_t)
# # print(np.where(forces[times_range] < 2.5))

# # plt.axvline(x=start_time, color='g', linestyle='--')

# # print("Reaction time: {:.1f} milliseconds".format((detect_t - start_time)*1000))

# fig.savefig("visualization_force_test.png")






bag = rosbag.Bag(join(pathhack.pkg_path, 'raw_datasets', 'yeji_test.bag'))
# bag = rosbag.Bag(join(pathhack.pkg_path, 'raw_datasets', '0.bag'))
forces = []
logs = []
tt_init = None
times = []
for topic, msg, t in bag.read_messages(topics=['/hri/ee_force']):
    if not tt_init:
        tt_init = t.to_sec()
    force_tt = msg.data
    forces.append(force_tt)
    times.append(t.to_sec()-tt_init)
for topic, msg, t in bag.read_messages(topics=['/hri/log']):
    log_tt = msg.data
    logs.append([log_tt, t.to_sec()-tt_init])
bag.close()


# print(logs)
print(times[0])
print(type(times[0]))


times = np.array(times)
forces = np.array(forces)
count = 0
for log_msg, detect_t in logs:
    if log_msg == "Switched to compliant mode.":
        times_range = np.where((detect_t-2<times)*(times<detect_t))[0]
        start_time = times[times_range[np.where(forces[times_range] < 2.5)[0][-1]]]
        reaction_time = (detect_t - start_time)*1000
        print("Reaction time: {:.1f} milliseconds".format(reaction_time))
        if reaction_time <50:
            print("Reaction time: {:.1f} milliseconds".format(reaction_time))
            fig, ax = plt.subplots()
            times_range_plot = np.where((detect_t-1.5<times)*(times<detect_t+0.1))[0]
            ax.plot(times[times_range_plot], forces[times_range_plot])
            plt.axvline(x=start_time, color='g', linestyle='--')
            plt.axvline(x=detect_t, color='r', linestyle='--')
            fig.savefig("visualization_force_test_split_"+str(count)+".png")
            break
        count += 1
        # break

fig, ax = plt.subplots()
ax.plot(times, forces)
for log_msg, detect_t in logs:
    if log_msg == "Switched to compliant mode.":
        times_range = np.where((detect_t-0.1<times)*(times<detect_t))[0]
        times_range_plot = np.where((detect_t-0.1<times)*(times<detect_t+0.1))[0]
        start_time = times[times_range[np.where(forces[times_range] < 2.5)[0][-1]]]
        print("Reaction time: {:.1f} milliseconds".format((detect_t - start_time)*1000))
        plt.axvline(x=start_time, color='g', linestyle='--')
        plt.axvline(x=detect_t, color='r', linestyle='--')

fig.savefig("visualization_force_test.png")
        # break

print("count of contact:", count)
# times_range = np.flip(times_range)

# print(forces[times_range])
# print(times_range)
# print(detect_t)
# print(np.where(forces[times_range] < 2.5))

# plt.axvline(x=start_time, color='g', linestyle='--')

# print("Reaction time: {:.1f} milliseconds".format((detect_t - start_time)*1000))






# bag = rosbag.Bag(join(pathhack.pkg_path, 'raw_datasets', '1.bag'))

# forces = []

# logs = []
# tt_init = None

# times = []
# for topic, msg, t in bag.read_messages(topics=['/hri/ee_force']):
#     if not tt_init:
#         tt_init = t.to_sec()
#     force_tt = msg.data
#     forces.append(force_tt)
#     times.append(t.to_sec()-tt_init)

# for topic, msg, t in bag.read_messages(topics=['/hri/log']):
#     log_tt = msg.data
#     logs.append([log_tt, t.to_sec()-tt_init])


# bag.close()

# # print(logs)
# print(times[0])
# print(type(times[0]))
# fig, ax = plt.subplots()
# ax.plot(times[1000:5000], forces[1000:5000])

# for log_msg, detect_t in logs:
#     if log_msg == "EE touch intervention is detected" and detect_t < 7:
#         plt.axvline(x=detect_t, color='r', linestyle='--')
#         break

# times = np.array(times)
# times_range = np.where((detect_t-0.1<times)*(times<detect_t))[0]
# # times_range = np.flip(times_range)
# forces = np.array(forces)
# # print(forces[times_range])
# # print(times_range)
# # print(detect_t)
# # print(np.where(forces[times_range] < 2.5))
# start_time = times[times_range[np.where(forces[times_range] < 2.5)[0][-1]]]
# plt.axvline(x=start_time, color='g', linestyle='--')

# print("Reaction time: {:.1f} milliseconds".format((detect_t - start_time)*1000))

# fig.savefig("visualization_force_1.png")
