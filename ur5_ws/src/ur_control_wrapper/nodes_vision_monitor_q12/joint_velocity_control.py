#!/usr/bin/env python
import rospy
import numpy as np

import tf
import message_filters

from std_srvs.srv import Trigger
from sensor_msgs.msg import JointState
from geometry_msgs.msg import WrenchStamped
from std_msgs.msg import Bool, Float64MultiArray, Float64

from ur_control_wrapper.msg import HumanIntervention, StringStamped
from controller_manager_msgs.srv import SwitchController, SwitchControllerRequest, ListControllers
from ur_control_wrapper.srv import SetPose, GetPose, GetJoints, SetJoints, InverseKinematics, InverseKinematicsRequest


class JointVelocityControl:
    def __init__(self):
        self.joint_velocity_pub = rospy.Publisher('/joint_group_vel_controller/command', Float64MultiArray, queue_size=10)
        self.default_joints_R = [0.0, -5.0 * np.pi / 12, 2.0 * np.pi / 3.0, -9.0 * np.pi / 12.0, -np.pi / 2.0, 0.0]
        self.default_joints_L = [0.0, -2.0 * np.pi / 3, -np.pi / 3.0, -np.pi / 2.0, np.pi / 2.0, 0.0]
        self.robot_idx = 1 # 0 represent robot on the right. 1 represent robot on the left (katie)
        if self.robot_idx == 1:
            self.jd_goal_position = np.array([-0.505052806614, 0.134387694165, 0.232087357934])
        else:
            self.jd_goal_position = np.array([0.586583595435, 0.133782635682, 0.295423997724])
        self.task_goal_sub = message_filters.Subscriber('task_goal', Float64MultiArray)
        self.task_vel_sub = message_filters.Subscriber('task_vel', Float64)
        self.wrench_sub = message_filters.Subscriber('/wrench', WrenchStamped)
        self.task_execution_flag = False

        rospy.Subscriber('hri/human_intervention', HumanIntervention, self.intervention_callback)
        rospy.Subscriber('/q3/human_intervention', Bool, self.vision_intervention_callback)
        ts = message_filters.ApproximateTimeSynchronizer([self.task_goal_sub, self.task_vel_sub, self.wrench_sub], queue_size=1, slop=0.1, allow_headerless=True)
        ts.registerCallback(self.task_callback)

        self.log_pub = rospy.Publisher('hri/log', StringStamped, queue_size=10)

        self.pose_vel_scale = 0.8
        self.vel_dec_scale = 1./15
        self.force_vel_scale = 1./10    # The amplifier of velocity
        self.force_acc_scale = 1./10    # The amplifier of acceleration
        self.force_threshold = 1.
        self.zero_force_torque_sensor()
        # self.tf = TransformListener()
        self.last_vel = np.zeros(3)

        self.human_touch_joint_flag = False
        self.human_touch_ee_flag = False
        self.human_in_shared_space = False
        self.human_intervention = False


    def set_default_angles(self):
        controller_state = self.check_controller_state(silent=True)
        if controller_state["scaled_pos_joint_traj_controller"] != "running":
            print("joint_group_vel_controller is not started.")
            self.switch_controller("scaled_pos")
        rospy.wait_for_service("ur_control_wrapper/set_joints")
        set_joints = rospy.ServiceProxy("ur_control_wrapper/set_joints", SetJoints)
        joints = JointState()
        joints.name = ["shoulder_pan_joint", "shoulder_lift_joint", "elbow_joint", "wrist_1_joint", "wrist_2_joint", "wrist_3_joint"]
        if self.robot_idx == 0:
            joints.position = self.default_joints_R
            print("set the right robot default")
        else:
            joints.position = self.default_joints_L
            print("set the left robot default")
        try:
            response = set_joints(joints)
        except rospy.ServiceException as exc:
            print("Service did not process request: " + str(exc))
        self.task_execution_flag = True


    def check_controller_state(self, silent=False):
        rospy.wait_for_service("/controller_manager/list_controllers")
        list_controllers = rospy.ServiceProxy("/controller_manager/list_controllers", ListControllers)
        response = list_controllers()
        controller_state = {}
        for single_controller in response.controller:
            if single_controller.name == "joint_group_vel_controller" \
                or single_controller.name == "scaled_pos_joint_traj_controller":
                controller_state[single_controller.name] = single_controller.state
        if not silent:
            if controller_state["scaled_pos_joint_traj_controller"] == "running":
                print("Current controller is scaled_pos_joint_traj_controller.")
            elif controller_state["joint_group_vel_controller"] == "running":
                print("Current controller is joint_group_vel_controller.")
            else:
                print("Neither scaled_pos_joint_traj_controller nor joint_group_vel_controller is running.")
        return controller_state


    def switch_controller(self, desired_controller):
        # desired_controller = 'joint_group' or 'scaled_pos'
        controller_switched = False
        if desired_controller != 'joint_group' and desired_controller != 'scaled_pos':
            print("Wrong controller input.")
            return controller_switched
        rospy.wait_for_service("controller_manager/switch_controller")
        switch_controller = rospy.ServiceProxy("controller_manager/switch_controller", SwitchController)
        try:
            req = SwitchControllerRequest()
            if desired_controller == 'joint_group':
                req.start_controllers = ['joint_group_vel_controller']
                req.stop_controllers = ['scaled_pos_joint_traj_controller']
            else:
                req.start_controllers = ['scaled_pos_joint_traj_controller']
                req.stop_controllers = ['joint_group_vel_controller']
            req.strictness = 2
            req.start_asap = False
            req.timeout = 0.0
            response = switch_controller(req)
            if response.ok:
                controller_switched = True
                if desired_controller == 'joint_group':
                    print("Controller successfully switched to joint_group_vel_controller.")
                else:
                    print("Controller successfully switched to scaled_pos_joint_traj_controller.")
            else:
                print("Controller switch failed.")
        except rospy.ServiceException as exc:
            print "Service did not process request: " + str(exc)
        return controller_switched


    def task_callback(self, task_goal, task_vel, force):
        if not self.task_execution_flag:
            return
        controller_state = self.check_controller_state(silent=True)
        if controller_state["joint_group_vel_controller"] != "running":
            self.switch_controller("joint_group")
        
        if self.human_intervention:
            self.set_joint_velocity(np.zeros(6))
        else:
            try:
                # goal directed motion
                current_pose = self.get_pose()
                current_pos = np.array([current_pose.position.x, current_pose.position.y, current_pose.position.z])
                goal = np.array(task_goal.data).astype(np.float)
                # print(goal)
                pose_diff = goal - current_pos
                # print(pose_diff)
                pose_diff = pose_diff/np.linalg.norm(pose_diff)
                desired_vel = pose_diff * np.float(task_vel.data)
                # print(desired_vel)
                self.vel_arrange(desired_vel)
            except (tf.LookupException, tf.ConnectivityException, tf.ExtrapolationException):
                print("Error on tf.")


    def vel_arrange(self, new_vel):
        self.joint_move(new_vel)
        

    def joint_move(self, next_vel):
        controller_state = self.check_controller_state(silent=True)
        if controller_state["joint_group_vel_controller"] != "running":
            print("joint_group_vel_controller is not started.")
            self.switch_controller("joint_group")
        current_pose = self.get_pose()
        current_pos = np.array([current_pose.position.x, current_pose.position.y, current_pose.position.z])
        pose_diff = next_vel
        magnitude = np.linalg.norm(pose_diff)
        # print(distance)
        pose_offset_vel = 0.08*pose_diff/magnitude
        # print(pose_offset_vel)
        desired_next_joint_state = self.compute_inverse_kinematics(pose_offset_vel)
        if len(desired_next_joint_state.position) == 0:
            self.set_joint_velocity(np.zeros(6))
            print("The desired pose cannot be reached.")
            # self.switch_controller("scaled_pos")
            return
        current_joint_state = self.get_angle()
        # print(velocity) # roughly 0-12
        jv_input = np.array(desired_next_joint_state.position)[:6] - np.array(current_joint_state.position)
        jv_input = jv_input * magnitude
        self.set_joint_velocity(jv_input)

    def get_pose(self):
        rospy.wait_for_service("ur_control_wrapper/get_pose")
        get_current_pose = rospy.ServiceProxy("ur_control_wrapper/get_pose", GetPose)
        current_pose = None
        try:
            current_pose = get_current_pose().pose
        except rospy.ServiceException as exc:
            print "Service did not process request: " + str(exc)
        return current_pose

    def compute_inverse_kinematics(self, desired_pose_offset, silent=True):
        # desired_pose_offset = [offset_x, offset_y, offset_z]
        joint_state = None
        current_pose = self.get_pose()
        desired_pose = current_pose
        desired_pose.position.x += desired_pose_offset[0]
        desired_pose.position.y += desired_pose_offset[1]
        desired_pose.position.z += desired_pose_offset[2]
        rospy.wait_for_service("ur_control_wrapper/inverse_kinematics")
        compute_ik = rospy.ServiceProxy("ur_control_wrapper/inverse_kinematics", InverseKinematics)
        try:
            req = InverseKinematicsRequest()
            req.pose = desired_pose
            response = compute_ik(req)
            # response = compute_ik(desired_pose)
            solution_found, joint_state = response.solution_found, response.joint_state
            if solution_found:
                if not silent:
                    print("joint state: ")
                    print(joint_state)
            else:
                print("Solution is not found.")
        except rospy.ServiceException as exc:
            print "Service did not process request: " + str(exc)
        return joint_state

    def set_joint_velocity(self, jv_input):
        controller_state = self.check_controller_state(silent=True)
        if controller_state["joint_group_vel_controller"] != "running":
            print("joint_group_vel_controller is not started. Failed to set joint velocity.")
            return
        # pass
        if isinstance(jv_input, str):
            jv_float = [float(entry) for entry in jv_input.split(" ")] # [0., 0., 0., 0., 0., 0.] # 6 entries
        else:
            jv_float = jv_input
        data = Float64MultiArray()
        data.data = jv_float
        self.joint_velocity_pub.publish(data)
        return


    def vision_intervention_callback(self, data):
        if not self.human_in_shared_space and data.data:
            # intervention starts
            log_msg = StringStamped()
            log_msg.header.stamp = rospy.Time.now()
            log_msg.data = "Pause (Human in shared space)"
            print("Pause (Human in shared space)")
            self.log_pub.publish(log_msg) 
            self.human_in_shared_space = True
        elif self.human_in_shared_space and not data.data:
            # intervention ends
            log_msg = StringStamped()
            log_msg.header.stamp = rospy.Time.now()
            log_msg.data = "Resume (Human not in shared space)"
            print("Resume (Human not in shared space)")
            self.log_pub.publish(log_msg) 
            self.human_in_shared_space = False
            
        if not self.human_intervention and (self.human_in_shared_space or self.human_touch_joint_flag or self.human_touch_ee_flag):
            self.human_intervention = True
        elif self.human_intervention and (not self.human_in_shared_space and not self.human_touch_joint_flag and not self.human_touch_ee_flag):
            self.human_intervention = False

    def intervention_callback(self, data):
        if data.source == 'ee_contact':
            if not data.data and self.human_touch_ee_flag:
                log_msg = StringStamped()
                log_msg.header.stamp = rospy.Time.now()
                log_msg.data = "Resume (Human not touch end-effector)"
                print("Resume (Human not touch end-effector)")
                self.log_pub.publish(log_msg)
            if data.data and not self.human_touch_ee_flag:
                log_msg = StringStamped()
                log_msg.header.stamp = rospy.Time.now()
                log_msg.data = "Pause (Human touch end-effector)"
                print("Pause (Human touch end-effector)")
                self.log_pub.publish(log_msg)
            self.human_touch_ee_flag = data.data
        if data.source == 'contact':
            if not data.data and self.human_touch_joint_flag:
                log_msg = StringStamped()
                log_msg.header.stamp = rospy.Time.now()
                log_msg.data = "Resume (Human not touch joint)"
                print("Resume (Human not touch joint)")
                self.log_pub.publish(log_msg)
            if data.data and not self.human_touch_joint_flag:
                log_msg = StringStamped()
                log_msg.header.stamp = rospy.Time.now()
                log_msg.data = "Pause (Human touch joint)"
                print("Pause (Human touch joint)")
                self.log_pub.publish(log_msg) 
            self.human_touch_joint_flag = data.data

        if not self.human_intervention and (self.human_in_shared_space or self.human_touch_joint_flag or self.human_touch_ee_flag):
            self.human_intervention = True
        elif self.human_intervention and (not self.human_in_shared_space and not self.human_touch_joint_flag and not self.human_touch_ee_flag):
            self.human_intervention = False

    def get_angle(self):
        rospy.wait_for_service("ur_control_wrapper/get_joints")
        get_current_joints = rospy.ServiceProxy("ur_control_wrapper/get_joints", GetJoints)
        current_joints = None
        try:
            current_joints = get_current_joints().joints
        except rospy.ServiceException as exc:
            print "Service did not process request: " + str(exc) 
        return current_joints

    def zero_force_torque_sensor(self):
        rospy.wait_for_service("ur_hardware_interface/zero_ftsensor")
        zero_ft_sensor = rospy.ServiceProxy("ur_hardware_interface/zero_ftsensor", Trigger)
        response = zero_ft_sensor()
        if response.success:
            print("Force torque sensor is zeroed.")
        else:
            print("Failed to zero force torque sensor.")
        return response.success

if __name__ == '__main__':
    try:
        rospy.init_node('ur_joint_velocity_control_node', anonymous=True)
        joint_velocity_control = JointVelocityControl()
        joint_velocity_control.set_default_angles()
        rospy.spin()
    except rospy.ROSInterruptException:
        pass