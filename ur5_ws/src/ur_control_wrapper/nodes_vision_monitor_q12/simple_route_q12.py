#!/usr/bin/env python
import rospy
import numpy as np

from ur_control_wrapper.srv import GetPose
from std_msgs.msg import Float64, Float64MultiArray


class route_planner:
    def __init__(self):
        self.task_goal_pub = rospy.Publisher('task_goal', Float64MultiArray, queue_size=1)
        self.task_vel_pub = rospy.Publisher('task_vel', Float64, queue_size=1)
        # self.right_goals = [[-0.505052806614, 0.034387694165, 0.232087357934], [-0.705052806614, 0.034387694165, 0.232087357934]]
        # self.left_goals = [[-0.505052806614, 0.034387694165, 0.232087357934], [-0.705052806614, 0.034387694165, 0.232087357934]]
        self.left_goals = [[-0.605052806614, -0.14387694165, 0.232087357934], [-0.605052806614, 0.14387694165, 0.232087357934]]
        self.right_goals = [[-0.605052806614, -0.14387694165, 0.232087357934], [-0.605052806614, 0.14387694165, 0.232087357934]]
        self.large_vel = 8
        self.small_vel = 8 # 0.5
        self.vel = self.large_vel
        self.robot_idx = 0 # 0 represent robot on the right. 1 represent robot on the left

    def get_pose(self):
        rospy.wait_for_service("ur_control_wrapper/get_pose")
        get_current_pose = rospy.ServiceProxy("ur_control_wrapper/get_pose", GetPose)
        current_pose = None
        try:
            current_pose = get_current_pose().pose
        except rospy.ServiceException as exc:
            print("Service did not process request: " + str(exc))

        return current_pose

    def planner(self):
        # set the publish rate in Hz
        rate = rospy.Rate(50)
        goal_idx = 0
        if(self.robot_idx == 0):
            goals = self.right_goals
            goal_num = len(goals)
        else:
            goals = self.left_goals
            goal_num = len(goals)

        while(not rospy.is_shutdown()):
            current_pose = self.get_pose()
            current_pos = np.array([current_pose.position.x, current_pose.position.y, current_pose.position.z])
            pose_diff = goals[goal_idx] - current_pos
            distance = np.linalg.norm(pose_diff)
            if distance < 0.03:
                self.vel = self.small_vel
            else:
                self.vel = self.large_vel
            if distance < 0.01:
                goal_idx += 1
                goal_idx = goal_idx % goal_num
            my_msg = Float64MultiArray()
            my_msg.data = goals[goal_idx]
            self.task_goal_pub.publish(my_msg)
            self.task_vel_pub.publish(self.vel)

if __name__ == '__main__':
    try:
        rospy.init_node('planner', anonymous=True)
        planner = route_planner()
        planner.planner()

    except rospy.ROSInterruptException:
        pass
